/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class BPParagraph implements Serializable {

    
    
//    private List<Metadata> paragraphSentencesMetadata;
    private List<BPSentence> sentenceList;
    

    public BPParagraph() {
        
//        paragraphSentencesMetadata = new ArrayList<Metadata>();
        //sentenceList = new ArrayList<PSentence>();
    }

    
    public int getParagraphLength() {
        return sentenceList.size();
    }

    @Override
    protected void finalize() throws Throwable {
        sentenceList = null;
        super.finalize();
    }


    public List<BPSentence> getSentenceList() {
        return sentenceList;
    }

    public void setSentenceList(List<BPSentence> sentenceList) {
        this.sentenceList = sentenceList;
    }

    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BPParagraph) {
            BPParagraph prg = (BPParagraph) obj;
            if (this.getParagraphLength() == prg.getParagraphLength()
//                    && isEqualParagraphSentence(prg.paragraphSentencesMetadata)) {
                    && isEqualParagraphSentence(prg.sentenceList)) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.getParagraphLength();
        return hash;
    }

  

    private boolean isEqualParagraphSentence(List<BPSentence> paragraphSentences) {
        for (int i = 0; i < paragraphSentences.size(); i++) {
//            if (!this.paragraphSentencesMetadata.get(i).equals(paragraphSentences.get(i))) {
            if (!this.sentenceList.get(i).equals(paragraphSentences.get(i))) {
                return false;
            }

        }
        return true;
    }
    
    public void clear(){
        for (BPSentence s: sentenceList) {
            s.clear();
        }
        sentenceList.clear();
    }
}
