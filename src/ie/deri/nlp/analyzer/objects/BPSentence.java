/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class BPSentence implements Serializable {

    
    private int sentenceLength;
    private List<PLexeme> lexemeList;
//private List<Metadata> sentenceLexemesMetadata;
    private List<Chunk> sentenceChunks;
    private List<SentenceDependency> sentenceDependencies;
    

    

    public BPSentence() {
        // sentenceLexemesMetadata = new ArrayList<Metadata>();
        sentenceChunks = new ArrayList<Chunk>();
        sentenceDependencies = new ArrayList<SentenceDependency>();
        lexemeList = new ArrayList<PLexeme>();
    }

    public void setSentenceLength(int sentenceLength) {
        this.sentenceLength = sentenceLength;
    }

    
    public void clear(){
        lexemeList.clear();
        sentenceChunks.clear();
        sentenceDependencies.clear();
        
    }
//    public void setSentenceLexemesMetadata(List<Metadata> sentenceLexemes) {
//        this.sentenceLexemesMetadata = sentenceLexemes;
//    }
    public int getSentenceLength() {
        return lexemeList.size();
    }

//    public List<Metadata> getSentenceLexemesMetadata() {
//        return sentenceLexemesMetadata;
//    }
    public void setSentenceChunks(List<Chunk> sentenceChunks) {
        this.sentenceChunks = sentenceChunks;
    }

    public List<Chunk> getSentenceChunks() {
        return sentenceChunks;
    }

    public void setSentenceDependencies(List<SentenceDependency> sentenceDependencies) {
        this.sentenceDependencies = sentenceDependencies;
    }

    public List<SentenceDependency> getSentenceDependencies() {
        return sentenceDependencies;
    }

    public void setLexemeList(List<PLexeme> lexemeList) {
        this.lexemeList = lexemeList;
    }

    public List<PLexeme> getPLexemeList() {
        return lexemeList;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BPSentence) {
            BPSentence snt = (BPSentence) obj;
            if (this.sentenceLength == snt.sentenceLength
                    && isEqualSentenceLexeme(snt.lexemeList)) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.sentenceLength;
        return hash;
    }

    public boolean isEqualSentenceLexeme(List<PLexeme> sntLexeme) {
        if (sntLexeme.size() == this.lexemeList.size()) {
            for (int i = 0; i < sntLexeme.size(); i++) {
                if (!sntLexeme.get(i).equals(this.lexemeList.get(i))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }

    }

    public String writePoSSequence() {
        String s = "";
        for (int i = 0; i < lexemeList.size(); i++) {
            s += lexemeList.get(i).getPos() + " ";
        }
        return s.trim();

    }
    
      public String writePoSWordSequence() {
        String s = "";
        for (int i = 0; i < lexemeList.size(); i++) {
            s += lexemeList.get(i).getWord()
                    + "/" +lexemeList.get(i).getPos() + " ";
        }
        return s.trim();

    }
    
    
}
