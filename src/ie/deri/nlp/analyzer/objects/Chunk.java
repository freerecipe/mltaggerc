/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;
import java.io.Serializable;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class Chunk  implements Serializable {

    private int start;
    private PChunk pchunk;

    public void setPchunk(PChunk pchunk) {
        this.pchunk = pchunk;
    }

    public PChunk getPchunk() {
        return pchunk;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getStart() {
        return start;
    }





}
