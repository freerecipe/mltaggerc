/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;

import java.io.Serializable;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class Dependency implements Serializable {
  private PLexeme governorLexeme;
  private PLexeme regentLexeme;
  private String dependencyType;

    public void setDependencyType(String dependencyType) {
        this.dependencyType = dependencyType;
    }

  
    public String getDependencyType() {
        return dependencyType;
    }

    public void setGovernorLexeme(PLexeme governorLexeme) {
        this.governorLexeme = governorLexeme;
    }


    public PLexeme getGovernorLexeme() {
        return governorLexeme;
    }

    public void setRegentLexeme(PLexeme regentLexeme) {
        this.regentLexeme = regentLexeme;
    }

    public PLexeme getRegentLexeme() {
        return regentLexeme;
    }





   

    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Dependency){
            Dependency dep = (Dependency) obj;
            if(this.dependencyType.equals(dep.getDependencyType()) &&
            this.governorLexeme.equals(dep.getGovernorLexeme()) &&
            this.regentLexeme.equals(dep.regentLexeme)){
                return true;
            } else {
                return false;
            }
            
        }else{
            return false;
        }
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.governorLexeme != null ? this.governorLexeme.hashCode() : 0);
        hash = 79 * hash + (this.regentLexeme != null ? this.regentLexeme.hashCode() : 0);
        hash = 79 * hash + (this.dependencyType != null ? this.dependencyType.hashCode() : 0);
        return hash;
    }

   
  
}
