/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;

/**
 *
 * @author behqas
 */
public class LexemeForSearch  implements Comparable {

    private double id;
    private int position;
    private int freq;

    public LexemeForSearch(Metadata lexemeMetadata, int position) {
        this.id = lexemeMetadata.getId();
        this.freq = lexemeMetadata.getFrequency();
        this.position = position;
    }

    public int getFreq() {
        return freq;
    }

    public double getId() {
        return id;
    }

    public int getPosition() {
        return position;
    }

    public int compareTo(Object o) {
        if (!(o instanceof LexemeForSearch)) {
            throw new ClassCastException("A TechnologyTerm object expected.");
        }
        int freq = ((LexemeForSearch) o).getFreq();
        return this.freq - freq;
    }
}







