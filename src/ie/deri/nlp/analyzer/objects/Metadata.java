/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class Metadata {

    private int Frequency;
    private double id;
    private boolean isNew = true;




    public double getId() {
        return id;
    }

    public int getFrequency() {
        return Frequency;
    }

    public void setId(double id) {
        this.id = id;
    }

    public void setFrequency(int Frequency) {
        this.Frequency = Frequency;
    }
    public void incFrequency() {
        this.Frequency++;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    public boolean isIsNew() {
        return isNew;
    }



    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Metadata) {
            Metadata md = (Metadata) obj;
            if (this.id == md.id) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.id) ^ (Double.doubleToLongBits(this.id) >>> 32));
        return hash;
    }



    

  



}
