/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PChunk implements Serializable {

    private String type;
    private List<PLexeme> pLexemeList;

    public PChunk() {
        pLexemeList = new ArrayList<PLexeme>();
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setpLexemeList(List<PLexeme> pLexemeList) {
        this.pLexemeList = pLexemeList;
    }

    public List<PLexeme> getpLexemeList() {
        return pLexemeList;
    }

    public void addPLexeme(PLexeme pLexeme) {
        this.pLexemeList.add(pLexeme);
    }

    public int getLength() {
        return pLexemeList.size();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PChunk) {
            PChunk pch = (PChunk) obj;
            if (pch.getLength() == getLength() && this.type.equals(pch.getType())) {
                for (int i = 0; i < pch.getLength(); i++) {
                    if (!pch.getpLexemeList().get(i).equals(this.pLexemeList.get(i))) {
                        return false;
                    }
                }
                return true; // besharto shoroot
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.type != null ? this.type.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        String str  ="";
        for (int i = 0; i < pLexemeList.size(); i++) {
            str += pLexemeList.get(i).getWord();
            str+=" ";
        }
        return str.trim();
    }


}
