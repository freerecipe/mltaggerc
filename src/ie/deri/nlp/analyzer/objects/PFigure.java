/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PFigure {
PParagraph figureCaption;
int pageNumber;

    public PFigure() {
        figureCaption = new PParagraph();
    }

    public void setFigureCaption(PParagraph figureCaption) {
        this.figureCaption = figureCaption;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public PParagraph getFigureCaption() {
        return figureCaption;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    @Override
       public boolean equals(Object obj) {
        if(obj instanceof PFigure ){
            PFigure pf = (PFigure) obj;
            if(pf.figureCaption.equals(this.figureCaption)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }


}
