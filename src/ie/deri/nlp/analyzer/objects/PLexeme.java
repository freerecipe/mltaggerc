/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;

import java.io.Serializable;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PLexeme implements Serializable {
    private String word;
    private String lemma;
    private String pos;
   


 

   
    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

 
    public void setWord(String word) {
        this.word = word;
    }

  

    public String getLemma() {
        return lemma;
    }

    public String getPos() {
        return pos;
    }

  
    public String getWord() {
        return word;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PLexeme) {
            PLexeme target = (PLexeme) obj;
            if (target.lemma.equals(this.lemma)
                    && this.pos.equals(target.pos)
                    && this.word.equals(target.word)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.word != null ? this.word.hashCode() : 0);
        hash = 47 * hash + (this.lemma != null ? this.lemma.hashCode() : 0);
        hash = 47 * hash + (this.pos != null ? this.pos.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return word+"/"+lemma+"/"  + pos;
    }




}
