/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PParagraph {

    int pageNumber;
    int paragraphLength;
    List<Metadata> paragraphSentencesMetadata;

    public PParagraph() {
        pageNumber = 0;
        paragraphSentencesMetadata = new ArrayList<Metadata>();
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageNumber() {
        return pageNumber;
    }


    public int getParagraphLength() {
        return paragraphLength;
    }

    public List<Metadata> getParagraphSentencesMetadata() {
        return paragraphSentencesMetadata;
    }



    public void setParagraphLength(int paragraphLength) {
        this.paragraphLength = paragraphLength;
    }

    public void setParagraphSentences(List<Metadata> paragraphSentences) {
        this.paragraphSentencesMetadata = paragraphSentences;
    }

    public void addParagraphSentence(Metadata paragraphSentenceMetadata) {
        this.paragraphSentencesMetadata.add( paragraphSentenceMetadata);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PParagraph) {
            PParagraph prg = (PParagraph) obj;
            if (this.paragraphLength == prg.paragraphLength
                    && isEqualParagraphSentence(prg.paragraphSentencesMetadata)) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.paragraphLength;
        return hash;
    }

  

    private boolean isEqualParagraphSentence(List<Metadata> paragraphSentences) {
        for (int i = 0; i < paragraphSentences.size(); i++) {
            if (!this.paragraphSentencesMetadata.get(i).equals(paragraphSentences.get(i))) {
                return false;
            }

        }
        return true;
    }
}
