/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PSection {
    List sectionContent;
    PSentence titleSentence;
    private String type;
    
    private int startPage;
    private int endPage;
    private List<PFigure> figureList;
    private List<PTable> tableList;

    public PSection() {

        sectionContent = new ArrayList();
        titleSentence = new PSentence();
        figureList = new ArrayList<PFigure>();
        tableList = new ArrayList<PTable>();
    }

    public int getSectionLength() {
        return sectionContent.size();
    }




    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

    public void setFigureList(List<PFigure> figureList) {
        this.figureList = figureList;
    }

     public void addFigure(PFigure figure) {
        this.figureList.add(figure);
    }

    public void setSectionContent(List sectionContent) {
        this.sectionContent = sectionContent;
    }

    public void addSectionContent(Object content) {
        this.sectionContent.add(content);
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public void setTableList(List<PTable> tableList) {
        this.tableList = tableList;
    }

        public void addTable(PTable table) {
        this.tableList.add(table);
    }


    public void setTitleSentence(PSentence titleSentence) {
        this.titleSentence = titleSentence;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getEndPage() {
        return endPage;
    }

    public List<PFigure> getFigureList() {
        return figureList;
    }

    public List getSectionContent() {
        return sectionContent;
    }

    public int getStartPage() {
        return startPage;
    }

    public List<PTable> getTableList() {
        return tableList;
    }

    public PSentence getTitleSentence() {
        return titleSentence;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PSection) {
            PSection ps = (PSection) obj;
            if (!this.titleSentence.equals(ps.getTitleSentence())) {
                return false;
            } else {
                if (this.sectionContent.size() != ps.sectionContent.size()) {
                    return false;
                } else {
                    for (int i = 0; i < ps.sectionContent.size(); i++) {
                        if (!this.sectionContent.get(i).equals(ps.sectionContent.get(i))) {
                            return false;
                        }
                    }

                if(this.figureList.size() != ps.figureList.size()){
                    return false;
                } else{
                    for (int i = 0; i < ps.figureList.size(); i++) {
                        if( !this.figureList.get(i).equals(ps.figureList.get(i))){
                            return false;
                        }
                    }
                    if (this.tableList.size() != ps.tableList.size()) {
                        return false;
                    } else {
                        for (int i = 0; i < ps.tableList.size(); i++) {
                            if (!this.tableList.get(i).equals(ps.tableList.get(i))) {
                                return false;
                            }
                        }

                    }
                }


                return true;
                }
            }

        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }




}
