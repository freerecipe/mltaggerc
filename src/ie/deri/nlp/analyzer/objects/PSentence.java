/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PSentence {
private int sentenceLength;
private List<Metadata> sentenceLexemes;
private List<Chunk> sentenceChunks;
private List<SentenceDependency> sentenceDependencies;
    
    
    public PSentence() {
        sentenceLexemes = new ArrayList<Metadata>();
        sentenceChunks = new ArrayList<Chunk>();
        sentenceDependencies = new  ArrayList<SentenceDependency>();

    }





    public void setSentenceLength(int sentenceLength) {
        this.sentenceLength = sentenceLength;
    }

    public void setSentenceLexemes(List<Metadata> sentenceLexemes) {
        this.sentenceLexemes = sentenceLexemes;
    }

    public int getSentenceLength() {
        return sentenceLength;
    }

    public List<Metadata> getSentenceLexemes() {
        return sentenceLexemes;
    }

    public void setSentenceChunks(List<Chunk> sentenceChunks) {
        this.sentenceChunks = sentenceChunks;
    }

    public List<Chunk> getSentenceChunks() {
        return sentenceChunks;
    }

    public void setSentenceDependencies(List<SentenceDependency> sentenceDependencies) {
        this.sentenceDependencies = sentenceDependencies;
    }

    public List<SentenceDependency> getSentenceDependencies() {
        return sentenceDependencies;
    }






    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PSentence) {
            PSentence snt = (PSentence) obj;
            if (this.sentenceLength == snt.sentenceLength
                    && isEqualSentenceLexeme(snt.sentenceLexemes)) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.sentenceLength;
        return hash;
    }




    public boolean isEqualSentenceLexeme(List<Metadata> sntLexeme){
        if( sntLexeme.size() == this.sentenceLexemes.size()){
            for (int i = 0; i < sntLexeme.size(); i++) {
                if(!sntLexeme.get(i).equals(this.sentenceLexemes.get(i))){
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }

    }

    






}
