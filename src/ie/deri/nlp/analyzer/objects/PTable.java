/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PTable {
    PParagraph tableCaptionParagrap;
    

    public PTable() {
        tableCaptionParagrap = new PParagraph();
    }


    public void setTableCaptionParagrap(PParagraph tableCaptionParagrap) {
        this.tableCaptionParagrap = tableCaptionParagrap;
    }


    public PParagraph getTableCaptionParagrap() {
        return tableCaptionParagrap;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof PTable ){
            PTable pt = (PTable) obj;
            if(pt.tableCaptionParagrap.equals(this.tableCaptionParagrap)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }





}
