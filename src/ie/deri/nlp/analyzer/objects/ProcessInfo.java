/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.analyzer.objects;

import java.sql.Timestamp;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class ProcessInfo {
double fileID;
double paperID;
int typeNumber;
int tokenNumber;
int sentenceNumber;
int paragraphNumber;
int sectionNumber;
int processTime;
int indexTime;
java.sql.Timestamp timeStamp;

    public ProcessInfo(double fileID, double paperID, int typeNumber, int tokenNumber, int sentenceNumber, int paragraphNumber, int sectionNumber, int processTime, int indexTime, Timestamp timeStamp) {
        this.fileID = fileID;
        this.paperID = paperID;
        this.typeNumber = typeNumber;
        this.tokenNumber = tokenNumber;
        this.sentenceNumber = sentenceNumber;
        this.paragraphNumber = paragraphNumber;
        this.sectionNumber = sectionNumber;
        this.processTime = processTime;
        this.indexTime = indexTime;
        this.timeStamp = timeStamp;
    }




    public void setFileID(double fileID) {
        this.fileID = fileID;
    }

    public void setIndexTime(int indexTime) {
        this.indexTime = indexTime;
    }

    public void setPaperID(double paperID) {
        this.paperID = paperID;
    }

    public void setParagraphNumber(int paragraphNumber) {
        this.paragraphNumber = paragraphNumber;
    }

    public void setProcessTime(int processTime) {
        this.processTime = processTime;
    }

    public void setSectionNumber(int sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    public void setSentenceNumber(int sentenceNumber) {
        this.sentenceNumber = sentenceNumber;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setTokenNumber(int tokenNumber) {
        this.tokenNumber = tokenNumber;
    }

    public void setTypeNumber(int typeNumber) {
        this.typeNumber = typeNumber;
    }

    public double getFileID() {
        return fileID;
    }

    public int getIndexTime() {
        return indexTime;
    }

    public double getPaperID() {
        return paperID;
    }

    public int getParagraphNumber() {
        return paragraphNumber;
    }

    public int getProcessTime() {
        return processTime;
    }

    public int getSectionNumber() {
        return sectionNumber;
    }

    public int getSentenceNumber() {
        return sentenceNumber;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public int getTokenNumber() {
        return tokenNumber;
    }

    public int getTypeNumber() {
        return typeNumber;
    }


    public String getAsTableRow(){
        int processTimeMin = this.processTime /1000;
        int indexTimeMin = this.indexTime /1000;
        String tableRow = "<tr>"
                + "<td>" + fileID + "</td>"
                + "<td>" + this.paperID + "</td>"
                + "<td>" + this.typeNumber + "</td>"
                + "<td>" + this.tokenNumber + "</td>"
                + "<td>" + this.sentenceNumber + "</td>"
                + "<td>" + this.paragraphNumber + "</td>"
                + "<td>" + this.sectionNumber + "</td>"
                + "<td>" + processTimeMin + "</td>"
                + "<td>" + indexTimeMin + "</td>"
                + "<td>" + this.timeStamp + "</td>"
                + "</tr>";
        return tableRow;
    }

}
