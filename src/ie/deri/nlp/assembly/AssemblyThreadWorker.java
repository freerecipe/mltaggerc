/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.assembly;

import ie.deri.nlp.analyzer.objects.ProcessInfo;
import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.nlp.db.h2.DBPaperAssertion;
import ie.deri.nlp.linguistic.analyzer.IndexPaper;
import ie.deri.nlp.linguistic.analyzer.PaperAnalyzer;
import ie.deri.nlp.linguistic.analyzer.Pipeline6;
import ie.deri.nlp.misc.SSLMail;
import ie.deri.nlp.parscit.parsCitParser.ProcessPDF;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Paper;
import ie.deri.nlp.pdfprocess.tcp.servers.PDFServer;
import ie.deri.nlp.report.html.FileInfoToTable;
import ie.deri.nlp.settings.Settings;
import ie.deri.nlp.db.h2.DBUser;
import ie.deri.nlp.intrface.objects.User;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.maltparser.core.exception.MaltChainedException;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class AssemblyThreadWorker implements Runnable {

    DBUser userDB;
    Pipeline6 pl6;
    Settings settings;
    PDFServer pss;
    DBConnection dbc;
    DBConnection userDBConnection;

    public AssemblyThreadWorker(int threadIDNumber, Settings settings, PDFServer pss) throws ClassNotFoundException, SQLException, IOException, InterruptedException, MaltChainedException {
        this.pss = pss;
        this.settings = settings;
        userDBConnection = new DBConnection();
        userDBConnection.initializeTCPDBUser(
             settings.getDBSettings().getUserDB().getUserH2DB(), "user", true);
        userDB = new DBUser();
        userDB.setCon(userDBConnection.getCon());
        pl6 = new Pipeline6(threadIDNumber, settings.getLingSettings());
        pl6.processAsSentence("Let's get it up");
        String threadName = "AssemblyWorker::" + Integer.toString(threadIDNumber);
        new Thread(this, threadName).start();
    }

    public void run() {
        DBPaperAssertion dba;
        while (true) {
            User user = null;
            double currentFileID = -1;
            List<ProcessInfo> pinfoList = null;
            try {
                user = pss.getUser();
            } catch (InterruptedException ex) {
                Logger.getLogger(
                        AssemblyThreadWorker.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
            if (user != null) {
                System.err.println("---->" + Thread.currentThread().getName() + 
                        " assigned for " + user.getName());
                try {
                    dbc = new DBConnection();
                    dbc.initializeEmbeddedDBPaper(
                            settings.getDBSettings().getH2DBIndexSettings(), user);
                    pinfoList = new ArrayList<ProcessInfo>();
                    for (double userFileID : user.getRequestedFilesByEmail()) {
                        try {
                            dba = new DBPaperAssertion(dbc.getCon());
                            currentFileID = userFileID;
                            userDB.setIsProcessedFile(userFileID);
                            String sourcePDF = settings.getPDFServerSettings().
                                    getPathSetting().getPDFRepository()
                                    + File.separatorChar + userFileID;
                            ProcessPDF pp = new ProcessPDF();
                            PaperAnalyzer ap;
                            Paper p = null;
                            long beforeProcess = (new Date()).getTime();
                            System.err.println("Start Pre Processing");
                            p = pp.processPDF(settings.getPDFServerSettings(), sourcePDF, userFileID);
                            System.err.println("Start Ling Process");
                            ap = new PaperAnalyzer(pl6);
                            ap.analyzePaper(p);
                            System.err.println(">> size of lexeme map: " + ap.getLexemeMap().size());
                            long afterProcess = (new Date()).getTime();
                            IndexPaper indexPaper = new IndexPaper();
                            System.err.println("Start Index Process");
                            double finalPaperID;
                            finalPaperID = indexPaper.indexPaper(dba, p, ap, sourcePDF);
                            long afterIndex = (new Date()).getTime();
                            int processTime = (int) (afterProcess - beforeProcess);
                            int indexTime = (int) (afterIndex - afterProcess);
                            java.util.Date javaDate = new java.util.Date();
                            long javaTime = javaDate.getTime();
                            java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(javaTime);
                            ProcessInfo pInfo = new ProcessInfo(userFileID, finalPaperID, ap.getLexemeMap().size(), ap.getTokenNumber(), ap.getSentenceNumber(), ap.getParagraphNumber(), ap.getSectionNumber(), processTime, indexTime, sqlTimestamp);
                            pinfoList.add(pInfo);
                            userDB.setProcessedFileInfo(pInfo);

                        } catch (Exception ex) {
                            userDB.setCriticalLog(currentFileID, ex.toString());
                        }
                    }
                } catch (Exception e) {
                    System.err.println("DB exception");
                }
                if (user != null) {
                    pss.remove(user);
                }
                sendNotification(pinfoList, user);
            }

        }
    }

    private void sendNotification(List<ProcessInfo> pinfoList, User user) {
        if (user != null) {
            if (pinfoList.size() > 0 && pinfoList != null) {
                try {
                    String report = "<p>" + pinfoList.size() + " paper(s) were analyzed and indexed. Here is a summay of processes:</p>" + FileInfoToTable.fileInfoListToTable(pinfoList);
                    SSLMail.sendProcessDoneMessage(settings.getPDFServerSettings().getEmailSetting(), user, report);
                } catch (Exception ex) {
                    Logger.getLogger(AssemblyThreadWorker.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    String report = "There has been a critical error while processing your file; would you please contact behrang.qasemizadeh@deri.org for bug report";
                    SSLMail.sendProcessDoneMessage(settings.getPDFServerSettings().getEmailSetting(), user, report);
                } catch (Exception ex) {
                    Logger.getLogger(AssemblyThreadWorker.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
