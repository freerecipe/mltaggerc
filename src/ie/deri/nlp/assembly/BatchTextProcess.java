/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.assembly;

import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.nlp.db.h2.DBPaperAssertion;
import ie.deri.nlp.linguistic.analyzer.IndexText;
import ie.deri.nlp.linguistic.analyzer.Pipeline6;
import ie.deri.nlp.linguistic.analyzer.TextAnalyzer;
import ie.deri.nlp.misc.GetFiles;
import ie.deri.nlp.settings.Settings;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author behqas
 */
public class BatchTextProcess {

    Pipeline6 pl6;
    DBPaperAssertion dbp;
    GetFiles gp;
    static final Logger logger = Logger.getLogger("BatchLog");
    Settings settings;
    Set<String> processedFile;
    DBConnection dBConnection;

    public void init(Settings settings, String corpusName, String loggerPath) throws ClassNotFoundException, SQLException, Exception {
        this.settings = settings;
        System.err.println("Initializing...");
        setAndAnalyzeLog(loggerPath);
        System.err.println("Loading ling pipeline...");
        pl6 = new Pipeline6(0, settings.getLingSettings());
        System.err.println("Init database connection");
        dBConnection =
                new DBConnection();
        dBConnection.initializeEmbeddedDBPaper(
                settings.getDBSettings().getH2DBIndexSettings(), corpusName, true);
        System.err.println("Processing text files from " + settings.getBatchProcessSettings().getCorpusPath());
        gp = new GetFiles();
        gp.getCorpusFiles(settings.getBatchProcessSettings().getCorpusPath());
        System.err.println("#files in corpus: " + gp.getFiles().size());
        System.err.println("#files previously analyzed: " + processedFile.size());
        dbp = new DBPaperAssertion(dBConnection.getCon());
    }

    public void process() throws Exception {
        TextAnalyzer ta = null;
        IndexText indexText;
        Double id = 0.0;
        for (int i = 0; i < gp.getFiles().size(); i++) {
            if (!processedFile.contains(gp.getFiles().get(i))) {
                System.err.println("Now Processing :" + gp.getFiles().get(i));
                logger.log(Level.FINEST, gp.getFiles().get(i));
                long beforePreProcess = (new Date()).getTime();
                
                try {
                    long afterPreProcess = (new Date()).getTime();
                    ta = new TextAnalyzer(pl6);
                    
                    String textToAnalyze = getTextFromFile(gp.getFiles().get(i));
                    
                    ta.processText(pl6, textToAnalyze);
                    
                    long afterProcess = (new Date()).getTime();
                    indexText = new IndexText();
                    try {
                        
                        /// here the ids will be generated
                        id = indexText.indexText(dbp, ta);
                        // get a record of this id e.g.add this id to your database 
                        
                    } catch (SQLException ex) {
                        logger.log(Level.SEVERE, gp.getFiles().get(i), "ERROR WHILE INDEXING: " + ex);
                    }
                    long afterIndexing = (new Date()).getTime();
                    long preprocesstime = (afterPreProcess - beforePreProcess) / 1000;
                    long lingprocess = (afterProcess - afterPreProcess) / 1000;
                    long indexTime = (afterIndexing - afterProcess) / 1000;
                    String log = gp.getFiles().get(i) + "," + i + "," + 
                            preprocesstime + ", " + 
                            lingprocess + ", " 
                            + indexTime + ", " +
                            ta.getTokenNumber()+ ", " 
                            + id;

                    logger.log(Level.INFO, log);
                    System.gc();
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "{0}ERROR WHILE PREPROCESS: {1}", new Object[]{gp.getFiles().get(i), ex});
                }
            }
        }
        dBConnection.close();
    }

    private void setAndAnalyzeLog(String loggerPath) throws IOException, ParserConfigurationException, SAXException {
        processedFile = new HashSet<String>();

         try{
            System.err.println("Load logs from " + loggerPath);
            File file = new File(loggerPath);
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            builder.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                    if (systemId.contains("logger.dtd")) {
                        return new InputSource(new StringReader(""));
                    } else {
                        return null;
                    }
                }
            });
            Document doc;
            doc = builder.parse(file);
            // StyleTable Node
            NodeList recordList = doc.getElementsByTagName("record");
            for (int i = 0; i < recordList.getLength(); i++) {
                NodeList recordNodes = recordList.item(i).getChildNodes();
                for (int j = 0; j < recordNodes.getLength(); j++) {
                    if(recordNodes.item(j).getNodeName().equals("level") ){
                        if(recordNodes.item(j).getTextContent().trim().equals("FINEST")){
                            for (int k = 0; k < recordNodes.getLength(); k++) {
                                if(recordNodes.item(k).getNodeName().trim().equals("message")){
                                    String fileName= recordNodes.item(k).getTextContent();
                                    processedFile.add(fileName);
                                }

                            }
                        }
                    }

                }

            }
        } catch(FileNotFoundException ef){
            System.err.println("No Previous log file" + ef);
        }

        FileHandler fh;
        fh = new FileHandler(loggerPath, true);
        logger.addHandler(fh);
        logger.setLevel(Level.ALL);
//        SimpleFormatter formatter = new SimpleFormatter();
//
//        fh.setFormatter(formatter);

    }

    
    // change this method to feed text from another source
    private String getTextFromFile(String fileName) throws FileNotFoundException {
        // do what ever you do on text here
        StringBuilder sb = new StringBuilder();
        BufferedReader input =  new BufferedReader(new FileReader(fileName));
        
        
        String line=null;
        try {
            while (( line = input.readLine()) != null){
             sb.append(line);
           }
        } catch (IOException ex) {
            Logger.getLogger(BatchTextProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            input.close();
        } catch (IOException ex) {
            Logger.getLogger(BatchTextProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        return sb.toString();
    }
}
