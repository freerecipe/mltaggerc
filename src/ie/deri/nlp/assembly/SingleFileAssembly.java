/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.assembly;

import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.nlp.db.h2.DBPaperAssertion;
import ie.deri.nlp.linguistic.analyzer.IndexPaper;
import ie.deri.nlp.linguistic.analyzer.PaperAnalyzer;
import ie.deri.nlp.linguistic.analyzer.Pipeline6;
import ie.deri.nlp.misc.UID;
import ie.deri.nlp.parscit.parsCitParser.ProcessPDF;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Paper;
import ie.deri.nlp.settings.Settings;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author behqas
 */
public class SingleFileAssembly {
    Pipeline6 pl6;
    DBPaperAssertion dbp;
    Settings settings;
    
    /**
     * 
     * @param settings
     * @param corpusName: corpus name will be considered as the name of database
     * @throws Exception 
     */

    public void init(Settings settings, String corpusName) throws Exception {
        this.settings = settings;
        System.err.println("Initializing...");
        System.err.println("Loading ling pipeline...");
        pl6 = new Pipeline6(0, settings.getLingSettings());
        System.err.println("Init database connection");
        DBConnection dBConnection =
                new DBConnection();
        
        dBConnection.initializeEmbeddedDBPaper(
                settings.getDBSettings().getH2DBIndexSettings(),
                corpusName, true);
        dbp = new DBPaperAssertion(dBConnection.getCon());
    }

    public void process(String filePath) throws Exception {
        PaperAnalyzer pla = null;
        Paper paper;
        ProcessPDF pp;
        IndexPaper indexPaper;

        long beforePreProcess = (new Date()).getTime();
        pp = new ProcessPDF();
        paper = new Paper();
        double uid = UID.getUID();
        //try {
            paper = pp.processPDF(settings.getPDFServerSettings(), filePath, uid);
            long afterPreProcess = (new Date()).getTime();
            pla = new PaperAnalyzer(pl6);
            pla.analyzePaper(paper);
            long afterProcess = (new Date()).getTime();
            indexPaper = new IndexPaper();
            try {
                indexPaper.indexPaper(dbp, paper, pla, filePath);
            } catch (SQLException ex) {
                System.err.println("ERROR WHILE INDEXING: " + ex);
            }
            long afterIndexing = (new Date()).getTime();
            long preprocesstime = (afterPreProcess - beforePreProcess) / 10;
            long lingprocess = (afterProcess - afterPreProcess) / 10;
            long indexTime = (afterIndexing - afterProcess) / 10;
            System.err.println("PreProcess time: " + preprocesstime + ", LingProcess: " + lingprocess + ", Indexing: " + indexTime);

//        } catch (Exception ex) {
//            System.err.println("EXCEPTION Here" + ex);
//        }
    }
}
