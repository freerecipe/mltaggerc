/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.h2;

import ie.deri.nlp.settings.DBSettings;
import ie.deri.nlp.settings.H2Settings;
import ie.deri.nlp.intrface.objects.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.h2.tools.Server;

/**
 *
 * @author behqas
 */
public final class DBConnection {

    private Connection con;
    

    public DBConnection() throws SQLException {
    }

    public void initializeEmbeddedDBPaper(H2Settings h2s, User user) throws ClassNotFoundException, SQLException {
        initializeEmbeddedDBPaper(h2s, user.getUserID(), true);
    }


    public void initializeDBUser(H2Settings h2s, String dbName, boolean createDB) throws ClassNotFoundException, SQLException {
        int cacheSize = 3145728;
        Class.forName("org.h2.Driver");
        String connectionString = h2s.getConnectionStringMode()
                + h2s.getDataFileRootPath() + "/" + dbName;
        try {
            this.con = DriverManager.getConnection(
                    connectionString + ";IFEXISTS=TRUE",
                    h2s.getUserName(), h2s.getPassword());
            System.err.println("Connections stablished successfully...");
        } catch (Exception e) {
            System.err.println("E" + e);
            if ((e.getMessage().endsWith("not found [90013-162]")
                    || e.getMessage().endsWith("not found [90013-151]")) && createDB) {
                System.err.println("USER Database does not exist!");
                System.err.println("Creating a new USER Database for " + dbName);
                this.con = DriverManager.getConnection(connectionString, h2s.getUserName(), h2s.getPassword());
                System.err.println("Crreating data scheme..");
                DBUserCreate dbuc = new DBUserCreate();
                dbuc.setCon(con);
                dbuc.creatTables();
                dbuc.close();
                
            } else {
                System.err.println("unexpected error");
                return;
            }
            
            
        }

        // this should be changed and read from somewhere
        PreparedStatement pstmtCach =
                this.con.prepareStatement("SET CACHE_SIZE ?");
        pstmtCach.setLong(1, cacheSize);
        pstmtCach.execute();
        
    }

    public void initializeEmbeddedDBPaper(H2Settings h2s, String dbName, boolean createDB) throws ClassNotFoundException, SQLException {
        int cacheSize = 3145728;
        Class.forName("org.h2.Driver");
        String connectionString = h2s.getConnectionStringMode()
                + h2s.getDataFileRootPath() + "/" + dbName;
        try {
            this.con = DriverManager.getConnection(connectionString + ";IFEXISTS=TRUE", h2s.getUserName(), h2s.getPassword());
            System.err.println("Connections stablished successfully...");
        } catch (Exception e) {
            System.err.println("E" + e);
            if (e.getMessage().endsWith("not found [90013-151]") && createDB) {
                System.err.println("Database does not exist!");
                System.err.println("Creating a new Database for " + dbName);
                this.con = DriverManager.getConnection(connectionString, h2s.getUserName(), h2s.getPassword());
                System.err.println("Crreating data scheme..");
                DBPaperCreate clc = new DBPaperCreate();
                clc.init(con);
                clc.creatTables();
                clc.close();
            } else if (e.getMessage().endsWith("not found [90013-162]") && createDB) {
                System.err.println("Database does not exist!");
                System.err.println("Creating a new Database for " + dbName);
                this.con = DriverManager.getConnection(connectionString, h2s.getUserName(), h2s.getPassword());
                System.err.println("Crreating data scheme..");
                DBPaperCreate clc = new DBPaperCreate();
                clc.init(con);
                clc.creatTables();
                clc.close();


            } else {
                System.err.println("unexpected error");
                return;
            }
        }

        // this should be changed and read from somewhere
        PreparedStatement pstmtCach =
                this.con.prepareStatement("SET CACHE_SIZE ?");
        pstmtCach.setLong(1, cacheSize);
        pstmtCach.execute();
    }

    public void initializeTCPDBPaper(H2Settings h2s, User user) throws ClassNotFoundException, SQLException {
        initializeTCPDBPaper(h2s, user.getUserID(), false);
    }

    public void initializeTCPDBUser(H2Settings h2s, String userDbName) throws ClassNotFoundException, SQLException {
        initializeTCPDBUser(h2s, userDbName, false);
    }

    public void initializeTCPDBUser(H2Settings h2s, String dbName, boolean createDB) throws ClassNotFoundException, SQLException {
        Class.forName("org.h2.Driver");
        String connectionString = "jdbc:h2:tcp://" + h2s.getIpAddress() + ":"
                + h2s.getPort() + "/" + // NOTE THAT THE CACHE SIZE SHOULD BE READ FROM SETTINGS
                h2s.getDataFileRootPath() + "/" + dbName + ";DB_CLOSE_ON_EXIT=FALSE;CACHE_SIZE=4194304";
        System.err.println(connectionString);
        try {
            this.con = DriverManager.getConnection(connectionString + ";IFEXISTS=TRUE", h2s.getUserName(), h2s.getPassword());
            System.err.println("USer DB Connections stablished successfully...");
        } catch (Exception e) {
            // System.err.println("E" + e);
            
            if (e.getMessage().endsWith("not found [90013-151]") && createDB) {
                System.err.println("Database does not exist!");
                System.err.println("Creating a new Database for " + dbName);
                this.con = DriverManager.getConnection(connectionString, h2s.getUserName(), h2s.getPassword());
                System.err.println("Crreating data scheme..");
                DBUserCreate dbc = new DBUserCreate();
                dbc.setCon(con);
                dbc.creatTables();
                con.commit();
                dbc.close();
            } else if(e.getMessage().endsWith("not found [90013-162]") && createDB){
                System.err.println("USER Database does not exist!");
                System.err.println("Creating a new USER Database for " + dbName);
                this.con = DriverManager.getConnection(connectionString, h2s.getUserName(), h2s.getPassword());
                System.err.println("Crreating data scheme..");
                DBUserCreate clc = new DBUserCreate();
                clc.setCon(con);
                clc.creatTables();
                con.commit();
                clc.close();
            } else {
                System.err.println("unexpected error");
                return;
            }
        }
    }

    public void initializeTCPDBPaper(H2Settings h2s, String dbName, boolean createDB) throws ClassNotFoundException, SQLException {
        Class.forName("org.h2.Driver");
        String connectionString = "jdbc:h2:tcp://" + h2s.getIpAddress() + ":"
                + h2s.getPort() + "/" + // NOTE THAT THE CACHE SIZE SHOULD BE READ FROM SETTINGS
                h2s.getDataFileRootPath() + "/" + dbName + ";DB_CLOSE_ON_EXIT=FALSE;CACHE_SIZE=4194304";
        try {
            this.con = DriverManager.getConnection(connectionString + ";IFEXISTS=TRUE", h2s.getUserName(), h2s.getPassword());
            System.err.println("Connections stablished successfully...");
        } catch (Exception e) {
            System.err.println("Error: " + e);
            if (e.getMessage().endsWith("not found [90013-151]") && createDB) {
                System.err.println("Database does not exist!");
                System.err.println("Creating a new Database for " + dbName);
                this.con = DriverManager.getConnection(connectionString, h2s.getUserName(), h2s.getPassword());
                System.err.println("Crreating data scheme..");
                DBPaperCreate clc = new DBPaperCreate();
                clc.init(con);
                clc.creatTables();
                clc.close();
            }else if(e.getMessage().endsWith("not found [90013-162]") && createDB){
                System.err.println("Database does not exist!");
                System.err.println("Creating a new Database for " + dbName);
                this.con = DriverManager.getConnection(connectionString, h2s.getUserName(), h2s.getPassword());
                System.err.println("Crreating data scheme..");
                DBPaperCreate clc = new DBPaperCreate();
                clc.init(con);
                clc.creatTables();
                clc.close();
            } else {
                System.err.println("unexpected error");
                return;
            }
        }
    }

    public void close() throws SQLException {
        con.commit();
        con.close();
    }

    public Connection getCon() {
        return con;
    }

    public void initMySQL(DBSettings dbSetting, User user) throws ClassNotFoundException, SQLException {
        String requestString = dbSetting.getMysqlIndexDB();
        String mysqlURL = null;
        String databaseName = null;
        String userName = null;
        String password = null;
        String[] elements = requestString.split("~>");
        if ("odbc".equals(elements[0])) {
            System.err.println("Not implemented yet!");
            return;
        } else if ("mysql".equals(elements[0])) {
            mysqlURL = elements[1];
            databaseName = user.getUserID(); //elements[2];
            userName = elements[3];
            password = elements[4];
            System.err.println(">>> submitted arguments for MYSQL connection: ");
            System.err.println("    URL: " + mysqlURL);
            System.err.println("    Catalog name: " + user.getUserID());
            System.err.println("    UserName: " + userName);
            System.err.println("    Password: " + password);
        } else {
            System.err.println("Error in initializing connection to data repository");
            System.err.println("For ODBC connection: use the string in the form of odbc~>\"your data source name\"");
            System.err.println("For MY-SQL connection: use mysql~>\"url\"~>\"database name\"~>\"User Name\"~>\"Password\"");
            return;
        }
        Class.forName("com.mysql.jdbc.Driver");
        Connection gCon = (Connection) DriverManager.getConnection(mysqlURL, userName, password);  // e.g "jdbc:mysql://localhost:3309", root, root
        Statement st = (Statement) gCon.createStatement();
        ResultSet rs = st.executeQuery("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" + databaseName + "'");
        if (rs.next()) {
            gCon.close();
            con = (Connection) DriverManager.getConnection(mysqlURL + databaseName, userName, password);
            con.setAutoCommit(false);
        } else {
            System.err.println("There is no data scheme as " + databaseName + " with provided information for connection");
            return;
        }
    }

    public void initializeMakeAndEmbedded(H2Settings h2s, String dbName, boolean createDB) throws ClassNotFoundException, SQLException {
        int cacheSize = 3145728;
        Class.forName("org.h2.Driver");
        String connectionString = h2s.getConnectionStringMode()
                + h2s.getDataFileRootPath() + "/" + dbName;
        try {
            this.con = DriverManager.getConnection(connectionString + ";IFEXISTS=TRUE", h2s.getUserName(), h2s.getPassword());
            System.err.println("Connections stablished successfully...");
        } catch (Exception e) {
            System.err.println("E" + e);
            if (e.getMessage().endsWith("not found [90013-151]") && createDB) {
                System.err.println("Database does not exist!");
                System.err.println("Creating a new Database for " + dbName);
                this.con = DriverManager.getConnection(connectionString, h2s.getUserName(), h2s.getPassword());
                System.err.println("Crreating data scheme..");
                DBPaperCreate clc = new DBPaperCreate();
                clc.init(con);
                clc.creatTables();
                clc.close();
            } else {
                System.err.println("unexpected error");
                return;
            }
        }
        

        // this should be changed and read from somewhere
        PreparedStatement pstmtCach =
                this.con.prepareStatement("SET CACHE_SIZE ?");
        pstmtCach.setLong(1, cacheSize);
        pstmtCach.execute();
    }
    
    
    public static void main(String[] y) throws ClassNotFoundException, SQLException{
        DBConnection dbc = new DBConnection();
        H2Settings h2s = new H2Settings();
        h2s.setDataFileRootPath("");
        h2s.setPassword("");
        h2s.setUserName("SA");
        h2s.setPort(9082);
        dbc.initializeMakeAndEmbedded( h2s, "db_user", true);
        
    }
}
