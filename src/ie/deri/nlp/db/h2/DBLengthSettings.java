/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.h2;

/**
 *
 * @author behqas
 */
public class DBLengthSettings {
    final protected int LEXEME_STRING_LENGTH = 40;
    final protected int POS_LENGTH = 7;
    final protected String ID_TYPE = "DOUBLE";
    final protected int REFERENCE_MARKER_LENGTH = 100;
}
