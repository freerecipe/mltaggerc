/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.h2;


import ie.deri.nlp.analyzer.objects.Chunk;
import ie.deri.nlp.analyzer.objects.Dependency;
import ie.deri.nlp.analyzer.objects.LexemeForSearch;
import ie.deri.nlp.analyzer.objects.Metadata;
import ie.deri.nlp.analyzer.objects.PChunk;
import ie.deri.nlp.analyzer.objects.PEquation;
import ie.deri.nlp.analyzer.objects.PFigure;
import ie.deri.nlp.analyzer.objects.PLexeme;
import ie.deri.nlp.analyzer.objects.PParagraph;
import ie.deri.nlp.analyzer.objects.PReference;
import ie.deri.nlp.analyzer.objects.PSection;
import ie.deri.nlp.analyzer.objects.PSentence;
import ie.deri.nlp.analyzer.objects.PTable;
import ie.deri.nlp.analyzer.objects.SentenceDependency;
import ie.deri.nlp.linguistic.analyzer.PaperAnalyzer;
import ie.deri.nlp.misc.UID;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Author;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Paper;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Reference;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;

/**
 *
 * @author behqas
 *
 * This class takes care of data-repository
 */
public class DBPaperAssertion extends DBLengthSettings {

    protected Connection con;

    public void init(Connection con) {
        this.con = con;
    }

    public Connection getCon() {
        return con;
    }

    public DBPaperAssertion() {
    }

 
    
    public DBPaperAssertion(Connection con) {
        this.con = con;

    }

    /**
     *
     * @param lexemeString
     * @param lexemeLemma
     * @param lexemePoS
     * @return long lexemeID
     * @throws java.sql.SQLException
     *
     * <p>This method provides indexing service for a lexeme. The method indexes
     * a lexeme and updates its id and frequency according to the database.
     * </P>
     */
    
    
    
    public void indexLexeme(PLexeme plexeme, Metadata metadata) throws SQLException {
        String lexemeString = plexeme.getWord();
        String lexemeLemma = plexeme.getLemma();
        String lexemePoS = plexeme.getPos();
        PreparedStatement pstmtLexeme = con.prepareStatement(
                "SELECT lexeme_id, lexeme_frequency"
                + " FROM dbo_lexeme WHERE (lexeme_string= ? ) "
                + "AND (lexeme_lemma= ? ) AND (penn_pos= ? )");
        if (lexemeString.length() > LEXEME_STRING_LENGTH) {
            pstmtLexeme.setString(1, lexemeString.substring(0, LEXEME_STRING_LENGTH - 1));
        } else {
            pstmtLexeme.setString(1, lexemeString);
        }
        if (lexemeLemma.length() > LEXEME_STRING_LENGTH) {
            pstmtLexeme.setString(2, lexemeLemma.substring(0, LEXEME_STRING_LENGTH - 1));
        } else {
            pstmtLexeme.setString(2, lexemeLemma);
        }
        pstmtLexeme.setString(3, lexemePoS);
        ResultSet rs = pstmtLexeme.executeQuery();

        if (rs.next()) {
            double currentLexemeID = rs.getDouble("lexeme_id");
            PreparedStatement jstmtUpdtLexeme =
                    con.prepareStatement(
                    "UPDATE dbo_lexeme SET lexeme_frequency=? "
                    + "WHERE lexeme_id=?");
            int NewFrequency = rs.getInt("lexeme_frequency")
                    + metadata.getFrequency();
            jstmtUpdtLexeme.setInt(1, NewFrequency);
            jstmtUpdtLexeme.setDouble(2, currentLexemeID);
            jstmtUpdtLexeme.executeUpdate();
            jstmtUpdtLexeme.close();
            rs.close();
            pstmtLexeme.close();
            metadata.setFrequency(NewFrequency);
            metadata.setId(currentLexemeID);
            con.commit();
        } else {
            PreparedStatement jstmtLexeme = con.prepareStatement(
                    "INSERT INTO dbo_lexeme(lexeme_id, lexeme_string,"
                    + " penn_pos, lexeme_lemma, lexeme_frequency) "
                    + "VALUES( ?, ?, ?, ?, ? )");
            jstmtLexeme.setDouble(1, metadata.getId());

            if (lexemeString.length() > LEXEME_STRING_LENGTH) {
                jstmtLexeme.setString(2, lexemeString.substring(0, LEXEME_STRING_LENGTH - 1));
            } else {
                jstmtLexeme.setString(2, lexemeString);
            }
            jstmtLexeme.setString(3, lexemePoS);
            if (lexemeLemma.length() > LEXEME_STRING_LENGTH) {
                jstmtLexeme.setString(4, lexemeLemma.substring(0, LEXEME_STRING_LENGTH - 1));
            } else {
                jstmtLexeme.setString(4, lexemeLemma);
            }
            jstmtLexeme.setInt(5, metadata.getFrequency());
            jstmtLexeme.executeUpdate();
            jstmtLexeme.close();
            jstmtLexeme.close();
            rs.close();
            pstmtLexeme.close();
            con.commit();
        }
    }

    /**
     *
     * @param PSentence psentence, Metadata sentenceMetadata
     * @return void
     * @throws java.sql.SQLException
     *
     */
    public void indexSentence(PSentence psentence, Metadata sentenceMetadata, HashMap<PChunk, Metadata> pchunkMap) throws SQLException {

        boolean isIndexed = isIndexedSentence3(psentence, sentenceMetadata);

        if (isIndexed) {
            PreparedStatement pstmtSentenceFreq = con.prepareStatement(
                    "SELECT frequency FROM dbo_sentence WHERE "
                    + "sentence_id= ? ");
            pstmtSentenceFreq.setDouble(1, sentenceMetadata.getId());
            ResultSet rs = pstmtSentenceFreq.executeQuery();
            rs.next();
            int newFrequency = rs.getInt("frequency") + sentenceMetadata.getFrequency();
            sentenceMetadata.setFrequency(newFrequency); // update metadata for sentence
            sentenceMetadata.setIsNew(false);
            pstmtSentenceFreq.close();
            rs.close();

            PreparedStatement pstmtUpdateSentenceFreq = con.prepareStatement(
                    "UPDATE dbo_sentence SET frequency= ?"
                    + " WHERE sentence_id= ?");
            pstmtUpdateSentenceFreq.setInt(1, newFrequency);
            pstmtUpdateSentenceFreq.setDouble(2, sentenceMetadata.getId());
            pstmtUpdateSentenceFreq.executeUpdate();
            con.commit();
            pstmtUpdateSentenceFreq.close();
        } else {
            sentenceMetadata.setIsNew(true);
            PreparedStatement sentenceInsrt = con.prepareStatement(
                    "INSERT INTO dbo_sentence VALUES( ?, ?, ?)");
            sentenceInsrt.setDouble(1, sentenceMetadata.getId());
            sentenceInsrt.setInt(2, psentence.getSentenceLength());
            sentenceInsrt.setInt(3, sentenceMetadata.getFrequency());
            sentenceInsrt.executeUpdate();
            con.commit();
            sentenceInsrt.close();


            for (int ix = 0; ix < psentence.getSentenceLexemes().size(); ix++) {
                PreparedStatement sentenceLexemeInsrt = con.prepareStatement(
                        "INSERT INTO dbo_sentence_lexeme VALUES( ?, ?, ?)");
                sentenceLexemeInsrt.setDouble(1, sentenceMetadata.getId());
                sentenceLexemeInsrt.setDouble(2,
                        psentence.getSentenceLexemes().get(ix).getId());
                sentenceLexemeInsrt.setInt(3, ix);
                sentenceLexemeInsrt.executeUpdate();
                sentenceLexemeInsrt.close();
            }
            con.commit();
            indexSentencePhrase(psentence, sentenceMetadata, pchunkMap);

            PreparedStatement jstmtSectionT = con.prepareStatement(
                    "INSERT INTO dbo_sentence_metadata (sentence_id) VALUES (?)");
            jstmtSectionT.setDouble(1, sentenceMetadata.getId());
            jstmtSectionT.executeUpdate();
            jstmtSectionT.close();
        }
    }

    /**
     *
     * @param sentence
     * @param sentenceMeta
     * @param pchunkHashmap
     * @throws SQLException
     */
    public void indexSentencePhrase(PSentence sentence, Metadata sentenceMeta, HashMap<PChunk, Metadata> pchunkHashmap) throws SQLException {
        PreparedStatement pstmtPQuery = null;
        for (Chunk chk : sentence.getSentenceChunks()) {
            pstmtPQuery = con.prepareStatement(
                    "INSERT INTO dbo_sentence_phrase "
                    + "VALUES( ?, ?, ?)");
            pstmtPQuery.setDouble(1, sentenceMeta.getId());
            pstmtPQuery.setDouble(2, pchunkHashmap.get(chk.getPchunk()).getId());
            pstmtPQuery.setInt(3, chk.getStart());
            pstmtPQuery.executeUpdate();
        }
        con.commit();
        if (pstmtPQuery != null) {
            pstmtPQuery.close();
        }
    }

    /**
     *
     * @param pchunk
     * @param metadata
     * @param lexemeMap
     * @throws SQLException
     */
    public void indexPhrase(PChunk pchunk, Metadata metadata, HashMap<PLexeme, Metadata> lexemeMap) throws SQLException {
        if (isIndexedPhrase2(pchunk, metadata, lexemeMap)) {
            PreparedStatement pstmtPQuery = con.prepareStatement(
                    "SELECT phrase_frequency FROM dbo_phrase WHERE "
                    + "phrase_id= ?");
            pstmtPQuery.setDouble(1, metadata.getId());
            ResultSet rs = pstmtPQuery.executeQuery();
            rs.next();
            int newFrequency = rs.getInt("phrase_frequency") + metadata.getFrequency();
            rs.close();
            pstmtPQuery.close();
            PreparedStatement pstmtUpdtPQuery = con.prepareStatement(
                    "UPDATE dbo_phrase SET phrase_frequency=? "
                    + " WHERE phrase_id= ? ");
            pstmtUpdtPQuery.setInt(1, newFrequency);
            pstmtUpdtPQuery.setDouble(2, metadata.getId());
            pstmtUpdtPQuery.executeUpdate();
            con.commit();
            pstmtUpdtPQuery.close();
        } else {
            PreparedStatement pstmtPQuery = con.prepareStatement(
                    "INSERT INTO dbo_phrase VALUES( ?, ?, ?, ? )");
            pstmtPQuery.setDouble(1, metadata.getId());
            pstmtPQuery.setString(2, pchunk.getType());
            pstmtPQuery.setInt(3, pchunk.getLength());
            pstmtPQuery.setInt(4, metadata.getFrequency());

            pstmtPQuery.executeUpdate();
            con.commit();
            pstmtPQuery.close();
            PreparedStatement pstmtPhraseLexemeQuery = null;
            for (int ix = 0; ix < pchunk.getpLexemeList().size(); ix++) {
                pstmtPhraseLexemeQuery = con.prepareStatement(
                        "INSERT INTO dbo_phrase_lexeme VALUES(?, ?, ?)");
                pstmtPhraseLexemeQuery.setDouble(1, metadata.getId());
                pstmtPhraseLexemeQuery.setDouble(2,
                        lexemeMap.get(pchunk.getpLexemeList().get(ix)).getId());
                pstmtPhraseLexemeQuery.setInt(3, ix);
                pstmtPhraseLexemeQuery.executeUpdate();
                con.commit();
            }
            pstmtPhraseLexemeQuery.close();
        }
//select dbo_sentence.sentence_id from dbo_sentence
//        inner join dbo_sentence_lexeme as s0 where dbo_sentence.sentence_length = 20 and s0.lexeme_id = 1 and s0.lexeme_position =2;
    }

    /**
     * Return value is true if the sentence is already asserted in the databse
     * in this case the id of metadata of the sentence will be updated to current value
     * in the database. else the return value is false
     *
     * @param sentenceLexemes
     * @return boolean
     * @throws java.sql.SQLException
     */
    public boolean isIndexedSentence2(PSentence psentence,
            Metadata sentenceMetadata) throws SQLException {
        if (psentence.getSentenceLength() == 1) {
            PreparedStatement pstmtSent = con.prepareStatement(
                    " SELECT dbo_sentence.sentence_id from dbo_sentence "
                    + " inner join dbo_sentence_lexeme as s0"
                    + " WHERE dbo_sentence.sentence_length = ? "
                    + " and s0.lexeme_id = ? and s0.lexeme_position = ?");
            pstmtSent.setInt(1, 1);
            pstmtSent.setDouble(2, psentence.getSentenceLexemes().get(0).getId());
            pstmtSent.setInt(3, 0);
            ResultSet rs = pstmtSent.executeQuery();
            if (rs.next()) {
                double ans = rs.getDouble("sentence_id");
                sentenceMetadata.setId(ans);
                rs.close();
                pstmtSent.close();
                return true;
            } else {
                // gettting here means that there were no answer
                rs.close();
                pstmtSent.close();
                return false;
            }
        } else {

            String query =
                    "SELECT dsl.sentence_id from dbo_sentence_lexeme as dsl, ";
//                    + " inner join ";

            for (int i = 0; i < psentence.getSentenceLength(); i++) {
                query += "dbo_sentence_lexeme as dsl" + i + " ";
                if (i != psentence.getSentenceLength() - 1) {
                    query += ", ";
                }
            }
            query += " WHERE (";
//                    + "(  dbo_sentence.sentence_length = "
//                    + psentence.getSentenceLength() + "  AND ";
            //dbo_sentence_lexeme as s0"
            //+ " WHERE dbo_sentence.sentence_length = ? "


            for (int i = 0; i < psentence.getSentenceLength(); i++) {
                query += " dsl" + i + ".lexeme_id = ? "
                        + "AND dsl" + i + ".lexeme_position = " + i + " ";
                if (i != psentence.getSentenceLength() - 1) {
                    query += " AND dsl" + i + ".sentence_id = dsl" + (i + 1) + ".sentence_id AND ";
                }
            }
            query += ")";
            PreparedStatement pstmtSent = con.prepareStatement(query);
            for (int i = 0; i < psentence.getSentenceLength(); i++) {
                pstmtSent.setDouble(i + 1, psentence.getSentenceLexemes().get(i).getId());
            }
            ResultSet rs = pstmtSent.executeQuery();
            if (rs.next()) {
                double ans = rs.getDouble("sentence_id");
                sentenceMetadata.setId(ans);
                rs.close();
                pstmtSent.close();
                return true;
            } else {
                // gettting here means that there were no answer
                rs.close();
                pstmtSent.close();
                return false;
            }
        }
    }

    public boolean isIndexedSentence(PSentence psentence, Metadata sentenceMetadata) throws SQLException {

        if (psentence.getSentenceLength() != 0) {

            ArrayList<ArrayList<Double>> sentenceID = new ArrayList<ArrayList<Double>>();
            PreparedStatement pstmtSentenceQuery = null;

            for (int i = 0; i < psentence.getSentenceLength(); i++) {
                pstmtSentenceQuery = con.prepareStatement(
                        "SELECT dbo_sentence_lexeme.sentence_id "
                        + "FROM dbo_sentence_lexeme "
                        + "INNER JOIN dbo_sentence ON"
                        + " dbo_sentence.sentence_id = "
                        + "dbo_sentence_lexeme.sentence_id "
                        + "WHERE (((dbo_sentence_lexeme.lexeme_id)= ? )"
                        + " AND ((dbo_sentence_lexeme.lexeme_position)= ? )"
                        + " AND ((dbo_sentence.sentence_length)= ? ))");
                pstmtSentenceQuery.setDouble(1, psentence.getSentenceLexemes().get(i).getId());
                pstmtSentenceQuery.setInt(2, i);
                pstmtSentenceQuery.setInt(3, psentence.getSentenceLength());

                ResultSet rsID = pstmtSentenceQuery.executeQuery();
                int count = 0;
                ArrayList<Double> idList = new ArrayList<Double>();
                while (rsID.next()) {
                    double ans = rsID.getDouble("sentence_id");
                    idList.add(count, ans);
                    count++;
                }
                if (idList.isEmpty()) {
                    rsID.close();
                    pstmtSentenceQuery.close();
                    return false;
                }
                pstmtSentenceQuery.close();
                sentenceID.add(i, idList);
                rsID.close();
            }
            if (sentenceID.isEmpty()) {
                if (pstmtSentenceQuery != null) {
                    if (!pstmtSentenceQuery.isClosed()) {
                        pstmtSentenceQuery.close();
                    }
                }
                return false;
            }
            if (sentenceID.size() == 1) {
                Iterator iterators = sentenceID.iterator();
                ArrayList element1 = (ArrayList) iterators.next();
                if (element1.isEmpty()) {
                    pstmtSentenceQuery.close();
                    return false;
                } else {
                    Iterator it2 = element1.iterator();
                    sentenceMetadata.setId((Double) it2.next());
                    pstmtSentenceQuery.close();
                    return true;
                }
            }
            if (sentenceID.size() == 2) {
                Iterator iterators = sentenceID.iterator();
                ArrayList element1 = (ArrayList) iterators.next();
                ArrayList element2 = (ArrayList) iterators.next();
                ArrayList interse = (ArrayList) CollectionUtils.intersection(element1, element2);
                if (interse.isEmpty()) {
                    pstmtSentenceQuery.close();
                    return false;
                } else {
                    Iterator it4 = interse.iterator();
                    sentenceMetadata.setId((Double) it4.next());
                    pstmtSentenceQuery.close();
                    return true;
                }
            } else {
                // this is the condition we have more than two arrays and we need to calculate the intersection between them
                Iterator iterators = sentenceID.iterator();
                ArrayList element1 = (ArrayList) iterators.next();
                ArrayList element2 = (ArrayList) iterators.next();
                ArrayList interse = (ArrayList) CollectionUtils.intersection(element1, element2);
                while (iterators.hasNext()) {
                    ArrayList nextElement = (ArrayList) iterators.next();
                    ArrayList nIntersection = (ArrayList) CollectionUtils.intersection(nextElement, interse);
                    if (nIntersection.isEmpty()) {
                        pstmtSentenceQuery.close();
                        return false;
                    }
                    interse = nIntersection;
                }
                Iterator it4 = interse.iterator();

                sentenceMetadata.setId((Double) it4.next());
                pstmtSentenceQuery.close();
                return true;
            }
            // sentence size is zero
        } else {

            PreparedStatement pstmtSentenceQuery = con.prepareStatement(
                    "SELECT sentence_id "
                    + "FROM dbo_sentence "
                    + "WHERE sentence_length = ?");
            pstmtSentenceQuery.setInt(1, 0);
            ResultSet rs = pstmtSentenceQuery.executeQuery();
            if (rs.next()) {
                Double sentenceID = rs.getDouble("sentence_id");
                sentenceMetadata.setId(sentenceID);
                rs.close();
                pstmtSentenceQuery.close();
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean isIndexedSentence3(PSentence psentence, Metadata sentenceMetadata) throws SQLException {

        if (psentence.getSentenceLength() != 0) {
            // if sentence length is 1 then it is a special ocasion 
            if (psentence.getSentenceLength() == 1) {
                PreparedStatement pstmtSentenceQuery = con.prepareStatement(
                        "SELECT dbo_sentence_lexeme.sentence_id "
                        + "FROM dbo_sentence_lexeme "
                        + "INNER JOIN dbo_sentence ON"
                        + " dbo_sentence.sentence_id = "
                        + "dbo_sentence_lexeme.sentence_id "
                        + "WHERE (((dbo_sentence_lexeme.lexeme_id)= ? )"
                        + " AND ((dbo_sentence_lexeme.lexeme_position)= ? )"
                        + " AND ((dbo_sentence.sentence_length)= ? ))");
                pstmtSentenceQuery.setDouble(1, psentence.getSentenceLexemes().get(0).getId());
                pstmtSentenceQuery.setInt(2, 0);
                pstmtSentenceQuery.setInt(3, psentence.getSentenceLength());
                ResultSet rsID = pstmtSentenceQuery.executeQuery();
                while (rsID.next()) {
                    double ans = rsID.getDouble("sentence_id");
                    sentenceMetadata.setId(ans);
                    pstmtSentenceQuery.close();
                    rsID.close();
                    return true;
                }
                rsID.close();
                pstmtSentenceQuery.close();

                return false;
            }

            // ok, the length of sentence is 2 or more
            // first let's sort based on the frequency of lexeme, this means 
            // stop words and other junks will go end
            List<LexemeForSearch> lfsList = new ArrayList<LexemeForSearch>();
            for (int i = 0; i < psentence.getSentenceLexemes().size(); i++) {
                lfsList.add(new LexemeForSearch(psentence.getSentenceLexemes().get(i), i));
            }
            Collections.sort(lfsList);
            PreparedStatement pstmtSentenceQuery = null;


            ArrayList<Double> finalIntersection = null;
            for (int i = 0; i < psentence.getSentenceLength() - 1; i++) {
                pstmtSentenceQuery = con.prepareStatement(
                        "SELECT dbo_sentence_lexeme.sentence_id "
                        + "FROM dbo_sentence_lexeme "
                        + "INNER JOIN dbo_sentence ON"
                        + " dbo_sentence.sentence_id = "
                        + "dbo_sentence_lexeme.sentence_id "
                        + "WHERE (((dbo_sentence_lexeme.lexeme_id)= ? )"
                        + " AND ((dbo_sentence_lexeme.lexeme_position)= ? )"
                        + " AND ((dbo_sentence.sentence_length)= ? ))");
                pstmtSentenceQuery.setDouble(1, lfsList.get(i).getId());
                pstmtSentenceQuery.setInt(2, lfsList.get(i).getPosition());
                pstmtSentenceQuery.setInt(3, psentence.getSentenceLength());

                ResultSet rsID = pstmtSentenceQuery.executeQuery();
                ArrayList<Double> idList1 = new ArrayList<Double>();
                while (rsID.next()) {
                    double ans = rsID.getDouble("sentence_id");
                    idList1.add(ans);
                }
                if (idList1.isEmpty()) {
                    rsID.close();
                    pstmtSentenceQuery.close();
                    return false;
                }

                pstmtSentenceQuery = con.prepareStatement(
                        "SELECT dbo_sentence_lexeme.sentence_id "
                        + "FROM dbo_sentence_lexeme "
                        + "INNER JOIN dbo_sentence ON"
                        + " dbo_sentence.sentence_id = "
                        + "dbo_sentence_lexeme.sentence_id "
                        + "WHERE (((dbo_sentence_lexeme.lexeme_id)= ? )"
                        + " AND ((dbo_sentence_lexeme.lexeme_position)= ? )"
                        + " AND ((dbo_sentence.sentence_length)= ? ))");
                pstmtSentenceQuery.setDouble(1, lfsList.get(i + 1).getId());
                pstmtSentenceQuery.setInt(2, lfsList.get(i + 1).getPosition());
                pstmtSentenceQuery.setInt(3, psentence.getSentenceLength());
                rsID = pstmtSentenceQuery.executeQuery();
                ArrayList<Double> idList2 = new ArrayList<Double>();
                while (rsID.next()) {
                    double ans = rsID.getDouble("sentence_id");
                    idList2.add(ans);
                }
                if (idList2.isEmpty()) {
                    rsID.close();
                    pstmtSentenceQuery.close();
                    return false;
                }
                ArrayList<Double> intersection =
                        (ArrayList<Double>) CollectionUtils.intersection(idList1, idList2);

                if (i == 0) {
                    finalIntersection = intersection;
                } else {
                    ArrayList<Double> fIntersection = (ArrayList<Double>) CollectionUtils.intersection(intersection, finalIntersection);
                    finalIntersection = fIntersection;
                    if (finalIntersection.isEmpty()) {
                        rsID.close();
                        pstmtSentenceQuery.close();
                        return false;
                    }
                }
            }


            if (!pstmtSentenceQuery.isClosed()) {
                pstmtSentenceQuery.close();
            }

            // Ok looks this sentence is repeated
            if (finalIntersection.isEmpty()) {
                return false;
            } else {
                sentenceMetadata.setId(finalIntersection.get(0));
                return true;
            }

        } else {
            PreparedStatement pstmtSentenceQuery = con.prepareStatement(
                    "SELECT sentence_id "
                    + "FROM dbo_sentence "
                    + "WHERE sentence_length = ?");
            pstmtSentenceQuery.setInt(1, 0);
            ResultSet rs = pstmtSentenceQuery.executeQuery();
            while (rs.next()) {
                Double sentenceID = rs.getDouble("sentence_id");
                sentenceMetadata.setId(sentenceID);
                rs.close();
                pstmtSentenceQuery.close();
                return true;
            }
            rs.close();
            pstmtSentenceQuery.close();
            return false;
        }
    }

    /**
     *
     * @param phraseLexemes
     * @param phraseType
     * @return long[]
     * @throws java.sql.SQLException
     * The method looks if a phrase already indexed in the database, the output of
     * method is in the form of an array of two long nomber, the first number
     * referes to phrase_id (if phrase is already indexed) and the second number
     * tells if the method has been succeed in his task or not
     * So the output is:
     *  if phrase has been already indexed: {phraseindex, 1}
     *  otherwise {-1, 0}
     */
    public boolean isIndexedPhrase2(PChunk pchunk, Metadata metadata, HashMap<PLexeme, Metadata> lexemeMap) throws SQLException {
        if (pchunk.getLength() != 0) {
            // if sentence length is 1 then it is a special ocasion
            PreparedStatement pstmtPhraseQuery = null;
            if (pchunk.getLength() == 1) {
                pstmtPhraseQuery = con.prepareStatement(
                        "SELECT dbo_phrase.phrase_id "
                        + "FROM dbo_phrase INNER JOIN dbo_phrase_lexeme"
                        + " ON dbo_phrase.phrase_id = "
                        + "dbo_phrase_lexeme.phrase_id "
                        + "WHERE (((dbo_phrase.phrase_type)= ? )"
                        + " AND ((dbo_phrase.phrase_length)= ? )"
                        + " AND ((dbo_phrase_lexeme.lexeme_id)= ?)"
                        + " AND ((dbo_phrase_lexeme.lexeme_position)= ? ))");

                pstmtPhraseQuery.setString(1, pchunk.getType());
                pstmtPhraseQuery.setInt(2, 1);
                pstmtPhraseQuery.setDouble(3, lexemeMap.get(
                        pchunk.getpLexemeList().get(0)).getId());
                pstmtPhraseQuery.setInt(4, 0);
                ResultSet rsID = pstmtPhraseQuery.executeQuery();
                while (rsID.next()) {
                    double ans = rsID.getDouble("phrase_id");
                    metadata.setId(ans);
                    pstmtPhraseQuery.close();
                    rsID.close();
                    return true;
                }
                rsID.close();
                pstmtPhraseQuery.close();
                return false;
            }
            // phrase length is more than 1
            List<LexemeForSearch> lfsList = new ArrayList<LexemeForSearch>();
            for (int i = 0; i < pchunk.getpLexemeList().size(); i++) {
                lfsList.add(new LexemeForSearch(lexemeMap.get(pchunk.getpLexemeList().get(i)), i));
            }
            Collections.sort(lfsList);
            ArrayList<Double> finalIntersection = null;
            for (int i = 0; i < pchunk.getLength() - 1; i++) {
                pstmtPhraseQuery = con.prepareStatement(
                        "SELECT dbo_phrase.phrase_id "
                        + "FROM dbo_phrase INNER JOIN dbo_phrase_lexeme"
                        + " ON dbo_phrase.phrase_id = "
                        + "dbo_phrase_lexeme.phrase_id "
                        + "WHERE (((dbo_phrase.phrase_type)= ? )"
                        + " AND ((dbo_phrase.phrase_length)= ? )"
                        + " AND ((dbo_phrase_lexeme.lexeme_id)= ?)"
                        + " AND ((dbo_phrase_lexeme.lexeme_position)= ? ))");

                pstmtPhraseQuery.setString(1, pchunk.getType());
                pstmtPhraseQuery.setInt(2, pchunk.getLength());
                pstmtPhraseQuery.setDouble(3, lfsList.get(i).getId());
                pstmtPhraseQuery.setInt(4, lfsList.get(i).getPosition());
                ResultSet rsID = pstmtPhraseQuery.executeQuery();
                ArrayList<Double> idList1 = new ArrayList<Double>();
                while (rsID.next()) {
                    double ans = rsID.getDouble("phrase_id");
                    idList1.add(ans);
                }
                if (idList1.isEmpty()) {
                    rsID.close();
                    pstmtPhraseQuery.close();
                    return false;
                }
                pstmtPhraseQuery = con.prepareStatement(
                        "SELECT dbo_phrase.phrase_id "
                        + "FROM dbo_phrase INNER JOIN dbo_phrase_lexeme"
                        + " ON dbo_phrase.phrase_id = "
                        + "dbo_phrase_lexeme.phrase_id "
                        + "WHERE (((dbo_phrase.phrase_type)= ? )"
                        + " AND ((dbo_phrase.phrase_length)= ? )"
                        + " AND ((dbo_phrase_lexeme.lexeme_id)= ?)"
                        + " AND ((dbo_phrase_lexeme.lexeme_position)= ? ))");

                pstmtPhraseQuery.setString(1, pchunk.getType());
                pstmtPhraseQuery.setInt(2, pchunk.getLength());
                pstmtPhraseQuery.setDouble(3, lfsList.get(i + 1).getId());
                pstmtPhraseQuery.setInt(4, lfsList.get(i + 1).getPosition());
                rsID = pstmtPhraseQuery.executeQuery();
                ArrayList<Double> idList2 = new ArrayList<Double>();
                while (rsID.next()) {
                    double ans = rsID.getDouble("phrase_id");
                    idList2.add(ans);
                }
                if (idList2.isEmpty()) {
                    rsID.close();
                    pstmtPhraseQuery.close();
                    return false;
                }

                ArrayList<Double> intersection =
                        (ArrayList<Double>) CollectionUtils.intersection(idList1, idList2);

                if (i == 0) {
                    finalIntersection = intersection;
                } else {
                    ArrayList<Double> fIntersection = (ArrayList<Double>) CollectionUtils.intersection(intersection, finalIntersection);
                    finalIntersection = fIntersection;
                    if (finalIntersection.isEmpty()) {
                        rsID.close();
                        pstmtPhraseQuery.close();
                        return false;
                    }
                }
            }


            if (!pstmtPhraseQuery.isClosed()) {
                pstmtPhraseQuery.close();
            }


            // Ok looks this sentence is repeated
            if (finalIntersection.isEmpty()) {
                return false;
            } else {
                metadata.setId(finalIntersection.get(0));
                return true;
            }
        }else {
            // phrase of length zero impossible but let's consider even impossible
            PreparedStatement pstmtPhraseQuery = con.prepareStatement(
                    "SELECT dbo_phrase.phrase_id "
                    + "FROM dbo_phrase "
                    + "WHERE (((dbo_phrase.phrase_type)= ? )"
                    + " AND ((dbo_phrase.phrase_length)= ? ))");

            pstmtPhraseQuery.setString(1, pchunk.getType());
            pstmtPhraseQuery.setInt(2, 0);
            ResultSet rsID = pstmtPhraseQuery.executeQuery();
            while (rsID.next()) {
                double ans = rsID.getDouble("phrase_id");
                metadata.setId(ans);
                pstmtPhraseQuery.close();
                rsID.close();
                return true;
            }
            rsID.close();
            pstmtPhraseQuery.close();
            return false;
        }
    }

    public boolean isIndexedPhrase(PChunk pchunk, Metadata metadata, HashMap<PLexeme, Metadata> lexemeMap) throws SQLException {

        ArrayList<ArrayList<Double>> phraseID = new ArrayList<ArrayList<Double>>();
        PreparedStatement pstmtPhraseQuery = null;
        for (int i = 0; i < pchunk.getpLexemeList().size(); i++) {
            pstmtPhraseQuery = con.prepareStatement(
                    "SELECT dbo_phrase.phrase_id "
                    + "FROM dbo_phrase INNER JOIN dbo_phrase_lexeme"
                    + " ON dbo_phrase.phrase_id = "
                    + "dbo_phrase_lexeme.phrase_id "
                    + "WHERE (((dbo_phrase.phrase_type)= ? )"
                    + " AND ((dbo_phrase.phrase_length)= ? )"
                    + " AND ((dbo_phrase_lexeme.lexeme_id)= ?)"
                    + " AND ((dbo_phrase_lexeme.lexeme_position)= ? ))");
            pstmtPhraseQuery.setString(1, pchunk.getType());
            pstmtPhraseQuery.setInt(2, pchunk.getLength());
            pstmtPhraseQuery.setDouble(3, lexemeMap.get(
                    pchunk.getpLexemeList().get(i)).getId());
            pstmtPhraseQuery.setInt(4, i);

            ResultSet rsID = pstmtPhraseQuery.executeQuery();
            int count = 0;
            ArrayList<Double> idList = new ArrayList<Double>();
            while (rsID.next()) {
                idList.add(count, rsID.getDouble("phrase_id"));
                count++;
            }
            rsID.close();
            if (idList.isEmpty()) {
                pstmtPhraseQuery.close();
                return false;
            }
            phraseID.add(i, idList);
        }

        if (phraseID.isEmpty()) {
            pstmtPhraseQuery.close();
            return false;
        }

        if (phraseID.size() == 1) {
            Iterator iterators = phraseID.iterator();
            ArrayList element1 = (ArrayList) iterators.next();
            if (element1.isEmpty()) {
                pstmtPhraseQuery.close();
                return false;
            } else {
                Iterator it2 = element1.iterator();
                pstmtPhraseQuery.close();
                metadata.setId((Double) it2.next());
                return true;
            }
        }
        if (phraseID.size() == 2) {
            Iterator iterators = phraseID.iterator();
            ArrayList element1 = (ArrayList) iterators.next();
            ArrayList element2 = (ArrayList) iterators.next();
            ArrayList interse = (ArrayList) CollectionUtils.intersection(element1, element2);

            if (interse.isEmpty()) {
                pstmtPhraseQuery.close();
                return false;

            } else {
                Iterator it4 = interse.iterator();
                pstmtPhraseQuery.close();
                metadata.setId((Double) it4.next());
                return true;
            }

        } else {
            Iterator iterators = phraseID.iterator();
            ArrayList element1 = (ArrayList) iterators.next();
            ArrayList element2 = (ArrayList) iterators.next();
            ArrayList interse = (ArrayList) CollectionUtils.intersection(element1, element2);
            while (iterators.hasNext()) {
                ArrayList nextElement = (ArrayList) iterators.next();
                ArrayList nIntersection = (ArrayList) CollectionUtils.intersection(nextElement, interse);
                if (nIntersection.isEmpty()) {
                    pstmtPhraseQuery.close();
                    return false;
                }
                interse = nIntersection;
            }
            Iterator it4 = interse.iterator();
            pstmtPhraseQuery.close();
            metadata.setId((Double) it4.next());
            return true;
        }
    }

    /**
     *
     * @param sentenceID
     * @param lexemesDependencyID
     * @param governorPosition
     * @param regentPosition
     * @throws java.sql.SQLException
     */
    public void indexSentenceLexemesDependency(double sentenceID,
            List<SentenceDependency> sentenceDependencies,
            HashMap<Dependency, Metadata> dependencyMap,
            String dependencySource)
            throws SQLException {

        // maybe need to check if there is already something asserted with the followings:
//                ResultSet isrs = stmtSentenceLexemesDependency.executeQuery(
//                "SELECT lexemes_dependency_id FROM dbo_sentence_dep_" + source + " WHERE "
//                + "(sentence_id=" + sentenceID + ") "
//                + "AND "
//                + "(lexemes_dependency_id=" + lexemesDependencyID + ") " + "AND "
//                + "(governor_position=" + governorPosition + ") "
//                + "AND "
//                + "(regent_position=" + regentPosition + ")");

        PreparedStatement pstmtDependencyInst = null;
        if (sentenceDependencies != null) {
            for (SentenceDependency sd : sentenceDependencies) {
                pstmtDependencyInst = con.prepareStatement(
                        "INSERT INTO dbo_sentence_dep_" + dependencySource
                        + " VALUES( ?, ?, ?, ? )");
                pstmtDependencyInst.setDouble(1, sentenceID);
                pstmtDependencyInst.setDouble(2,
                        dependencyMap.get(sd.getDependency()).getId());
                pstmtDependencyInst.setInt(3, sd.getGovernorPosition());
                pstmtDependencyInst.setInt(4, sd.getRegentPosition());
                pstmtDependencyInst.executeUpdate();
            }
        }
        con.commit();
        if (pstmtDependencyInst != null) {
            pstmtDependencyInst.close();
        }
    }

    /**
     * 
     * @param dep
     * @param metadata
     * @param dependencySource
     * @param lexemeMap
     * @throws SQLException 
     */
    public void indexLexemeDependency(Dependency dep,
            Metadata metadata,
            String dependencySource,
            HashMap<PLexeme, Metadata> lexemeMap) throws SQLException {
        PreparedStatement pstmtDependencyQuery = con.prepareStatement(
                "SELECT lexemes_dependency_id, dependency_frequency"
                + " FROM dbo_lexemes_dep_" + dependencySource + " WHERE ("
                + " ( governor_lexeme_id = ? ) AND"
                + " ( regent_lexeme_id = ? ) AND "
                + " ( dependency_type = ? ))");
        pstmtDependencyQuery.setDouble(1,
                lexemeMap.get(dep.getGovernorLexeme()).getId());
        pstmtDependencyQuery.setDouble(2,
                lexemeMap.get(dep.getRegentLexeme()).getId());
        pstmtDependencyQuery.setString(3, dep.getDependencyType());
        ResultSet rsd = pstmtDependencyQuery.executeQuery();
        if (rsd.next()) {
            Double depndencyID = rsd.getDouble("lexemes_dependency_id");
            int dependencyFrequency = rsd.getInt("dependency_frequency");
            int NewFrequency = dependencyFrequency + metadata.getFrequency();
            PreparedStatement pstmtDependencyUpdate = con.prepareStatement(
                    "UPDATE dbo_lexemes_dep_" + dependencySource
                    + " SET dependency_frequency = ? "
                    + " WHERE ( lexemes_dependency_id = ? )");
            pstmtDependencyUpdate.setInt(1, NewFrequency);
            pstmtDependencyUpdate.setDouble(2, depndencyID);
            pstmtDependencyUpdate.executeUpdate();
            con.commit();
            pstmtDependencyUpdate.close();
            pstmtDependencyQuery.close();
            rsd.close();

            metadata.setId(depndencyID);
            metadata.setFrequency(NewFrequency);
        } else {
            PreparedStatement pstmtDependencyInsert = con.prepareStatement(
                    "INSERT INTO dbo_lexemes_dep_" + dependencySource
                    + " (LEXEMES_DEPENDENCY_ID,	"
                    + " DEPENDENCY_TYPE,"
                    + " GOVERNOR_LEXEME_ID,"
                    + " REGENT_LEXEME_ID,"
                    + " DEPENDENCY_FREQUENCY ) VALUES(?, ?, ?, ?, ? ) ");
            pstmtDependencyInsert.setDouble(1, metadata.getId());
            pstmtDependencyInsert.setString(2, dep.getDependencyType());
            pstmtDependencyInsert.setDouble(3,
                    lexemeMap.get(dep.getGovernorLexeme()).getId());
            pstmtDependencyInsert.setDouble(4,
                    lexemeMap.get(dep.getRegentLexeme()).getId());
            pstmtDependencyInsert.setInt(5, metadata.getFrequency());
            pstmtDependencyInsert.executeUpdate();
            con.commit();
            pstmtDependencyQuery.close();
            pstmtDependencyInsert.close();
        }
    }

    public boolean isIndexedParagraph(PParagraph paragraph, Metadata metadata)
            throws SQLException {
        if (!paragraph.getParagraphSentencesMetadata().isEmpty()) {
            ArrayList<ArrayList<Double>> paragraphID =
                    new ArrayList<ArrayList<Double>>();

            for (int i = 0; i < paragraph.getParagraphLength(); i++) {
                PreparedStatement pstmtPara = con.prepareStatement(
                        "SELECT dbo_paragraph_sentence.paragraph_id "
                        + "FROM dbo_paragraph_sentence "
                        + "INNER JOIN dbo_paragraph ON"
                        + " dbo_paragraph.paragraph_id = "
                        + "dbo_paragraph_sentence.paragraph_id "
                        + "WHERE (((dbo_paragraph_sentence.sentence_id)= ? )"
                        + " AND ((dbo_paragraph_sentence.sentence_position)= ? )"
                        + " AND ((dbo_paragraph.paragraph_sentence_length)= ? ))");
                pstmtPara.setDouble(1,
                        paragraph.getParagraphSentencesMetadata().get(i).getId());

                pstmtPara.setInt(2, i);
                pstmtPara.setInt(3, paragraph.getParagraphLength());
                ResultSet rsID = pstmtPara.executeQuery();
                int count = 0;
                ArrayList<Double> idList = new ArrayList<Double>();
                while (rsID.next()) {
                    double ans = rsID.getDouble("paragraph_id");
                    idList.add(count, ans);
                    count++;
                }
                if (idList.isEmpty()) {
                    pstmtPara.close();
                    rsID.close();
                    return false;
                }
                paragraphID.add(i, idList);
                rsID.close();
                pstmtPara.close();
            }

            if (paragraphID.isEmpty()) {
                return false;
            }
            if (paragraphID.size() == 1) {
                Iterator iterators = paragraphID.iterator();
                ArrayList element1 = (ArrayList) iterators.next();
                if (element1.isEmpty()) {
                    return false;
                } else {
                    Iterator it2 = element1.iterator();
                    metadata.setId((Double) it2.next());
                    return true;
                }
            }
            if (paragraphID.size() == 2) {
                Iterator iterators = paragraphID.iterator();
                ArrayList element1 = (ArrayList) iterators.next();
                ArrayList element2 = (ArrayList) iterators.next();
                ArrayList interse = (ArrayList) CollectionUtils.intersection(element1, element2);
                if (interse.isEmpty()) {
                    return false;
                } else {
                    Iterator it4 = interse.iterator();
                    metadata.setId((Double) it4.next());
                    return true;
                }
            } else {
                // this is the condition we have more than two arrays and we need
                // to calculate the intersection between them
                Iterator iterators = paragraphID.iterator();
                ArrayList element1 = (ArrayList) iterators.next();
                ArrayList element2 = (ArrayList) iterators.next();
                ArrayList interse = (ArrayList) CollectionUtils.intersection(element1, element2);
                while (iterators.hasNext()) {
                    ArrayList nextElement = (ArrayList) iterators.next();
                    ArrayList nIntersection = (ArrayList) CollectionUtils.intersection(nextElement, interse);
                    if (nIntersection.isEmpty()) {
                        return false;
                    }
                    interse = nIntersection;
                }
                Iterator it4 = interse.iterator();
                metadata.setId((Double) it4.next());
                return true;
            }
        } else {
            PreparedStatement pstmtPrguery = con.prepareStatement(
                    "SELECT paragraph_id "
                    + "FROM dbo_paragraph "
                    + "WHERE paragraph_sentence_length = ?");
            pstmtPrguery.setInt(1, 0);
            ResultSet rs = pstmtPrguery.executeQuery();
            if (rs.next()) {
                Double paragraphID = rs.getDouble("paragraph_id");
                metadata.setId(paragraphID);
                rs.close();
                pstmtPrguery.close();
                return true;
            } else {
                return false;
            }
        }

    }

    public void indexParagraph(PParagraph paragraph, Metadata metadata) throws SQLException {
        if (isIndexedParagraph(paragraph, metadata)) {
            PreparedStatement pstmtParaFreq = con.prepareStatement(
                    "SELECT paragraph_frequency FROM dbo_paragraph WHERE "
                    + "paragraph_id= ? ");
            pstmtParaFreq.setDouble(1, metadata.getId());
            ResultSet rs = pstmtParaFreq.executeQuery();
            rs.next();
            int newFrequency = rs.getInt("paragraph_frequency") + metadata.getFrequency();
            metadata.setFrequency(newFrequency);
            pstmtParaFreq.close();
            rs.close();

            PreparedStatement pstmtUpdatePara = con.prepareStatement(
                    "UPDATE dbo_paragraph SET paragraph_frequency= ?"
                    + " WHERE paragraph_id= ? ");
            pstmtUpdatePara.setInt(1, newFrequency);
            pstmtUpdatePara.setDouble(2, metadata.getId());
            pstmtUpdatePara.executeUpdate();
            con.commit();
            pstmtUpdatePara.close();
        } else {
            PreparedStatement pstmtInstPara = con.prepareStatement(
                    "INSERT INTO dbo_paragraph VALUES( ?, ?, ?)");
            pstmtInstPara.setDouble(1, metadata.getId());
            pstmtInstPara.setInt(2, paragraph.getParagraphLength());
            pstmtInstPara.setInt(3, metadata.getFrequency());
            pstmtInstPara.executeUpdate();
            con.commit();
            pstmtInstPara.close();

            for (int ix = 0; ix < paragraph.getParagraphSentencesMetadata().size(); ix++) {
                PreparedStatement pstmtParaSnt = con.prepareStatement(
                        "INSERT INTO dbo_paragraph_sentence VALUES(?, ?, ?)");
                pstmtParaSnt.setDouble(1, metadata.getId());
                pstmtParaSnt.setDouble(2,
                        paragraph.getParagraphSentencesMetadata().get(ix).getId());
                pstmtParaSnt.setInt(3, ix);
                pstmtParaSnt.executeUpdate();
                pstmtParaSnt.close();
            }
            con.commit();
            PreparedStatement jstmtParaContent = con.prepareStatement(
                    "INSERT INTO dbo_content VALUES( ?, ?)");
            jstmtParaContent.setDouble(1, metadata.getId());
            jstmtParaContent.setString(2, "paragraph");
            jstmtParaContent.executeUpdate();
            con.commit();
            jstmtParaContent.close();
        }

    }

    public void indexSection(PSection psection, Metadata metadata,
            HashMap<PSection, Metadata> sectionHashMap,
            HashMap<PSentence, Metadata> sentenceHashMap,
            HashMap<PParagraph, Metadata> paragraphHashMap,
            HashMap<PFigure, Metadata> figureHashMap,
            HashMap<PTable, Metadata> tableHashMap,
            HashMap<PEquation, Metadata> equationHashMap) throws SQLException {

        for (int i = 0; i < psection.getSectionContent().size(); i++) {
            if (psection.getSectionContent().get(i) instanceof PSection) {

                indexSection((PSection) psection.getSectionContent().get(i),
                        sectionHashMap.get((PSection) psection.getSectionContent().get(i)),
                        sectionHashMap,
                        sentenceHashMap,
                        paragraphHashMap,
                        figureHashMap,
                        tableHashMap,
                        equationHashMap);
            }

        }


        if (psection.getSectionContent().isEmpty()) {

            Double sectionTitleSentence = sentenceHashMap.get(
                    psection.getTitleSentence()).getId();
            PreparedStatement jstmtSection = con.prepareStatement(
                    "SELECT * FROM dbo_section WHERE "
                    + "( (section_type = ?) AND "
                    + "(section_header_sentence_id= ?)"
                    + "AND (section_length = ? )");
            jstmtSection.setString(1, psection.getType());
            jstmtSection.setDouble(2, sectionTitleSentence);
            jstmtSection.setInt(3, 0);
            ResultSet rs = jstmtSection.executeQuery();
            while (rs.next()) {
                System.err.println("There are section with 0 length in the answers");
                double finalSectionID = rs.getDouble("section_id");
                boolean isIndexedFigureTbl = true;
                for (int i = 0; i < psection.getFigureList().size(); i++) {
                    Metadata figMeta = figureHashMap.get(psection.getFigureList().get(i));
                    isIndexedFigureTbl =
                            (isIndexedSectionFigure(finalSectionID, figMeta.getId())
                            && isIndexedFigureTbl);
                    System.err.println("is indexed figure  " + isIndexedFigureTbl);
                }
                for (int i = 0; i < psection.getTableList().size(); i++) {
                    Metadata tblMeta =
                            tableHashMap.get(psection.getTableList().get(i));
                    isIndexedFigureTbl =
                            (isIndexedSectionTable(finalSectionID,
                            tblMeta.getId())
                            && isIndexedFigureTbl);
                    System.err.println("is indexed table " + isIndexedFigureTbl);
                }
                if (isIndexedFigureTbl) {
                    System.err.println("passed the test for table and figure");
                    int freq = rs.getInt("section_frequency");
                    int newFrequency = freq + metadata.getFrequency();
                    System.err.println("frequency from tables is " + freq);
                    PreparedStatement updtStmtSection =
                            con.prepareStatement(
                            "UPDATE dbo_section SET section_frequency= ? "
                            + "WHERE section_id= ?");

                    updtStmtSection.setInt(1, newFrequency);
                    updtStmtSection.setDouble(2, finalSectionID);
                    metadata.setFrequency(newFrequency);
                    metadata.setId(finalSectionID);
                    System.err.println("and return here");
                    return;
                }
            }

            // getting here means the section is new
            System.err.println("section length 0 and not found in db");

            PreparedStatement jstmtInsSection = con.prepareStatement(
                    "INSERT INTO dbo_section"
                    + "(section_id, section_type, "
                    + "section_header_sentence_id, "
                    + "section_length, "
                    + "section_frequency) "
                    + "VALUES( ?, ?, ?, ?, ? )");
            jstmtInsSection.setDouble(1, metadata.getId());
            jstmtInsSection.setString(2, psection.getType());
            jstmtInsSection.setDouble(3, sectionTitleSentence);
            jstmtInsSection.setInt(4, 0);
            jstmtInsSection.setInt(4, metadata.getFrequency());
            jstmtInsSection.executeUpdate();
            con.commit();
            jstmtInsSection.close();


            PreparedStatement jstmtSectContent = con.prepareStatement(
                    "INSERT INTO dbo_content("
                    + "content_id, "
                    + "content_type) "
                    + " VALUES( ?, ?)");
            jstmtSectContent.setDouble(1, metadata.getId());
            jstmtSectContent.setString(2, "section");
            jstmtSectContent.executeUpdate();
            con.commit();
            jstmtSectContent.close();

            // insert tables
            System.err.println("inserting tables for section with length zero");
            for (int i = 0; i < psection.getTableList().size(); i++) {
                double tblID = tableHashMap.get(psection.getTableList().get(i)).getId();
                PreparedStatement jstmtSectTblContent = con.prepareStatement(
                        "INSERT INTO dbo_section_table "
                        + "( section_id, table_id ) "
                        + " VALUES( ?, ? )");
                jstmtSectTblContent.setDouble(1, metadata.getId());
                jstmtSectTblContent.setDouble(2, tblID);
                jstmtSectTblContent.executeUpdate();
                con.commit();
                jstmtSectTblContent.close();
            }

            System.err.println("inserting figures for section with length zero");

            //insert figures
            for (int i = 0; i < psection.getFigureList().size(); i++) {
                Double figID = figureHashMap.get(psection.getFigureList().get(i)).getId();
                PreparedStatement jstmtSectfigContent = con.prepareStatement(
                        "INSERT INTO dbo_section_figure "
                        + "( section_id, figure_id ) "
                        + " VALUES( ?, ? )");
                jstmtSectfigContent.setDouble(1, metadata.getId());
                jstmtSectfigContent.setDouble(2, figID);
                jstmtSectfigContent.executeUpdate();
                con.commit();
                jstmtSectfigContent.close();
            }
            System.err.println("and finish");
        } else {

            // the section length is more than zero
            if (isIndexedSection(
                    psection,
                    metadata,
                    sectionHashMap,
                    paragraphHashMap,
                    equationHashMap,
                    tableHashMap,
                    figureHashMap)) {

                PreparedStatement jstmtSect = con.prepareStatement(
                        "SELECT section_frequency FROM dbo_section WHERE "
                        + "section_id= ?");
                jstmtSect.setDouble(1, metadata.getId());
                ResultSet rs = jstmtSect.executeQuery();
                rs.next();
                int dbFreq = rs.getInt("section_frequency");
                int newFrequency =
                        dbFreq
                        + metadata.getFrequency();
                con.commit();
                jstmtSect.close();
                rs.close();
                PreparedStatement jstmtUpdtSect = con.prepareStatement(
                        "UPDATE dbo_section SET section_frequency= ? "
                        + " WHERE section_id= ?");
                jstmtUpdtSect.setInt(1, newFrequency);
                jstmtUpdtSect.setDouble(2, metadata.getId());
                jstmtUpdtSect.executeUpdate();
                con.commit();
                metadata.setFrequency(newFrequency);
                jstmtUpdtSect.close();
            } else {

                // new section and content list
                double sectionTitleSentence = sentenceHashMap.get(
                        psection.getTitleSentence()).getId();
                PreparedStatement jstmtInsSection = con.prepareStatement(
                        "INSERT INTO dbo_section"
                        + "(section_id, section_type, "
                        + "section_header_sentence_id, "
                        + "section_length, "
                        + "section_frequency) "
                        + "VALUES( ?, ?, ?, ?, ? )");
                jstmtInsSection.setDouble(1, metadata.getId());
                jstmtInsSection.setString(2, psection.getType());
                jstmtInsSection.setDouble(3, sectionTitleSentence);
                jstmtInsSection.setInt(4, psection.getSectionLength());
                jstmtInsSection.setInt(5, metadata.getFrequency());
                jstmtInsSection.executeUpdate();
                con.commit();
                jstmtInsSection.close();

                PreparedStatement jstmtSectContent = con.prepareStatement(
                        "INSERT INTO dbo_content("
                        + "content_id, "
                        + "content_type) "
                        + " VALUES( ?, ?)");
                jstmtSectContent.setDouble(1, metadata.getId());
                jstmtSectContent.setString(2, "section");
                jstmtSectContent.executeUpdate();
                con.commit();
                jstmtSectContent.close();

                for (int ix = 0; ix < psection.getSectionContent().size(); ix++) {

                    if (psection.getSectionContent().get(ix) instanceof PParagraph) {
                        PParagraph pp = (PParagraph) psection.getSectionContent().get(ix);
                        PreparedStatement jstmtSectParaContent = con.prepareStatement(
                                "INSERT INTO dbo_content_position "
                                + "VALUES( ?, ?, ? )");
                        jstmtSectParaContent.setDouble(1, metadata.getId());
                        jstmtSectParaContent.setDouble(2, paragraphHashMap.get(pp).getId());
                        jstmtSectParaContent.setInt(3, ix);
                        jstmtSectParaContent.executeUpdate();
                        con.commit();
                        jstmtSectParaContent.close();
                    } else if (psection.getSectionContent().get(ix) instanceof PEquation) {
                        PEquation pe = (PEquation) psection.getSectionContent().get(ix);
                        PreparedStatement jstmtSectEqContent = con.prepareStatement(
                                "INSERT INTO dbo_content_position "
                                + "VALUES( ?, ?, ? )");
                        jstmtSectEqContent.setDouble(1, metadata.getId());
                        jstmtSectEqContent.setDouble(2, equationHashMap.get(pe).getId());
                        jstmtSectEqContent.setInt(3, ix);
                        jstmtSectEqContent.executeUpdate();
                        con.commit();
                        jstmtSectEqContent.close();

                    } else if (psection.getSectionContent().get(ix) instanceof PSection) {
                        PSection psect = (PSection) psection.getSectionContent().get(ix);
                        PreparedStatement jstmtSectEqContent = con.prepareStatement(
                                "INSERT INTO dbo_content_position "
                                + "VALUES( ?, ?, ? )");
                        jstmtSectEqContent.setDouble(1, metadata.getId());
                        jstmtSectEqContent.setDouble(2, sectionHashMap.get(psect).getId());
                        jstmtSectEqContent.setInt(3, ix);
                        jstmtSectEqContent.executeUpdate();
                        con.commit();
                        jstmtSectEqContent.close();

                    } else {
                        System.err.println(">>> !!! >>> unseen type of class in indexSection method: " + psection.getSectionContent().getClass().getCanonicalName());
                    }
                }
                // add figures and tables
                for (int i = 0; i < psection.getTableList().size(); i++) {
                    double tblID = tableHashMap.get(psection.getTableList().get(i)).getId();
                    PreparedStatement jstmtSectTblContent = con.prepareStatement(
                            "INSERT INTO dbo_section_table "
                            + "( section_id, table_id ) "
                            + " VALUES( ?, ?)");
                    jstmtSectTblContent.setDouble(1, metadata.getId());
                    jstmtSectTblContent.setDouble(2, tblID);
                    jstmtSectTblContent.executeUpdate();
                    con.commit();
                    jstmtSectTblContent.close();
                }
                for (int i = 0; i < psection.getFigureList().size(); i++) {
                    double figID = figureHashMap.get(psection.getFigureList().get(i)).getId();
                    PreparedStatement jstmtSectfigContent = con.prepareStatement(
                            "INSERT INTO dbo_section_figure "
                            + "( section_id, figure_id ) "
                            + " VALUES( ?, ? )");
                    jstmtSectfigContent.setDouble(1, metadata.getId());
                    jstmtSectfigContent.setDouble(2, figID);
                    jstmtSectfigContent.executeUpdate();
                    con.commit();
                    jstmtSectfigContent.close();
                }
            }
        }
    }

    public boolean isIndexedSection(PSection psection,
            Metadata metadata,
            HashMap<PSection, Metadata> sectionHashMap,
            HashMap<PParagraph, Metadata> paraHashMap,
            HashMap<PEquation, Metadata> equationHashMap,
            HashMap<PTable, Metadata> tableHashMap,
            HashMap<PFigure, Metadata> figureHashMap) throws SQLException {



        ArrayList<ArrayList<Double>> sectionID = new ArrayList<ArrayList<Double>>();
        for (int i = 0; i < psection.getSectionContent().size(); i++) {
            PreparedStatement pstmtSectCont = con.prepareStatement(
                    "SELECT dbo_content_position.super_content_id "
                    + "FROM dbo_content_position "
                    + "INNER JOIN dbo_section ON"
                    + " dbo_section.section_id = dbo_content_position.super_content_id "
                    + "WHERE (((dbo_content_position.sub_content_id)= ? )"
                    + " AND ((dbo_content_position.content_position)= ? )"
                    + " AND ((dbo_section.section_length)= ? ))");

            if (psection.getSectionContent().get(i) instanceof PParagraph) {
                PParagraph pp = (PParagraph) psection.getSectionContent().get(i);
                Metadata ppm = paraHashMap.get(pp);
                pstmtSectCont.setDouble(1, ppm.getId());
            } else if (psection.getSectionContent().get(i) instanceof PEquation) {
                PEquation pe = (PEquation) psection.getSectionContent().get(i);
                Metadata ppe = equationHashMap.get(pe);
                pstmtSectCont.setDouble(1, ppe.getId());
            } else if (psection.getSectionContent().get(i) instanceof PSection) {

                PSection psect = (PSection) psection.getSectionContent().get(i);
                Metadata psectm = sectionHashMap.get(psect);
                pstmtSectCont.setDouble(1, psectm.getId());

                // add section
                // if any other type rather equation and paragraph be added to
                // to section content it should appear here
            }
            pstmtSectCont.setInt(2, i);
            pstmtSectCont.setInt(3, psection.getSectionContent().size());
            ResultSet rsIDSect = pstmtSectCont.executeQuery();
            int count = 0;
            ArrayList<Double> idList = new ArrayList<Double>();
            while (rsIDSect.next()) {
                double ans = rsIDSect.getDouble("super_content_id");
                idList.add(count, ans);
                count++;
            }
            if (idList.isEmpty()) {
                rsIDSect.close();
                return false;
            }
            rsIDSect.close();
            pstmtSectCont.close();
            sectionID.add(i, idList);
        }
        if (sectionID.isEmpty()) {
            return false;
        }
        if (sectionID.size() == 1) {
            Iterator iterators = sectionID.iterator();
            ArrayList element1 = (ArrayList) iterators.next();
            if (element1.isEmpty()) {
                return false;
            } else {
                Iterator it2 = element1.iterator();
                while (it2.hasNext()) {
                    double finalSectionID = (Double) it2.next();
                    boolean isIndexedFigureTbl = true;
                    for (int i = 0; i < psection.getFigureList().size(); i++) {
                        Metadata figMeta = figureHashMap.get(psection.getFigureList().get(i));
                        isIndexedFigureTbl =
                                (isIndexedSectionFigure(finalSectionID, figMeta.getId())
                                && isIndexedFigureTbl);
                    }
                    for (int i = 0; i < psection.getTableList().size(); i++) {
                        Metadata tblMeta =
                                tableHashMap.get(psection.getTableList().get(i));
                        isIndexedFigureTbl =
                                (isIndexedSectionTable(finalSectionID,
                                tblMeta.getId())
                                && isIndexedFigureTbl);
                    }

                    if (isIndexedFigureTbl) {
                        metadata.setId(finalSectionID);
                        return true;
                    }
                }
                return false;
            }
        }
        if (sectionID.size() == 2) {
            Iterator iterators = sectionID.iterator();
            ArrayList element1 = (ArrayList) iterators.next();
            ArrayList element2 = (ArrayList) iterators.next();
            ArrayList interse = (ArrayList) CollectionUtils.intersection(element1, element2);
            if (interse.isEmpty()) {
                return false;
            } else {
                Iterator it4 = interse.iterator();
                while (it4.hasNext()) {
                    double finalSectionID = (Double) it4.next();
                    boolean isIndexedFigureTbl = true;
                    for (int i = 0; i < psection.getFigureList().size(); i++) {
                        Metadata figMeta = figureHashMap.get(psection.getFigureList().get(i));
                        isIndexedFigureTbl =
                                (isIndexedSectionFigure(finalSectionID, figMeta.getId())
                                && isIndexedFigureTbl);
                    }
                    for (int i = 0; i < psection.getTableList().size(); i++) {
                        Metadata tblMeta =
                                tableHashMap.get(psection.getTableList().get(i));
                        isIndexedFigureTbl =
                                (isIndexedSectionTable(finalSectionID,
                                tblMeta.getId())
                                && isIndexedFigureTbl);
                    }
                    if (isIndexedFigureTbl) {
                        metadata.setId(finalSectionID);
                        return true;
                    }
                }
                return false;
            }
        } else {
            // this is the condition we have more than two arrays and we need to calculate the intersection between them
            Iterator iterators = sectionID.iterator();
            ArrayList element1 = (ArrayList) iterators.next();
            ArrayList element2 = (ArrayList) iterators.next();
            ArrayList interse = (ArrayList) CollectionUtils.intersection(element1, element2);
            while (iterators.hasNext()) {
                ArrayList nextElement = (ArrayList) iterators.next();
                ArrayList nIntersection = (ArrayList) CollectionUtils.intersection(nextElement, interse);
                if (nIntersection.isEmpty()) {
                    return false;
                }
                interse = nIntersection;
            }
            Iterator it4 = interse.iterator();
            while (it4.hasNext()) {
                double finalSectionID = (Double) it4.next();
                boolean isIndexedFigureTbl = true;
                for (int i = 0; i < psection.getFigureList().size(); i++) {
                    Metadata figMeta = figureHashMap.get(psection.getFigureList().get(i));
                    isIndexedFigureTbl =
                            (isIndexedSectionFigure(finalSectionID, figMeta.getId())
                            && isIndexedFigureTbl);
                }
                for (int i = 0; i < psection.getTableList().size(); i++) {
                    Metadata tblMeta =
                            tableHashMap.get(psection.getTableList().get(i));
                    isIndexedFigureTbl =
                            (isIndexedSectionTable(finalSectionID,
                            tblMeta.getId())
                            && isIndexedFigureTbl);
                }
                if (isIndexedFigureTbl) {
                    metadata.setId(finalSectionID);
                    return true;
                }
            }
            return false;
        }
    }

    public boolean isIndexedSectionTable(
            double sectionID,
            double tableID) throws SQLException {
        PreparedStatement pstmtSectCont = con.prepareStatement(
                "SELECT * FROM dbo_section_table "
                + " WHERE (( section_id = ? )"
                + " AND (table_id= ? ))");
        pstmtSectCont.setDouble(1, sectionID);
        pstmtSectCont.setDouble(2, tableID);
        ResultSet rsID = pstmtSectCont.executeQuery();
        if (rsID.next()) {
            rsID.close();
            pstmtSectCont.close();
            return true;
        } else {
            return false;
        }
    }

    public boolean isIndexedSectionFigure(
            double sectionID,
            double figureID) throws SQLException {
        PreparedStatement pstmtSectCont = con.prepareStatement(
                "SELECT * FROM dbo_section_figure "
                + " WHERE (( section_id = ? )"
                + " AND (figure_id= ? ))");
        pstmtSectCont.setDouble(1, sectionID);
        pstmtSectCont.setDouble(2, figureID);
        ResultSet rsID = pstmtSectCont.executeQuery();
        if (rsID.next()) {
            rsID.close();
            pstmtSectCont.close();
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<Double> isIndexedDocument(double titleSentenceID,
            List<PSection> sectionList,
            HashMap<PSection, Metadata> sectionMap,
            List<PReference> referenceList,
            List<Double> authorIDList) throws SQLException {
        if (sectionList.isEmpty()) {
            PreparedStatement pstmtTitle = con.prepareStatement(
                    "SELECT document_id FROM dbo_document WHERE "
                    + "(( document_title_sentence_id = ? ) "
                    + " AND ( document_section_length =? ))");
            pstmtTitle.setDouble(1, titleSentenceID);
            pstmtTitle.setInt(2, sectionList.size());
            ResultSet rsTitle = pstmtTitle.executeQuery();
            ArrayList<Double> documentWithSameTitle = new ArrayList<Double>();
            while (rsTitle.next()) {
                documentWithSameTitle.add(rsTitle.getDouble("document_id"));
            }
            rsTitle.close();
            pstmtTitle.close();
            if (documentWithSameTitle.isEmpty()) {
                return null;
            } else {
                return documentWithSameTitle;
            }
        } else {
            int position = 0;
            boolean isFirst = true;
            ArrayList<Double> prevIDList = new ArrayList<Double>();
            // find documents that contains all the sections in the section list
            for (PSection psection : sectionList) {
                double psectionID = sectionMap.get(psection).getId();
                PreparedStatement pstmtContentPosition = con.prepareStatement(
                        "SELECT super_content_id FROM dbo_content_position WHERE "
                        + "( sub_content_id = ? AND content_position = ? )");
                pstmtContentPosition.setDouble(1, psectionID);
                pstmtContentPosition.setInt(2, position);
                ResultSet rs = pstmtContentPosition.executeQuery();
                ArrayList<Double> documentIDList = new ArrayList<Double>();
                while (rs.next()) {
                    documentIDList.add(rs.getDouble("super_content_id"));
                }
                if (documentIDList.isEmpty()) {
                    pstmtContentPosition.close();
                    return null;
                }
                position++;
                if (isFirst) {
                    prevIDList = documentIDList;
                    isFirst = false;
                } else {
                    ArrayList<Double> interSection =
                            (ArrayList) CollectionUtils.intersection(prevIDList, documentIDList);
                    if (interSection.isEmpty()) {
                        pstmtContentPosition.close();
                        return null;
                    } else {
                        pstmtContentPosition.close();
                        prevIDList = interSection;
                    }
                }
            }

            if (prevIDList.isEmpty()) {
                return null;
            } else {

                // check whether the title is the same
                // note that the extracted information here can be stored
                // in the databse e.g. as the most simmilar documents
                PreparedStatement pstmtTitle = con.prepareStatement(
                        "SELECT document_id FROM dbo_document WHERE "
                        + "(( document_title_sentence_id = ? ) "
                        + " AND ( document_section_length =? ))");
                pstmtTitle.setDouble(1, titleSentenceID);
                pstmtTitle.setInt(2, sectionList.size());
                ResultSet rsTitle = pstmtTitle.executeQuery();
                List<Double> documentWithSameTitle = new ArrayList<Double>();
                while (rsTitle.next()) {
                    documentWithSameTitle.add(rsTitle.getDouble("document_id"));
                }
                ArrayList<Double> interSection =
                        (ArrayList) CollectionUtils.intersection(prevIDList, documentWithSameTitle);
                if (interSection.isEmpty()) {
                    pstmtTitle.close();
                    rsTitle.close();
                    System.err.println("COULD NOT FIND TITLTE SENTENCE!!!!!");
                    return null;
                } else {
                    //select documents that have all author
                    // each document at the end is stored as a bibtex entry

                    for (double authorID : authorIDList) {
                        PreparedStatement pstmtAuthor =
                                con.prepareStatement(
                                "SELECT bibtex_id "
                                + "FROM dbo_bibtex_author WHERE "
                                + "( author_id = ? )");
                        pstmtAuthor.setDouble(1, authorID);
                        ResultSet rsAuthor = pstmtAuthor.executeQuery();
                        ArrayList<Double> docAuthorIDList = new ArrayList<Double>();
                        while (rsAuthor.next()) {
                            docAuthorIDList.add(rsAuthor.getDouble("bibtex_id"));
                        }
                        ArrayList<Double> interSectionWithAuthor =
                                (ArrayList) CollectionUtils.intersection(interSection, docAuthorIDList);
                        if (interSectionWithAuthor.isEmpty()) {
                            pstmtAuthor.close();
                            rsAuthor.close();
                            return null;
                        } else {
                            interSection = interSectionWithAuthor;
                        }
                    }

                    if (interSection.isEmpty()) {
                        return null;
                    } else {
                        List<Double> refDocList = new ArrayList<Double>();
                        boolean isFirstRef = true;
                        PreparedStatement pstmtReferemce = null;
                        ResultSet rsReference = null;
                        for (PReference reference : referenceList) {
                            pstmtReferemce =
                                    con.prepareStatement(
                                    "SELECT document_id "
                                    + "FROM dbo_document_reference WHERE "
                                    + "( bibtex_id = ? )");

                            pstmtReferemce.setDouble(1, reference.getReferenceID());
                            rsReference = pstmtReferemce.executeQuery();
                            ArrayList<Double> docRefIDList = new ArrayList<Double>();
                            while (rsReference.next()) {
                                docRefIDList.add(rsReference.getDouble("document_id"));
                            }
                            if (isFirstRef) {
                                refDocList = docRefIDList;
                                isFirstRef = false;
                            } else {
                                ArrayList<Double> docRefIDListInters = new ArrayList<Double>();
                                docRefIDListInters = (ArrayList) CollectionUtils.intersection(refDocList,
                                        docRefIDList);
                                refDocList = docRefIDListInters;
                            }
                        }
                        System.err.println("Size of ref intersect is " + refDocList.size());

                        ArrayList<Double> interSectionWithReference =
                                (ArrayList) CollectionUtils.intersection(interSection, refDocList);
                        if (interSectionWithReference.isEmpty()) {
                            System.err.println("GOT HERE BECYASE EMPTY");
                            pstmtReferemce.close();
                            rsReference.close();
                            return null;
                        } else {
                            interSection = interSectionWithReference;
                        }
                    }
                    if (interSection.isEmpty()) {
                        return null;
                    } else {
                        return interSection;
                    }
                }
            }
        }
    }

//    public void assertSentenceParse(long sentenceID, ArrayList<RawParse> parseArray, ArrayList<DBLexeme> sentenceLexemes, String dependencySource) throws ClassNotFoundException, SQLException {
//        for (int i = 0; i < parseArray.size(); i++) {
//            String depType = parseArray.get(i).getDependencyType();
//            int govPosition = parseArray.get(i).getGovernorPosition();
//            int regPosition = parseArray.get(i).getRegentPosition();
//            long governorID = sentenceLexemes.get(govPosition - 1).getLexemeID();
//            long regentID = sentenceLexemes.get(regPosition - 1).getLexemeID();
//            long lexemeDependencyID = indexLexemeDependency(depType, governorID, regentID, dependencySource);
//            indexSentenceLexemesDependency(sentenceID, lexemeDependencyID, govPosition, regPosition, dependencySource);
//        }
//
//    }
    public boolean isAttemptToIndexDocumnet(String path) throws SQLException {

        java.sql.PreparedStatement pstmtDoc = con.prepareStatement(
                "SELECT document_id FROM dbo_document WHERE (URL= ? )");
        pstmtDoc.setString(1, path);

        ResultSet rs = pstmtDoc.executeQuery();
        if (rs.next()) {
            return true;
        } else {
            return false;
        }

    }

   
    public double indexDocument(double titleSentenceID,
            Paper p,
            PaperAnalyzer ap,
            List<PReference> referenceList,
            List<Double> authorList, String url) throws SQLException {


        ArrayList<Double> documentIDList =
                isIndexedDocument(
                titleSentenceID, ap.getPsectionList(),
                ap.getSectionMap(), referenceList, authorList);


        if (documentIDList == null || documentIDList.isEmpty()) {
            // document not indexed yet
            PreparedStatement jstmtDoc = con.prepareStatement(
                    "INSERT INTO dbo_document("
                    + "document_id, "
                    + "document_title_sentence_id, "
                    + "document_frequency, "
                    + "document_section_length "
                    + ") VALUES( ?, ?, ?, ? )");
            jstmtDoc.setDouble(1, p.getUid());
            jstmtDoc.setDouble(2, titleSentenceID);
            jstmtDoc.setInt(3, 1);
            jstmtDoc.setInt(4, ap.getPsectionList().size());
            jstmtDoc.executeUpdate();
            con.commit();
            jstmtDoc.close();


            // add document to dbo_content
            PreparedStatement jstmtDocContent = con.prepareStatement(
                    "INSERT INTO dbo_content("
                    + "content_id, "
                    + "content_type ) "
                    + " VALUES( ?, ? )");
            jstmtDocContent.setDouble(1, p.getUid());
            jstmtDocContent.setString(2, "document");
            jstmtDocContent.executeUpdate();
            con.commit();
            jstmtDocContent.close();

            // add document sections
            PreparedStatement jstmtDocSection = null;
            for (int i = 0; i < ap.getPsectionList().size(); i++) {
                jstmtDocSection = con.prepareStatement(
                        "INSERT INTO dbo_content_position("
                        + "super_content_id, "
                        + "sub_content_id, "
                        + "content_position"
                        + ") VALUES( ?, ?, ? )");
                jstmtDocSection.setDouble(1, p.getUid());
                jstmtDocSection.setDouble(2,
                        ap.getSectionMap().get(ap.getPsectionList().get(i)).getId());
                jstmtDocSection.setInt(3, i);
                jstmtDocSection.executeUpdate();
            }
            con.commit();
            if (jstmtDocSection != null) {
                jstmtDocSection.close();
            }
            // add document references

            PreparedStatement jstmtDocRef = null;
            // add check before insert
            for (int i = 0; i < referenceList.size(); i++) {
                jstmtDocRef = con.prepareStatement(
                        "INSERT INTO dbo_document_reference("
                        + "document_id, "
                        + "bibtex_id, "
                        + "cit_str,"
                        + "marker"
                        + ") VALUES( ?, ?, ?, ? )");
                jstmtDocRef.setDouble(1, p.getUid());
                jstmtDocRef.setDouble(2,
                        referenceList.get(i).getReferenceID());
                jstmtDocRef.setString(3, referenceList.get(i).getCitStr());
                jstmtDocRef.setString(4, referenceList.get(i).getMarker());
                jstmtDocRef.executeUpdate();
            }
            con.commit();
            if (jstmtDocRef != null) {
                jstmtDocRef.close();
            }

            // index document in bibtex repository
            // i am not sure for this: first need to search for the current
            // bibtex enteries and if not there then assert the bibtex

            PreparedStatement jstmtDocAsBibtex = con.prepareStatement(
                    "INSERT INTO dbo_bibtex("
                    + "bibtex_id, "
                    + "title,"
                    + "url"
                    + ") VALUES( ?, ?, ? )");
            jstmtDocAsBibtex.setDouble(1, p.getUid());
            jstmtDocAsBibtex.setString(2, p.getTitle());
            jstmtDocAsBibtex.setString(3, url);
            jstmtDocAsBibtex.executeUpdate();
            con.commit();
            jstmtDocAsBibtex.close();


            // add document authors for bibtex
            PreparedStatement jstmtDocAuthorBibtex = null;
            for (int i = 0; i < authorList.size(); i++) {
                jstmtDocAuthorBibtex = con.prepareStatement(
                        "INSERT INTO dbo_bibtex_author("
                        + "bibtex_id, "
                        + "author_id"
                        + ") VALUES( ?, ? )");
                jstmtDocAuthorBibtex.setDouble(1, p.getUid());
                jstmtDocAuthorBibtex.setDouble(2, authorList.get(i));
                jstmtDocAuthorBibtex.executeUpdate();
            }
            con.commit();
            if (jstmtDocAuthorBibtex != null) {
                jstmtDocAuthorBibtex.close();
            }
            return p.getUid();

        } else {
            double finalPaperID = -1;
            PreparedStatement jstmtDocFreq = con.prepareStatement(
                    "SELECT document_frequency FROM dbo_document "
                    + " WHERE document_id= ?");
            finalPaperID = documentIDList.get(0);
            jstmtDocFreq.setDouble(1, documentIDList.get(0));

            ResultSet rsFreq = jstmtDocFreq.executeQuery();
            rsFreq.next();
            int newFreq = rsFreq.getInt("document_frequency") + 1;
            rsFreq.close();
            jstmtDocFreq.close();
            PreparedStatement jstmtUpdtDoc = con.prepareStatement(
                    "UPDATE dbo_document SET document_frequency= ? "
                    + " WHERE document_id= ?");
            jstmtUpdtDoc.setInt(1, newFreq);
            jstmtUpdtDoc.setDouble(2, documentIDList.get(0));
            jstmtUpdtDoc.executeUpdate();
            con.commit();
            jstmtUpdtDoc.close();
            return finalPaperID;
        }
    }

    
    /**
     *
     * @param author
     * @return null; or String author_id
     * @throws SQLException
     */
    public double isIndexAuthor(Author author) throws SQLException {
        // it is a hard descision , this is a simplification
        // the problem is that information may not be complete
        PreparedStatement pstmtAuthor = con.prepareStatement(
                "SELECT author_id "
                + " FROM dbo_author WHERE "
                + "( (first_name= ? ) AND (middle_name= ? ) AND (last_name=?)) ");
        pstmtAuthor.setString(1, author.getFirstName());
        pstmtAuthor.setString(2, author.getMiddleName());
        pstmtAuthor.setString(3, author.getLastName());
        ResultSet rs = pstmtAuthor.executeQuery();
        if (rs.next()) {
            double authorID = rs.getDouble("author_id");
            rs.close();
            pstmtAuthor.close();
            return authorID;
        } else {
            rs.close();
            pstmtAuthor.close();
            return -1;
        }
    }

    public double indexAuthor(Author author) throws SQLException {
        double authorID = indexAuthorName(author);
        double affiliationID = indexAffiliation(author.getAffiliation());
        double emailID = indexEmail(author.getEmailAddress());

        // inserting affiliation
        if (affiliationID != -1) {
            PreparedStatement pstmtAuthorAffiliation = con.prepareStatement(
                    "SELECT * "
                    + " FROM dbo_author_affiliation WHERE "
                    + " ((author_id = ?) AND (affiliation_id = ?))");
            pstmtAuthorAffiliation.setDouble(1, authorID);
            pstmtAuthorAffiliation.setDouble(2, affiliationID);
            ResultSet rs = pstmtAuthorAffiliation.executeQuery();
            if (rs.next()) {
                // the info is already there
                rs.close();
                pstmtAuthorAffiliation.close();
            } else {
                PreparedStatement instAuthorAffiliation = con.prepareStatement(
                        "INSERT INTO dbo_author_affiliation VALUES( ?, ? )");
                instAuthorAffiliation.setDouble(1, authorID);
                instAuthorAffiliation.setDouble(2, affiliationID);
                instAuthorAffiliation.executeUpdate();
                con.commit();
                instAuthorAffiliation.close();
            }
        }

        // inserting emails
        if (emailID != -1) {
            PreparedStatement pstmtAuthorEmail = con.prepareStatement(
                    "SELECT * "
                    + " FROM dbo_author_email WHERE "
                    + " ((author_id = ?) AND (email_id = ?))");
            pstmtAuthorEmail.setDouble(1, authorID);
            pstmtAuthorEmail.setDouble(2, emailID);
            ResultSet rsEmail = pstmtAuthorEmail.executeQuery();
            if (rsEmail.next()) {
                // the info is already there
                rsEmail.close();
                pstmtAuthorEmail.close();
            } else {
                PreparedStatement instAuthorEmail = con.prepareStatement(
                        "INSERT INTO dbo_author_email VALUES( ?, ? )");
                instAuthorEmail.setDouble(1, authorID);
                instAuthorEmail.setDouble(2, emailID);
                instAuthorEmail.executeUpdate();
                con.commit();
                instAuthorEmail.close();
            }
        }
        return authorID;

    }

    public double indexAuthorName(Author author) throws SQLException {
        double authorID = isIndexAuthor(author);
        if (authorID == -1) {
            PreparedStatement pstmtAuthor = con.prepareStatement(
                    "INSERT INTO dbo_author VALUES( ?, ?, ?, ? )");
            Double uid = UID.getUID();
            pstmtAuthor.setDouble(1, uid);

            if (author.getFirstName().length() > 255) {
                pstmtAuthor.setString(2, author.getFirstName().substring(0, 255));
            } else {
                pstmtAuthor.setString(2, author.getFirstName());
            }

            if (author.getMiddleName().length() > 255) {
                pstmtAuthor.setString(3, author.getMiddleName().substring(0, 255));
            } else {
                pstmtAuthor.setString(3, author.getMiddleName());
            }
            if (author.getLastName().length() > 255) {
                pstmtAuthor.setString(4, author.getLastName().substring(0, 255));
            } else {
                pstmtAuthor.setString(4, author.getLastName());
            }
            pstmtAuthor.executeUpdate();
            con.commit();
            pstmtAuthor.close();

            return uid;
        } else {
            return authorID;
        }
    }

    /**
     *
     * @param affiliation
     * @return Stirng affiliationID or Null
     * @throws SQLException
     */
    public double isIndexAffiliation(String affiliation) throws SQLException {
        if (affiliation != null) {
            PreparedStatement pstmtAffiliation = con.prepareStatement(
                    "SELECT affiliation_id "
                    + " FROM dbo_affiliation WHERE "
                    + " (affiliation_string = ?) ");
            if (affiliation.length() > 400) {
                pstmtAffiliation.setString(1, affiliation.substring(0, 399));
            } else {
                pstmtAffiliation.setString(1, affiliation);
            }
            ResultSet rs = pstmtAffiliation.executeQuery();
            if (rs.next()) {
                double affiliationID = rs.getDouble("affiliation_id");
                rs.close();
                pstmtAffiliation.close();
                return affiliationID;
            } else {
                rs.close();
                pstmtAffiliation.close();
                return -1;
            }
        } else {
            return -1;
        }
    }

    public double indexAffiliation(String affiliation) throws SQLException {
        double affiliationID = isIndexAffiliation(affiliation);
        if (affiliationID == -1) {
            if (affiliation != null) {
                PreparedStatement pstmtAffiliation = con.prepareStatement(
                        "INSERT INTO dbo_affiliation VALUES( ?, ? )");
                double uid = UID.getUID();
                pstmtAffiliation.setDouble(1, uid);
                if (affiliation.length() > 400) {
                    pstmtAffiliation.setString(2, affiliation.substring(0, 399));
                } else {
                    pstmtAffiliation.setString(2, affiliation);
                }
                pstmtAffiliation.executeUpdate();
                con.commit();
                pstmtAffiliation.close();
                return uid;
            } else {
                return -1;
            }
        } else {
            return affiliationID;
        }
    }

    /**
     *
     * @param email
     * @return String emailID or null
     * @throws SQLException
     */
    public double isIndexEmail(String email) throws SQLException {
        PreparedStatement pstmtEmail = con.prepareStatement(
                "SELECT email_id "
                + " FROM dbo_email WHERE "
                + " (email= ?) ");
        if (email.length() > 255) {
            pstmtEmail.setString(1, email.substring(0, 255));
        } else {
            pstmtEmail.setString(1, email);
        }
        ResultSet rs = pstmtEmail.executeQuery();
        if (rs.next()) {
            double emailID = rs.getDouble("email_id");
            rs.close();
            pstmtEmail.close();
            return emailID;
        } else {
            rs.close();
            pstmtEmail.close();
            return -1;
        }
    }

    public double indexEmail(String email) throws SQLException {
        if (email != null) {
            // agar email khali bashe moshkel peyda mikone
            double emailID = isIndexEmail(email);
            if (emailID == -1) {
                PreparedStatement pstmtEmail = con.prepareStatement(
                        "INSERT INTO dbo_email VALUES( ?, ? )");
                double uid = UID.getUID();
                pstmtEmail.setDouble(1, uid);
                if (email.length() > 255) {
                    pstmtEmail.setString(2, email.substring(0, 255));
                } else {
                    pstmtEmail.setString(2, email);
                }
                pstmtEmail.executeUpdate();
                con.commit();
                pstmtEmail.close();
                return uid;
            } else {
                return emailID;
            }
        } else {
            return -1;
        }
    }

    public void indexTable(PTable pTable, Metadata metadata,
            HashMap<PParagraph, Metadata> paragraphMap) throws SQLException {

        PreparedStatement pstmtTable = con.prepareStatement(
                "SELECT table_id, frequency"
                + " FROM dbo_table WHERE "
                + "(table_caption_paragraph_id= ? ) ");
        double paragraphID = paragraphMap.get(pTable.getTableCaptionParagrap()).getId();

        pstmtTable.setDouble(1, paragraphID);
        ResultSet rs = pstmtTable.executeQuery();

        if (rs.next()) {
            double currentTableID = rs.getDouble("table_id");
            PreparedStatement jstmtUpdtTable =
                    con.prepareStatement(
                    "UPDATE dbo_table SET frequency=? "
                    + "WHERE table_id=?");
            int NewFrequency = rs.getInt("frequency")
                    + metadata.getFrequency();
            jstmtUpdtTable.setInt(1, NewFrequency);
            jstmtUpdtTable.setDouble(2, currentTableID);
            jstmtUpdtTable.executeUpdate();
            jstmtUpdtTable.close();
            rs.close();
            pstmtTable.close();
            metadata.setFrequency(NewFrequency);
            metadata.setId(currentTableID);
            con.commit();
        } else {
            PreparedStatement jstmtTable = con.prepareStatement(
                    "INSERT INTO dbo_table VALUES( ?, ?, ? )");
            jstmtTable.setDouble(1, metadata.getId());
            jstmtTable.setDouble(2, paragraphID);
            jstmtTable.setInt(3, metadata.getFrequency());
            jstmtTable.executeUpdate();
            con.commit();
            jstmtTable.close();
            pstmtTable.close();
            rs.close();

        }
    }

    public void indexFigure(PFigure pFigure, Metadata metadata,
            HashMap<PParagraph, Metadata> paragraphMap) throws SQLException {

        PreparedStatement pstmtFigure = con.prepareStatement(
                "SELECT figure_id, frequency"
                + " FROM dbo_figure WHERE "
                + "(figure_caption_paragraph_id= ? ) ");
        double paragraphID = paragraphMap.get(pFigure.getFigureCaption()).getId();
        pstmtFigure.setDouble(1, paragraphID);
        ResultSet rs = pstmtFigure.executeQuery();

        if (rs.next()) {
            double currentFigureID = rs.getDouble("figure_id");
            PreparedStatement jstmtUpdtFigure =
                    con.prepareStatement(
                    "UPDATE dbo_figure SET frequency=? "
                    + "WHERE figure_id=?");
            int NewFrequency = rs.getInt("frequency")
                    + metadata.getFrequency();
            jstmtUpdtFigure.setInt(1, NewFrequency);
            jstmtUpdtFigure.setDouble(2, currentFigureID);
            jstmtUpdtFigure.executeUpdate();
            jstmtUpdtFigure.close();
            rs.close();
            pstmtFigure.close();
            metadata.setFrequency(NewFrequency);
            metadata.setId(currentFigureID);
            con.commit();
        } else {
            PreparedStatement jstmtFigure = con.prepareStatement(
                    "INSERT INTO dbo_figure VALUES( ?, ?, ? )");

            jstmtFigure.setDouble(1, metadata.getId());
            jstmtFigure.setDouble(2, paragraphID);
            jstmtFigure.setInt(3, metadata.getFrequency());
            jstmtFigure.executeUpdate();
            con.commit();
            jstmtFigure.close();
            pstmtFigure.close();
            rs.close();

        }
    }

    public void indexEquation(PEquation pEquation, Metadata metadata,
            HashMap<PParagraph, Metadata> paragraphMap) throws SQLException {

        PreparedStatement pstmtEquation = con.prepareStatement(
                "SELECT equation_id, frequency"
                + " FROM dbo_equation WHERE "
                + "(equation_paragraph_id= ? ) ");
        double equationID = paragraphMap.get(pEquation.getEquationParagprah()).getId();
        pstmtEquation.setDouble(1, equationID);
        ResultSet rs = pstmtEquation.executeQuery();

        if (rs.next()) {
            double currentEquationID = rs.getDouble("equation_id");
            PreparedStatement jstmtUpdtEquation =
                    con.prepareStatement(
                    "UPDATE dbo_equation SET frequency=? "
                    + "WHERE equation_id=?");
            int NewFrequency = rs.getInt("frequency")
                    + metadata.getFrequency();
            jstmtUpdtEquation.setInt(1, NewFrequency);
            jstmtUpdtEquation.setDouble(2, currentEquationID);
            jstmtUpdtEquation.executeUpdate();
            jstmtUpdtEquation.close();
            rs.close();
            pstmtEquation.close();
            metadata.setFrequency(NewFrequency);
            metadata.setId(currentEquationID);
            con.commit();
        } else {
            PreparedStatement jstmtEquation = con.prepareStatement(
                    "INSERT INTO dbo_equation VALUES( ?, ?, ? )");

            jstmtEquation.setDouble(1, metadata.getId());
            jstmtEquation.setDouble(2, equationID);
            jstmtEquation.setInt(3, metadata.getFrequency());
            jstmtEquation.executeUpdate();
            jstmtEquation.close();
            pstmtEquation.close();

            PreparedStatement jstmtEquationContent = con.prepareStatement(
                    "INSERT INTO dbo_content("
                    + "content_id, "
                    + "content_type) "
                    + " VALUES( ?, ?)");
            jstmtEquationContent.setDouble(1, metadata.getId());
            jstmtEquationContent.setString(2, "equation");
            jstmtEquationContent.executeUpdate();
            con.commit();
            jstmtEquationContent.close();
            rs.close();
            con.commit();
        }
    }

    public double indexReference(Reference reference) throws SQLException {
        double bibtexEntry = isIndexedReference(reference);
        if (bibtexEntry == -1) {
            // new entry
            double BibtexID = UID.getUID();
            List<Double> authorIDs = new ArrayList<Double>();
            for (String author : reference.getAuthor()) {
                Author a = new Author();
                String firstName = "";
                String middleName = "";
                String lastName = "";
                String[] authorName = author.split(" ");
                if (authorName.length > 2) {
                    firstName = authorName[0];
                    middleName = authorName[1];
                    for (int i = 2; i < authorName.length; i++) {
                        lastName += authorName[i];
                        lastName += " ";
                    }
                    lastName = lastName.trim();
                } else if (authorName.length == 2) {
                    firstName = authorName[0];
                    lastName = authorName[1];
                } else if (authorName.length == 1) {
                    lastName = authorName[0];
                }

                a.setFirstName(firstName);
                a.setMiddleName(middleName);
                a.setLastName(lastName);
                authorIDs.add(indexAuthorName(a));
            }
            PreparedStatement instBibtex = con.prepareStatement(
                    "INSERT INTO dbo_bibtex("
                    + " `bibtex_id`,"
                    + " `title`,"
                    + " `date`,"
                    + " `publisher`,"
                    + " `journal`,"
                    + " `volume`,"
                    + " `location`,"
                    + " `pages`,"
                    + " `book_title`,"
                    + " `institution`,"
                    + " `editors`,"
                    + " `issn`,"
                    + " `isbn`,"
                    + " `doi`,"
                    + " `url`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            instBibtex.setDouble(1, BibtexID);
            if (reference.getTitle().length() > 500) {
                instBibtex.setString(2, reference.getTitle().substring(0, 499).trim());
            } else {
                instBibtex.setString(2, reference.getTitle().trim());
            }
            instBibtex.setString(3, reference.getDate());
            instBibtex.setString(4, reference.getPublisher());
            instBibtex.setString(5, reference.getJournal());
            instBibtex.setString(6, reference.getVolume());
            instBibtex.setString(7, reference.getLocation());
            instBibtex.setString(8, reference.getPages());
            instBibtex.setString(9, reference.getBookTitle());
            instBibtex.setString(10, reference.getInstitution());
            instBibtex.setString(11, reference.getEditors());
            instBibtex.setString(12, "na");
            instBibtex.setString(13, "na");
            instBibtex.setString(14, "na");
            instBibtex.setString(15, "na");
            instBibtex.executeUpdate();
            con.commit();
            instBibtex.close();
            for (double authorID : authorIDs) {

                PreparedStatement instBibtexAuthorID = con.prepareStatement(
                        "INSERT INTO dbo_bibtex_author VALUES(?, ?)");
                instBibtexAuthorID.setDouble(1, BibtexID);
                instBibtexAuthorID.setDouble(2, authorID);
                instBibtexAuthorID.executeUpdate();
                con.commit();
                instBibtexAuthorID.close();
            }
            return BibtexID;
        } else {
            // there is a bibtex entry, try to update the content with new contents
            // write up the update
            return bibtexEntry;
        }

    }
    // this is tricky because not all the information about a reference may be valid
    // so start with all records with the same title and then refine the result
    // based on constraints available by the reference; i wont spend time at this
    // time

    public double isIndexedReference(Reference reference) throws SQLException {
        // this implementation only considers title for the paper
        // and the author list for the paper
        // if the title of paper is null then it will consider the book title

        // select records with the specified name
        PreparedStatement pstmtBibtex = con.prepareStatement(
                "SELECT *"
                + " FROM dbo_bibtex WHERE "
                + "(title = ? ) ");
        pstmtBibtex.setString(1, reference.getTitle().trim());
        ResultSet rsBibtex = pstmtBibtex.executeQuery();

        while (rsBibtex.next()) {

            double bibtexID = rsBibtex.getDouble("bibtex_id");
            return bibtexID;
//            // get last names for the bibtex entry and put the last anmes in a list
//            PreparedStatement pstmtBibtexAuthor = con.prepareStatement(
//                    "SELECT last_name"
//                    + " FROM dbo_author"
//                    + " INNER JOIN  dbo_bibtex_author ON dbo_bibtex_author.author_id "
//                    + " = dbo_author.author_id "
//                    + " WHERE ( dbo_bibtex_author.bibtex_id = ? ) ");
//            pstmtBibtexAuthor.setString(1, bibtexID);
//            ResultSet authorLastName = pstmtBibtexAuthor.executeQuery();
//            List<String> lastNameAuthorInDB = new ArrayList<String>();
//            while (authorLastName.next()) {
//                lastNameAuthorInDB.add(authorLastName.getString("last_name"));
//            }
//
//            // get last names for the authors from the reference and put them in alist
//            List<String> lastNameAuthorInRefrence = new ArrayList<String>();
//
//            for (String author : reference.getAuthor()) {
//                String lastName = "";
//                String[] authorName = author.trim().split(" ");
//                if (authorName.length > 2) {
//                    for (int i = 2; i < authorName.length; i++) {
//                        lastName += authorName[i];
//                        lastName += " ";
//                    }
//                    lastName = lastName.trim();
//                } else if (authorName.length == 2) {
//                    lastName = authorName[1];
//                } else if (authorName.length == 1) {
//                    lastName = authorName[0];
//                }
//                lastNameAuthorInRefrence.add(lastName);
//            }
//
//            // in the case that both of the current bibtext in the db and the
//            // reference item in the paper
//            // do not have author then there will be a problem
//            // before the next check I add another check and it is this:
//            // if both of the bibtex author and the reference author are nil
//            // then choose the current bibtext as the bibtex entry for reference
//            // there are tones of other ways to improve this
//            // in practice since we are dealing with none-completed info in
//            // the bibtex entry the process getting hards
//            // it is sort of search on uncomplete info
//            // furthur check like the year etc can be helpfull!
//            // see if the two lists are euqal or the db one contains the other one
//            // if yes this is the bibtex entry
//
//            if (lastNameAuthorInRefrence.size()
//                    == lastNameAuthorInDB.size()) {
//                ArrayList<String> interSection =
//                        (ArrayList) CollectionUtils.intersection(
//                        lastNameAuthorInRefrence, lastNameAuthorInDB);
//                if (interSection.size() == lastNameAuthorInRefrence.size()) {
//                    return bibtexID;
//                }
//            }
//            if(lastNameAuthorInRefrence.isEmpty() && lastNameAuthorInDB.isEmpty()){
//                System.err.println("-------------->> Here we go this is it");
//                 return bibtexID;
//            }
        }
//        System.err.println(">>>----------------------->>>>>>> in referencessssssssssss");
        return -1;
    }

    private void assertDocumentReferences(double uid,
            double referenceID, String citStr, String marker) throws SQLException {
        PreparedStatement jstmtDocRef;
        jstmtDocRef = con.prepareStatement(
                "SELECT document_id,bibtex_id FROM"
                + " dbo_document_reference WHERE("
                + "( document_id =? ) AND "
                + " ( bibtex_id = ?))");
        jstmtDocRef.setDouble(1, uid);
        jstmtDocRef.setDouble(2, referenceID);
        ResultSet rs = jstmtDocRef.executeQuery();
        if (rs.next()) {
            jstmtDocRef.close();
            rs.close();
            return;
        } else {
            jstmtDocRef = con.prepareStatement(
                    "INSERT INTO dbo_document_reference("
                    + "document_id, "
                    + "bibtex_id, "
                    + "cit_str,"
                    + "marker"
                    + ") VALUES( ?, ?, ?, ? )");
            jstmtDocRef.setDouble(1, uid);
            jstmtDocRef.setDouble(2, referenceID);
            if (citStr.length() > REFERENCE_MARKER_LENGTH) {
                jstmtDocRef.setString(3, citStr.substring(0, REFERENCE_MARKER_LENGTH - 1));
            } else {
                jstmtDocRef.setString(3, citStr);
            }
            if (marker.length() > REFERENCE_MARKER_LENGTH) {
                jstmtDocRef.setString(4, marker.substring(0, REFERENCE_MARKER_LENGTH - 1));
            } else {
                jstmtDocRef.setString(4, marker);
            }
            jstmtDocRef.executeUpdate();
            con.commit();
            jstmtDocRef.close();
        }
    }



   

   

}
