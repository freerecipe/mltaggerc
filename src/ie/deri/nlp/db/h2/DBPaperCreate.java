/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.h2;


import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author behqas
 *
 * this is for creating new databases in mysql server. For example, each document
 * collection can be stored in one database
 *
 */
public class DBPaperCreate extends DBLengthSettings {

    private Connection con;
    private Statement st;
    private String mysqlURL = null;
    private String databaseName = null;
    private String userName = null;
    private String password = null;

//    final private int LEXEME_STRING_LENGTH = 40;
//    final private int POS_LENGTH = 4;

    public void init(Connection c) {
        this.con = c;
    }

    public void creatTables() {

        // create dbo_document
        createDBODocument();
        // create section
        createDBOSection();
        // create paragraph
        createDBOParagraph();
        // dbo_sentence
        createDBOSentence();
        // sentence meta data e.g. if it is parsed!
        createDBOSentenceMetaData();
        // create phrase
        createDBOPhrase();
        // create dbo_lexeme
        createDBOLexeme();

        // create dbo_figure
        createDBOFigure();

        // create dbo_table
        createDBOTable();

        // create dbo_content
        createDBOContent();

        // create dbo_content_position
        createDBOContentPosition();

        createDBOSectionFigure();
        createDBOSectionTable();

        // create dbo_lexeme_dep_? --> ? is the name of dependency parser, for the time being this is stanford, malt and biolg
//        createDBOLexemeDependency("lg");
//        createDBOLexemeDependency("sd");
        createDBOLexemeDependency("mp");

        // dbo_Sentence_dep_? --> ? is the name of parser
//        createDBOSentenceDependency("lg");
//        createDBOSentenceDependency("sd");
        createDBOSentenceDependency("mp");

        createDBOParagraphSentence();

        createDBOSentencePhrase();
        createDBOSentenceLexeme();

        createDBOPhraseLexeme();

        createDBOAuthor();
        createDBOEmail();
        createDBOAffiliation();
        createDBOAuthorEmail();
        createDBOAuthorAffiliation();

        createDBOBibtex();
        createDBOBibtexAuthor();
//                createDBODocumentBibtex();

//                createDocumentAuthor();
        createDBODocumentReference();
        createDBOEquation();


    }

    private boolean createDBODocument() {
        try {
            st = (Statement) con.createStatement();


            st.executeUpdate("CREATE TABLE  dbo_document ( "
                    + "  document_id " + ID_TYPE + " NOT NULL,"
                    + "  document_title_sentence_id " + ID_TYPE + " NOT NULL,"
                    + "  document_frequency INTEGER DEFAULT NULL,"
                    + "  document_section_length INTEGER DEFAULT NULL,"
                    + "  PRIMARY KEY (document_id)"
                    + ")");

            st.executeUpdate("CREATE INDEX Index_length ON dbo_document (document_section_length )");
            st.executeUpdate("CREATE INDEX Index_title_sentence ON dbo_document (document_title_sentence_id )");



            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOLexeme() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_lexeme ("
                    + "  lexeme_id " + ID_TYPE + " NOT NULL,"
                    + "  lexeme_string varchar(" + LEXEME_STRING_LENGTH + ") DEFAULT NULL,"
                    + "  penn_pos varchar(" + POS_LENGTH + ") DEFAULT NULL,"
                    + "  lexeme_lemma varchar(" + LEXEME_STRING_LENGTH + ") DEFAULT NULL,"
                    + "  lexeme_frequency INTEGER DEFAULT NULL,"
                    + "  PRIMARY KEY (lexeme_id))");
            st.executeUpdate("CREATE INDEX Index_lemma ON dbo_lexeme (lexeme_lemma )");
            st.executeUpdate("CREATE HASH INDEX Index_lexeme_all ON dbo_lexeme (lexeme_string,penn_pos,lexeme_lemma )");
            st.executeUpdate("CREATE INDEX Index_penn_pos ON dbo_lexeme (penn_pos )");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOPhrase() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE  dbo_phrase ("
                    + "  phrase_id " + ID_TYPE + " NOT NULL,"
                    + "  phrase_type varchar(" + POS_LENGTH + ") DEFAULT NULL,"
                    + "  phrase_length INTEGER DEFAULT NULL,"
                    + "  phrase_frequency INTEGER DEFAULT NULL,"
                    + "  PRIMARY KEY (phrase_id))");
            st.executeUpdate("CREATE INDEX Index_phrase_type ON dbo_phrase (phrase_type)");
            st.executeUpdate("CREATE INDEX Index_phrase_length ON dbo_phrase (phrase_length)");
            st.executeUpdate("CREATE HASH INDEX Index_phrase_hash ON dbo_phrase (phrase_type,phrase_length)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOSentence() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE  dbo_sentence ("
                    + "  sentence_id " + ID_TYPE + " NOT NULL,"
                    + "  sentence_length INTEGER DEFAULT NULL,"
                    + "  frequency INTEGER DEFAULT NULL,"
                    + "  PRIMARY KEY (sentence_id))");
            st.executeUpdate("CREATE INDEX Index_sentence_length ON dbo_sentence (sentence_length)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOFigure() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE  dbo_figure ("
                    + " figure_id " + ID_TYPE + " NOT NULL,"
                    + " figure_caption_paragraph_id " + ID_TYPE + " NOT NULL,"
                    + " frequency INTEGER DEFAULT NULL,"
                    + " PRIMARY KEY (figure_id))");
            st.executeUpdate("CREATE INDEX Index_fig_caption ON dbo_figure (figure_caption_paragraph_id)");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOEquation() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE  dbo_equation ("
                    + " equation_id " + ID_TYPE + " NOT NULL,"
                    + " equation_paragraph_id " + ID_TYPE + " NOT NULL,"
                    + " frequency INTEGER DEFAULT NULL,"
                    + " PRIMARY KEY (equation_id)"
                    + ")");
            st.executeUpdate("CREATE INDEX INDEX_EQU_CAPTION "
                    + "ON dbo_equation (equation_paragraph_id)");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOTable() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE  dbo_table ("
                    + " table_id " + ID_TYPE + " NOT NULL,"
                    + " table_caption_paragraph_id " + ID_TYPE + " NOT NULL,"
                    + " frequency INTEGER DEFAULT NULL,"
                    + " PRIMARY KEY (table_id))"
                    + "");
            st.executeUpdate("CREATE INDEX INDEX_table_caption_paragraph_id ON dbo_table (table_caption_paragraph_id)");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOParagraph() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE  dbo_paragraph ("
                    + "  paragraph_id " + ID_TYPE + " NOT NULL,"
                    + "  paragraph_sentence_length INTEGER DEFAULT NULL,"
                    + "  paragraph_frequency INTEGER DEFAULT NULL,"
                    + "  PRIMARY KEY (paragraph_id))");
            st.executeUpdate("CREATE INDEX INDEX_paragraph_sentence_length ON dbo_paragraph (paragraph_sentence_length)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOSection() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE  dbo_section ("
                    + "  section_id  " + ID_TYPE + " NOT NULL,"
                    + "  section_type  VARCHAR(255),"
                    + "  section_header_sentence_id " + ID_TYPE + ","
                    + "  section_length INTEGER DEFAULT NULL,"
                    + "  section_frequency INTEGER DEFAULT NULL,"
                    + "  PRIMARY KEY (section_id)"
                    + ")");
            st.executeUpdate("CREATE INDEX Index_section_length ON dbo_section (section_length)");
            st.executeUpdate("CREATE INDEX Index_section_header_sentence ON dbo_section ( section_header_sentence_id)");
            st.executeUpdate("CREATE INDEX Index_section_type ON dbo_section (section_type )");
            st.executeUpdate("CREATE HASH INDEX Index_section_length_type ON dbo_section (section_length, section_type )");


            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOContent() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE dbo_content ("
                    + " content_id " + ID_TYPE + " NOT NULL,"
                    + " content_type VARCHAR(50) NULL,"
                    + " PRIMARY KEY (content_id))");
            st.executeUpdate("CREATE INDEX Index_content_id_type ON dbo_content (content_id, content_type )");


            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOContentPosition() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE  dbo_content_position ("
                    + "  super_content_id " + ID_TYPE + " NOT NULL,"
                    + "  sub_content_id  " + ID_TYPE + " NOT NULL,"
                    + "  content_position INTEGER NOT NULL,"
                    + "  PRIMARY KEY (super_content_id, sub_content_id, content_position))");
            st.executeUpdate("CREATE UNIQUE HASH INDEX HASH_Index_super_content_id ON "
                    + "dbo_content_position (super_content_id, sub_content_id, content_position)");

            st.executeUpdate("CREATE INDEX Index_super_content_id ON "
                    + "dbo_content_position (super_content_id)");
            st.executeUpdate("CREATE INDEX Index_sub_content_id ON "
                    + "dbo_content_position (sub_content_id)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOSentencePhrase() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE dbo_sentence_phrase ("
                    + "  sentence_id " + ID_TYPE + " NOT NULL,"
                    + "  phrase_id " + ID_TYPE + " NOT NULL,"
                    + "  phrase_position INTEGER NOT NULL,"
                    + "  PRIMARY KEY (sentence_id,phrase_id,phrase_position))");
            st.executeUpdate("CREATE UNIQUE HASH INDEX HASH_Index_phr_sentence_id ON "
                    + "dbo_sentence_phrase (sentence_id,phrase_id,phrase_position)");



            st.executeUpdate("CREATE INDEX Index_phr_sentence_id ON "
                    + "dbo_sentence_phrase (sentence_id)");
            st.executeUpdate("CREATE INDEX Index_phrase_id ON "
                    + "dbo_sentence_phrase (phrase_id)");
            st.executeUpdate("CREATE INDEX Index_phrase_position ON "
                    + "dbo_sentence_phrase ( phrase_position)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOSentenceLexeme() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    " CREATE TABLE  dbo_sentence_lexeme ("
                    + " sentence_id " + ID_TYPE + " NOT NULL,"
                    + " lexeme_id " + ID_TYPE + " NOT NULL,"
                    + " lexeme_position SMALLINT  NOT NULL,"
                    + " PRIMARY KEY (sentence_id,lexeme_id,lexeme_position))");
            st.executeUpdate("CREATE HASH INDEX hash_index_sentence_lex ON"+
                    " dbo_sentence_lexeme (sentence_id,lexeme_id, lexeme_position)" );
            st.executeUpdate("CREATE INDEX Index_sentence_lex_id ON "
                    + "dbo_sentence_lexeme (sentence_id)");

            st.executeUpdate("CREATE INDEX INDEX_lexeme_id ON "
                    + "dbo_sentence_lexeme (lexeme_id)");

            st.executeUpdate("CREATE INDEX INDEX_lexeme_position ON "
                    + "dbo_sentence_lexeme (lexeme_position)");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOSentenceMetaData() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE dbo_sentence_metadata ("
                    + "  sentence_id " + ID_TYPE + " NOT NULL,"
                    + "  is_parsed_sd BOOLEAN unsigned NOT NULL DEFAULT 'FALSE',"
                    + "  is_parsed_mp BOOLEAN unsigned NOT NULL DEFAULT 'FALSE',"
                    + "  is_parsed_lg BOOLEAN unsigned NOT NULL DEFAULT 'FALSE',"
                    + "  PRIMARY KEY (sentence_id))");

            st.executeUpdate("CREATE INDEX Index_sentence_id ON "
                    + "dbo_sentence_metadata (sentence_id)");

            st.executeUpdate("CREATE INDEX Index_sentence_sd ON "
                    + "dbo_sentence_metadata (is_parsed_sd)");
            st.executeUpdate("CREATE INDEX Index_sentence_lg ON "
                    + "dbo_sentence_metadata (is_parsed_lg)");


            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOSentenceDependency(String parser) {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE dbo_sentence_dep_" + parser + " ("
                    + "  sentence_id " + ID_TYPE + " DEFAULT NULL,"
                    + "  lexemes_dependency_id " + ID_TYPE + " DEFAULT NULL,"
                    + "  governor_position SMALLINT DEFAULT NULL,"
                    + "  regent_position SMALLINT DEFAULT NULL)");

            st.executeUpdate("CREATE UNIQUE INDEX Index_dep_sentence"+parser+" ON "
                    + "dbo_sentence_dep_" + parser + "(sentence_id,lexemes_dependency_id,governor_position, regent_position  )");
            st.executeUpdate("CREATE INDEX Index_sentence_id"+parser+" ON "
                    + "dbo_sentence_dep_" + parser + "(sentence_id )");
            st.executeUpdate("CREATE INDEX Index_dep_id"+parser+" ON "
                    + "dbo_sentence_dep_" + parser + "(lexemes_dependency_id )");


            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOPhraseLexeme() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE  dbo_phrase_lexeme ("
                    + "  phrase_id " + ID_TYPE + " NOT NULL,"
                    + "  lexeme_id " + ID_TYPE + " NOT NULL,"
                    + "  lexeme_position SMALLINT NOT NULL,"
                    + "  PRIMARY KEY (phrase_id,lexeme_id,lexeme_position))");
            st.executeUpdate("CREATE UNIQUE HASH INDEX HASH_Index_phrase_lexeme ON "
                    + "dbo_phrase_lexeme (phrase_id,lexeme_id,lexeme_position)");
            st.executeUpdate("CREATE INDEX Index_phrase_lex_id ON "
                    + "dbo_phrase_lexeme (phrase_id)");
            st.executeUpdate("CREATE INDEX Index_lexeme_phr_id ON "
                    + "dbo_phrase_lexeme (lexeme_id)");
            st.executeUpdate("CREATE INDEX Index_phrase_lexeme_position ON "
                    + "dbo_phrase_lexeme (lexeme_position)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOParagraphSentence() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE  dbo_paragraph_sentence ("
                    + "  paragraph_id " + ID_TYPE + " NOT NULL,"
                    + "  sentence_id " + ID_TYPE + " NOT NULL,"
                    + "  sentence_position SMALLINT NOT NULL,"
                    + "  PRIMARY KEY (paragraph_id,sentence_id,sentence_position))");

            st.executeUpdate("CREATE UNIQUE HASH INDEX HASH_Index_paragraph_id ON "
                    + "dbo_paragraph_sentence (paragraph_id,sentence_id,sentence_position)");

            st.executeUpdate("CREATE INDEX Index_paragraph_id ON "
                    + "dbo_paragraph_sentence (paragraph_id)");
            st.executeUpdate("CREATE INDEX Index_para_sentence_id ON "
                    + "dbo_paragraph_sentence (sentence_id)");
            st.executeUpdate("CREATE INDEX Index_position ON "
                    + "dbo_paragraph_sentence (sentence_position)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOLexemeDependency(String parserName) {
        try {
            st = (Statement) con.createStatement();
            String dbo_lexeme_depString = "CREATE TABLE dbo_lexemes_dep_" + parserName
                    + " (" + "  lexemes_dependency_id " + ID_TYPE + " NOT NULL,"
                    + "  governor_lexeme_id " + ID_TYPE + " DEFAULT NULL,"
                    + "  regent_lexeme_id " + ID_TYPE + " DEFAULT NULL,"
                    + "  dependency_type VARCHAR(10) DEFAULT NULL,"
                    + "  dependency_frequency INTEGER DEFAULT NULL,"
                    + "  PRIMARY KEY (lexemes_dependency_id) )";
            st.executeUpdate(dbo_lexeme_depString);
            st.executeUpdate("CREATE HASH INDEX Index_hash_dependency_frequency" + parserName + " ON "
                    + "dbo_lexemes_dep_" + parserName + "(governor_lexeme_id,regent_lexeme_id, dependency_type)");
//            st.executeUpdate("CREATE INDEX Index_dep_type_"+parserName+" ON "
//                    + "dbo_lexemes_dep_" + parserName + "(dependency_type)");
//            st.executeUpdate("CREATE INDEX Index_governor_lexeme_id"+parserName+" ON "
//                    + "dbo_lexemes_dep_" + parserName + "(governor_lexeme_id)");
//            st.executeUpdate("CREATE INDEX Index_regent_lexeme_id"+parserName+" ON "
//                    + "dbo_lexemes_dep_" + parserName + "(regent_lexeme_id)");
//            st.executeUpdate("CREATE INDEX Index_dependency_frequency"+parserName+" ON "
//                    + "dbo_lexemes_dep_" + parserName + "(dependency_frequency)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOAffiliation() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_affiliation ("
                    + " affiliation_id " + ID_TYPE + " NOT NULL,"
                    + " affiliation_string VARCHAR(400) NULL,"
                    + " PRIMARY KEY (affiliation_id)"
                    + ")");



            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOAuthor() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_author ("
                    + " author_id " + ID_TYPE + " NOT NULL,"
                    + " first_name VARCHAR(255) NULL,"
                    + " middle_name VARCHAR(255) NULL,"
                    + " last_name VARCHAR(255) NULL,"
                    + " PRIMARY KEY (author_id))"
                    + "");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOAuthorAffiliation() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_author_affiliation ("
                    + " author_id " + ID_TYPE + " NOT NULL,"
                    + " affiliation_id " + ID_TYPE + " NOT NULL,"
                    + " PRIMARY KEY (author_id, affiliation_id))");
            st.executeUpdate("CREATE INDEX Index_author_aff_id ON "
                    + "dbo_author_affiliation (author_id)");

            st.executeUpdate("CREATE INDEX Index_affiliation_id ON "
                    + "dbo_author_affiliation (affiliation_id)");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOAuthorEmail() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_author_email ("
                    + " author_id " + ID_TYPE + " NOT NULL,"
                    + " email_id " + ID_TYPE + " NOT NULL,"
                    + " PRIMARY KEY (author_id, email_id))");
            st.executeUpdate("CREATE INDEX Index_author_id ON "
                    + "dbo_author_email (author_id)");
            st.executeUpdate("CREATE INDEX Index_email_id ON "
                    + "dbo_author_email (email_id)");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOBibtex() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_bibtex ("
                    + " bibtex_id " + ID_TYPE + " NOT NULL,"
                    + " title VARCHAR(500) NULL,"
                    + " date VARCHAR(255) NULL,"
                    + " publisher VARCHAR(255) NULL,"
                    + " journal VARCHAR(500) NULL,"
                    + " volume VARCHAR(255) NULL,"
                    + " location VARCHAR(255) NULL,"
                    + " pages VARCHAR(255) NULL,"
                    + " book_title VARCHAR(500) NULL,"
                    + " institution VARCHAR(400) NULL,"
                    + " editors VARCHAR(500) NULL,"
                    + " issn VARCHAR(255) NULL,"
                    + " isbn VARCHAR(255) NULL,"
                    + " doi VARCHAR(255) NULL,"
                    + " url VARCHAR(255) NULL,"
                    + " PRIMARY KEY (bibtex_id)"
                    + ")");
            st.executeUpdate("CREATE INDEX Index_title ON "
                    + "dbo_bibtex (title)");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOBibtexAuthor() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_bibtex_author ("
                    + " bibtex_id " + ID_TYPE + " NOT NULL,"
                    + " author_id " + ID_TYPE + " NOT NULL,"
                    + " PRIMARY KEY (bibtex_id, author_id))");
            st.executeUpdate("CREATE INDEX Index_bib_author_id ON "
                    + "dbo_bibtex_author (author_id)");
            st.executeUpdate("CREATE INDEX Index_bibtex_id ON "
                    + "dbo_bibtex_author (bibtex_id)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBODocumentBibtex() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_document_bibtex ("
                    + " document_id " + ID_TYPE + " NOT NULL,"
                    + " bibtex_id " + ID_TYPE + " NOT NULL,"
                    + " PRIMARY KEY (document_id, bibtex_id))");
            st.executeUpdate("CREATE INDEX Index_document_id ON "
                    + "dbo_document_bibtex (document_id)");
            st.executeUpdate("CREATE INDEX Index_bibtex_id ON "
                    + "dbo_document_bibtex (bibtex_id)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBODocumentReference() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_document_reference ("
                    + " document_id " + ID_TYPE + " NOT NULL, "
                    + " bibtex_id " + ID_TYPE + " NOT NULL,"
                    + " cit_str VARCHAR("+ REFERENCE_MARKER_LENGTH+") NULL,"
                    + " marker VARCHAR("+ REFERENCE_MARKER_LENGTH +") NULL,"
                    + " PRIMARY KEY (document_id, bibtex_id, cit_str,marker ))");
            st.executeUpdate("CREATE INDEX Index_bibtex_doc_id ON "
                    + "dbo_document_reference (bibtex_id)");

            st.executeUpdate("CREATE INDEX Index_document_id ON "
                    + "dbo_document_reference (document_id)");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOEmail() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_email ("
                    + " email_id " + ID_TYPE + " NOT NULL,"
                    + " email VARCHAR(255) NULL,"
                    + " PRIMARY KEY (email_id))"
                    + "");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOSectionFigure() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_section_figure ("
                    + " section_id " + ID_TYPE + " NOT NULL,"
                    + " figure_id " + ID_TYPE + " NOT NULL,"
                    + " PRIMARY KEY (section_id, figure_id))");
            st.executeUpdate("CREATE INDEX Index_section_id ON "
                    + "dbo_section_figure (section_id)");
            st.executeUpdate("CREATE INDEX Index_figure_id ON "
                    + "dbo_section_figure (figure_id)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOSectionTable() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_section_table ("
                    + " section_id " + ID_TYPE + " NOT NULL,"
                    + " table_id " + ID_TYPE + " NOT NULL,"
                    + " PRIMARY KEY (section_id, table_id))");
            st.executeUpdate("CREATE INDEX Index__table_section_id ON "
                    + "dbo_section_table (section_id)");

            st.executeUpdate("CREATE INDEX Index_table_id ON "
                    + "dbo_section_table (table_id)");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDocumentAuthor() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE dbo_document_author ("
                    + " document_id " + ID_TYPE + " NOT NULL,"
                    + " author_id " + ID_TYPE + " NOT NULL,"
                    + " PRIMARY KEY (document_id, author_id))");
            st.executeUpdate("CREATE INDEX Index_document_id ON "
                    + "dbo_document_author (document_id)");
            st.executeUpdate("CREATE INDEX Index_author_id ON "
                    + "dbo_document_author (author_id)");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBPaperCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }


        public void close() throws SQLException{
        con.commit();
        st.close();
    }

}
