/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.h2;

import ie.deri.nlp.db.objects.DBChunk;
import ie.deri.nlp.db.objects.DBLexeme;
import ie.deri.nlp.db.objects.DBSentence;
import ie.deri.nlp.db.objects.DBSentenceChunk;
import ie.deri.nlp.db.objects.DBDependency;
import ie.deri.nlp.db.objects.DBSentenceDependency;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class DBPaperRetrieve extends DBLengthSettings {

    protected Connection con;

    public void init(Connection con) {
        this.con = con;
    }

    public List<DBLexeme> retLexeme(String word) throws SQLException {
        List<DBLexeme> dbLexemeList = new ArrayList<DBLexeme>();
        PreparedStatement pstmtLexeme = con.prepareStatement(
                "SELECT lexeme_id, penn_pos, lexeme_string, lexeme_lemma, lexeme_frequency"
                + " FROM dbo_lexeme WHERE (lexeme_string= ? ) ");
        if (word.length() > LEXEME_STRING_LENGTH) {
            pstmtLexeme.setString(1, word.substring(0, LEXEME_STRING_LENGTH - 1));
        } else {
            pstmtLexeme.setString(1, word);
        }
        ResultSet rs = pstmtLexeme.executeQuery();
        while (rs.next()) {
            DBLexeme dbl = new DBLexeme();
            dbl.setWord(rs.getString("lexeme_string"));
            dbl.setPos(rs.getString("penn_pos"));
            dbl.setLemma(rs.getString("lexeme_lemma"));
            dbl.setFrequency(rs.getInt("lexeme_frequency"));
            dbl.setId(rs.getDouble("lexeme_id"));
            dbLexemeList.add(dbl);
        }
        pstmtLexeme.close();
        rs.close();
        return dbLexemeList;
    }

    public List<DBLexeme> retLexemeByLemma(String lemma) throws SQLException {
        List<DBLexeme> dbLexemeList = new ArrayList<DBLexeme>();
        PreparedStatement pstmtLexeme = con.prepareStatement(
                "SELECT lexeme_id, penn_pos, lexeme_string, lexeme_lemma, lexeme_frequency"
                + " FROM dbo_lexeme WHERE (lexeme_lemma= ? ) ");
        if (lemma.length() > 255) {
            pstmtLexeme.setString(1, lemma.substring(0, 254));
        } else {
            pstmtLexeme.setString(1, lemma);
        }
        ResultSet rs = pstmtLexeme.executeQuery();
        while (rs.next()) {
            DBLexeme dbl = new DBLexeme();
            dbl.setWord(rs.getString("lexeme_string"));
            dbl.setPos(rs.getString("penn_pos"));
            dbl.setLemma(rs.getString("lexeme_lemma"));
            dbl.setId(rs.getDouble("lexeme_id"));
            dbl.setFrequency(rs.getInt("lexeme_frequency"));
            dbLexemeList.add(dbl);
        }
        pstmtLexeme.close();
        rs.close();
        return dbLexemeList;
    }

    public List<DBLexeme> retLexeme(String word, String pos) throws SQLException {
        List<DBLexeme> dbLexemeList = new ArrayList<DBLexeme>();
        PreparedStatement pstmtLexeme = con.prepareStatement(
                "SELECT lexeme_id, penn_pos, lexeme_string, lexeme_lemma, lexeme_frequency"
                + " FROM dbo_lexeme WHERE ((lexeme_string= ? ) AND ( penn_pos=?))");
        if (word.length() > 255) {
            pstmtLexeme.setString(1, word.substring(0, 254));
        } else {
            pstmtLexeme.setString(1, word);
        }
        pstmtLexeme.setString(2, pos);
        ResultSet rs = pstmtLexeme.executeQuery();
        while (rs.next()) {
            DBLexeme dbl = new DBLexeme();
            dbl.setWord(rs.getString("lexeme_string"));
            dbl.setPos(rs.getString("penn_pos"));
            dbl.setLemma(rs.getString("lexeme_lemma"));
            dbl.setId(rs.getDouble("lexeme_id"));
            dbl.setFrequency(rs.getInt("lexeme_frequency"));
            dbLexemeList.add(dbl);
        }
        pstmtLexeme.close();
        rs.close();
        return dbLexemeList;
    }

    public DBLexeme retLexeme(String word, String lemma, String pos) throws SQLException {
        DBLexeme dbl = new DBLexeme();
        PreparedStatement pstmtLexeme = con.prepareStatement(
                "SELECT lexeme_id, penn_pos, lexeme_string, lexeme_lemma, "
                + "lexeme_frequency FROM dbo_lexeme WHERE ((lexeme_string =?) "
                + "AND (lexeme_lemma= ? )"
                + " AND ( penn_pos=?))");
        if (word.length() > LEXEME_STRING_LENGTH) {
            pstmtLexeme.setString(1, word.substring(0, LEXEME_STRING_LENGTH - 1));
        } else {
            pstmtLexeme.setString(1, word);
        }
        if (lemma.length() > 255) {
            pstmtLexeme.setString(2, lemma.substring(0, LEXEME_STRING_LENGTH - 1));
        } else {
            pstmtLexeme.setString(2, lemma);
        }
        pstmtLexeme.setString(3, pos);
        ResultSet rs = pstmtLexeme.executeQuery();
        if (rs.next()) {
            dbl.setWord(rs.getString("lexeme_string"));
            dbl.setPos(rs.getString("penn_pos"));
            dbl.setLemma(rs.getString("lexeme_lemma"));
            dbl.setId(rs.getDouble("lexeme_id"));
            dbl.setFrequency(rs.getInt("lexeme_frequency"));
            pstmtLexeme.close();
        }
        pstmtLexeme.close();
        rs.close();
        return dbl;
    }

    public String retSentenceStringByID(double sentenceID) throws SQLException {
        PreparedStatement pstmtSentence = con.prepareStatement(
                "SELECT lexeme_id "
                + " FROM DBO_SENTENCE_LEXEME where sentence_id= ? order by lexeme_position");
        pstmtSentence.setDouble(1, sentenceID);
        ResultSet rsLexeme = pstmtSentence.executeQuery();
        String sentence = "";
        while (rsLexeme.next()) {
            double lexemeID = rsLexeme.getDouble("lexeme_id");
            PreparedStatement pstmtLexeme = con.prepareStatement(
                    "SELECT lexeme_string "
                    + " FROM dbo_lexeme WHERE (lexeme_id= ? ) ");
            pstmtLexeme.setDouble(1, lexemeID);
            ResultSet rsLexemeId = pstmtLexeme.executeQuery();
            rsLexemeId.next();
            String lexemeString = rsLexemeId.getString("lexeme_string");
            sentence += lexemeString + " ";
        }
        return sentence;
    }

  public ResultSet retAllSentencesIDs() throws SQLException {
        PreparedStatement pstmtSentence = con.prepareStatement(
                "SELECT sentence_id FROM DBO_SENTENCE");
        ResultSet rsSentence = pstmtSentence.executeQuery();
        return rsSentence;
    }

    public DBLexeme retLexemeByID(double id) throws SQLException {
        DBLexeme dbl = new DBLexeme();
        PreparedStatement pstmtLexeme = con.prepareStatement(
                "SELECT lexeme_id, penn_pos, lexeme_string, lexeme_lemma, lexeme_frequency"
                + " FROM dbo_lexeme WHERE (lexeme_id= ? ) ");
        pstmtLexeme.setDouble(1, id);
        ResultSet rs = pstmtLexeme.executeQuery();
        if (rs.next()) { // there should be a sedult
            dbl.setWord(rs.getString("lexeme_string"));
            dbl.setPos(rs.getString("penn_pos"));
            dbl.setLemma(rs.getString("lexeme_lemma"));
            dbl.setId(rs.getDouble("lexeme_id"));
            dbl.setFrequency(rs.getInt("lexeme_frequency"));
        }
        pstmtLexeme.close();
        rs.close();
        return dbl;
    }

    public DBSentence retSentenceByID(double sentenceID) throws SQLException {
        DBSentence dbs = new DBSentence();
        dbs.setSentenceID(sentenceID);
        dbs.setDbLexemeList(retSentenceLexemes(sentenceID));
        dbs.setSentenceChunk(retSentenceChunkList(sentenceID));
        dbs.setSentenceDependencyList(retSentenceDependencies(sentenceID));
        PreparedStatement pstmtLexeme = con.prepareStatement(
                "SELECT frequency"
                + " FROM dbo_sentence WHERE (sentence_id= ? ) ");
        pstmtLexeme.setDouble(1, sentenceID);
        ResultSet rs = pstmtLexeme.executeQuery();
        if (rs.next()) {
            dbs.setFrequency(rs.getInt("frequency"));
        }
        pstmtLexeme.close();
        rs.close();
        return dbs;
    }

    public List<DBLexeme> retSentenceLexemes(double sentenceID) throws SQLException {
        List<DBLexeme> dbll = new ArrayList<DBLexeme>();
        PreparedStatement pstmtSent = con.prepareStatement(
                "SELECT lexeme_id FROM dbo_sentence_lexeme"
                + " WHERE sentence_id=? ORDER BY lexeme_position");
        pstmtSent.setDouble(1, sentenceID);
        ResultSet rs = pstmtSent.executeQuery();
        while (rs.next()) {
            double lexemeID = rs.getDouble("lexeme_id");
            dbll.add(retLexemeByID(lexemeID));
        }
        rs.close();
        pstmtSent.close();
        return dbll;

    }

    public List<DBSentenceChunk> retSentenceChunkList(double sentenceID) throws SQLException {
        List<DBSentenceChunk> sentenceChunkList = new ArrayList<DBSentenceChunk>();
        PreparedStatement pstmtSent = con.prepareStatement(
                "SELECT phrase_id, phrase_position FROM dbo_sentence_phrase"
                + " WHERE sentence_id=? ");
        pstmtSent.setDouble(1, sentenceID);
        ResultSet rs = pstmtSent.executeQuery();
        while (rs.next()) {
            DBSentenceChunk dbsc = new DBSentenceChunk();
            DBChunk dbc = new DBChunk();
            dbc = retChunkByID(rs.getDouble("phrase_id"));
            dbsc.setDbChunk(dbc);
            dbsc.setStart(rs.getInt("phrase_position"));
            sentenceChunkList.add(dbsc);
        }
        rs.close();
        pstmtSent.close();
        return sentenceChunkList;
    }

    public DBChunk retChunkByID(double chunkID) throws SQLException {
        DBChunk dbChunk = new DBChunk();
        PreparedStatement pstmtSent = con.prepareStatement(
                "SELECT phrase_type, phrase_frequency FROM dbo_phrase"
                + " WHERE phrase_id= ? ");
        pstmtSent.setDouble(1, chunkID);
        ResultSet rs = pstmtSent.executeQuery();
        if (rs.next()) {
            dbChunk.setChunkType(rs.getString("phrase_type"));
            dbChunk.setChunkFrequency(rs.getInt("phrase_frequency"));
        }
        List<DBLexeme> dbll = new ArrayList<DBLexeme>();
        dbll = retChunkLexemeList(chunkID);
        dbChunk.setDbLexemeList(dbll);
        rs.close();
        pstmtSent.close();
        return dbChunk;

    }

    public List<DBLexeme> retChunkLexemeList(double chunkID) throws SQLException {
        List<DBLexeme> dbll = new ArrayList<DBLexeme>();
        PreparedStatement pstmtSent = con.prepareStatement(
                "SELECT lexeme_id FROM dbo_phrase_lexeme "
                + " WHERE phrase_id=? ORDER BY lexeme_position ");
        pstmtSent.setDouble(1, chunkID);
        ResultSet rs = pstmtSent.executeQuery();
        while (rs.next()) {
            double lexemeID = rs.getDouble("lexeme_id");
            DBLexeme dbl = new DBLexeme();
            dbl = retLexemeByID(lexemeID);
            dbll.add(dbl);
        }
        pstmtSent.close();
        rs.close();
        return dbll;
    }

    public List<DBSentenceDependency> retSentenceDependencies(double sentenceID) throws SQLException {
        List<DBSentenceDependency> sdbdList =
                new ArrayList<DBSentenceDependency>();

        PreparedStatement pstmtSent = con.prepareStatement(
                "SELECT lexemes_dependency_id, governor_position,"
                + "regent_position FROM dbo_sentence_dep_mp "
                + " WHERE sentence_id = ? ");
        pstmtSent.setDouble(1, sentenceID);
        ResultSet rs = pstmtSent.executeQuery();
        while (rs.next()) {
            DBSentenceDependency dbsd = new DBSentenceDependency();
            dbsd.setGovernorPosition(rs.getInt("governor_position"));
            dbsd.setRegentPosition(rs.getInt("regent_position"));
            DBDependency dbp = new DBDependency();
            dbp = retDependency(rs.getDouble("lexemes_dependency_id"));
            dbsd.setDbDependency(dbp);
            sdbdList.add(dbsd);
        }
        pstmtSent.close();
        rs.close();
        return sdbdList;
    }

    public DBDependency retDependency(double dependencyID) throws SQLException {
        DBDependency dbd = new DBDependency();
        dbd.setDependencyID(dependencyID);
        PreparedStatement pstmtSent = con.prepareStatement(
                "SELECT dependency_type, governor_lexeme_id,"
                + "regent_lexeme_id, dependency_frequency"
                + " FROM dbo_lexemes_dep_mp "
                + " WHERE lexemes_dependency_id = ? ");
        pstmtSent.setDouble(1, dependencyID);
        ResultSet rs = pstmtSent.executeQuery();
        if (rs.next()) {
            dbd.setDependencyType(rs.getString("dependency_type"));
            dbd.setFrequency(rs.getInt("dependency_frequency"));
            dbd.setGovernorLexemeID(rs.getDouble("governor_lexeme_id"));
            dbd.setRegentLexemeID(rs.getDouble("regent_lexeme_id"));
        }
        pstmtSent.close();
        rs.close();
        return dbd;
    }

    public ResultSet retSentenceContainLexeme(double lexemeID) throws SQLException {
        PreparedStatement pstmtSent = con.prepareStatement(
                "SELECT sentence_id FROM dbo_sentence_lexeme"
                + " WHERE lexeme_id = ? ");
        pstmtSent.setDouble(1, lexemeID);
        ResultSet rs = pstmtSent.executeQuery();
        return rs;

    }

    public ResultSet retSentenceLexemeChain(List<Double> lexemeIDList) throws SQLException {
        if (!lexemeIDList.isEmpty()) {
            if (lexemeIDList.size() == 1) {
                PreparedStatement pstmtSent = con.prepareStatement(
                        "SELECT sentence_id,"
                        + " lexeme_position as start_position, "
                        + " lexeme_position as end_position"
                        + " FROM dbo_sentence_lexeme"
                        + " WHERE lexeme_id = ? ");
                pstmtSent.setDouble(1, lexemeIDList.get(0));
                ResultSet rs = pstmtSent.executeQuery();
//                pstmtSent.close();
                return rs;
            } else {
                String query = "SELECT a0.sentence_id,"
                        + " a0.lexeme_position as start_position, "
                        + " a" + (lexemeIDList.size() - 1) + ".lexeme_position as end_position"
                        + " FROM ";
                for (int i = 0; i < lexemeIDList.size(); i++) {
                    query += "dbo_sentence_lexeme as a" + i;
                    if (i != lexemeIDList.size() - 1) {
                        query += ",";
                    }
                }
                query += " WHERE ";
                for (int i = 0; i < lexemeIDList.size(); i++) {
                    query += "a" + i + ".lexeme_id = ? and ";
                }
                for (int i = 0; i < lexemeIDList.size() - 1; i++) {
                    query += "a0.lexeme_position = a" + (i + 1) + ".lexeme_position -" + (i + 1);
                    query += " and ";
                    query += "a0.sentence_id = a" + (i + 1) + ".sentence_id";
                    if (i != lexemeIDList.size() - 2) {
                        query += " and ";
                    }
                }

                PreparedStatement pstmtSent = con.prepareStatement(query);
                for (int i = 0; i < lexemeIDList.size(); i++) {
                    pstmtSent.setDouble(i + 1, lexemeIDList.get(i));
                }
                ResultSet rs = pstmtSent.executeQuery();
//                pstmtSent.close();
                return rs;
            }
        } else {
            return null;
        }
    }





}
