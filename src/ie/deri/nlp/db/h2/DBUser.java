/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.h2;

import ie.deri.nlp.analyzer.objects.ProcessInfo;
import ie.deri.nlp.intrface.objects.User;
import ie.deri.nlp.intrface.objects.UserFile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class DBUser {

    private Connection con;

    public void close() throws SQLException {
        con.commit();
        con.close();
    }

    public Connection getCon() {
        return con;
    }

     public void setCon(Connection con) {
        this.con = con;
    }
    
    

    public boolean isUser(String userID, String password) {
        try {
            PreparedStatement pstmtUser = con.prepareStatement(
                    "SELECT user_id FROM dbo_user"
                    + " WHERE ( (user_id= ? ) " + "AND (user_pass= ? ) )");
            pstmtUser.setString(1, userID);
            pstmtUser.setString(2, password);
            ResultSet rs = pstmtUser.executeQuery();
            if (rs.next()) {
                rs.close();
                pstmtUser.close();
                return true;
            } else {
                rs.close();
                pstmtUser.close();

                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBUser.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean isEmailUser(String email) {
        try {
            PreparedStatement pstmtUser = con.prepareStatement(
                    "SELECT user_id FROM dbo_user_info"
                    + " WHERE (user_email= ? )");
            pstmtUser.setString(1, email);
            ResultSet rs = pstmtUser.executeQuery();
            if (rs.next()) {
                rs.close();
                pstmtUser.close();
                return true;
            } else {
                rs.close();
                pstmtUser.close();
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBUser.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public User getUserByEmail(String email) {
        User user = new User();
        try {
            PreparedStatement pstmtUser = con.prepareStatement(
                    "SELECT * FROM dbo_user_info"
                    + " WHERE  (user_email= ? ) ");
            pstmtUser.setString(1, email);
            ResultSet rs = pstmtUser.executeQuery();
            rs.next();
            user.setUserID(rs.getString("user_id"));
            user.setName(rs.getString("user_name"));
            user.setSurname(rs.getString("user_surname"));
            user.setEmail(rs.getString("user_email"));
            return user;
        } catch (SQLException ex) {
            Logger.getLogger(DBUser.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public User getUser(String userID) {
        User user = new User();
        try {
            PreparedStatement pstmtUser = con.prepareStatement(
                    "SELECT * FROM dbo_user_info"
                    + " WHERE  (user_id= ? ) ");
            pstmtUser.setString(1, userID);
            ResultSet rs = pstmtUser.executeQuery();
            
            boolean anyResult = rs.next();
            if(anyResult){
            user.setName(rs.getString("user_name"));
            user.setSurname(rs.getString("user_surname"));
            user.setEmail(rs.getString("user_email"));
            user.setUserID(userID);
            return user;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBUser.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public boolean setUserFile(String userID, double fileID, String fileName) {
        // implement it here
        try {
            PreparedStatement pstmtUserFile = con.prepareStatement(
                    "INSERT INTO dbo_user_file"
                    + " VALUES  (?, ?, ?, ?, ?  ) ");
            pstmtUserFile.setString(1, userID);
            pstmtUserFile.setDouble(2, fileID);
            pstmtUserFile.setString(3, fileName);
            Date date = new Date();
            pstmtUserFile.setString(4, date.toString());
            pstmtUserFile.setBoolean(5, false);
            pstmtUserFile.executeUpdate();
            con.commit();
            pstmtUserFile.close();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(DBUser.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public List<UserFile> getUserFiles(String userID) {
        List<UserFile> userFileList = new ArrayList<UserFile>();
        try {
            PreparedStatement pstmtUserFile = con.prepareStatement(
                    "SELECT * from dbo_user_file WHERE ( user_id = ? )");
            pstmtUserFile.setString(1, userID);

            ResultSet rs = pstmtUserFile.executeQuery();
            while (rs.next()) {
                UserFile userFile = new UserFile();
                userFile.setUserID(userID);
                userFile.setFileID(rs.getDouble("file_id"));
                userFile.setFileName(rs.getString("file_name"));
                userFile.setDate(rs.getString("date_uploaded"));
                userFile.setIs_processed(rs.getBoolean("is_processed"));
                userFileList.add(userFile);
            }
            rs.close();
            pstmtUserFile.close();
            return userFileList;
        } catch (SQLException ex) {
            Logger.getLogger(DBUser.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public List<UserFile> getUserNotProcessedFileList(String userID) {
        List<UserFile> userFileList = new ArrayList<UserFile>();
        try {
            PreparedStatement pstmtUserFile = con.prepareStatement(
                    "SELECT * from dbo_user_file "
                    + "WHERE ( ( user_id = ? ) AND ( is_processed = ? ));");
            pstmtUserFile.setString(1, userID);
            pstmtUserFile.setBoolean(2, false);
            ResultSet rs = pstmtUserFile.executeQuery();
            while (rs.next()) {
                UserFile userFile = new UserFile();
                userFile.setUserID(userID);
                userFile.setFileID(rs.getDouble("file_id"));
                userFile.setFileName(rs.getString("file_name"));
                userFile.setDate(rs.getString("date_uploaded"));
                userFile.setIs_processed(rs.getBoolean("is_processed"));
                userFileList.add(userFile);
            }
            rs.close();
            pstmtUserFile.close();
            return userFileList;
        } catch (SQLException ex) {
            Logger.getLogger(DBUser.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public UserFile getUserNotProcessedFile(String userID) {

        try {
            PreparedStatement pstmtUserFile = con.prepareStatement(
                    "SELECT * from dbo_user_file "
                    + "WHERE ( ( user_id = ? ) AND ( is_processed = ? ));");
            pstmtUserFile.setString(1, userID);
            pstmtUserFile.setBoolean(2, false);
            ResultSet rs = pstmtUserFile.executeQuery();
            if (rs.next()) {
                UserFile userFile = new UserFile();
                userFile.setUserID(userID);
                userFile.setFileID(rs.getDouble("file_id"));
                userFile.setFileName(rs.getString("file_name"));
                userFile.setDate(rs.getString("date_uploaded"));
                userFile.setIs_processed(rs.getBoolean("is_processed"));
                pstmtUserFile.close();
                rs.close();
                return userFile;
            } else {
                pstmtUserFile.close();
                rs.close();
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBUser.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void setIsProcessedFile(double fileID) {
        try {
            PreparedStatement pstmtUserFile = con.prepareStatement(
                    "UPDATE dbo_user_file SET  is_processed = ?  "
                    + "WHERE  file_id = ? ");
            pstmtUserFile.setBoolean(1, true);
            pstmtUserFile.setDouble(2, fileID);
            pstmtUserFile.executeUpdate();
            con.commit();
            pstmtUserFile.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // to complete
    public void setProcessedFileInfo(ProcessInfo pInfo) {
        try {
            PreparedStatement pstmtUserFile = con.prepareStatement(
                    "INSERT INTO dbo_file_process_info VALUES"
                    + " (?,?,?,?,?,?,?,?,?,? ) ");
            pstmtUserFile.setDouble(1, pInfo.getFileID());
            pstmtUserFile.setDouble(2, pInfo.getPaperID());
            pstmtUserFile.setInt(3, pInfo.getTypeNumber());
            pstmtUserFile.setInt(4, pInfo.getTokenNumber());
            pstmtUserFile.setInt(5, pInfo.getSentenceNumber());
            pstmtUserFile.setInt(6, pInfo.getParagraphNumber());
            pstmtUserFile.setInt(7, pInfo.getSectionNumber());
            pstmtUserFile.setInt(8, pInfo.getProcessTime());
            pstmtUserFile.setInt(9, pInfo.getIndexTime());
            pstmtUserFile.setTimestamp(10, pInfo.getTimeStamp());
            pstmtUserFile.executeUpdate();
            con.commit();
            pstmtUserFile.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setCriticalLog(double fileID, String logString) {
        try {
            PreparedStatement pstmtUserFile = con.prepareStatement(
                    "INSERT INTO dbo_critical_log VALUES"
                    + " (?,?,? ) ");
            pstmtUserFile.setDouble(1, fileID);
            if (logString.length() > 500) {
                pstmtUserFile.setString(2, logString.substring(0, 499));
            } else {
                pstmtUserFile.setString(2, logString);
            }
            java.util.Date javaDate = new java.util.Date();
            long javaTime = javaDate.getTime();
            java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(javaTime);
            pstmtUserFile.setTimestamp(3, sqlTimestamp);
            pstmtUserFile.executeUpdate();
            con.commit();
            pstmtUserFile.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConnection() throws ClassNotFoundException, SQLException {
        con.commit();
        con.close();
    }
    // other method for handleing user comes here
}
