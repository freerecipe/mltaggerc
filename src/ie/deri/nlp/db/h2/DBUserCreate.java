/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.h2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class DBUserCreate extends DBLengthSettings {

    private Connection gCon;
    private Statement st;
    private Connection con;

    public void creatTables() {
        createDBOUser();
        createDBOUserInfo();
        createDBOUserFiles();
        createDBOFileProcessInfo();
        createDBOCriticalLog();
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    private boolean createDBOUser() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE IF NOT EXIST dbo_user ("
                    + "  user_id varchar(255) NOT NULL,"
                    + "  user_pass varchar(255) DEFAULT NULL,"
                    + "  PRIMARY KEY (user_id)"
                    + ")");

               

            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBUserCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOUserInfo() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate("CREATE TABLE IF NOT EXIST dbo_user_info ("
                    + "  user_id varchar(255) NOT NULL,"
                    + "  user_name varchar(255) DEFAULT NULL,"
                    + "  user_surname varchar(255) DEFAULT NULL,"
                    + "  user_email varchar(255) DEFAULT NULL,"
                    + "  PRIMARY KEY (user_id)"
                    + ")");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBUserCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOUserFiles() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE IF NOT EXIST dbo_user_file ( "
                    + " user_id varchar(255) NOT NULL, "
                    + " file_id double NOT NULL, "
                    + " file_name varchar(255) NULL, "
                    + " date_uploaded varchar(255) NULL, "
                    + " is_processed boolean NOT NULL, "
                    + " PRIMARY KEY (file_id) "
                    + ")");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBUserCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOFileProcessInfo() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE IF NOT EXIST dbo_file_process_info ( "
                    + " file_id DOUBLE NOT NULL,"
                    + " index_id DOUBLE NULL,"
                    + " num_type INT(10) NULL,"
                    + " num_token INT(10) NULL,"
                    + " num_sentence INT(10) NULL,"
                    + " num_paragraph INT(10) NULL,"
                    + " num_section INT(10) NULL,"
                    + " process_time INT(10) NULL,"
                    + " index_time INT(10) NULL,"
                    + " time_date_indexed TIMESTAMP NULL,"
                    + " PRIMARY KEY (file_id)"
                    + ")");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBUserCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOCriticalLog() {
        try {
            st = (Statement) con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE IF NOT EXIST `dbo_critical_log` ( "
                    + " `file_id` DOUBLE NOT NULL,"
                    + " `log_text` VARCHAR(255) NULL,"
                    + " `date` TIMESTAMP NULL,"
                    + "  PRIMARY KEY (`file_id`))"
                    + "");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBUserCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public void createCatalog(String requestString) {
        String mysqlURL = null;
        String databaseName = null;
        String userName = null;
        String password = null;
        System.out.println("Repository Database creation ");
        System.err.println("Arg submitted for database creation: " + requestString);
        String[] elements = requestString.split("~>");
        if ("odbc".equals(elements[0])) {
            System.err.println("Not implemented yet!");
            return;
        } else if ("mysql".equals(elements[0])) {
            mysqlURL = elements[1];
            databaseName = elements[2];
            userName = elements[3];
            password = elements[4];
            System.err.println(">>> submitted arguments for Catalog Creation: ");
            System.err.println("    URL: " + mysqlURL);
            System.err.println("    Catalog name: " + databaseName);
            System.err.println("    UserName: " + userName);
            System.err.println("    Password: " + password);
        } else {
            System.err.println("Error in initializing connection to data repository");
            System.err.println("For ODBC connection: use the string in the form of odbc~>\"your data source name\"");
            System.err.println("For MY-SQL connection: use mysql~>\"url\"~>\"database name\"~>\"User Name\"~>\"Password\"");
            return;
        }

        try {
            Class.forName("com.mysql.jdbc.Driver");
            gCon = (Connection) DriverManager.getConnection(mysqlURL, userName, password);  // e.g "jdbc:mysql://localhost:3309", root, root

            try {
                st = (Statement) gCon.createStatement();

                ResultSet rs = st.executeQuery("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" + databaseName + "'");
                if (rs.next()) {
                    System.err.println("There is already a database with the specified name!");
                    System.err.println("Database creation is cancelled");
                    return;
                }
                System.err.println(">> Creating Catalog");
                st.executeUpdate("CREATE DATABASE " + databaseName);
                System.err.println(">>> Creating connection to the new catalog i.e. " + mysqlURL + databaseName);
                con = (Connection) DriverManager.getConnection(mysqlURL + databaseName, userName, password);
                createDBOUser();
                createDBOUserInfo();
                createDBOUserFiles();
                createDBOFileProcessInfo();
                createDBOCriticalLog();
                if (st.isClosed()) {
                    System.err.println("Data scheme has been created");
                } else {
                    st.close();
                    System.err.println("Data scheme has been created");
                }

            } catch (SQLException s) {
                System.err.println(s);
                System.out.println("SQL statement is not executed!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void close() throws SQLException {
        con.commit();
        st.close();
    }
}
