/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.objects;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class DBChunk {
    private double chunkID;
    private String chunkType;
    private int chunkFrequency;
    private List<DBLexeme> dbLexemeList;

    public DBChunk() {
        dbLexemeList = new ArrayList<DBLexeme>();
    }

    public void setChunkType(String chunkType) {
        this.chunkType = chunkType;
    }

    public String getChunkType() {
        return chunkType;
    }


    public void setChunkFrequency(int chunkFrequency) {
        this.chunkFrequency = chunkFrequency;
    }

    public void setChunkID(double chunkID) {
        this.chunkID = chunkID;
    }

    public int getChunkFrequency() {
        return chunkFrequency;
    }

    public double getChunkID() {
        return chunkID;
    }

    public void setDbLexemeList(List<DBLexeme> dbLexemeList) {
        this.dbLexemeList = dbLexemeList;
    }

    public List<DBLexeme> getDbLexemeList() {
        return dbLexemeList;
    }




}
