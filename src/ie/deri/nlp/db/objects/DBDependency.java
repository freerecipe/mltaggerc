/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.objects;

import java.io.Serializable;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class DBDependency implements Serializable {

    double dependencyID;
    double regentLexemeID;
    double governorLexemeID;
    String dependencyType;
    private int Frequency;

    public void setDependencyType(String dependencyType) {
        this.dependencyType = dependencyType;
    }

    public void setGovernorLexemeID(double governorLexemeID) {
        this.governorLexemeID = governorLexemeID;
    }

    public void setRegentLexemeID(double regentLexemeID) {
        this.regentLexemeID = regentLexemeID;
    }

    public String getDependencyType() {
        return dependencyType;
    }

    public double getGovernorLexemeID() {
        return governorLexemeID;
    }

    public double getRegentLexemeID() {
        return regentLexemeID;
    }

    public int getFrequency() {
        return Frequency;
    }

    public void setFrequency(int Frequency) {
        this.Frequency = Frequency;
    }

    public void setDependencyID(double dependencyID) {
        this.dependencyID = dependencyID;
    }

    public double getDependencyID() {
        return dependencyID;
    }

    @Override
    public String toString() {
        return dependencyID + "/" + dependencyType + "/" + regentLexemeID + "/" + governorLexemeID;
    }



}

