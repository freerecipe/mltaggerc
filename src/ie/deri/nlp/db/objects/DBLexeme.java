/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.objects;

import ie.deri.nlp.analyzer.objects.PLexeme;
import java.io.Serializable;
import org.omg.CORBA.TIMEOUT;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class DBLexeme extends PLexeme implements Serializable{

  

    private double id;
    private int frequency;


    public DBLexeme() {
        id = 0;
        frequency = 0;
    }


    public double getId() {
        return id;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }


    public void setId(double id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DBLexeme) {
            DBLexeme dl = (DBLexeme) obj;
            if (dl.getId() == this.id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.id) ^ (Double.doubleToLongBits(this.id) >>> 32));
        return hash;
    }



    @Override
    public String toString() {
        return super.toString() + " | " + id;
    }





}
