/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.objects;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class DBParagraph {
    double paragraphID;
    List<DBSentence> dbSentence;

    public DBParagraph() {
        dbSentence = new ArrayList<DBSentence>();
    }

    public List<DBSentence> getDbSentence() {
        return dbSentence;
    }

    public void setDbSentence(List<DBSentence> dbSentence) {
        this.dbSentence = dbSentence;
    }

    public void setParagraphID(double paragraphID) {
        this.paragraphID = paragraphID;
    }

    public double getParagraphID() {
        return paragraphID;
    }



}
