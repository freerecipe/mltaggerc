/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.objects;


import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class DBSentence {

    double sentenceID;
    int frequency;
    List<DBLexeme> dbLexemeList;
    List<DBSentenceChunk> sentenceChunk;
    List<DBSentenceDependency> sentenceDependencyList;

    public DBSentence() {
        dbLexemeList = new ArrayList<DBLexeme>();
        sentenceChunk = new ArrayList<DBSentenceChunk>();
        sentenceDependencyList = new ArrayList<DBSentenceDependency>();
    }

    public void setSentenceDependencyList(List<DBSentenceDependency> sentenceDependencyList) {
        this.sentenceDependencyList = sentenceDependencyList;
    }

    public List<DBSentenceDependency> getSentenceDependencyList() {
        return sentenceDependencyList;
    }


    public void setSentenceID(double sentenceID) {
        this.sentenceID = sentenceID;
    }


    public List<DBLexeme> getDbLexemeList() {
        return dbLexemeList;
    }

    public int getFrequency() {
        return frequency;
    }

    public double getSentenceID() {
        return sentenceID;
    }

    public void setDbLexemeList(List<DBLexeme> dbLexemeList) {
        this.dbLexemeList = dbLexemeList;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public void setSentenceChunk(List<DBSentenceChunk> sentenceChunk) {
        this.sentenceChunk = sentenceChunk;
    }

    public List<DBSentenceChunk> getSentenceChunk() {
        return sentenceChunk;
    }


    public String prettyPrint(){
        String str ="";
        for (int i = 0; i < dbLexemeList.size(); i++) {
            str +=dbLexemeList.get(i).getWord() + " ";
        }
        return str;
    }



}
