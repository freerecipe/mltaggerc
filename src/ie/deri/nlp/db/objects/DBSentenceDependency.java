/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.objects;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class DBSentenceDependency {

    private int regentPosition;
    private int governorPosition;
    private DBDependency dbDependency;

    public DBSentenceDependency() {
        dbDependency = new DBDependency();
    }

    public void setDbDependency(DBDependency dbDependency) {
        this.dbDependency = dbDependency;
    }

    public void setGovernorPosition(int governorPosition) {
        this.governorPosition = governorPosition;
    }

    public void setRegentPosition(int regentPosition) {
        this.regentPosition = regentPosition;
    }

    public DBDependency getDbDependency() {
        return dbDependency;
    }

    public int getGovernorPosition() {
        return governorPosition;
    }

    public int getRegentPosition() {
        return regentPosition;
    }
}
