/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.retrieve.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class MultiplyList {

    public List<List<Double>> multiply(List<List<Double>> strList) {
        if (strList.isEmpty()) {
            return null;
        } else if (strList.size() == 1) {
            List<List<Double>> retList = new ArrayList<List<Double>>();
            for (int i = 0; i < strList.get(0).size(); i++) {
                List<Double> st = new ArrayList<Double>();
                st.add(strList.get(0).get(i));
                retList.add(st);
            }
            return retList;
        } else {
            List<Double> current = strList.get(0);
            strList.remove(0);
            List<List<Double>> remain = multiply(strList);

            List<List<Double>> finalList = new ArrayList<List<Double>>();
            for (int i = 0; i < current.size(); i++) {
                for (int j = 0; j < remain.size(); j++) {
                    List<Double> ff = new ArrayList<Double>();
                    ff.add(current.get(i));
                    ff.addAll(remain.get(j));
                    finalList.add(ff);
                }
            }
            return finalList;
        }
    }

//    public List<List<String>> multiply(List<List<String>> strList) {
//        if (strList.size() == 0) {
//            return null;
//        } else if (strList.size() == 1) {
//            List<List<String>> retList = new ArrayList<List<String>>();
//            for (int i = 0; i < strList.get(0).size(); i++) {
//                List<String> st = new ArrayList<String>();
//                st.add(strList.get(0).get(i));
//                retList.add(st);
//            }
//            return retList;
//        } else {
//            List<String> current = strList.get(0);
//            strList.remove(0);
//            List<List<String>> remain = multiply(strList);
//
//            List<List<String>> finalList = new ArrayList<List<String>>();
//            for (int i = 0; i < current.size(); i++) {
//                for (int j = 0; j < remain.size(); j++) {
//                    List<String> ff = new ArrayList<String>();
//                    ff.add(current.get(i));
//                    ff.addAll(remain.get(j));
//                    finalList.add(ff);
//                }
//            }
//            return finalList;
//        }
//    }

}
