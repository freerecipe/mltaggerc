/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.retrieve.util;

import ie.deri.nlp.db.h2.DBPaperRetrieve;
import ie.deri.nlp.db.objects.DBLexeme;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PhraseSearch {
    private DBPaperRetrieve dbr;

    public void setDbr(DBPaperRetrieve dbr) {
        this.dbr = dbr;
    }

    public PhraseSearch(DBPaperRetrieve dbr) {
        this.dbr = dbr;
    }


    public List<PhraseSearchResult> exactSearchForPhraseString(String phrase) throws SQLException {
        List<PhraseSearchResult> psrList = new ArrayList<PhraseSearchResult>();
        String[] tokens = phrase.split(" ");
        List<List<DBLexeme>> dbList = new ArrayList<List<DBLexeme>>();
        for (int i = 0; i < tokens.length; i++) {
            List<DBLexeme> dbl = dbr.retLexeme(tokens[i]);
            dbList.add(dbl);
        }
        List<List<Double>> idList = new ArrayList<List<Double>>();
        for (int i = 0; i < dbList.size(); i++) {
            List<Double> idL = new ArrayList<Double>();
            for (int j = 0; j < dbList.get(i).size(); j++) {
                idL.add(dbList.get(i).get(j).getId());
            }
            idList.add(idL);
        }
        MultiplyList ml = new MultiplyList();
        List<List<Double>> lst = ml.multiply(idList);
        java.sql.ResultSet rs2 = null;
        for (int i = 0; i < lst.size(); i++) {
            rs2 = dbr.retSentenceLexemeChain(lst.get(i));
            while (rs2.next()) {
                PhraseSearchResult psr = new PhraseSearchResult();
                psr.setSentenceID(rs2.getDouble("sentence_id"));
                psr.setStartPostion(rs2.getInt("start_position"));
                psr.setEndPosition(rs2.getInt("end_position"));
                psrList.add(psr);
            }
        }
        if (rs2 != null) {
            rs2.close();
        }
        return psrList;
    }




}
