/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.db.retrieve.util;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PhraseSearchResult {
private double sentenceID;
private int startPostion;
private int endPosition;

    public int getStartPostion() {
        return startPostion;
    }

    public double getSentenceID() {
        return sentenceID;
    }

    public int getEndPosition() {
        return endPosition;
    }

    public void setStartPostion(int startPostion) {
        this.startPostion = startPostion;
    }

    public void setSentenceID(double sentenceID) {
        this.sentenceID = sentenceID;
    }

    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }


}
