/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.fe;

import java.io.Serializable;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class FeatMap implements Serializable {
private int featureNumericID;
private double featureDBID ;
private String featureRealID;
private String featureType;

    public void setFeatureDBID(double featureIndex) {
        this.featureDBID = featureIndex;
    }

    public void setFeatureNumericID(int featureNumericID) {
        this.featureNumericID = featureNumericID;
    }

    public double getFeatureDBID() {
        return featureDBID;
    }

    public int getFeatureNumericID() {
        return featureNumericID;
    }

    public void setFeatureRealID(String featureRealID) {
        this.featureRealID = featureRealID;
    }

    public void setFeatureType(String featureType) {
        this.featureType = featureType;
    }

    public String getFeatureRealID() {
        return featureRealID;
    }

    public String getFeatureType() {
        return featureType;
    }







}
