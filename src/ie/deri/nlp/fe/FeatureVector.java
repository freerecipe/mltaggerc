/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.fe;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class FeatureVector {

    private double sourceSentenceID;
    private double bootstrapID;
    private boolean annotation;
    private List<DBFeatureValue> dbfeatureValueList;

    public FeatureVector() {
        dbfeatureValueList = new ArrayList<DBFeatureValue>();
    }

    public List<DBFeatureValue> getDbfeatureValueList() {
        return dbfeatureValueList;
    }

    public double getBootstrapID() {
        return bootstrapID;
    }

    public void setDbfeatureValueList(List<DBFeatureValue> dbfeatureValueList) {
        this.dbfeatureValueList = dbfeatureValueList;
    }

    public void setBootstrapID(double bootstrapID) {
        this.bootstrapID = bootstrapID;
    }

    public void setAnnotation(boolean annotation) {
        this.annotation = annotation;
    }

    public boolean isAnnotation() {
        return annotation;
    }

    public double getSourceSentenceID() {
        return sourceSentenceID;
    }

    public void setSourceSentenceID(double sourceSentenceID) {
        this.sourceSentenceID = sourceSentenceID;
    }


}
