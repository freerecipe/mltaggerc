/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.intrface.objects;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class User implements Serializable, Comparable {

    private String userID;
    private String name;
    private String surname;
    private String email;
    private List<Double> requestedFilesByEmail;

    public User() {
        requestedFilesByEmail = new ArrayList<Double>();
    }

    public void addRequestedFilesByEmail(double requestedFile) {
        this.requestedFilesByEmail.add(requestedFile);
    }

    public List<Double> getRequestedFilesByEmail() {
        return requestedFilesByEmail;
    }

    public void setRequestedFilesByEmail(List<Double> requestedFilesByEmail) {
        this.requestedFilesByEmail = requestedFilesByEmail;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUserID() {
        return userID;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int compareTo(Object o) {
        if (!(o instanceof User)) {
            throw new ClassCastException("A User object expected.");
        } else {
            User oUser = (User) o;
            if (oUser.getUserID().equals(userID)
                    && oUser.getName().equals(name)
                    && oUser.getSurname().equals(surname)
                    && oUser.getEmail().equals(email)) {
                return 0;
            } else {
                return -1;
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (compareTo(obj) == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.userID != null ? this.userID.hashCode() : 0);
        hash = 97 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 97 * hash + (this.surname != null ? this.surname.hashCode() : 0);
        hash = 97 * hash + (this.email != null ? this.email.hashCode() : 0);
        return hash;
    }
}
