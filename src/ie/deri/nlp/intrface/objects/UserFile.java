/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.intrface.objects;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class UserFile {

    private String userID;
    private String fileName;
    private double fileID;
    private String date;
    private boolean is_processed;

    public void setDate(String date) {
        this.date = date;
    }

    public void setFileID(double fileID) {
        this.fileID = fileID;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setIs_processed(boolean is_processed) {
        this.is_processed = is_processed;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDate() {
        return date;
    }

    public double getFileID() {
        return fileID;
    }

    public String getFileName() {
        return fileName;
    }

    public String getUserID() {
        return userID;
    }

    public boolean isIs_processed() {
        return is_processed;
    }

}
