/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.liblinear;


import ie.deri.parsie.mltagger.object.DBFeatureValue;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import liblinear.FeatureNode;
import liblinear.Linear;
import liblinear.Model;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class LiblinearModelPredict {

    private Model liblinearModel;
    private boolean init = false;

    public void loadModel(String pathModelFile) throws IOException {
        System.err.println("Loading model file from " + pathModelFile);
        liblinearModel = Model.load(new File(pathModelFile));
        init = true;

    }

    public LibLinearAnswer predict(Set<DBFeatureValue> featureSet, boolean flagPredictProbability) throws Exception {
        if (!init) {
            throw new Exception("You need to load a model file before TTI");
        }


        int nrClass = liblinearModel.getNrClass();
        double[] probEstimates = new double[nrClass];


        List<FeatureNode> x = new ArrayList<FeatureNode>();
        for (DBFeatureValue dbfv : featureSet) {
            FeatureNode node = new FeatureNode(dbfv.getNumericID(), dbfv.getValue());
            x.add(node);
        }
        FeatureNode[] nodes = new FeatureNode[x.size()];
        nodes = x.toArray(nodes);

//        if (flagPredictProbability) {
//            Linear.predictProbability(liblinearModel, nodes, probEstimates);
//            return probEstimates;
//        }
        double[] decVal =  new double[2];
        int predictLabel = Linear.predictValues(liblinearModel, nodes, decVal);
//        if (probEstimates.length <= predictLabel) {
//            probEstimates = new double[probEstimates.length + 1];
//        }
//        probEstimates[predictLabel] = 1d;
     
        
//        System.err.println("Lebel " + label);
//        System.err.println("PredictLable "+ predictLabel +
//                " /prob est" + probEstimates[0] + "  ** " +
//               decVal[0] + "/"  );
      //  double ans = ;
        return new LibLinearAnswer(decVal[0], predictLabel);

    }
}
