/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.liblinear;

import java.io.File;
import java.io.IOException;
import liblinear.InvalidInputDataException;
import liblinear.Linear;
import liblinear.Model;
import liblinear.Parameter;
import liblinear.Problem;
import liblinear.SolverType;
import liblinear.Train;

/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
public class LiblinearTrain {


    public static void minimalTrain(String trainFile, String modelFile) throws IOException, InvalidInputDataException {
        double bias = 1;
        Parameter param = null;
        Problem prob = Train.readProblem(new File(trainFile),  bias);
        param = new Parameter(SolverType.L2R_L1LOSS_SVC_DUAL, bias, .1);
        Model model = Linear.train(prob, param);
        Linear.saveModel(new File(modelFile), model);
    }
    
    public static void main(String s[]) throws IOException, InvalidInputDataException {
        String trainsetPath = "c:\\MLTagger\\acl_arc_a.train";
        String modelPath = "C:\\MLTagger\\model\\acl_arc_a.model";
        LiblinearTrain.minimalTrain(trainsetPath, modelPath);
    }

}

