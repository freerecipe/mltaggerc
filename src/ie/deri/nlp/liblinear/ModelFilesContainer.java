/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.liblinear;

import ie.deri.nlp.analyzer.objects.PLexeme;
import ie.deri.nlp.db.objects.DBDependency;
import ie.deri.nlp.db.objects.DBLexeme;

import ie.deri.parsie.mltagger.object.FeatMap;
import ie.deri.parsie.mltagger.object.FeatureDependency;
import ie.deri.parsie.mltagger.setting.TaggerSetting;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class ModelFilesContainer {

    private String taggerID;
    private Map<PLexeme, Double> pLexemeMap;
    private Map<FeatureDependency, Double> pFeatureDepMap;
    private Map<String, Integer> featureMap;
    private LiblinearModelPredict libModel;

    public void init(TaggerSetting ttis) throws Exception{
        this.taggerID = ttis.getTaggerID();
        loadDBLexemeIDList(ttis.getDbLexemePath());
        loadDBDependencyIDList(ttis.getDbDependencyPath());
        loadFeatureTTIMAPFromFile(ttis.getTtiMap());
        libModel = new LiblinearModelPredict();
        libModel.loadModel(ttis.getTtiModel());
    }


    private void loadDBLexemeIDList(String fileName) throws FileNotFoundException, IOException, ClassNotFoundException {
        System.err.println("Loading DBLexeme map from " + fileName);
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));

        this.pLexemeMap = new HashMap<PLexeme, Double>();
        DBLexeme dbl;
        while ((dbl = (DBLexeme) in.readObject()) != null) {
            PLexeme pl = new PLexeme();
            pl.setLemma(dbl.getLemma());
            pl.setPos(dbl.getPos());
            pl.setWord(dbl.getWord());
            pLexemeMap.put(pl, dbl.getId());
        }
        in.close();
        System.err.println("Done loading Plexeme map of size " + pLexemeMap.size());
    }

    private void loadDBDependencyIDList(String fileName) throws FileNotFoundException, IOException, ClassNotFoundException {
        System.err.println("Loading DBDependency map from " + fileName);
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
        this.pFeatureDepMap = new HashMap<FeatureDependency, Double>();
        DBDependency dbd;
        while ((dbd = (DBDependency) in.readObject()) != null) {
            FeatureDependency fd = new FeatureDependency();
            fd.setGovernorLexemeID(dbd.getGovernorLexemeID());
            fd.setRegentLexemeID(dbd.getRegentLexemeID());
            fd.setDepType(dbd.getDependencyType());
            pFeatureDepMap.put(fd, dbd.getDependencyID());
        }
        in.close();
        System.err.println("Done loading Plexeme map of size " + pFeatureDepMap.size());
    }

    private void loadFeatureTTIMAPFromFile(String fileName) throws IOException, ClassNotFoundException, IOException, FileNotFoundException {
        System.err.println("Loading TTI feature map from " + fileName);
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));

        this.featureMap = new HashMap<String, Integer>();
        FeatMap fm;
        while ((fm = (FeatMap) in.readObject()) != null) {
            featureMap.put(fm.getFeatureRealID(), fm.getFeatureNumericID());
        }
        in.close();
        System.err.println("Done loading feature map of size " + featureMap.size());

    }

    public Map<String, Integer> getFeatureMap() {
        return featureMap;
    }

    public Map<FeatureDependency, Double> getpFeatureDepMap() {
        return pFeatureDepMap;
    }

    public Map<PLexeme, Double> getpLexemeMap() {
        return pLexemeMap;
    }

    public LiblinearModelPredict getLibModel() {
        return libModel;
    }

    public String getTaggerID() {
        return taggerID;
    }
    


}
