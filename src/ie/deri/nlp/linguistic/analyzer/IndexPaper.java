/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.linguistic.analyzer;

import ie.deri.nlp.analyzer.objects.Dependency;
import ie.deri.nlp.analyzer.objects.PLexeme;
import ie.deri.nlp.analyzer.objects.PParagraph;
import ie.deri.nlp.analyzer.objects.PTable;
import ie.deri.nlp.analyzer.objects.PEquation;
import ie.deri.nlp.analyzer.objects.PSection;
import ie.deri.nlp.analyzer.objects.PChunk;
import ie.deri.nlp.analyzer.objects.PFigure;
import ie.deri.nlp.analyzer.objects.PReference;
import ie.deri.nlp.analyzer.objects.PSentence;
import ie.deri.nlp.db.h2.DBPaperAssertion;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Author;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Paper;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Reference;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class IndexPaper {



    public double indexPaper(DBPaperAssertion dba, Paper p, PaperAnalyzer ap, String url) throws SQLException {

        List<PReference> referenceList = new ArrayList<PReference>();
        List<Double> authorList = new ArrayList<Double>();

        System.err.println("indexing lexemes:" + ap.getLexemeMap().size());
        for (PLexeme l : ap.getLexemeMap().keySet()) {
            dba.indexLexeme(l, ap.getLexemeMap().get(l));
        }

        System.err.println("indexing phrase:" + ap.getPchunkHashMap().size());
        for (PChunk pc : ap.getPchunkHashMap().keySet()) {
            dba.indexPhrase(pc, ap.getPchunkHashMap().get(pc), ap.getLexemeMap());
        }


        System.err.println("indexing sentences: " + ap.getSentenceMap().size());
        for (PSentence psi : ap.getSentenceMap().keySet()) {
            dba.indexSentence(psi, ap.getSentenceMap().get(psi), ap.getPchunkHashMap());
        }


        System.err.println("indexing malt dependencies: " + ap.getDependencyMap().size());
        for (Dependency dep : ap.getDependencyMap().keySet()) {
            dba.indexLexemeDependency(dep,
                    ap.getDependencyMap().get(dep),
                    "mp", ap.getLexemeMap());
        }

        System.err.println("indexing sentence dependencies: " + ap.getSentenceMap().size());
        for (PSentence psi : ap.getSentenceMap().keySet()) {
            if (ap.getSentenceMap().get(psi).isIsNew()) {
                dba.indexSentenceLexemesDependency(
                        ap.getSentenceMap().get(psi).getId(),
                        psi.getSentenceDependencies(),
                        ap.getDependencyMap(), "mp");
            }
        }

        System.err.println("indexing Paragraphs:" + ap.getParagraphMap().size());
        for (PParagraph psix : ap.getParagraphMap().keySet()) {
            dba.indexParagraph(psix, ap.getParagraphMap().get(psix));
        }

        System.err.println("indexing Figure: " + ap.getFigureMap().size());
        for (PFigure pf : ap.getFigureMap().keySet()) {
            dba.indexFigure(pf, ap.getFigureMap().get(pf), ap.getParagraphMap());
        }

        System.err.println("indexing Table: " + ap.getTableMap().size());
        for (PTable pt : ap.getTableMap().keySet()) {
            dba.indexTable(pt, ap.getTableMap().get(pt), ap.getParagraphMap());
        }

        System.err.println("indexing Equation: " + ap.getEquationMap().size());

        for (PEquation pe : ap.getEquationMap().keySet()) {
            dba.indexEquation(pe, ap.getEquationMap().get(pe), ap.getParagraphMap());
        }

        System.err.println("Indeixng Section");
        for (PSection psect : ap.getPsectionList()) {
            dba.indexSection(psect, ap.getSectionMap().get(psect),
                    ap.getSectionMap(),
                    ap.getSentenceMap(),
                    ap.getParagraphMap(),
                    ap.getFigureMap(),
                    ap.getTableMap(),
                    ap.getEquationMap());
        }

        // return value mikhahad ya iek jaee metadata tarif bayad beshe
        for (Author author : p.getAuthorList()) {
            double authorID = dba.indexAuthor(author);
            authorList.add(authorID);
        }

        for (Reference ref : p.getReferenceList()) {
            double refID = dba.indexReference(ref);
            PReference pref = new PReference();
            pref.setReferenceID(refID);
            pref.setCitStr(ref.getCiteStr());
            pref.setMarker(ref.getMarker());
            referenceList.add(pref);
        }

        double docID = dba.indexDocument(
                ap.getSentenceMap().get(ap.getPaperTitleSentence()).getId(),
                p,
                ap,
                referenceList,
                authorList, url);

        // chizaee ke baraye document mikhay list reference ha
        // title sentence
        // list section ha
        // agar ke az ghabl index shode bashe roll back bayad bekoni
        // age na ke tammom mishe
        // list author ha


        return docID;

    }

}
