/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.linguistic.analyzer;

import ie.deri.nlp.analyzer.objects.Dependency;
import ie.deri.nlp.analyzer.objects.PLexeme;
import ie.deri.nlp.analyzer.objects.PChunk;
import ie.deri.nlp.analyzer.objects.PSentence;
import ie.deri.nlp.db.h2.DBPaperAssertion;
import java.sql.SQLException;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class IndexText {



    public double indexText(DBPaperAssertion dba, TextAnalyzer ta) throws SQLException {
//        System.err.println("indexing lexemes:" + ta.getLexemeMap().size());

        for (PLexeme l : ta.getLexemeMap().keySet()) {
            dba.indexLexeme(l, ta.getLexemeMap().get(l));
        }
//        System.err.println("indexing phrase:" + ta.getPchunkHashMap().size());
        for (PChunk pc : ta.getPchunkHashMap().keySet()) {
            dba.indexPhrase(pc, ta.getPchunkHashMap().get(pc), ta.getLexemeMap());
        }

//        System.err.println("indexing sentences: " + ta.getSentenceMap().size());
        for (PSentence psi : ta.getSentenceMap().keySet()) {
            dba.indexSentence(psi, ta.getSentenceMap().get(psi), ta.getPchunkHashMap());
        }

//        System.err.println("indexing malt dependencies: " + ta.getDependencyMap().size());
        for (Dependency dep : ta.getDependencyMap().keySet()) {
            dba.indexLexemeDependency(dep,
                    ta.getDependencyMap().get(dep),
                    "mp", ta.getLexemeMap());
        }

//        System.err.println("indexing sentence dependencies: " + ta.getSentenceMap().size());
        for (PSentence psi : ta.getSentenceMap().keySet()) {
            if (ta.getSentenceMap().get(psi).isIsNew()) {
                dba.indexSentenceLexemesDependency(
                        ta.getSentenceMap().get(psi).getId(),
                        psi.getSentenceDependencies(),
                        ta.getDependencyMap(), "mp");
            }
        }

//        System.err.println("indexing Paragraph" );
        dba.indexParagraph(ta.getParagraph(), ta.getMetadataParagraph());
        return ta.getMetadataParagraph().getId();
    }

}
