/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.linguistic.analyzer;

import ie.deri.nlp.analyzer.objects.Metadata;
import ie.deri.nlp.analyzer.objects.PLexeme;
import ie.deri.nlp.analyzer.objects.PParagraph;
import ie.deri.nlp.analyzer.objects.PEquation;
import ie.deri.nlp.analyzer.objects.PTable;
import ie.deri.nlp.analyzer.objects.PSection;
import ie.deri.nlp.analyzer.objects.PFigure;
import ie.deri.nlp.analyzer.objects.PChunk;
import ie.deri.nlp.analyzer.objects.PSentence;
import edu.stanford.nlp.ling.WordLemmaTag;
import ie.deri.nlp.analyzer.objects.Dependency;
import ie.deri.nlp.analyzer.objects.SentenceDependency;
import ie.deri.nlp.analyzer.objects.Chunk;
import ie.deri.nlp.misc.UID;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Equation;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Paper;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Paragraph;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Section;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PaperAnalyzer {

    private PSentence paperTitleSentence;
    private HashMap<PLexeme, Metadata> lexemeMap;
    private HashMap<PSentence, Metadata> sentenceMap;
    private HashMap<PParagraph, Metadata> paragraphMap;
    private HashMap<PEquation, Metadata> equationMap;
    private HashMap<PFigure, Metadata> figureMap;
    private HashMap<PTable, Metadata> tableMap;
    private HashMap<PSection, Metadata> sectionMap;
    private HashMap<PChunk, Metadata> pchunkHashMap; // to work on that tommorow
    private HashMap<Dependency, Metadata> dependencyMap;
    private List<PParagraph> paragraphList;
    private List<PSentence> sentenceList;
    private List<PSection> psectionList;
    private Pipeline6 pl6;
    private int tokenNumber;
    private int sentenceNumber;
    private int paragraphNumber;
    private int sectionNumber;
    
    

    public PaperAnalyzer(Pipeline6 pl6) {
        tokenNumber = 0;
        sentenceNumber = 0;
        paragraphNumber = 0;
        sectionNumber = 0;
        paperTitleSentence = new PSentence();
        lexemeMap = new HashMap<PLexeme, Metadata>();
        sentenceMap = new HashMap<PSentence, Metadata>();
        paragraphMap = new HashMap<PParagraph, Metadata>();
        equationMap = new HashMap<PEquation, Metadata>();
        figureMap = new HashMap<PFigure, Metadata>();
        tableMap = new HashMap<PTable, Metadata>();
        sectionMap = new HashMap<PSection, Metadata>();
        psectionList = new ArrayList<PSection>();
        pchunkHashMap = new HashMap<PChunk, Metadata>();
        sentenceList = new ArrayList<PSentence>();
        dependencyMap = new HashMap<Dependency, Metadata>();
        paragraphList = new ArrayList<PParagraph>();
        this.pl6 = pl6;
    }


    public void analyzePaper(Paper p) {
        paperTitleSentence = processAsSentenceText(pl6, p.getTitle());
        for (int i = 0; i < p.getSectionList().size(); i++) {
            PSection ps = analyzeSection(p.getSectionList().get(i));
            if (sectionMap.containsKey(ps)) {
                sectionMap.get(ps).incFrequency();
            } else {
                Metadata m = new Metadata();
                m.setFrequency(1);
                m.setId(UID.getUID());
                sectionMap.put(ps, m);
            }
            psectionList.add(ps);
        }
    }

    public PSection analyzeSection(Section section) {
        sectionNumber++;
        PSection ps = new PSection();
        PSentence titleSentence =
                processAsSentenceText(pl6, section.getTitle());
        ps.setTitleSentence(titleSentence);

        for (int i = 0; i < section.getContent().size(); i++) {
            if (section.getContent().get(i) instanceof Paragraph) {
                Paragraph p = (Paragraph) section.getContent().get(i);
                PParagraph pp =
                        processParagraphText(pl6, p.getParagraphText());
                ps.addSectionContent(pp);
                updateMap(paragraphMap, pp);
            } else if (section.getContent().get(i) instanceof Equation) {
                Equation eq = (Equation) section.getContent().get(i);
                PEquation peq = new PEquation();
                PParagraph equatPara =
                        processParagraphText(pl6, eq.getEquationText());
                peq.setEquationParagprah(equatPara);
                peq.setPageNumber(eq.getPageNumber());
                ps.addSectionContent(peq);
                updateMap(paragraphMap, equatPara);
                updateMap(equationMap, peq);
            } else if (section.getContent().get(i) instanceof Section) {
               // process for nested sections
                Section nestedSection = (Section) section.getContent().get(i);
                PSection psNested = analyzeSection(nestedSection);
                ps.addSectionContent(psNested);
                updateMap(sectionMap, psNested );
            }
        }

        // process figures
        for (int i = 0; i < section.getFigureList().size(); i++) {
            PFigure pf = new PFigure();
            PParagraph pp = processParagraphText(pl6, section.getFigureList().get(i).getFigureCaption());
            pf.setFigureCaption(pp);
            pf.setPageNumber(section.getFigureList().get(i).getPageNumber());
            updateMap(paragraphMap, pp);
            updateMap(figureMap, pf);
            ps.addFigure(pf);
        }

        // process tables
        for (int i = 0; i < section.getTableList().size(); i++) {
            PTable pt = new PTable();
            PParagraph pp = 
                    processParagraphText(pl6,
                    section.getTableList().get(i).getTableCaption());
            pt.setTableCaptionParagrap(pp);
            updateMap(paragraphMap, pp);
            updateMap(tableMap, pt);
            ps.addTable(pt);
        }

        ps.setType(section.getType());
        ps.setStartPage(section.getStartPage());
        ps.setEndPage(section.getEndPage());
        return ps;
    }

    public PParagraph processParagraphText(Pipeline6 pl6, String paragraphText) {
        paragraphNumber++;
        List<List<WordLemmaTag>> lwlt = pl6.process(paragraphText);
        PParagraph pp = new PParagraph();
    //    pp.setParagraphLength(lwlt.size());

        for (int i = 0; i < lwlt.size(); i++) {
            sentenceNumber++;
            PSentence s = new PSentence();
            List<Metadata> mlList = new ArrayList<Metadata>();
            List<PLexeme> sentenceLexemes = new ArrayList<PLexeme>();
            for (int j = 0; j < lwlt.get(i).size(); j++) {
                tokenNumber++;
                PLexeme l = new PLexeme();
                l.setLemma(lwlt.get(i).get(j).lemma());
                l.setPos(lwlt.get(i).get(j).tag());
                l.setWord(lwlt.get(i).get(j).word());
                sentenceLexemes.add(l);
                updateMap(lexemeMap, l);
                mlList.add(lexemeMap.get(l));
            }
            s.setSentenceLength(mlList.size());
            s.setSentenceLexemes(mlList);
            if (sentenceMap.containsKey(s)) {
                sentenceMap.get(s).incFrequency();
                pp.addParagraphSentence(sentenceMap.get(s));
            } else {
                // malt dependency parsing
                s.setSentenceDependencies(maltParse(sentenceLexemes));
                // adding chunking
                List<Chunk> chunkSentence = pl6.doChunking(lwlt.get(i));
                for (int j = 0; j < chunkSentence.size(); j++) {
                    updateMap(
                         pchunkHashMap, chunkSentence.get(j).getPchunk());
                }
                s.setSentenceChunks(chunkSentence);
                Metadata m = new Metadata();
                m.setFrequency(1);
                m.setId(UID.getUID());
                sentenceMap.put(s, m);
                pp.addParagraphSentence(m);
            }
        }
        return pp;
    }

    public PSentence processAsSentenceText(Pipeline6 pl6, String sentenceText) {
        sentenceNumber++;
        List<WordLemmaTag> lwlt = pl6.processAsSentence(sentenceText);
        PSentence s = new PSentence();
        s.setSentenceLength(lwlt.size());
        List<Metadata> mlList = new ArrayList<Metadata>();
        List<PLexeme> sentenceLexemes = new ArrayList<PLexeme>();
        for (int j = 0; j < lwlt.size(); j++) {
            tokenNumber++;
            PLexeme l = new PLexeme();
            l.setLemma(lwlt.get(j).lemma());
            l.setPos(lwlt.get(j).tag());
            l.setWord(lwlt.get(j).word());
            if (lexemeMap.containsKey(l)) {
                lexemeMap.get(l).incFrequency();
            } else {
                Metadata m = new Metadata();
                m.setFrequency(1);
                m.setId(UID.getUID());
                lexemeMap.put(l, m);
            }
            mlList.add(lexemeMap.get(l));
            sentenceLexemes.add(l);
        }
        s.setSentenceLength(mlList.size());
        s.setSentenceLexemes(mlList);

        if (sentenceMap.containsKey(s)) {
            sentenceMap.get(s).incFrequency();
        } else {
            List<Chunk> chunkSentence = pl6.doChunking(lwlt);
            for (int j = 0; j < chunkSentence.size(); j++) {
                updateMap(
                        pchunkHashMap, chunkSentence.get(j).getPchunk());
            }
            s.setSentenceChunks(chunkSentence);
            s.setSentenceDependencies(maltParse(sentenceLexemes));
            Metadata m = new Metadata();
            m.setFrequency(1);
            m.setId(UID.getUID());
            sentenceMap.put(s, m);
        }
        return s;
    }

    public HashMap<PLexeme, Metadata> getLexemeMap() {
        return lexemeMap;
    }


    public HashMap<PParagraph, Metadata> getParagraphMap() {
        return paragraphMap;
    }

    public Pipeline6 getPl6() {
        return pl6;
    }

    public HashMap<PSentence, Metadata> getSentenceMap() {
        return sentenceMap;
    }


    public List<PParagraph> getParagraphList() {
        return paragraphList;
    }

    public List<PSentence> getSentenceList() {
        return sentenceList;
    }

    public HashMap<PEquation, Metadata> getEquationMap() {
        return equationMap;
    }

    public HashMap<PFigure, Metadata> getFigureMap() {
        return figureMap;
    }

    public HashMap<PSection, Metadata> getSectionMap() {
        return sectionMap;
    }

    public HashMap<PTable, Metadata> getTableMap() {
        return tableMap;
    }

    public List<PSection> getPsectionList() {
        return psectionList;
    }

    public HashMap<PChunk, Metadata> getPchunkHashMap() {
        return pchunkHashMap;
    }

    public List<SentenceDependency> maltParse(List<PLexeme> lexemeList) {
        List<SentenceDependency> sdp = new ArrayList<SentenceDependency>();
        sdp = pl6.maltParse(lexemeList);
        if (sdp != null) {
            for (SentenceDependency sd : sdp) {
                if (dependencyMap.containsKey(sd.getDependency())) {
                    dependencyMap.get(sd.getDependency()).incFrequency();
                } else {
                    Metadata m = new Metadata();
                    m.setFrequency(1);
                    m.setId(UID.getUID());
                    dependencyMap.put(sd.getDependency(), m);
                }
            }
        }
        return sdp;

    }

    public HashMap<Dependency, Metadata> getDependencyMap() {
        return dependencyMap;
    }


    private void updateMap(
            HashMap<PParagraph, Metadata> paragraphMap,
            PParagraph pp) {
        if (paragraphMap.containsKey(pp)) {
            paragraphMap.get(pp).incFrequency();
        } else {
            Metadata m = new Metadata();
            m.setFrequency(1);
            m.setId(UID.getUID());
            paragraphMap.put(pp, m);
        }
    }

    private void updateMap(
            HashMap<PEquation, Metadata> equationMap, PEquation peq) {
        if (equationMap.containsKey(peq)) {
            equationMap.get(peq).incFrequency();
        } else {
            Metadata m = new Metadata();
            m.setFrequency(1);
            m.setId(UID.getUID());
            equationMap.put(peq, m);
        }

    }

    private void updateMap(HashMap<PSection, Metadata> sectionMap, PSection psNested) {
        if (sectionMap.containsKey(psNested)) {
            sectionMap.get(psNested).incFrequency();
        } else {
            Metadata m = new Metadata();
            m.setFrequency(1);
            m.setId(UID.getUID());
            sectionMap.put(psNested, m);
        }

    }

    private void updateMap(HashMap<PFigure, Metadata> figureMap, PFigure pf) {
        if (figureMap.containsKey(pf)) {
            figureMap.get(pf).incFrequency();
        } else {
            Metadata m = new Metadata();
            m.setFrequency(1);
            m.setId(UID.getUID());
            figureMap.put(pf, m);
        }

    }

    private void updateMap(HashMap<PTable, Metadata> tableMap, PTable pt) {
         if (tableMap.containsKey(pt)) {
            tableMap.get(pt).incFrequency();
        } else {
            Metadata m = new Metadata();
            m.setFrequency(1);
            m.setId(UID.getUID());
            tableMap.put(pt, m);
        }
    }

    private void updateMap(HashMap<PLexeme, Metadata> lexemeMap, PLexeme l) {
        if (lexemeMap.containsKey(l)) {
            lexemeMap.get(l).incFrequency();
        } else {
            Metadata m = new Metadata();
            m.setFrequency(1);
            m.setId(UID.getUID());
            lexemeMap.put(l, m);
        }

    }


    private void updateMap(HashMap<PChunk, Metadata> pchunkHashMap, PChunk pchunk) {

        if (pchunkHashMap.containsKey(pchunk)) {
            pchunkHashMap.get(pchunk).incFrequency();
        } else {
            Metadata m = new Metadata();
            m.setFrequency(1);
            m.setId(UID.getUID());
            pchunkHashMap.put(pchunk, m);
        }
    }

    public PSentence getPaperTitleSentence() {
        return paperTitleSentence;
    }

    public int getParagraphNumber() {
        return paragraphNumber;
    }

    public int getSectionNumber() {
        return sectionNumber;
    }

    public int getSentenceNumber() {
        return sentenceNumber;
    }

    public int getTokenNumber() {
        return tokenNumber;
    }




}
