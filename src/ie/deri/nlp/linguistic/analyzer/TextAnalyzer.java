/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.linguistic.analyzer;

import ie.deri.nlp.analyzer.objects.Metadata;
import ie.deri.nlp.analyzer.objects.PLexeme;
import ie.deri.nlp.analyzer.objects.PParagraph;
import ie.deri.nlp.analyzer.objects.PChunk;
import ie.deri.nlp.analyzer.objects.PSentence;
import edu.stanford.nlp.ling.WordLemmaTag;
import ie.deri.nlp.analyzer.objects.Dependency;
import ie.deri.nlp.analyzer.objects.SentenceDependency;
import ie.deri.nlp.analyzer.objects.Chunk;
import ie.deri.nlp.misc.UID;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class TextAnalyzer {


    private HashMap<PLexeme, Metadata> lexemeMap;
    private HashMap<PSentence, Metadata> sentenceMap;
    private HashMap<PChunk, Metadata> pchunkHashMap; // to work on that tommorow
    private HashMap<Dependency, Metadata> dependencyMap;
    private List<PSentence> sentenceList;
    private PParagraph paragraph;
    private Pipeline6 pl6;
    private int tokenNumber;
    private int sentenceNumber;
    private Metadata metadataParagraph;

    
    

    public TextAnalyzer(Pipeline6 pl6) {
        tokenNumber = 0;
        sentenceNumber = 0;
        lexemeMap = new HashMap<PLexeme, Metadata>();
        sentenceMap = new HashMap<PSentence, Metadata>();      
        pchunkHashMap = new HashMap<PChunk, Metadata>();
        sentenceList = new ArrayList<PSentence>();
        dependencyMap = new HashMap<Dependency, Metadata>();
        metadataParagraph = new Metadata();
      
        this.pl6 = pl6;
    }



    public void processText(Pipeline6 pl6, String paragraphText) {
        List<List<WordLemmaTag>> lwlt = pl6.process(paragraphText);
        paragraph = new PParagraph();
        for (int i = 0; i < lwlt.size(); i++) {
            sentenceNumber++;
            PSentence s = new PSentence();
            List<Metadata> mlList = new ArrayList<Metadata>();
            List<PLexeme> sentenceLexemes = new ArrayList<PLexeme>();
            for (int j = 0; j < lwlt.get(i).size(); j++) {
                tokenNumber++;
                PLexeme l = new PLexeme();
                l.setLemma(lwlt.get(i).get(j).lemma());
                l.setPos(lwlt.get(i).get(j).tag());
                l.setWord(lwlt.get(i).get(j).word());
                sentenceLexemes.add(l);
                updateMap(lexemeMap, l);
                mlList.add(lexemeMap.get(l));
            }
            s.setSentenceLength(mlList.size());
            s.setSentenceLexemes(mlList);
            if (sentenceMap.containsKey(s)) {
                sentenceMap.get(s).incFrequency();
                paragraph.addParagraphSentence(sentenceMap.get(s));
            } else {
                // malt dependency parsing
                s.setSentenceDependencies(maltParse(sentenceLexemes));
                // adding chunking
                List<Chunk> chunkSentence = pl6.doChunking(lwlt.get(i));
                for (int j = 0; j < chunkSentence.size(); j++) {
                    updateMap(
                         pchunkHashMap, chunkSentence.get(j).getPchunk());
                }
                s.setSentenceChunks(chunkSentence);
                Metadata m = new Metadata();
                m.setFrequency(1);
                m.setId(UID.getUID());
                sentenceMap.put(s, m);
                paragraph.addParagraphSentence(m);
            }
        }
        metadataParagraph.setFrequency(1);
        metadataParagraph.setId(UID.getUID());
    }

    public PSentence processAsSentenceText(Pipeline6 pl6, String sentenceText) {
        sentenceNumber++;
        List<WordLemmaTag> lwlt = pl6.processAsSentence(sentenceText);
        PSentence s = new PSentence();
        s.setSentenceLength(lwlt.size());
        List<Metadata> mlList = new ArrayList<Metadata>();
        List<PLexeme> sentenceLexemes = new ArrayList<PLexeme>();
        for (int j = 0; j < lwlt.size(); j++) {
            tokenNumber++;
            PLexeme l = new PLexeme();
            l.setLemma(lwlt.get(j).lemma());
            l.setPos(lwlt.get(j).tag());
            l.setWord(lwlt.get(j).word());
            if (lexemeMap.containsKey(l)) {
                lexemeMap.get(l).incFrequency();
            } else {
                Metadata m = new Metadata();
                m.setFrequency(1);
                m.setId(UID.getUID());
                lexemeMap.put(l, m);
            }
            mlList.add(lexemeMap.get(l));
            sentenceLexemes.add(l);
        }
        s.setSentenceLength(mlList.size());
        s.setSentenceLexemes(mlList);

        if (sentenceMap.containsKey(s)) {
            sentenceMap.get(s).incFrequency();
        } else {
            List<Chunk> chunkSentence = pl6.doChunking(lwlt);
            for (int j = 0; j < chunkSentence.size(); j++) {
                updateMap(
                        pchunkHashMap, chunkSentence.get(j).getPchunk());
            }
            s.setSentenceChunks(chunkSentence);
            s.setSentenceDependencies(maltParse(sentenceLexemes));
            Metadata m = new Metadata();
            m.setFrequency(1);
            m.setId(UID.getUID());
            sentenceMap.put(s, m);
        }
        return s;
    }

    public HashMap<PLexeme, Metadata> getLexemeMap() {
        return lexemeMap;
    }



    public Pipeline6 getPl6() {
        return pl6;
    }

    public HashMap<PSentence, Metadata> getSentenceMap() {
        return sentenceMap;
    }

    public List<PSentence> getSentenceList() {
        return sentenceList;
    }

    public HashMap<PChunk, Metadata> getPchunkHashMap() {
        return pchunkHashMap;
    }

    public List<SentenceDependency> maltParse(List<PLexeme> lexemeList) {
        List<SentenceDependency> sdp = new ArrayList<SentenceDependency>();
        sdp = pl6.maltParse(lexemeList);
        if (sdp != null) {
            for (SentenceDependency sd : sdp) {
                if (dependencyMap.containsKey(sd.getDependency())) {
                    dependencyMap.get(sd.getDependency()).incFrequency();
                } else {
                    Metadata m = new Metadata();
                    m.setFrequency(1);
                    m.setId(UID.getUID());
                    dependencyMap.put(sd.getDependency(), m);
                }
            }
        }
        return sdp;

    }

    public HashMap<Dependency, Metadata> getDependencyMap() {
        return dependencyMap;
    }



    private void updateMap(HashMap<PLexeme, Metadata> lexemeMap, PLexeme l) {
        if (lexemeMap.containsKey(l)) {
            lexemeMap.get(l).incFrequency();
        } else {
            Metadata m = new Metadata();
            m.setFrequency(1);
            m.setId(UID.getUID());
            lexemeMap.put(l, m);
        }

    }


    private void updateMap(HashMap<PChunk, Metadata> pchunkHashMap, PChunk pchunk) {

        if (pchunkHashMap.containsKey(pchunk)) {
            pchunkHashMap.get(pchunk).incFrequency();
        } else {
            Metadata m = new Metadata();
            m.setFrequency(1);
            m.setId(UID.getUID());
            pchunkHashMap.put(pchunk, m);
        }
    }



    public int getSentenceNumber() {
        return sentenceNumber;
    }

    public int getTokenNumber() {
        return tokenNumber;
    }

    public PParagraph getParagraph() {
        return paragraph;
    }

    public Metadata getMetadataParagraph() {
        return metadataParagraph;
    }

    
    
    




}
