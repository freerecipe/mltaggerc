/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.malt;

import ie.deri.nlp.analyzer.objects.PLexeme;
import ie.deri.nlp.analyzer.objects.SentenceDependency;
import ie.deri.nlp.settings.MaltParserSetting;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.maltparser.MaltParserService;
import org.maltparser.core.exception.MaltChainedException;
import org.maltparser.core.symbol.SymbolTable;
import org.maltparser.core.syntaxgraph.DependencyStructure;
import org.maltparser.core.syntaxgraph.edge.Edge;
import org.maltparser.core.syntaxgraph.node.DependencyNode;

/**
 *
 * @author behqas
 */
public class MaltSimpleWrapper {

    private MaltParserService maltService = null;
    private List<SentenceDependency> sdp = null;

    public void initMaltService(MaltParserSetting maltSettings) throws MaltChainedException, IOException {
        String mco = "-c "
                + maltSettings.getMaltModelFile()
                + " -w " + maltSettings.getMaltWorkingDir() + " -m parse";
        // malt needs to have the path to malt.jar in class path
        String systemProperty = System.getProperty("java.class.path");
        System.err.println("java classpath is: " + systemProperty);
        if (!systemProperty.contains("malt")) {
            String maltClassPath = (new java.io.File(".").getCanonicalPath()) + File.separatorChar + "lib" + File.separatorChar + "malt.jar";
            systemProperty += ";" + maltClassPath;
            System.setProperty("java.class.path", systemProperty);
            System.err.println(System.getProperty("java.class.path"));
        }
        System.err.println(">>> Initializing Malt Service");
        maltService = new MaltParserService();
        maltService.initializeParserModel(mco);
        System.err.println(">>> Let's get Malt really up!");
        String[] token = new String[18];
        token[0] = "1\tPierre\t_\tNNP\tNNP\t_";
        token[1] = "2\tVinken\t_\tNNP\tNNP\t_";
        token[2] = "3\t,\t_\t,\t,\t_";
        token[3] = "4\t61\t_\tCD\tCD\t_";
        token[4] = "5\tyears\t_\tNNS\tNNS\t_";
        token[5] = "6\told\t_\tJJ\tJJ\t_";
        token[6] = "7\t,\t_\t,\t,\t_";
        token[7] = "8\twill\t_\tMD\tMD\t_";
        token[8] = "9\tjoin\t_\tVB\tVB\t_";
        token[9] = "10\tthe\t_\tDT\tDT\t_";
        token[10] = "11\tboard\t_\tNN\tNN\t_";
        token[11] = "12\tas\t_\tIN\tIN\t_";
        token[12] = "13\ta\t_\tDT\tDT\t_";
        token[13] = "14\tnonexecutive\t_\tJJ\tJJ\t_";
        token[14] = "15\tdirector\t_\tNN\tNN\t_";
        token[15] = "16\tNov.\t_\tNNP\tNNP\t_";
        token[16] = "17\t29\t_\tCD\tCD\t_";
        token[17] = "18\t.\t_\t.\t.\t_";
        maltService.parse(token);
        System.err.println(">>> init done!");
    }
    private String[] tokens;
    private DependencyStructure depStructure;

    public List<SentenceDependency> maltParse(List<PLexeme> lexemeList) {
        sdp = new ArrayList<SentenceDependency>();
        tokens = generateMaltParseInput(lexemeList);
        if (tokens != null) {

            try {
                depStructure = maltService.parse(tokens);
                for (int i = 1; i <= depStructure.getHighestDependencyNodeIndex(); i++) {
                    try {
                        int regentPosition = 0;
                        int govPosition = 0;
                        String depType = "";
                        DependencyNode node = depStructure.getDependencyNode(i);
                        if (node != null) {
                            regentPosition = Integer.parseInt(node.getLabelSymbol(node.getLabelTypes().iterator().next()));
                            if (node.hasHead()) {
                                Edge e = node.getHeadEdge();
                                govPosition = e.getSource().getIndex();
                                if (e.isLabeled()) {
                                    for (SymbolTable table : e.getLabelTypes()) {
                                        depType = e.getLabelSymbol(table);
                                    }
                                }
                            }
                        }
                        if (regentPosition > 0 && govPosition > 0 && !depType.equals("null") && !depType.equals("")) {
                            ie.deri.nlp.analyzer.objects.Dependency dep =
                                    new ie.deri.nlp.analyzer.objects.Dependency();
                            dep.setDependencyType(depType);
                            dep.setGovernorLexeme(lexemeList.get(govPosition - 1));
                            dep.setRegentLexeme(lexemeList.get(regentPosition - 1));
                            SentenceDependency sd =
                                    new SentenceDependency(
                                    govPosition - 1, regentPosition - 1, dep);
                            sdp.add(sd);
                        }
                    } catch (Exception e) {
                        System.err.println(">>>>> Exception while inner loop malt parse " + e);
                    }
                }
            } catch (Exception e) {
                System.err.println("Catch the Malt Exception ;)" + e);
            }
        }
        return sdp;

    }

    public String[] generateMaltParseInput(List<PLexeme> lexemeList) {
        String[] maltParseInput = new String[lexemeList.size()];
        for (int i = 0; i < lexemeList.size(); i++) {
            maltParseInput[i] =
                    Integer.toString(i + 1) + "\t"
                    + lexemeList.get(i).getWord() + "\t-\t"
                    + lexemeList.get(i).getPos() + "\t"
                    + lexemeList.get(i).getPos() + "\t-";
        }
        return maltParseInput;
    }
}
