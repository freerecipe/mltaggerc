/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.misc;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class Download {

    public void download(String source, String destination) throws MalformedURLException,
            IOException {
        if (source != null &&  destination != null) {


            BufferedInputStream bis = null;
            BufferedOutputStream bos = null;
            try {
                URL url = new URL(source);
                URLConnection urlc = url.openConnection();

                bis = new BufferedInputStream(urlc.getInputStream());
                bos = new BufferedOutputStream(new FileOutputStream(destination));

                int i;
                while ((i = bis.read()) != -1) {
                    bos.write(i);
                }
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
                if (bos != null) {
                    try {
                        bos.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            }
        } else {
            System.out.println("Input not available");
        }
    }
    
    public static void main(String[] ars) throws MalformedURLException, IOException{
     Download d = new Download();
     d.download("http://uk.wrs.yahoo.com/_ylt=A7x9Qbyz1FFN0xQApBJLBQx.;_ylu=--/SIG=12e1j7ojb/EXP=1297230099/**http%3a//homepages.inf.ed.ac.uk/kbyrne3/docs/phdproppres.pdf", "c:\\psdf.pdf");
    }
}


