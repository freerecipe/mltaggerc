/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.misc;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

public final class PDFToImageConverter {

    int dpi = 180;
    int type = 8;
    private File inputFile;
    private String inputFileName;
    PDDocument document = null;
    String output;

    public PDFToImageConverter(String input, String output) {
        inputFile = new File(input);
        try {
            document = PDDocument.load(inputFile);
        } catch (IOException ex) {
            Logger.getLogger(PDFToImageConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.output = output;
    }

    public PDFToImageConverter(PDDocument document, String output) {
        this.document = document;
        this.output = output;
    }


    public PDFToImageConverter() {
    }


    public void convert() {
        StringBuilder sbFileName = new StringBuilder();
        if (document != null) {
            try {
                List<PDPage> pages = document.getDocumentCatalog().getAllPages();
                int iCount = 0;
                for (int i = 0; i < pages.size(); i++) {
                    iCount = i + 1;
                    sbFileName.append(output).append(iCount).append("_pg_img");
                    File outfile = new File(sbFileName.toString() + ".jpg");
                    sbFileName.setLength(0);
                    
                    PDPage singlePage = pages.get(i);
                    
                    BufferedImage buffImage = singlePage.convertToImage(type, dpi);
                    // write image to disk
                    ImageIO.write(buffImage, "png", outfile);
                }
            } catch (Exception e) {
                System.err.println("Exception while pdf to image conversion" + e);
            }
        }
    }

    public String getInputFileName() {
        return inputFileName;
    }

    public void setInputPDFFile(PDDocument document) {
        this.document = document;
    }

}
