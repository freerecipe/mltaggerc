/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.misc;


import ie.deri.nlp.intrface.objects.User;
import javax.mail.*;
import javax.mail.internet.*;

import java.util.Properties;

public class SSLMail {
    public static void sendProcessDoneMessage(
            ie.deri.nlp.settings.EmailSetting emailSetting, User user, String  report) throws Exception {
        Properties props = new Properties();

        props.put("mail.transport.protocol", "smtps");
        props.put("mail.smtps.host", emailSetting.getSMTPHOSTNAME());
        props.put("mail.smtps.auth", "true");
        // props.put("mail.smtps.quitwait", "false");
        Session mailSession = Session.getDefaultInstance(props);
        mailSession.setDebug(true);
        Transport transport = mailSession.getTransport();

        MimeMessage message = new MimeMessage(mailSession);
        message.setSubject("EEYORE's done processing your papers! :)");
        String messageText = "<i>Hello " + user.getName() + ",</i> <br />\n"
                + "EEYORE's finished reading your papers. You can find"
                + " more info about your corpus on the ?!website!?.\n<br />";
        messageText += report;
        message.setContent(messageText, "text/html");
        message.addRecipient(Message.RecipientType.TO,
                new InternetAddress(user.getEmail()));
        transport.connect(emailSetting.getSMTPHOSTNAME(),
                emailSetting.getSMTPHOSTPORT(),
                emailSetting.getSMTPAUTHUSER(), emailSetting.getSMTPAUTHPWD());
        transport.sendMessage(message,
                message.getRecipients(Message.RecipientType.TO));
        transport.close();
    }
}
