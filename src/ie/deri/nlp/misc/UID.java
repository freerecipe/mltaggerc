/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.misc;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;


public class UID {

    private static double dateOffset;

    static {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        dateOffset = Double.parseDouble(sdf.format(cal.getTime())) * 100000000000000000.00;
    }

    public static double getUID() {
        double randomDouble;
        double rand1 = Math.abs(UUID.randomUUID().getMostSignificantBits()) * 100000000000000.0;
        randomDouble = rand1 + dateOffset + UUID.randomUUID().getMostSignificantBits();

        return randomDouble;
    }
//    public static void main(String[] args) throws Exception {
//        for (int i = 0; i < 10; i++) {
//            long lngStart = System.currentTimeMillis();
//            m_log.info(UID.getUID());
//            m_log.info("Elapsed time: " + (System.currentTimeMillis() - lngStart));
//        }
//    }
}
