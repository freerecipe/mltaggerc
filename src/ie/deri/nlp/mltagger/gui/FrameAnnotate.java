/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.mltagger.gui;

/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.parsie.mltagger.db.MLTaggerIndex;
import ie.deri.parsie.mltagger.db.MLTaggerRetrieve;
import ie.deri.parsie.mltagger.object.BootstrapInstance;
import ie.deri.parsie.mltagger.object.BootstrapTerm;
import java.awt.GridLayout;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class FrameAnnotate extends JPanel {

    DBConnection dbc = null;
    MLTaggerRetrieve mltr = null;
    Map<Integer, BootstrapTerm> checkBootBot =
            new HashMap<Integer, BootstrapTerm>();
    ActionListener al = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            JCheckBox cb = (JCheckBox) e.getSource();

            System.err.println(
                    checkBootBot.get(e.getSource().hashCode()).getId());
            try {
                if (cb.isSelected()) {
                    mltr.updateBootstrapTerm(checkBootBot.get(e.getSource().hashCode()).getId(), true);
                } else {
                    mltr.updateBootstrapTerm(checkBootBot.get(e.getSource().hashCode()).getId(), false);
                }
            } catch (SQLException ex) {
                Logger.getLogger(FrameAnnotate.class.getName()).log(Level.SEVERE, null, ex);
            }
            checkBootBot.get(e.getSource().hashCode()).setIsReviewed(true);

        }
    };

    public void fireAnnotateFrame(List<BootstrapTerm> btList, MLTaggerRetrieve mltr) {

        this.mltr = mltr;
        
        JFrame frame = new JFrame("Annotation Frame");
        JPanel panel = new JPanel(new GridLayout(0, 1));
        JCheckBox checkBox;
        for (BootstrapTerm bt : btList) {
            checkBox = new JCheckBox(bt.getBootstrapString());
            if (bt.isPositiveExample()) {
              
                checkBox.setSelected(true);
            }
            checkBox.addActionListener(al);
            checkBootBot.put(checkBox.hashCode(), bt);
            panel.add(checkBox);
        }
        JScrollPane scrollPane = new JScrollPane(panel);
        frame.add(scrollPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

  
}