/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.mltagger.result.filter;

import ie.deri.nlp.analyzer.objects.BPSentence;
import ie.deri.nlp.analyzer.objects.Chunk;
import java.lang.Exception;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
public class PhraseBoundaryFilter {
    
     Set<Integer> endBoundaryList;
     Set<Integer> startPositionSet;

    public PhraseBoundaryFilter() throws Exception {
       throw new Exception("this is not acceptable") ;
    }
    
     
    public PhraseBoundaryFilter(BPSentence analSent) {
        endBoundaryList = new HashSet<Integer>();
        startPositionSet =  new HashSet<Integer>();
        //System.err.println("--> Phrase numbers " + analSent.getSentenceChunks().size());
        for (int i = 0; i < analSent.getSentenceChunks().size(); i++) {
            if (analSent.getSentenceChunks().get(i).getPchunk().getType().startsWith("N")) {
                endBoundaryList.add(analSent.getSentenceChunks().get(i).getStart() 
                        + analSent.getSentenceChunks().get(i).getPchunk().getLength() - 1 );
                startPositionSet.add(analSent.getSentenceChunks().get(i).getStart());
//                 System.err.println("--> sp " +
//                analSent.getSentenceChunks().get(i).getStart());
            }
        }
        
//        System.err.println("Size of phrase boundary lists " + startPositionSet.size()+"/"+endBoundaryList.size());

    }

     // simple phrase filtering based on open nlp chunker
    public boolean isAcceptedByOpenNLP(int startPosition, int endPosition){
        return (endBoundaryList.contains(endPosition) 
                //&& startPositionSet.contains(startPosition)
                );
        // implement filters, gazzeteer list or use LOD to furthur play with the output
        
        
    }
    

 
    
    
}
