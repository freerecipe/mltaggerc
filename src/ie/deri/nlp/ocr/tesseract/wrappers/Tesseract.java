/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.ocr.tesseract.wrappers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class Tesseract {

    String pathToTesseractExe;
    String lang;
    int ocrMode = 1;

    public Tesseract(String pathToTesseractExe, String lang) {
        this.pathToTesseractExe = pathToTesseractExe;
        this.lang = lang;
    }

    public String doOCR(String inputImageDirectory) throws FileNotFoundException, IOException {
        StringBuilder ocrResult = new StringBuilder();
        File inputDirectory = new File(inputImageDirectory);
        if (!inputDirectory.exists()) {
            return "";
        }
        System.err.println("There are " + inputDirectory.list().length + " pages to OCR");
        int pageNumber = inputDirectory.list().length;
        for (int i = 0; i < pageNumber ; i++) {

            String outputRndmFileName = inputImageDirectory + "tesse_output_" + i;
            String commandLine = pathToTesseractExe + " "
                    + inputImageDirectory + File.separator + (i + 1) + "_pg_img.jpg "
                    + outputRndmFileName + " -l " + lang + " -psm " + ocrMode;
            try {
                Runtime run = Runtime.getRuntime();
                Process tesseract = run.exec(commandLine);
                tesseract.waitFor();
     
                BufferedReader input =
                        new BufferedReader(new FileReader(outputRndmFileName + ".txt"));
                String line = null;
                while ((line = input.readLine()) != null) {
                    ocrResult.append(line);
                    ocrResult.append(System.getProperty("line.separator"));
                }

                // delete the result
                File singlePageResultFile = new File(outputRndmFileName + ".txt");
                if (singlePageResultFile.exists()) {
                    singlePageResultFile.delete();
                }
            } catch (Exception e) {
                // handle error
                System.err.println("Error running Tesseract OCR");
                System.err.println(e);
            }
        }
        return ocrResult.toString();
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void setOcrMode(int ocrMode) {
        this.ocrMode = ocrMode;
    }

    public String getLang() {
        return lang;
    }

    public int getOcrMode() {
        return ocrMode;
    }

    public String getPathToTesseractExe() {
        return pathToTesseractExe;
    }

    public void setPathToTesseractExe(String pathToTesseractExe) {
        this.pathToTesseractExe = pathToTesseractExe;
    }
    
    

    public static void main(String[] s) throws FileNotFoundException, IOException {
        Tesseract ts = new Tesseract(
                "C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe", "eng");

        ts.doOCR("C:\\test_final\\img\\1.6917593533460653E32\\");

    }
}
