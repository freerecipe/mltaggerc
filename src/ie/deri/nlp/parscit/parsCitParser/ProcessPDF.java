/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.parscit.parsCitParser;

import ie.deri.nlp.misc.PDFToImageConverter;
import ie.deri.nlp.ocr.tesseract.wrappers.Tesseract;
import ie.deri.nlp.parscit.parsCitParser.parseObjects.Paper;
import ie.deri.nlp.parscit.wrapper.ParseCitClient;
import ie.deri.nlp.settings.PDFServerSettings;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import net.sf.junidecode.Junidecode;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class ProcessPDF {

    String imageFormat = "jpg";
    String color = "rgb";
    int resolution = 180;
    String password = null;
    String imgOutputPrefix = "";
    int imageType = BufferedImage.TYPE_INT_RGB;
    private PDFParser parser;
    private File inputFile;
    private COSDocument cosDocument;
    private PDFTextStripper pdfStripper;
    private PDDocument pdDoc;
    private File txtFile;
    private FileOutputStream fos;
    private OutputStreamWriter out;
    private File parsCit;
    private Parser parserCit;
    private Paper paper;
    private SerializeContent sc;
    private PDFToImageConverter converter;
    private Tesseract ocr =
            new Tesseract("Tesseract-OCR//tesseract.exe", "eng");

    public Paper processPDF(PDFServerSettings pss,
            String pdfFile, double uid) throws Exception {

        //get the text
        inputFile = new File(pdfFile);
        parser = new PDFParser(new FileInputStream(inputFile));
        parser.parse();
        cosDocument = parser.getDocument();
        pdfStripper = new PDFTextStripper();
        pdDoc = new PDDocument(cosDocument);
        String parsedText = pdfStripper.getText(pdDoc);
        if (parsedText.length() < 200) {
            //         make images       
            String imgOutputPath = pss.getPathSetting().getImgRepository()
                    + File.separator + Double.toString(uid);
            File file = new File(imgOutputPath);

            if (!file.exists()) {
                file.mkdirs();
            }
            imgOutputPrefix = imgOutputPath + File.separator;
            converter = new PDFToImageConverter(pdDoc, imgOutputPrefix);
            converter.convert();
            pdDoc.close();

            parsedText = ocr.doOCR(imgOutputPrefix);

            // add OCR here
        }
        // write the txt file into the folder
        String txtOutputPath = pss.getPathSetting().getTextRepository();
        File txtDir = new File(txtOutputPath);
        if (!txtDir.exists()) {
            txtDir.mkdirs();
        }
        String txtFileName = txtOutputPath + File.separator + Double.toString(uid) + ".txt";
        txtFile = new File(txtFileName);
        fos = new FileOutputStream(txtFile);
//        OutputStreamWriter out = new OutputStreamWriter(fos, "ISO-8859-1");
        out = new OutputStreamWriter(fos, "ASCII");
        String fTxt = Junidecode.unidecode(parsedText);
        out.write(fTxt);
        out.flush();
        fos.close();
        out.close();
        // request parscit for analysis
        // this may be replaced
        String parsCitFile = pss.getPathSetting().getParsCitRepository()
                + File.separator + Double.toString(uid) + ".xml";
        parsCit = new File(pss.getPathSetting().getParsCitRepository());
        if (!parsCit.exists()) {
            parsCit.mkdirs();
        }
        ParseCitClient.parsCitClient(pss.getParsCitSetting().getParscitIPAddress(),
                pss.getParsCitSetting().getParscitBindPort(), txtFileName, parsCitFile);

        // do parsing on parscit
        parserCit = new Parser();
        paper = parserCit.parse(parsCitFile, uid);
        sc = new SerializeContent();
        sc.serialize(pss.getPathSetting().getXMLRepository(), paper);
        return paper;
    }
}
