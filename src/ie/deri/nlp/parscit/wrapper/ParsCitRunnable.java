/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.parscit.wrapper;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class ParsCitRunnable implements Runnable {

    protected Socket clientSocket = null;
    protected String serverText = null;

    public ParsCitRunnable(Socket clientSocket, String serverText) {
        this.clientSocket = clientSocket;
        this.serverText = serverText;
    }

    public void run() {

        System.err.println("Connection Accepted From: " + clientSocket.getInetAddress());
        System.setProperty("file.encoding", "ASCII");
        BufferedInputStream bis = null;
        OutputStream os = null;
        try {
            // prepare input buffer from socket
            bis = new BufferedInputStream(clientSocket.getInputStream());
            // get output stream of the socket
            os = clientSocket.getOutputStream();
        } catch (IOException ex) {
            System.err.println("Fatal Error ...");
            System.err.println(ex);
        }
        // prepare a temp file output for feeding into parscit perl module
        String inputRndmFileName = "input" + UID.getUID();
        String outputRndmFileName = "output" + UID.getUID();
        FileOutputStream fosx = null;
        try {
            fosx = new FileOutputStream(inputRndmFileName);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ParsCitRunnable.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedOutputStream bos = new BufferedOutputStream(fosx);
        // get the file from socket
        int in;
        byte[] receivedData;
        receivedData = new byte[8192];
        try {
            bos.flush();
            while ((in = bis.read(receivedData)) != -1) {
                bos.write(receivedData, 0, in);
            }
            bos.flush();
            bos.close();
            fosx.close();

        } catch (IOException ex) {
            Logger.getLogger(ParsCitRunnable.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.err.println(">> Finish file transfer from "
                + clientSocket.getInetAddress());
        String commandLine = "./citeExtract.pl -m extract_all "
                + inputRndmFileName + " " + outputRndmFileName;

        try {
            Runtime run = Runtime.getRuntime();
            Process parsCit = run.exec(commandLine);
            parsCit.waitFor();
            BufferedReader parsCitOutput =
                    new BufferedReader(
                    new InputStreamReader(parsCit.getInputStream()));
            String line;
            while ((line = parsCitOutput.readLine()) != null) {
                System.err.print(line);
            }
        } catch (Exception e) {
            // handle error
            System.err.println("Error running parscit");
            System.err.println(e);
        }
        // stream back the results
        File parscitResult = new File(outputRndmFileName);
        try {
            byte[] mybytearrayInput =
                    new byte[(int) parscitResult.length()];
            BufferedInputStream bisResult = null;

            bisResult = new BufferedInputStream(new FileInputStream(parscitResult));

            bisResult.read(mybytearrayInput, 0, mybytearrayInput.length);
            os.write(mybytearrayInput, 0, mybytearrayInput.length);
            os.flush();
            bis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ParsCitRunnable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ParsCitRunnable.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//         deleting temp files
            if (parscitResult.exists()) {
                parscitResult.delete();
            }
            File inputFile = new File(inputRndmFileName);
            if (inputFile.exists()) {
                inputFile.delete();
            }
            File inputFileBody = new File(inputRndmFileName + ".body");
            if (inputFileBody.exists()) {
                inputFileBody.delete();
            }
            File inputFileCite = new File(inputRndmFileName + ".cite");
            if (inputFileCite.exists()) {
                inputFileCite.delete();
            }
            try {
                clientSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(ParsCitRunnable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}



