/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.parscit.wrapper;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class ParsCitServerWrapper {
    
public static void main(String args[]){
    if (args.length < 1) {
        System.err.println("Error: Please Provide Port Number you want to run the parser \n");
        System.exit(1);
    }
    System.err.println(">>> init the server..");
    ParseCitServer server = new ParseCitServer(Integer.parseInt(args[0]));
    System.err.println(">>> Server running at " + args[0]);
    new Thread(server).start();
}
}
