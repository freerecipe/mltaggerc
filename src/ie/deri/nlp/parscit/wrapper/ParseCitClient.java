/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.parscit.wrapper;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class ParseCitClient {
/**
     * 
     * @param hostIP
     * @param port
     * @param rawTextFileToParse
     * @param parseCitResult
     * @throws UnknownHostException
     * @throws IOException 
     */
    public static void parsCitClient(String hostIP, int port, String rawTextFileToParse, String parseCitResult) throws UnknownHostException, IOException {
        Socket sock = new Socket(hostIP, port);


        OutputStream os = sock.getOutputStream();
        InputStream is = sock.getInputStream();

        File myFile = new File(rawTextFileToParse);
        byte[] mybytearrayInput = new byte[(int) myFile.length()];
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(myFile));
        bis.read(mybytearrayInput, 0, mybytearrayInput.length);
        os.write(mybytearrayInput, 0, mybytearrayInput.length);
        bis.close();
        os.flush();
//        os.close();


        sock.shutdownOutput();
        // os.close();

        byte[] receivedData;
        int in;
        receivedData = new byte[8192];
        BufferedOutputStream bos = new BufferedOutputStream(
                new FileOutputStream(parseCitResult));
        while ((in = is.read(receivedData)) != -1) {
            bos.write(receivedData, 0, in);
        }
        bos.flush();
        bos.close();
        sock.close();

    }

    public static void main(String args[]) throws UnknownHostException, IOException{
        parsCitClient("127.0.0.9",10000, "c:\\1.txt", "c:\\1.now");
    }
    

}
