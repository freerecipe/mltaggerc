/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.parscit.wrapper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Logger;

public class UID {

    private static Logger m_log = Logger.getAnonymousLogger();
    private static char separator;
    private static StringBuilder IPAddressSegment;
    private static SecureRandom prng;

    static {
        try {
            // Get IPAddress Segment
            IPAddressSegment = new StringBuilder();
            InetAddress addr = InetAddress.getLocalHost();
            StringBuilder strTemp = new StringBuilder();
            byte[] ipaddr = addr.getAddress();
            for (int i = 0; i < ipaddr.length; i++) {
                Byte b = new Byte(ipaddr[i]);

                strTemp = new StringBuilder(Integer.toHexString(b.intValue() & 0x000000ff));
                while (strTemp.length() < 2) {
                    strTemp.insert(0, '0');
                }
                IPAddressSegment.append(strTemp);
            }

            if (separator != '\u0000') {
                IPAddressSegment.append(separator);
            }

            //Get Random Segment Algoritm
            prng = SecureRandom.getInstance("SHA1PRNG");

        } catch (UnknownHostException ex) {
            m_log.severe("Unknown Host Exception Caught: " + ex.getMessage());
        } catch (NoSuchAlgorithmException nsae) {
            m_log.severe("No Such Algorithm Exception Caught: " + nsae.getMessage());
        }
    }

    public static final String getUID() {
        StringBuilder strRetVal = new StringBuilder(IPAddressSegment);
        StringBuilder strTemp = new StringBuilder();

        //Get CurrentTimeMillis() segment
        strTemp = new StringBuilder(Long.toHexString(System.currentTimeMillis()));
        while (strTemp.length() < 12) {
            strTemp.insert(0, '0');
        }
        strRetVal.append(strTemp);
        if (separator != '\u0000') {
            IPAddressSegment.append(separator);
        }

        // Get Random Segment
        strTemp = new StringBuilder(Integer.toHexString(prng.nextInt()));
        while (strTemp.length() < 8) {
            strTemp.insert(0, '0');
        }

        strRetVal.append(strTemp.substring(4));
        if (separator != '\u0000') {
            IPAddressSegment.append(separator);
        }

        //Get IdentityHash() segment
        strTemp = new StringBuilder(Long.toHexString(System.identityHashCode((Object) new UID())));
        while (strTemp.length() < 8) {
            strTemp.insert(0, '0');
        }
        strRetVal.append(strTemp);

        return strRetVal.toString().toUpperCase();
    }

//    public static void main(String[] args) throws Exception {
//        for (int i = 0; i < 10; i++) {
//            long lngStart = System.currentTimeMillis();
//            m_log.info(UID.getUID());
//            m_log.info("Elapsed time: " + (System.currentTimeMillis() - lngStart));
//        }
//    }


}
