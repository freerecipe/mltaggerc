/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdfprocess.tcp.clients;





import ie.deri.nlp.settings.PDFServerSetting;
import ie.deri.nlp.intrface.objects.User;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;



/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PDFProcessClient {
 public static boolean request(PDFServerSetting pss, User user) {
      ObjectOutputStream oos = null;
      ObjectInputStream ois = null;
      Socket socket = null;
      try {

        // open a socket connection
       socket = new Socket(pss.getIPAddress(),
               pss.getBindPort());
        // open I/O streams for objects
        oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject(user);
        ois = new ObjectInputStream(socket.getInputStream());
        // read an object from the server
        oos.close();
        ois.close();
      } catch(Exception e) {
        System.out.println(e.getMessage());
        return false;
      }
      return true;
   }

 
}
