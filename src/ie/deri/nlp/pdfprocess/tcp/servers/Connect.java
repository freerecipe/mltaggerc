/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdfprocess.tcp.servers;


import ie.deri.nlp.intrface.objects.User;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;


/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
class Connect extends Thread {

    PDFServer pdfServer;
    private Socket client = null;
    private ObjectInputStream ois = null;
    private ObjectOutputStream oos = null;

    public Connect() {
    }

    public Connect(Socket clientSocket,
           PDFServer aThis) {
        pdfServer = aThis;
        client = clientSocket;
        try {
            ois = new ObjectInputStream(client.getInputStream());
            oos = new ObjectOutputStream(client.getOutputStream());
        } catch (Exception e1) {
            try {
                client.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            return;
        }
        this.start();
    }


    @Override
    public void run() {
        try {
            User user = (User) ois.readObject();
            System.err.println("PRF >> " + user.getName());
            pdfServer.put(user);
            oos.flush();
            // close streams and connections
            ois.close();
            oos.close();

            client.close();

        } catch (Exception e) {
        }
    }
}
