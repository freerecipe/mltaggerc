/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdfprocess.tcp.servers;


import ie.deri.nlp.settings.PDFServerSetting;
import ie.deri.nlp.intrface.objects.User;
import java.net.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PDFServer
        extends Thread {

    private ServerSocket pdfServer;
    private static PDFServerSetting settings;
//    UserQueue userQueue;
    List<User> userRequsetList = new ArrayList<User>();
    Set<User> userLocked = new HashSet<User>();

    public PDFServer(PDFServerSetting pss) throws Exception {
//        this.userQueue = userQueue;
        settings = pss;
        pdfServer = new ServerSocket(
                settings.getBindPort());
        System.out.println("PDF Server listening on port >>> "
                + settings.getBindPort());
        this.start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("Waiting for connections.");
                Socket client = pdfServer.accept();
                System.out.println("Accepted a connection from: "
                        + client.getInetAddress());
                Connect c = new Connect(client, this);
            } catch (Exception e) {
            }
        }
    }

    public synchronized void put(User user) throws InterruptedException {
        userRequsetList.add(user);
        notifyAll();

    }

    public synchronized User getUser()
            throws InterruptedException {
        notify();
        while (userRequsetList.isEmpty()) {
            wait();
        }
        User user = userRequsetList.get(0);
        if (userLocked.contains(user) || userRequsetList.isEmpty()) {
            return null;
        } else {
            userRequsetList.remove(0);
            userLocked.add(user);
            return user;
        }
    }

    public synchronized void remove(User user) {
        userLocked.remove(user);
        notify();
    }
}



