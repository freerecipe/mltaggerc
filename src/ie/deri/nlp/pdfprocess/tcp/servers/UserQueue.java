/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdfprocess.tcp.servers;

///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package ie.deri.nlp.pdfprocess.tcp.servers;
//
//import ie.deri.nlp.vaadin.objects.User;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// *
// * @author Your Name <behrang.qasemizadeh at deri.org>
// */
//public class UserQueue {
//
//    List<User> userRequsetList = new ArrayList<User>();
//
//   public synchronized User get() {
//        if (userRequsetList.size() == 0) {
//            try {
//                wait();
//            } catch (InterruptedException e) {
//                System.out.println("InterruptedException caught");
//            }
//        }
//
//        User user = userRequsetList.get(0);
//        userRequsetList.remove(0);
//        notify();
//        return user;
//    }
//
//  public synchronized void put(User user) {
//        userRequsetList.add(user);
//        notify();
//    }
//}
