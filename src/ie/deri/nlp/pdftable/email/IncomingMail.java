/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdftable.email;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.nlp.db.h2.DBUser;
import ie.deri.nlp.intrface.objects.User;
// import ie.deri.nlp.misc.IsPDF;
import ie.deri.nlp.misc.UID;
import ie.deri.nlp.pdfprocess.tcp.clients.PDFProcessClient;
import ie.deri.nlp.settings.DBSettings;
import ie.deri.nlp.settings.PDFServerSettings;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;

public class IncomingMail {

    private  DBUser vDB;
    private  PDFServerSettings pss;
    private  DBSettings dbSettings;

    @SuppressWarnings("static-access")
    public void setvDBSettings(DBSettings dbSet)  {
        this.dbSettings = dbSet;
    }

     

    public void setvDB() throws ClassNotFoundException, SQLException {
        vDB = new DBUser();
        DBConnection con = new DBConnection();
        con.initializeTCPDBUser(dbSettings.getUserDB().getUserH2DB(), "user_db", true);
        vDB.setCon(con.getCon());
    }

    public void setPss(PDFServerSettings pDFServerSettings) {
        pss = pDFServerSettings;
    }

    @SuppressWarnings("static-access")
    public void downloadEmails() throws Exception {
        Properties props = new Properties();
        Session session = Session.getInstance(props, null);
        Store store = session.getStore(pss.getEmailSetting().getPOPSESSIONTYPE());
        System.err.println("Connected to eeyore mail box");
        store.connect(
                pss.getEmailSetting().getPOPHOST(),
                pss.getEmailSetting().getPOPPORT(),
                pss.getEmailSetting().getSMTPAUTHUSER(),
                pss.getEmailSetting().getSMTPAUTHPWD());
        // Get folder
        Folder folder = store.getFolder("INBOX");
        folder.open(Folder.READ_WRITE);
        try {
            // Get directory listing
            Message messages[] = folder.getMessages();
            System.err.println("There are " + messages.length + " unread messages");
            for (int i = 0; i < messages.length; i++) {

                // from
                String userEmailString = messages[i].getFrom()[0].toString();
                String emailStr[] = userEmailString.split("<");
                String emailx[] = emailStr[1].split(">");

                System.err.println(emailx[0] + " has sent an email");
                boolean requestProcess = false;
                // check the email address is registered!
                if(!vDB.getCon().isValid(1000)){
                    setvDB();
                }
                if (vDB.isEmailUser(emailx[0])) {
                    User user = vDB.getUserByEmail(emailx[0]);
                    Object content = messages[i].getContent();
                    if (content instanceof Multipart) {
                        Multipart mp = (Multipart) content;
                        List<Double> requestFileList = new ArrayList<Double>();
                        for (int j = 0; j < mp.getCount(); j++) {
                            Part part = mp.getBodyPart(j);
                            String disposition = part.getDisposition();
                            if ((disposition != null)
                                    && (disposition.equals(Part.ATTACHMENT)
                                    || disposition.equals(Part.INLINE))) {
                                // Check if plain
                                MimeBodyPart mbp = (MimeBodyPart) part;
                                // here should check waether it is pdf!
                                if (!mbp.isMimeType("text/plain")) {
                                    System.err.println(">> mbpcontenttype "
                                            + mbp.getContentType());
                                    decodeName(part.getFileName());
                                    Double uid = saveFile(part);
                                    if (uid != null) {
                                        vDB.setUserFile(user.getUserID(),
                                                uid, part.getFileName());
                                        requestFileList.add(uid);
                                        requestProcess = true;
                                    }
                                }
                            }
                        } // end of multipart for loop
                        //
                        if (requestProcess == true) {
                            System.err.println("Request processing");
                            user.setRequestedFilesByEmail(requestFileList);
                            PDFProcessClient.request(pss.getPDFServerSetting(), user);
                        }
                    } // end messages for loop
                }
                messages[i].setFlag(Flags.Flag.DELETED, true);
            }

            // Close connection
            folder.close(true); // true tells the mail server to expunge deleted messages
            store.close();
        } catch (Exception e) {
            folder.close(true); // true tells the mail server to expunge deleted
            store.close();
            throw e;
        }
    }

    private static String decodeName(String name) throws Exception {
        if (name == null || name.length() == 0) {
            return "unknown";
        }
        String ret = java.net.URLDecoder.decode(name, "UTF-8");

        // also check for a few other things in the string:
        ret = ret.replaceAll("=\\?utf-8\\?q\\?", "");
        ret = ret.replaceAll("\\?=", "");
        ret = ret.replaceAll("=20", " ");

        return ret;
    }

    private Double saveFile(Part part) throws Exception {
        // check weather it is a pdf file
        Double fileID = UID.getUID();
        File dir = new File(pss.getPathSetting().getPDFRepository());
        if (!dir.exists()) {
            System.err.println(">>>> Make directory at " +
                    pss.getPathSetting().getPDFRepository());
            dir.mkdirs();
        }
        String fileName = pss.getPathSetting().getPDFRepository() + File.separatorChar + fileID;
        BufferedOutputStream bos = new BufferedOutputStream(
                new FileOutputStream(fileName));
        byte[] buff = new byte[2048];
        InputStream is = part.getInputStream();
        int ret = 0, count = 0;
        while ((ret = is.read(buff)) > 0) {
            bos.write(buff, 0, ret);
            count += ret;
        }
        bos.close();
        is.close();
        File recFile = new File(fileName);
//        if (
//                !IsPDF.isPDF(recFile)
//                ) {
//            recFile.delete();
//            return null;
//        } else {
            return fileID;
//        }
    }
}
