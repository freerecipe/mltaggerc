/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdftable.email;


import ie.deri.nlp.settings.Settings;
import java.io.IOException;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class Main {

    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException, ParserConfigurationException, SAXException {
        try {
            Settings setting = new Settings();
            setting.fromXmlFile(args[0]);
            System.err.println(">> Path setting " + setting.getPDFServerSettings().getPathSetting());
            System.err.println(setting.getDBSettings().getUserDB());

            IncomingMail incomMail = new IncomingMail();
            
            incomMail.setvDBSettings(setting.getDBSettings());
            incomMail.setvDB();
            incomMail.setPss(setting.getPDFServerSettings());
            while (true) {
                incomMail.downloadEmails();
                Thread.sleep(60000);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

    }
}
