/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdftable.scripts;


import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.nlp.db.h2.DBUser;
import ie.deri.nlp.settings.Settings;


/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
public class CreateDataCatalogs {
    public static void main(String[] ard) throws Exception{
        
        Settings settings = new Settings();
        settings.fromXmlFile("C:\\UNLP-Projects\\Settings\\Settings.xml");

        System.err.println(
        settings.getDBSettings().getUserDB().getUserH2DB().getPort());

        DBConnection dbcUser = new DBConnection();
        dbcUser.initializeTCPDBUser( 
                settings.getDBSettings().getUserDB().getUserH2DB(), "user_db", true);
        
        DBUser db = new DBUser();
        db.setCon(dbcUser.getCon());
        db.getUser("acl_arc_a");
        
        
            
              
        
    }
    
}
