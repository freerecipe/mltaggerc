/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdftable.scripts;

import java.sql.SQLException;
import org.h2.tools.Server;

/**
 *
 * @author behqas
 */
public class H2DBServer {

    /**
     * Running H2DB server
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException {
       
//        if (args.length != 1) {
//            System.err.println("USAGE: \nargs[0]: port number for h2db server");
//            return;
//        }
//        Server server1 = 
//                Server.createPgServer("-baseDir", "c:\\h2db", "-tcpPort", "9081");
//               System.err.println(server1.getURL());
//        server1.start();
        
//          Server server1 = 
//                Server.createTcpServer("-baseDir", "c:\\h2db", "-tcpPort", "8081");
//               System.err.println(server1.getURL());
//        server1.start();
//      
        
                Server server2 = 
                Server.createTcpServer("-baseDir", "c:\\h2db", "-tcpPort", "9082");
               System.err.println(server2.getURL());
        server2.start();
        System.err.println("even goes here");

        
    }

}
