/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdftable.scripts;

import ie.deri.nlp.assembly.AssemblyThreadWorker;
import ie.deri.nlp.pdfprocess.tcp.servers.PDFServer;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PDFTableServer {

    public static void main(String args[]) throws Exception {
            if (args.length < 2) {
            System.err.println("USAGE: \nargs[0]: setting file"
                    + "\nargs[1] Number of Threads");
            return;
        }

        ie.deri.nlp.settings.Settings settings = new ie.deri.nlp.settings.Settings();
        System.err.println(">> Loading settings ");
        settings.fromXmlFile(args[0]);
        System.err.println(">> Setting up socket interface");
//        new PDFServer(settings.getPDFServerSettings().getPDFServerSetting(), userQueue);
       PDFServer pss = new PDFServer(settings.getPDFServerSettings().getPDFServerSetting());
        System.err.println(">> The requested thread workers is " + args[1]);
        System.err.println(">> Setting up the worker threads");
        for (int i = 0; i < Integer.parseInt(args[1]); i++) {
            System.err.println(" >> Setting up worker #" + i);
            new AssemblyThreadWorker(i, settings, pss);
        }
    }
}
