/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdftable.scripts;

import ie.deri.nlp.assembly.BatchProcess;
import ie.deri.nlp.settings.Settings;

/**
 *
 * @author behqas
 */
public class ProcessBatchModePDF {

    public static void main(String[] args) throws Exception {
        if (args.length < 3) {
            System.err.println("USAGE: \nargs[0]: log file"
                    + "\nargs[1]: setting file"
                    + "\nargs[2] corpus_name that will be the db name as well");
            return;
        }
        Settings settings = new Settings();
        settings.fromXmlFile(args[1]);
        BatchProcess bp = new BatchProcess();
        bp.init(settings, args[2],args[0]);
        bp.process();
        System.err.println("Done batch processing");

    }
}
