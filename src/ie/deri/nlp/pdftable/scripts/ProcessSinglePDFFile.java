/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdftable.scripts;

import ie.deri.nlp.assembly.SingleFileAssembly;
import ie.deri.nlp.settings.Settings;

/**
 *
 * @author behqas
 * 
 */
public class ProcessSinglePDFFile {

    public static void main(String[] sweat) throws Exception {
            if (sweat.length < 3) {
            System.err.println("USAGE: "
                    + "\nargs[0]: setting file"
                    + "\nargs[1] corpus_name that will be the db name as well"
                    + "\nargs[2] path to single pdf file");
            return;
        }
        Settings settings = new Settings();
        settings.fromXmlFile(sweat[0]);
        SingleFileAssembly sfa = new SingleFileAssembly();
        sfa.init(settings, sweat[1]);
        sfa.process(sweat[2]);
    }
}
