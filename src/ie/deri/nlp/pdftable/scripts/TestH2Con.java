/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.pdftable.scripts;

import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.nlp.settings.Settings;
import javax.xml.parsers.ParserConfigurationException;

/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
public class TestH2Con {
    
    public static void main(String[] p) throws Exception{
    Settings s = new Settings();
    s.fromXmlFile("C:\\UNLP-Projects\\Settings\\PDFTable-Settings.xml");
    
           DBConnection userConnection = new DBConnection();
            userConnection.initializeTCPDBUser(
                    s.getDBSettings().getUserDB().getUserH2DB(), "user_x", true);

    
    }
}
