/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.report.html;

import ie.deri.nlp.analyzer.objects.ProcessInfo;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class FileInfoToTable {

    public static String fileInfoListToTable(List<ProcessInfo> fileInfoList) {

        String header = "<table border=\"1\">"
                + "<tr>"
                + "<td>FileID</td>"
                + "<td>PaperID</td>"
                + "<td>Type #</td>"
                + "<td>Token #</td>"
                + "<td>Sentence #</td>"
                + "<td> Paragraph # </td>"
                + "<td> Section # </td>"
                + "<td> Process Time (scnds)</td>"
                + "<td> Index Time (scnds)</td>"
                + "<td> Time Stamp </td>"
                + "</tr>";
        for (ProcessInfo fi : fileInfoList) {
            header += fi.getAsTableRow();
        }
        header += "</table>";
        return header;

    }
}
