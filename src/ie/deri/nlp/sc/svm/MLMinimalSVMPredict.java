/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.sc.svm;





import ie.deri.parsie.mltagger.object.DBFeatureValue;
import libsvm.*;
import java.io.*;
import java.util.*;
import liblinear.FeatureNode;

public class MLMinimalSVMPredict {

    
    private svm_model model;

    public double predict(Set<DBFeatureValue> featureSet) throws IOException {
       
        
     
        
        if (model != null) {
            List<svm_node> xList = new ArrayList<svm_node>();
            for (DBFeatureValue dbfv : featureSet) {
                svm_node x = new svm_node();
                x.index = atoi("" + dbfv.getNumericID());
                x.value = atof("" + dbfv.getValue());
                xList.add(x);
            }
            svm_node[] nodes = new svm_node[xList.size()];
            nodes = xList.toArray(nodes);
            double v;
            v = svm.svm_predict(model, nodes);
//            double[] f = new double[2];
//            svm.svm_predict_values(model, nodes, f);
//            double[] f2 = new double[2];
//            svm.svm_predict_values(model, nodes, f2);
          //  System.err.println("F " + f[0] +f[1] + "F2" + f2[0] +f[1]);
            
            return v;
        } else {
            System.err.println("First load model");
            System.exit(0);
            return 2;
        }
    }

    public void loadML2Model(String modelFile) throws IOException {
        this.model = svm.svm_load_model(modelFile);
    }

    private static double atof(String s) {
        return Double.valueOf(s).doubleValue();
    }

    private static int atoi(String s) {
        return Integer.parseInt(s);
    }
}
