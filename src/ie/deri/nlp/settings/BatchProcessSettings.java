/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.settings;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author behqas
 */
public class BatchProcessSettings {

    private String corpusPath;

    public String getCorpusPath() {
        return corpusPath;
    }

  public void initBatchProcessSettingsFromNode(Node batchProcessNode) {
        NodeList batchProcessNodeList = batchProcessNode.getChildNodes();
        System.err.println(">>> Load batch settings");
        for (int i = 0; i < batchProcessNodeList.getLength(); i++) {
            if ("CorpusPath".equals(batchProcessNodeList.item(i).getNodeName())) {
                corpusPath = batchProcessNodeList.item(i).getTextContent().trim();
            }
        }

    }

}
