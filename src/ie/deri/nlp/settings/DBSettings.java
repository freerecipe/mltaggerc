/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.settings;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DBSettings {

    private UserDB userDB;
    private String mysqlIndexDB;
    private H2Settings h2dbIndexSettings;

    public DBSettings() {
        h2dbIndexSettings = new H2Settings();
    }

    public void dbSettingsFromNode(Node dbSettingsNode) {
        NodeList dbSettingNodeList = dbSettingsNode.getChildNodes();
        System.err.println(">>> Load db settings");
        for (int i = 0; i < dbSettingNodeList.getLength(); i++) {

            if ("MySQLIndexDB".equals(dbSettingNodeList.item(i).getNodeName())) {
                System.err.println("Loading mysql index settings..");
                mysqlIndexDB = dbSettingNodeList.item(i).getTextContent().trim();
            } else 
              if ("UserDB".equals(dbSettingNodeList.item(i).getNodeName())) {
                System.err.println("Loading User DB Settings ...");
                userDB = new UserDB();
                userDB.initFromNode(dbSettingNodeList.item(i));
            } else if ("H2DBIndexSettings".equals(dbSettingNodeList.item(i).getNodeName())) {
                System.err.println("Loading H2DB settings...");
                Node h2Node = dbSettingNodeList.item(i);
                h2dbIndexSettings.initFromNode(h2Node);
            }
        }

    }

    public String getMysqlIndexDB() {
        return mysqlIndexDB;
    }

    public UserDB getUserDB() {
        return userDB;
    }

    public H2Settings getH2DBIndexSettings() {
        return h2dbIndexSettings;
    }
}
