/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.settings;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class EmailSetting {

    String pOPHOST;
    int pOPPORT;
    String pOPSESSIONTYPE;
    String sMTPHOSTNAME;
    int sMTPHOSTPORT;
    String sMTPAUTHUSER;
    String sMTPAUTHPWD;

    public String getPOPHOST() {
        return pOPHOST;
    }

    public int getPOPPORT() {
        return pOPPORT;
    }

    public String getPOPSESSIONTYPE() {
        return pOPSESSIONTYPE;
    }

    public String getSMTPAUTHPWD() {
        return sMTPAUTHPWD;
    }

    public String getSMTPAUTHUSER() {
        return sMTPAUTHUSER;
    }

    public String getSMTPHOSTNAME() {
        return sMTPHOSTNAME;
    }

    public int getSMTPHOSTPORT() {
        return sMTPHOSTPORT;
    }

    public EmailSetting() {
    }

    public void initFromNode(Node emailNode) {
        NodeList emailNodeList = emailNode.getChildNodes();
        for (int i = 0; i < emailNodeList.getLength(); i++) {
            if ("POPHOST".equals(emailNodeList.item(i).getNodeName())) {
                pOPHOST = emailNodeList.item(i).getTextContent().trim();
            } else if ("POPPORT".equals(emailNodeList.item(i).getNodeName())) {
                pOPPORT = Integer.parseInt(emailNodeList.item(i).getTextContent().trim());
            } else if ("POPSESSIONTYPE".equals(emailNodeList.item(i).getNodeName())) {
                pOPSESSIONTYPE = emailNodeList.item(i).getTextContent().trim();
            } else if ("SMTPHOSTNAME".equals(emailNodeList.item(i).getNodeName())) {
                sMTPHOSTNAME = emailNodeList.item(i).getTextContent().trim();
            } else if ("SMTPHOSTPORT".equals(emailNodeList.item(i).getNodeName())) {
                sMTPHOSTPORT = Integer.parseInt(emailNodeList.item(i).getTextContent().trim());
            } else if ("SMTPAUTHUSER".equals(emailNodeList.item(i).getNodeName())) {
                sMTPAUTHUSER = emailNodeList.item(i).getTextContent().trim();
            } else if ("SMTPAUTHPWD".equals(emailNodeList.item(i).getNodeName())) {
                sMTPAUTHPWD = emailNodeList.item(i).getTextContent().trim();
            }

        }

    }
}



