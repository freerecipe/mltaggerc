/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.settings;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author behqas
 */
public class H2Settings {

    private int port;
    private String dataFileRootPath;
    private String serverOptions;
    private String userName;
    private String password;
    private String connectionStringMode;
    private String ipAddress;

    public String getConnectionStringMode() {
        return connectionStringMode;
    }



    public void setDataFileRootPath(String dataFileRootPath) {
        this.dataFileRootPath = dataFileRootPath;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setServerOptions(String serverOptions) {
        this.serverOptions = serverOptions;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDataFileRootPath() {
        return dataFileRootPath;
    }

    public String getPassword() {
        return password;
    }

    public int getPort() {
        return port;
    }

    public String getServerOptions() {
        return serverOptions;
    }

    public String getUserName() {
        return userName;
    }

    public String getIpAddress() {
        return ipAddress;
    }


 

    public void initFromNode(Node h2Node) {
        NodeList h2NodeList = h2Node.getChildNodes();
        for (int i = 0; i < h2NodeList.getLength(); i++) {
            if ("ServerPort".equals(h2NodeList.item(i).getNodeName())) {
                port = Integer.parseInt(h2NodeList.item(i).getTextContent().trim());
            } else if ("DataFileRootPath".equals(h2NodeList.item(i).getNodeName())) {
                dataFileRootPath = h2NodeList.item(i).getTextContent().trim();
            } else if ("ServerOptions".equals(h2NodeList.item(i).getNodeName())) {
                serverOptions = h2NodeList.item(i).getTextContent().trim();
            } else if ("User".equals(h2NodeList.item(i).getNodeName())) {
                userName = h2NodeList.item(i).getTextContent().trim();
            } else if ("Password".equals(h2NodeList.item(i).getNodeName())) {
                password = h2NodeList.item(i).getTextContent().trim();
            } else if ("ConnectionModeString".equals(h2NodeList.item(i).getNodeName())) {
                connectionStringMode = h2NodeList.item(i).getTextContent().trim();
            } else if ("IPAddress".equals(h2NodeList.item(i).getNodeName())) {
                ipAddress = h2NodeList.item(i).getTextContent().trim();
            }
        }

    }
}
