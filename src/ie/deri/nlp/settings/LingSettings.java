/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.settings;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class LingSettings {

    private String stanfordPoSTaggerModel;
    private String openNLPSentencizerModel;
    private MaltParserSetting maltParserSetting;
    private String openNLPCunkerModel;


  

    public MaltParserSetting getMaltParserSetting() {
        return maltParserSetting;
    }

    public String getOpenNLPCunkerModel() {
        return openNLPCunkerModel;
    }

    public String getOpenNLPSentencizerModel() {
        return openNLPSentencizerModel;
    }


    public String getStanfordPoSTaggerModel() {
        return stanfordPoSTaggerModel;
    }

    public LingSettings() {
        stanfordPoSTaggerModel = "";
        openNLPSentencizerModel = "";
        maltParserSetting = new MaltParserSetting();
        openNLPCunkerModel = "";



    }

    public void fromXMLNode(Node lingNode) {
        NodeList lingSettingList = lingNode.getChildNodes();
        for (int idx = 0; idx < lingSettingList.getLength(); idx++) {

            if ("StanfordPoSTaggerModel".equals(lingSettingList.item(idx).getNodeName())) {
                stanfordPoSTaggerModel = lingSettingList.item(idx).getTextContent().trim();
            } else if ("OpenNLPSentencizerModel".equals(lingSettingList.item(idx).getNodeName())) {
                openNLPSentencizerModel = lingSettingList.item(idx).getTextContent().trim();
            } else if ("OpenNLPCunkerModel".equals(lingSettingList.item(idx).getNodeName())) {
                openNLPCunkerModel = lingSettingList.item(idx).getTextContent().trim();
            } else if ("MaltParser".equals(lingSettingList.item(idx).getNodeName())) {
                maltParserSetting.initFromNode(lingSettingList.item(idx));
            }

        }

    }
}



