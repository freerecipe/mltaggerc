/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.settings;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MaltParserSetting {

    private String maltWorkingDir;
    private String maltModelFile;


    public String getMaltModelFile() {
        return maltModelFile;
    }

    public String getMaltWorkingDir() {
        return maltWorkingDir;
    }

    public void setMaltModelFile(String maltModelFile) {
        this.maltModelFile = maltModelFile;
    }

    public void setMaltWorkingDir(String maltWorkingDir) {
        this.maltWorkingDir = maltWorkingDir;
    }



    public void initFromNode(Node maltSettingNode) {
        NodeList maltSettingNodeList = maltSettingNode.getChildNodes();
        for (int i = 0; i < maltSettingNodeList.getLength(); i++) {
            if ("MaltWorkingDir".equals(maltSettingNodeList.item(i).getNodeName())) {
                maltWorkingDir = maltSettingNodeList.item(i).getTextContent().trim();
            } else if ("MaltModelFile".equals(maltSettingNodeList.item(i).getNodeName())) {
                maltModelFile = maltSettingNodeList.item(i).getTextContent().trim();
            } 
        }
    }
}



