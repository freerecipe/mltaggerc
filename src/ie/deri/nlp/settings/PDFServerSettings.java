/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.settings;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PDFServerSettings {

    private ParsCitSetting parsCitSetting;
    private PDFServerSetting pDFServerSetting;
    private PathSetting pathSetting;
    private EmailSetting emailSetting;
    private YahooSettings yahooSettings;

    public PDFServerSettings() {
        parsCitSetting = new ParsCitSetting();
        pDFServerSetting = new PDFServerSetting();
        pathSetting = new PathSetting();
        emailSetting = new EmailSetting();
        yahooSettings = new YahooSettings();
    }

    public EmailSetting getEmailSetting() {
        return emailSetting;
    }

    public ParsCitSetting getParsCitSetting() {
        return parsCitSetting;
    }

    public PathSetting getPathSetting() {
        return pathSetting;
    }

    public PDFServerSetting getPDFServerSetting() {
        return pDFServerSetting;
    }

    public YahooSettings getYahooSettings() {
        return yahooSettings;
    }

    public void initFromNode(Node pdfServerSetting) {
        NodeList pdfServerSettingList = pdfServerSetting.getChildNodes();
        for (int i = 0; i < pdfServerSettingList.getLength(); i++) {
            if ("ParsCitSetting".equals(pdfServerSettingList.item(i).getNodeName())) {
                parsCitSetting.initFromNode(pdfServerSettingList.item(i));
            } else if ("PDFServerSetting".equals(pdfServerSettingList.item(i).getNodeName())) {
                pDFServerSetting.initFromNode(pdfServerSettingList.item(i));
            } else if ("PathSetting".equals(pdfServerSettingList.item(i).getNodeName())) {
                pathSetting.initFromNode(pdfServerSettingList.item(i));
            } else if ("EmailSetting".equals(pdfServerSettingList.item(i).getNodeName())) {
                emailSetting.initFromNode(pdfServerSettingList.item(i));
            } else if ("YahooSettings".equals(pdfServerSettingList.item(i).getNodeName())) {
                yahooSettings.initFromNode(pdfServerSettingList.item(i));
            }
        }
    }
}
