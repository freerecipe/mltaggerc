/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.settings;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ParsCitSetting {

    private String parscitIPAddress;
    private int parscitBindPort;

    public int getParscitBindPort() {
        return parscitBindPort;
    }

    public String getParscitIPAddress() {
        return parscitIPAddress;
    }

    public void initFromNode(Node parsCitNode) {
        NodeList parsCitNodeList = parsCitNode.getChildNodes();

        for (int i = 0; i < parsCitNodeList.getLength(); i++) {
            if ("ParscitIPAddress".equals(parsCitNodeList.item(i).getNodeName())) {
                parscitIPAddress = parsCitNodeList.item(i).getTextContent().trim();
            } else if ("ParscitBindPort".equals(parsCitNodeList.item(i).getNodeName())) {
                parscitBindPort = Integer.parseInt(
                        parsCitNodeList.item(i).getTextContent().trim());
            }
        }
    }
}



