/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.settings;

import java.io.File;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PathSetting {

    private String rootDirectory;
    private String pDFRepository;
    private String textRepository;
    private String imgRepository;
    private String parsCitRepository;
    private String xMLRepository;

    public String getImgRepository() {
        return rootDirectory + File.separatorChar +imgRepository;
    }

    public String getParsCitRepository() {
        return rootDirectory + File.separatorChar + parsCitRepository;
    }

    public String getRootDirectory() {
        return rootDirectory;
    }

    public String getTextRepository() {
        return rootDirectory + File.separatorChar +textRepository;
    }

    public String getPDFRepository() {
        return rootDirectory + File.separatorChar +pDFRepository;
    }

    public String getXMLRepository() {
        return rootDirectory + File.separatorChar + xMLRepository;
    }

    public void initFromNode(Node pathSettingNode) {

        NodeList pathNodeList = pathSettingNode.getChildNodes();
        for (int i = 0; i < pathNodeList.getLength(); i++) {
            if ("RootDirectory".equals(pathNodeList.item(i).getNodeName())) {
                rootDirectory = pathNodeList.item(i).getTextContent().trim();
            } else if ("PDFRepository".equals(pathNodeList.item(i).getNodeName())) {
                pDFRepository = pathNodeList.item(i).getTextContent().trim();
            } else if ("TextRepository".equals(pathNodeList.item(i).getNodeName())) {
                textRepository = pathNodeList.item(i).getTextContent().trim();
            } else if ("ImgRepository".equals(pathNodeList.item(i).getNodeName())) {
                imgRepository = pathNodeList.item(i).getTextContent().trim();
            } else if ("ParsCitRepository".equals(pathNodeList.item(i).getNodeName())) {
                parsCitRepository = pathNodeList.item(i).getTextContent().trim();
            } else if ("XMLRepository".equals(pathNodeList.item(i).getNodeName())) {
                xMLRepository = pathNodeList.item(i).getTextContent().trim();
            }

        }


    }
}



