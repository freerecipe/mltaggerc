/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.settings;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Settings {

    private LingSettings lingSettings;
    private DBSettings dBSettings;
    private PDFServerSettings pDFServerSettings;
    private BatchProcessSettings batchProcessSettings;
    protected Document doc;

    public Settings() {
        lingSettings = new LingSettings();
        dBSettings = new DBSettings();
        pDFServerSettings = new PDFServerSettings();
        batchProcessSettings = new BatchProcessSettings();
    }

    public void fromXmlFile(String filePath) throws ParserConfigurationException, SAXException, IOException {

        System.err.println("Load setting from " + filePath);
        File file = new File(filePath);
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

        doc = builder.parse(file);
        // StyleTable Node
        
        NodeList dbSettingNode = doc.getElementsByTagName("DBSettings");
        NodeList lingSettingsNode = doc.getElementsByTagName("LingSettings");
        NodeList pDFServerSettingsNode = doc.getElementsByTagName("PDFServerSettings");
        NodeList batchProcessNodeList = doc.getElementsByTagName("BatchProcessSettings");


        if (dBSettings != null && dbSettingNode.item(0)!=null) {
            dBSettings.dbSettingsFromNode(dbSettingNode.item(0));
        } else {
            System.err.println("DBSettting not found");
        }
        lingSettings.fromXMLNode(lingSettingsNode.item(0));
        if (pDFServerSettingsNode.item(0) != null && pDFServerSettingsNode.item(0) != null) {
            pDFServerSettings.initFromNode(pDFServerSettingsNode.item(0));
        } else {
            System.err.println("PDF Server Setting not found!");
        }
        if(batchProcessSettings!=null && batchProcessNodeList.item(0)!=null){
        batchProcessSettings.initBatchProcessSettingsFromNode(batchProcessNodeList.item(0));
        } else {
            System.err.println("Could not find batch setting node!");
        }

    }

    public LingSettings getLingSettings() {
        return lingSettings;
    }

    public DBSettings getDBSettings() {
        return dBSettings;
    }

    public PDFServerSettings getPDFServerSettings() {
        return pDFServerSettings;
    }

    public BatchProcessSettings getBatchProcessSettings() {
        return batchProcessSettings;
    }

    public Document getDoc() {
        return doc;
    }
}
