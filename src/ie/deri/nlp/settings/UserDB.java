/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.nlp.settings;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
public class UserDB {
    
     
    private String mysqlIndexDB;
    private H2Settings h2dbIndexSettings;

    public UserDB() {
        h2dbIndexSettings  = new H2Settings();
    }

    
        
    
    public void initFromNode(Node dbSettingsNode) {
        NodeList dbSettingNodeList = dbSettingsNode.getChildNodes();
        System.err.println(">>> Load db settings");
        for (int i = 0; i < dbSettingNodeList.getLength(); i++) {
            if ("MySQLDB".equals(dbSettingNodeList.item(i).getNodeName())) {
                System.err.println("Loading mysql index settings..");
                mysqlIndexDB = dbSettingNodeList.item(i).getTextContent().trim();
            } else if ("H2DB".equals(dbSettingNodeList.item(i).getNodeName())) {
                System.err.println("Loading H2DB settings...");
                Node h2Node = dbSettingNodeList.item(i);
                h2dbIndexSettings.initFromNode(h2Node);
            }
        }

    }

    public String getMysqlIndexDB() {
        return mysqlIndexDB;
    }

    
    public H2Settings getUserH2DB() {
        return h2dbIndexSettings;
    }


    
}
