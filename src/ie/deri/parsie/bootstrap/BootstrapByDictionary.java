/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.bootstrap;

import ie.deri.nlp.misc.UID;
import ie.deri.parsie.mltagger.db.MLTaggerIndex;
import ie.deri.parsie.mltagger.db.MLTaggerRetrieve;
import ie.deri.parsie.mltagger.object.BootstrapTerm;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
public class BootstrapByDictionary {
    

    public Set<BootstrapTerm> bootStrap(String filename, boolean negativeExample) throws FileNotFoundException, IOException {
        Set<BootstrapTerm> btList = new HashSet<BootstrapTerm>();
        FileInputStream fstream = new FileInputStream(filename);
        // Get the object of DataInputStream
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        //Read File Line By Line
        while ((strLine = br.readLine()) != null) {
            if (strLine.trim().length() > 0) {
                BootstrapTerm bt = new BootstrapTerm();
                bt.setBootstrapString(strLine.trim());
                bt.setPositiveExample(negativeExample);
                bt.setId(UID.getUID());
                if (!btList.contains(bt)) {
                    btList.add(bt);
                }

            }
        }
        //Close the input stream
        in.close();
        return btList;
    }
}
