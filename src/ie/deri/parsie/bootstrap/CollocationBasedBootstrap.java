/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.bootstrap;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import ie.deri.parsie.mltagger.object.TTRefineStopWord;
import ie.deri.parsie.mltagger.object.BootstrapTerm;
import ie.deri.parsie.mltagger.object.TechnologyTerm;


import ie.deri.nlp.db.h2.DBPaperRetrieve;
import ie.deri.nlp.db.objects.DBLexeme;
import ie.deri.nlp.db.objects.DBSentence;
import ie.deri.nlp.db.retrieve.util.PhraseSearch;
import ie.deri.nlp.misc.UID;
import ie.deri.parsie.mltagger.db.MLTaggerIndex;
import ie.deri.parsie.mltagger.db.MLTaggerRetrieve;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class CollocationBasedBootstrap {

    private MLTaggerRetrieve dbr;
    private MLTaggerIndex dba;
//    private WebSearch ws;

    public CollocationBasedBootstrap(MLTaggerRetrieve dbr, MLTaggerIndex dba) {
        this.dba = dba;
        this.dbr = dbr;
//        this.ws = new WebSearch();
//        ws.setAppKey(ys.getAppID());
    }

    public Set<BootstrapTerm> bootStrap(String bootStrapLemma) throws SQLException {
        long currentTime = System.currentTimeMillis();
        Set<BootstrapTerm> refinedTechTerm = new HashSet<BootstrapTerm>();
        List<DBLexeme> dbl = dbr.retLexemeByLemma(bootStrapLemma);
        int countOfTerms = 0;
        for (DBLexeme db : dbl) {
            java.sql.ResultSet rs = dbr.retSentenceContainLexeme(db.getId());
            while (rs.next()) {
                double sentenceID = rs.getDouble("sentence_id");
                DBSentence dbs = dbr.retSentenceByID(sentenceID);
                for (int i = 0; i < dbs.getDbLexemeList().size(); i++) {
                    if (bootStrapLemma.equals(dbs.getDbLexemeList().get(i).getLemma())) {
                        TechnologyTerm ctt = new TechnologyTerm();
                        countOfTerms++;
                        ctt.setOriginSentenceID(sentenceID);
                        for (int j = i - 1; j > 0; j--) {
                            String pos = dbs.getDbLexemeList().get(j).getPos();
                            String word = dbs.getDbLexemeList().get(j).toString();
                            if (pos.equals("NN")
                                    || pos.equals("NP")
                                    || pos.equals("NPS")
                                    || pos.equals("NNS")
                                    || pos.equals("NNP")
                                    || pos.equals("NNPS")
                                    || pos.equals("VBG")
                                    || pos.equals("VBN")
                                    || pos.equals("JJ")
                                    || word.equals("of")
                                    
//                                    || pos.equals("IN")
                                    || pos.equals("CC")
                                    || pos.equals("SYM")
                                    || pos.equals("TO")) 
                            {
                                ctt.pushDbLexeme(dbs.getDbLexemeList().get(j));
                                // System.err.println(dbs.getDbLexemeList().get(j).getWord() + " ");
                            } else {
                                if (j > 0) {
                                    ctt.setLexemeBefore(dbs.getDbLexemeList().get(j));
                                }
                                break;
                            }
                        }
                        if (!ctt.getDbLexemeList().isEmpty()) {
                            TechnologyTerm ttr = refineTerm(ctt);
                            if (ttr != null) {
                                BootstrapTerm bt = new BootstrapTerm();
                                bt.setId(UID.getUID());
                                bt.setPositiveExample(false);
                                String term = ttr.toPlainText();
                                bt.setLength(ttr.getDbLexemeList().size());
                                // new: if the length is 1 then
                                // also add the bootstrapping word to the term
                                bt.setBootstrapString(term);
                                if(ttr.getLength()==1){
                                bt.setBootstrapString(term + " " + bootStrapLemma);
                                }
                                // ac- stands for auto-collocation
                                bt.setSource("ac-" + bootStrapLemma);
                                bt.setJackarValue(generateJackardValue(dbr, term));
                                // commented to speed up the process
//                                try{
//                                ws.search("\"" + term +"\""+ " filetype:pdf");
//                                bt.setSearchHits(ws.getTotalResults());
//                                } catch (Exception e) {
                                    bt.setSearchHits(0);
//                                }

//                                try {
//                                    ws.search("\"" + term + " " + bootStrapLemma + "\"" + " filetype:pdf");
//                                    System.err.println("Searching yahoo for " + term);
//                                    bt.setCollocationYahooHits(ws.getTotalResults());
//                                } catch (Exception e) {
                                    bt.setCollocationYahooHits(0);
//                                }
                                dba.indexBootstrapTerm(bt);
                                if (!refinedTechTerm.contains(bt)) {
                                    refinedTechTerm.add(bt);
                                }

                            }
                        }
                    }
                }
            }
        }
        System.err.println("Number of boot-strapped terms by " + bootStrapLemma + ": " + countOfTerms);
                long now = System.currentTimeMillis();
        System.err.println("Bootstrapping time in milisec: " + (now-currentTime));
        return refinedTechTerm;
    }

    public TechnologyTerm refineTerm(TechnologyTerm tt) {
        if (tt.getDbLexemeList().isEmpty()) {
            return null;
        } else if (TTRefineStopWord.isStopWord(tt.getDbLexemeList().get(0).getWord())) {
            tt.getDbLexemeList().remove(0);
            return (refineTerm(tt));
        } else if ("VBG".equals(tt.getDbLexemeList().get(0).getPos())) {
            if ("VB".equals(tt.getLexemeBefore().getPos())) {
                tt.getDbLexemeList().remove(0);
                return (refineTerm(tt));
            } else if ("VBD".equals(tt.getLexemeBefore().getPos())) {
                tt.getDbLexemeList().remove(0);
                return (refineTerm(tt));
            } else if ("IN".equals(tt.getLexemeBefore().getPos())) {
                tt.getDbLexemeList().remove(0);
                return (refineTerm(tt));
            } else {
                tt.setLexemeBefore(null);
                return tt;
            }
        } else {
            tt.setLexemeBefore(null);
            return tt;
        }
    }

    private double generateJackardValue(DBPaperRetrieve dbr, String term) throws SQLException {
        PhraseSearch ps = new PhraseSearch(dbr);
        int freq = ps.exactSearchForPhraseString(term).size();
        String[] tokens = term.split(" ");
        int tokenFreq = 0;
        for (int i = 0; i < tokens.length; i++) {
            List<DBLexeme> dblList = dbr.retLexeme(tokens[i]);
            for (int j = 0; j < dblList.size(); j++) {
                tokenFreq += dblList.get(j).getFrequency();
            }
        }
        double jackard = (double) freq / tokenFreq + 1;
        return jackard;
    }


    

}
