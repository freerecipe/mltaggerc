/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.bootstrap;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import ie.deri.parsie.mltagger.object.BootstrapInstance;
import ie.deri.parsie.mltagger.object.BootstrapTerm;
import ie.deri.nlp.db.h2.DBPaperAssertion;
import ie.deri.nlp.db.h2.DBPaperRetrieve;
import ie.deri.nlp.db.retrieve.util.PhraseSearch;
import ie.deri.nlp.db.retrieve.util.PhraseSearchResult;
import ie.deri.nlp.misc.UID;
import ie.deri.parsie.mltagger.db.MLTaggerIndex;
import ie.deri.parsie.mltagger.db.MLTaggerRetrieve;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class PopulateBootstap {
    private MLTaggerRetrieve dbr;
    private MLTaggerIndex dba;
    
    

    public PopulateBootstap(MLTaggerIndex dba, MLTaggerRetrieve dbr ) {
        this.dba = dba;
        this.dbr = dbr;
    }

    public void setDba(MLTaggerIndex dba) {
        this.dba = dba;
    }

    public void setDbr(MLTaggerRetrieve dbr) {
        this.dbr = dbr;
    }

    public DBPaperAssertion getDba() {
        return dba;
    }

    public DBPaperRetrieve getDbr() {
        return dbr;
    }



    public void populateBootstrappedConcepts() throws SQLException {
        List<BootstrapTerm> bootStrapList = dbr.retBootstrapTerm();
        System.err.println(">>> " + bootStrapList.size() + " bootsrtap terms were loaded for pbt");
        for (BootstrapTerm bt : bootStrapList) {
            List<BootstrapInstance> btInstList = getBootstrapInstance(bt);
            for (BootstrapInstance bti: btInstList) {
                dba.indexBootstrapInstance(bti);
            }
        }
    }


    public List<BootstrapInstance> getBootstrapInstance(BootstrapTerm bootstrap) throws SQLException {
        List<BootstrapInstance> btInsList = new ArrayList<BootstrapInstance>();
        PhraseSearch ps = new PhraseSearch(dbr);
        List<PhraseSearchResult> psrList = ps.exactSearchForPhraseString(bootstrap.getBootstrapString());
        for (PhraseSearchResult psr : psrList) {
            BootstrapInstance bti = new BootstrapInstance(bootstrap.getId(), psr);
            bti.setInstanceID(UID.getUID());
            btInsList.add(bti);

        }
        return btInsList;
    }



}
