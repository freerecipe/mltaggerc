/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger;

import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.nlp.intrface.objects.User;
import ie.deri.nlp.settings.H2Settings;
import ie.deri.nlp.settings.Settings;
import ie.deri.parsie.bootstrap.BootstrapByDictionary;
import ie.deri.parsie.bootstrap.PopulateBootstap;
import ie.deri.parsie.mltagger.db.MLTaggerIndex;
import ie.deri.parsie.mltagger.db.MLTaggerRetrieve;
import ie.deri.parsie.mltagger.db.MLTaggerTableCreate;
import ie.deri.parsie.mltagger.object.BootstrapTerm;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Set;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
public class CiscoBootstrap {

    public static void main(String[] s) throws ParserConfigurationException, SAXException, IOException, SQLException, ClassNotFoundException {
        
        String settingPath = "C:\\UNLP-Projects\\Settings\\cisco.xml";
        String dbName = "cisco_email";
        String dictionaryPath = "C:\\cisco\\scml-tagger\\white_list.txt";
        String negativeExample = "C:\\cisco\\scml-tagger\\negative_examples.txt";
        System.err.println("This is delivery for June 29");

        Settings jsetting = new Settings();
        jsetting.fromXmlFile(settingPath);
        bootStrap(dbName,
            jsetting.getDBSettings().getH2DBIndexSettings(), dictionaryPath, true);
        
                bootStrap(dbName,
           jsetting.getDBSettings().getH2DBIndexSettings(), negativeExample, false);

        
        DBConnection dbc = new DBConnection();
        dbc.initializeEmbeddedDBPaper(
                jsetting.getDBSettings().getH2DBIndexSettings(),
                dbName, false);
        MLTaggerRetrieve mltr = new MLTaggerRetrieve();
        mltr.init(dbc.getCon());
        

        // this should be developed 

        System.err.println("Done bootstrapping!");
        MLTaggerIndex dba = new MLTaggerIndex(dbc.getCon());
        
        PopulateBootstap pb = new PopulateBootstap(dba, mltr);
        pb.populateBootstrappedConcepts();
        dbc.close();

    }

    public static void bootStrap(String dbName,
            H2Settings h2Settings, String dictionaryPath, boolean state) throws ClassNotFoundException, SQLException, FileNotFoundException, IOException {
        DBConnection dbc = new DBConnection();
        dbc.initializeEmbeddedDBPaper(h2Settings, dbName, false);
        MLTaggerRetrieve dbr = new MLTaggerRetrieve();
        dbr.init(dbc.getCon());
        MLTaggerIndex dba = new MLTaggerIndex(dbc.getCon());
        MLTaggerTableCreate ct =
                new MLTaggerTableCreate();
        ct.cerateTTRecognitionTable(dbc.getCon());
        System.err.println("Start bootstrapping");
        BootstrapByDictionary bbd =
                new BootstrapByDictionary();
        Set<BootstrapTerm> btSet = bbd.bootStrap(dictionaryPath, state );
        System.err.println("boot done for");
        Iterator<BootstrapTerm> ibt = btSet.iterator();
        while (ibt.hasNext()) {
            dba.indexBootstrapTerm(ibt.next());
        }
        System.err.println("Done bootstrapping!");
        System.err.println("Locate terms in emails");
        PopulateBootstap pb = new PopulateBootstap(dba, dbr);
        pb.populateBootstrappedConcepts();
        dbc.close();

    }
    

}
