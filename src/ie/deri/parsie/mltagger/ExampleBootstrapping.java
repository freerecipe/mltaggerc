/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger;



import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.nlp.intrface.objects.User;
import ie.deri.nlp.settings.H2Settings;
import ie.deri.nlp.settings.Settings;
import ie.deri.parsie.bootstrap.CollocationBasedBootstrap;
import ie.deri.parsie.bootstrap.PopulateBootstap;
import ie.deri.parsie.mltagger.db.MLTaggerIndex;
import ie.deri.parsie.mltagger.db.MLTaggerRetrieve;
import ie.deri.parsie.mltagger.db.MLTaggerTableCreate;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


public class ExampleBootstrapping {

    public static void main(String[] example) throws Exception {
        Settings setting = new Settings();
        setting.fromXmlFile("C:\\UNLP-Projects\\Settings\\Settings.xml");
        DBConnection dbc = new DBConnection();
        User user = new User();
        user.setUserID("acl_arc_a");
         String[] bootTerm = { "technology"
                 //,  // "system", "tool", "framework",
               //   "technique" 
    };
         
         
         
bootStrapByPDFTable(user, setting.getDBSettings().getH2DBIndexSettings(), bootTerm);
    }
    
    public static void bootStrapByPDFTable(User user, 
             H2Settings h2Settings, String[] bootstrapWords) throws ClassNotFoundException, SQLException{
        
        DBConnection dbc = new DBConnection();
        dbc.initializeEmbeddedDBPaper(h2Settings, user.getUserID(), false);
        MLTaggerRetrieve dbr = new MLTaggerRetrieve();
        dbr.init(dbc.getCon());
        MLTaggerIndex dba = new MLTaggerIndex(dbc.getCon());
        MLTaggerTableCreate ct =
                new MLTaggerTableCreate();
        ct.cerateTTRecognitionTable(dbc.getCon());
        System.err.println("Start bootstrapping" );
        CollocationBasedBootstrap cbt =
                new CollocationBasedBootstrap(
                dbr, dba);
        System.err.println("boot done for");
        for(String signWord: bootstrapWords){
            cbt.bootStrap(signWord);
            
            System.err.print(" " + signWord);
        }
        System.err.println("Done bootstrapping!");
        PopulateBootstap pb = new PopulateBootstap(dba, dbr);
        pb.populateBootstrappedConcepts();
        dbc.close();
        
        
    }
    
}
