/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger;

import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.nlp.intrface.objects.User;
import ie.deri.nlp.settings.Settings;
import ie.deri.parsie.mltagger.db.MLTaggerIndex;
import ie.deri.parsie.mltagger.db.MLTaggerRetrieve;
import ie.deri.parsie.mltagger.fe.TTIFeatureExtraction;


/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class ExampleFeatExtraction {

    public static void main(String[] ars) throws Exception{
//        String section = "sepid_arc_I";
//                String dst = "mysql~>jdbc:mysql://127.0.0.1:3306/~>"
//                + section + "~>root~>xxx";
//

        Settings setting = new Settings();
        setting.fromXmlFile("C:\\UNLP-Projects\\Settings\\Settings.xml");
        DBConnection dbc = new DBConnection();
        User user = new User();
//        user.setUserID("acl_arc_a");
//        String modelVersion = "tech";
        
                user.setUserID("cisco_email");
        String modelVersion = "cisco_email";

        setting.getDBSettings().getH2DBIndexSettings().setDataFileRootPath("C:\\h2db");
        
        dbc.initializeEmbeddedDBPaper(setting.getDBSettings().getH2DBIndexSettings(), user.getUserID(), false);
        
//        System.err.println(setting.getDBSettings().getH2Settings().getDataFileRootPath());

//        dbc.initMySQL(setting.getDBSettings(), user);

        MLTaggerRetrieve dbr = new MLTaggerRetrieve();
        dbr.init(dbc.getCon());
        MLTaggerIndex dba = new MLTaggerIndex(dbc.getCon());

        TTIFeatureExtraction ttit = new TTIFeatureExtraction(dba, dbr);
        ttit.generateFeatures();
        ttit.storeDBDependencyAsFeat("C:\\MLTagger\\DBDependency\\"
                +user.getUserID()+modelVersion+".dbdep");
        ttit.storeDBLExemeAsFeat("C:\\MLTagger\\DBlexeme\\"
                +user.getUserID()+modelVersion+".dblexeme");

        dbc.close();
    }
}
