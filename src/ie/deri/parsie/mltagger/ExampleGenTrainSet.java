/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger;

import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.nlp.intrface.objects.User;
import ie.deri.nlp.liblinear.LiblinearTrain;
import ie.deri.nlp.settings.Settings;
import ie.deri.parsie.mltagger.db.MLTaggerRetrieve;
import ie.deri.parsie.mltagger.fe.GenTrainingSet;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class ExampleGenTrainSet {
  public static void main(String[] s) throws Exception{
        Settings setting = new Settings();
        setting.fromXmlFile("C:\\UNLP-Projects\\Settings\\Settings.xml");
        DBConnection dbc = new DBConnection();
        User user = new User();
        user.setUserID("cisco_email");
        dbc.initializeEmbeddedDBPaper(setting.getDBSettings().getH2DBIndexSettings(), user.getUserID(), false);
        MLTaggerRetrieve dbr = new MLTaggerRetrieve();
        dbr.init(dbc.getCon());
        GenTrainingSet gt = new GenTrainingSet();
        gt.init(dbr);
        
//        Map<String, Integer> imap = 
//        gt.loadFeatureTTIMAPFromFile("C:\\TTIdentification\\Train\\FVM\\a_section.fvm");
//        gt.setSdhFeatureTTIMap(imap);

        gt.loadDBOFeatureTTIMapFromDB();
        gt.storeDBFeatureMap("C:\\MLTagger\\FVM\\cisco_email.fvm");
        gt.genTrainingSetForSentence();
  //     gt.
//        dumpFoldedTrainingSet("c:\\TTIdentification\\Train\\acl_arc_a_system");
        String trainsetPath = "c:\\MLTagger\\cisco_email.train";
        String modelPath = "C:\\MLTagger\\Model\\cisco_email.model";
        gt.dumpTrainingSet(trainsetPath);
        LiblinearTrain.minimalTrain(trainsetPath, modelPath);
//        gt.dumpTrainingSetForExperiment1("c:\\unlp-projects\\acl_arc_n");
//        gt.storeDBFeatureMapForExperiment1("c:\\unlp-projects\\acl_arc_n-feature.data");
        
        
    }
}

