/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger;

import ie.deri.nlp.db.h2.DBConnection;
import ie.deri.nlp.db.retrieve.util.PhraseSearch;
import ie.deri.nlp.intrface.objects.User;
import ie.deri.nlp.settings.H2Settings;
import ie.deri.nlp.settings.Settings;
import ie.deri.parsie.mltagger.db.MLTaggerRetrieve;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
public class PhraseSearch2 {

    public static void main(String[] example) throws Exception {
        Settings setting = new Settings();
        setting.fromXmlFile("C:\\UNLP-Projects\\Settings\\Settings.xml");
        DBConnection dbc = new DBConnection();
        User user = new User();
        user.setUserID("acl_arc_a");
        String[] phraseSearch = {"this is", "based on", "using pos tagger"};
        //,  // "system", "tool", "framework",
        //   "technique" 

        searchQuery(user, setting.getDBSettings().getH2DBIndexSettings(), phraseSearch);
    }

    public static void searchQuery(User user,
            H2Settings h2Settings, String[] phrase) throws ClassNotFoundException, SQLException {
        DBConnection dbc = new DBConnection();
        dbc.initializeEmbeddedDBPaper(h2Settings, user.getUserID(), false);
        MLTaggerRetrieve dbr = new MLTaggerRetrieve();
        dbr.init(dbc.getCon());
        PhraseSearch phraseSearch = new PhraseSearch(dbr);
        int sizeall = 0;
        long before = (new Date()).getTime();
        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < phrase.length; i++) {
                String string = phrase[i];
                sizeall += phraseSearch.exactSearchForPhraseString(string).size();

            }
        }

        long after = (new Date()).getTime();

        long time = after - before;

        System.err.println(sizeall);
        System.err.println(time);

        dbc.close();

    }
}
