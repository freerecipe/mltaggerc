/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger;

import ie.deri.parsie.mltagger.assembly.ServerMultiplelModelMode;
import ie.deri.parsie.mltagger.assembly.SingleClassServerMultiplelModelMode;
import ie.deri.parsie.mltagger.server.TTIServer;
import ie.deri.parsie.mltagger.setting.Settings;


/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class SCMLMainMultiThreadServer {

    /**
     * The main predicate to call and set up a server at a tcp ip port that is defined in a setting xml file. the package depends on several other packages, including Stanford Java Libraries, Malt Parser, ParsCit, Apache OpenNLP and PDFBox, in addition to another package from Unit for Natural Language Processing at DERI, NUI Galway as part of research grant from Science Foundation Ireland under Grant Lion/2.
     * @param args
     * @throws Exception 
     */
    public static void main(String args[]) throws Exception {

        Settings settings = new Settings();
        System.err.println(">> Loading settings ");
        settings.fromXmlFile(args[0]);
        System.err.println(">> Setting up socket interface");
//        new PDFServer(settings.getPDFServerSettings().getPDFServerSetting(), userQueue);

        
        
        TTIServer ttiServer = new TTIServer(settings.getTaggerListSetting().getPortNumber());
        System.err.println(">> The requested thread workers is " + args[1]);
        System.err.println(">> Setting up the worker threads");
        for (int i = 0; i < Integer.parseInt(args[1]); i++) {
            System.err.println(" >> Setting up worker #" + i);
            new SingleClassServerMultiplelModelMode(i, settings, ttiServer);
        }
    }
}
