/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger;

import ie.deri.parsie.mltagger.object.SentenceTags;
import ie.deri.parsie.mltagger.object.TTIResultSet;
import ie.deri.parsie.mltagger.assembly.MultipleModelEmbeddedMode;
import ie.deri.parsie.mltagger.setting.Settings;
import javax.xml.parsers.ParserConfigurationException;

/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
public class SampleEmbeddedJava {
    public static void main(String[] sugar) throws Exception, ParserConfigurationException{
        
                Settings settings = new Settings();
        System.err.println(">> Loading settings ");
        settings.fromXmlFile(sugar[0]);
        System.err.println(">> Setting up the worker object assembly");
       MultipleModelEmbeddedMode sma = new MultipleModelEmbeddedMode(0, settings);
       
       sma.processParagraph("Protégé with the SKOSEd plugin but it's a bit overkill and Protégé is an ontology editor and not great for just browsing the hierarchy. Thanks");
        for (int i = 0;i<sma.getAnalSenList().size(); i++) {
            for (int j = 0; j < sugar.length; j++) {
                String string = sugar[j];
                
            }
            
        }
       for( SentenceTags s: sma.getSentenceTagsList()){
           for(TTIResultSet x:s.getTagSet()){
               System.err.println("Tag #" + x.getTaggerID());
           }
           
       };

        
        
    }
}
