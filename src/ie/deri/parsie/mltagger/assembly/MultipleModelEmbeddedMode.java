/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.assembly;

import ie.deri.nlp.analyzer.objects.BPParagraph;
import ie.deri.nlp.analyzer.objects.BPSentence;
import ie.deri.nlp.db.objects.DBSentence;
import ie.deri.nlp.linguistic.analyzer.Pipeline6;
import ie.deri.parsie.mltagger.fe.FeatureExtraction;
import ie.deri.parsie.mltagger.object.FeatureRecordPredict;
import ie.deri.nlp.liblinear.LibLinearAnswer;
import ie.deri.nlp.liblinear.ModelFilesContainer;
import ie.deri.nlp.mltagger.result.filter.PhraseBoundaryFilter;
import ie.deri.parsie.mltagger.object.SentenceTags;
import ie.deri.parsie.mltagger.object.TTIResult;
import ie.deri.parsie.mltagger.object.TTIResultSet;
import ie.deri.parsie.mltagger.server.LingAnalysis;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ie.deri.parsie.mltagger.setting.Settings;
import ie.deri.parsie.mltagger.setting.TaggerSetting;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class MultipleModelEmbeddedMode {

    private List<ModelFilesContainer> lmfcList;
    private Pipeline6 pl6;
//    private Settings settings;
    private FeatureExtraction fe;
    private List<BPSentence> analSenList;
    private List<SentenceTags> sentenceTagsList;
    private LingAnalysis linga;
    private LibLinearAnswer libAnsw;
    private BPParagraph bparagph;
    private PhraseBoundaryFilter phraseBoundFilter;
    
    public MultipleModelEmbeddedMode(int threadIDNumber,
            Settings settings) throws ClassNotFoundException, SQLException, IOException, InterruptedException, Exception {
        lmfcList = new ArrayList<ModelFilesContainer>();
        for (TaggerSetting ts : settings.getTaggerListSetting().getTaggerSettingList()) {
            ModelFilesContainer mfc = new ModelFilesContainer();
            mfc.init(ts);
            lmfcList.add(mfc);
        }
        pl6 = new Pipeline6(threadIDNumber, settings.getLingSettings());
        pl6.processAsSentence("Let's get it up");
        linga = new LingAnalysis();
        linga.init(pl6);
        fe = new FeatureExtraction();
    }

    public void processParagraph(String paragraphString) {
        analSenList = linga.analyzeParagraph(paragraphString);
        bparagph = new BPParagraph();
        bparagph.setSentenceList(analSenList);
        sentenceTagsList = new ArrayList<SentenceTags>();
        int sentencePosition = 0;
        for (BPSentence analSent : analSenList) {
                                phraseBoundFilter = new PhraseBoundaryFilter(analSent);
            SentenceTags sentTags = new SentenceTags();
            sentTags.setSentencePosition(sentencePosition);
            sentencePosition++;
            sentTags.setAnalyzedSentence(analSent);
            for (ModelFilesContainer mfc : lmfcList) {
                fe = new FeatureExtraction();
                DBSentence dbs = linga.analyzedSentToFeatDBSentence(analSent, mfc);
                fe.generateFeatures(dbs, mfc.getFeatureMap());
                TTIResultSet ttrts = new TTIResultSet();
                ttrts.setTaggerID(mfc.getTaggerID());
                List<TTIResult> ttiList = new ArrayList<TTIResult>();
                for (FeatureRecordPredict frp : fe.getSentenceFeatRecordList()) {
                    try {

                        libAnsw = mfc.getLibModel().predict(frp.getDbFeatureValueList(), false);
                        // there are plenty of room to make it much more clear... bad bad code!
                        // Ok it was a fast implementation

                        if (//true
                                libAnsw.getPredictLabel() == 1
                                ||
                                libAnsw.getDescionValue() > -0.30
                                ) {
                            String termString = "";
                            for (int i = frp.getStartPosition(); i < frp.getEndPosition() + 1; i++) {
                                termString += dbs.getDbLexemeList().get(i).getWord() + " ";
                            }
                            if (phraseBoundFilter.isAcceptedByOpenNLP(frp.getStartPosition(), frp.getEndPosition())) {
                            TTIResult ttr = new TTIResult();
                            ttr.setStartPosition(frp.getStartPosition());
                            ttr.setEndPosition(frp.getEndPosition());
                            ttr.setConfidenceValue(libAnsw.getDescionValue());
                            ttr.setTermString(termString.trim());
                            ttr.setTaggerID(mfc.getTaggerID());
                            ttiList.add(ttr);}
                        } else {
                          // you can do furthur classification
                        
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(MultipleModelEmbeddedMode.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                ttrts.setTtiResultList(ttiList);
                sentTags.addTagSet(ttrts);
            }
            sentenceTagsList.add(sentTags);
        }
    }

    
    
    public List<BPSentence> getAnalSenList() {
        return analSenList;
    }

    public List<SentenceTags> getSentenceTagsList() {
        return sentenceTagsList;
    }

    public LingAnalysis getLinga() {
        return linga;
    }
    
    public BPParagraph getBParagraph(){
        return bparagph;
    }
}
