/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.assembly;

import ie.deri.nlp.analyzer.objects.BPSentence;
import ie.deri.nlp.analyzer.objects.Chunk;
import ie.deri.nlp.db.objects.DBSentence;
import ie.deri.nlp.linguistic.analyzer.Pipeline6;
import ie.deri.parsie.mltagger.fe.FeatureExtraction;
import ie.deri.parsie.mltagger.object.FeatureRecordPredict;
import ie.deri.nlp.liblinear.LibLinearAnswer;
import ie.deri.nlp.liblinear.ModelFilesContainer;
import ie.deri.nlp.mltagger.result.filter.PhraseBoundaryFilter;
import ie.deri.parsie.mltagger.object.Request;
import ie.deri.parsie.mltagger.object.SentenceTags;
import ie.deri.parsie.mltagger.object.TTIResult;
import ie.deri.parsie.mltagger.object.TTIResultSet;
import ie.deri.parsie.mltagger.server.LingAnalysis;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import ie.deri.parsie.mltagger.server.TTIServer;

import ie.deri.parsie.mltagger.setting.Settings;
import ie.deri.parsie.mltagger.setting.TaggerSetting;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class ServerMultiplelModelMode implements Runnable {
    
    private List<ModelFilesContainer> lmfcList;
    private Pipeline6 pl6;
    private TTIServer ttiServer;
    private FeatureExtraction fe;
    //DBSentence dbs;
    private List<BPSentence> analSenList;
    private ObjectOutputStream oos;
    private List<SentenceTags> sentenceTagsList;
    private LingAnalysis linga;
    private LibLinearAnswer libAnsw;
    private PhraseBoundaryFilter phraseBoundFilter;
    
    @SuppressWarnings("CallToThreadStartDuringObjectConstruction")
    public ServerMultiplelModelMode(int threadIDNumber,
            Settings settings, TTIServer ttiServer) throws ClassNotFoundException, SQLException, IOException, InterruptedException, Exception {
        this.ttiServer = ttiServer;
        lmfcList = new ArrayList<ModelFilesContainer>();
        for (TaggerSetting ts : settings.getTaggerListSetting().getTaggerSettingList()) {
            ModelFilesContainer mfc = new ModelFilesContainer();
            mfc.init(ts);
            lmfcList.add(mfc);
        }
        pl6 = new Pipeline6(threadIDNumber, settings.getLingSettings());
        pl6.processAsSentence("Let's get it up");
        String threadName = "AssemblyWorker::" + Integer.toString(threadIDNumber);
        linga = new LingAnalysis();
        linga.init(pl6);
        fe = new FeatureExtraction();
        new Thread(this, threadName).start();
    }
    
    @Override
    public void run() {
        while (true) {
            Request userRequest = null;
            try {
                userRequest = ttiServer.getUser();
            } catch (InterruptedException ex) {
                Logger.getLogger(ServerMultiplelModelMode.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (userRequest != null) {
                this.oos = userRequest.getOos();
                System.err.println(">" + Thread.currentThread().getName()
                        + " assigned for " + userRequest.getRequestID());
                analSenList = linga.analyzeParagraph(userRequest.getSentenceString());
                sentenceTagsList = new ArrayList<SentenceTags>();
                int sentencePosition = 0;
                for (BPSentence analSent : analSenList) {
                    phraseBoundFilter = new PhraseBoundaryFilter(analSent);
                    SentenceTags sentTags = new SentenceTags();
                    sentTags.setSentencePosition(sentencePosition);
                    sentencePosition++;
                    sentTags.setAnalyzedSentence(analSent);
                    for (ModelFilesContainer mfc : lmfcList) {
                        fe = new FeatureExtraction();
                        DBSentence dbs = linga.analyzedSentToFeatDBSentence(analSent, mfc);
                        fe.generateFeatures(dbs, mfc.getFeatureMap());
                        TTIResultSet ttrts = new TTIResultSet();
                        ttrts.setTaggerID(mfc.getTaggerID());
                        List<TTIResult> ttiList = new ArrayList<TTIResult>();
                        for (FeatureRecordPredict frp : fe.getSentenceFeatRecordList()) {
                            try {
                                
                                libAnsw = mfc.getLibModel().predict(frp.getDbFeatureValueList(), false);
                                // there are plenty of room to make it much more clear... bad bad code!
                                // Ok it was a fast implementation
                                
                                if (//true
                                        libAnsw.getPredictLabel() == 1) {
                                    String termString = "";
                                    for (int i = frp.getStartPosition(); i < frp.getEndPosition() + 1; i++) {
                                        termString += dbs.getDbLexemeList().get(i).getWord() + " ";
                                    }
                                    
                                    if (phraseBoundFilter.isAcceptedByOpenNLP(frp.getStartPosition(), frp.getEndPosition())) {
                                        TTIResult ttr = new TTIResult();
                                        ttr.setStartPosition(frp.getStartPosition());
                                        ttr.setEndPosition(frp.getEndPosition());
                                        ttr.setConfidenceValue(libAnsw.getDescionValue());
                                        ttr.setTermString(termString.trim());
                                        ttr.setTaggerID(mfc.getTaggerID());
                                        ttiList.add(ttr);
                                    }
                                }
                            } catch (Exception ex) {
                                Logger.getLogger(ServerMultiplelModelMode.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        ttrts.setTtiResultList(ttiList);
                        sentTags.addTagSet(ttrts);
                    }
                    sentenceTagsList.add(sentTags);
                }
                
                
                
                
            }
            try {
                System.err.println("Transmitting " + sentenceTagsList.size() + " tagged sentence(s) ");
                for (SentenceTags sTags : sentenceTagsList) {
                    oos.writeObject(sTags);
                    //      for (TTIResult ttri : ttr) {

                    //    }
                }


//                    userRequest.getClient().close();
            } catch (Exception e) {
                System.err.println("Exception while transmitting the result");
                e.printStackTrace();
            }
            try {
                oos.writeObject(null);
                oos.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
            
        }
    }
}
