/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.clients;

import ie.deri.parsie.mltagger.assembly.MultipleModelEmbeddedMode;
import ie.deri.parsie.mltagger.setting.Settings;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 *
 * @author behqas
 */
public class BatchEMLProcess {
    static final Logger logger = Logger.getLogger("BatchLog");
	static Set<String> processedFile;

  
    public static void main(String[] art) throws FileNotFoundException, MessagingException, IOException, javax.mail.MessagingException, ParserConfigurationException, SAXException, ClassNotFoundException, SQLException, InterruptedException, Exception {
              Settings settings = new Settings();
        System.err.println(">> Loading settings ");
        settings.fromXmlFile(art[0]);
        System.err.println(">> Setting up the worker object assembly");
       MultipleModelEmbeddedMode sma = new MultipleModelEmbeddedMode(0, settings);
       setAndAnalyzeLog(art[1]);
       // load the files 
       
       // pass them one by one
       // stream out the output
       sma.processParagraph(null);
    }

    public static String parsEML(String fileName) throws FileNotFoundException, MessagingException, IOException, javax.mail.MessagingException {
        Whitelist whitelist = Whitelist.none();
        StringBuilder sb = new StringBuilder();
        InputStream source = new FileInputStream(fileName);
        MimeMessage mimeMessage = new MimeMessage(null, source);
        if (mimeMessage.getContent() instanceof Multipart) {
            Multipart mp = (Multipart) mimeMessage.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                if (mp.getBodyPart(i).getContentType().startsWith("text")) {
                    String clean = Jsoup.clean((String) mp.getBodyPart(i).getContent(), whitelist);
                    sb.append(clean);
                }
            }
            return sb.toString();
        } else if (mimeMessage.getContent() instanceof String) {
            String toProcess = (String) mimeMessage.getContent();
            String clean = Jsoup.clean(toProcess, whitelist);
            return clean;

        } else {
            return "ERROR 000";
        }
    }


 private static void setAndAnalyzeLog(String loggerPath) throws IOException, ParserConfigurationException, SAXException {
        processedFile = new HashSet<String>();

         try{
            System.err.println("Load logs from " + loggerPath);
            File file = new File(loggerPath);
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            builder.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                    if (systemId.contains("logger.dtd")) {
                        return new InputSource(new StringReader(""));
                    } else {
                        return null;
                    }
                }
            });
            Document doc;
            doc = builder.parse(file);
            // StyleTable Node
            NodeList recordList = doc.getElementsByTagName("record");
            for (int i = 0; i < recordList.getLength(); i++) {
                NodeList recordNodes = recordList.item(i).getChildNodes();
                for (int j = 0; j < recordNodes.getLength(); j++) {
                    if(recordNodes.item(j).getNodeName().equals("level") ){
                        if(recordNodes.item(j).getTextContent().trim().equals("FINEST")){
                            for (int k = 0; k < recordNodes.getLength(); k++) {
                                if(recordNodes.item(k).getNodeName().trim().equals("message")){
                                    String fileName= recordNodes.item(k).getTextContent();
                                    processedFile.add(fileName);
                                }

                            }
                        }
                    }

                }

            }
        } catch(FileNotFoundException ef){
            System.err.println("No Previous log file" + ef);
        }

        FileHandler fh;
        fh = new FileHandler(loggerPath, true);
        logger.addHandler(fh);
        logger.setLevel(Level.ALL);
//        SimpleFormatter formatter = new SimpleFormatter();
//
//        fh.setFormatter(formatter);

    }
}
