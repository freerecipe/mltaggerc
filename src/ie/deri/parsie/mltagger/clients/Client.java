/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.clients;

import ie.deri.parsie.mltagger.object.TTIResult;
import ie.deri.parsie.mltagger.object.SentenceTags;
import ie.deri.parsie.mltagger.object.Request;
import ie.deri.parsie.mltagger.object.TTIResultSet;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;


/**
 *
 * @author behqas
 */
public class Client {
    

    private String ipAddress;
    private int port;

    public Client(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
    }
    
    
    
    public List<SentenceTags> requestTTIProcess(String sentence) {
        UUID id = UUID.randomUUID();
        Request rq = new Request();
        rq.setRequestID(id.toString());
        rq.setSentenceString(sentence);
        rq.setOos(null);
        rq.setClient(null);
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        Socket socket = null;
        try {
            // open a socket connection
            socket = new Socket(ipAddress, port);
            // open I/O streams for objects
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());
            oos.writeObject(rq);

            List<SentenceTags> ttiLi = new ArrayList<SentenceTags>();
            SentenceTags tt;

            while ((tt = (SentenceTags) ois.readObject()) != null) {
                ttiLi.add(tt);
            }
            oos.close();
            ois.close();
//            System.err.println("List has " + ttiLi.size());
//            for (TTIResult tr : ttiLi) {
//                System.err.println(tr.getTermString() + tr.getConfidenceValue());
//            }
            return ttiLi;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }


    }

    public static void main(String[] art) {
        
        String abst1 = "While faceted navigation interfaces can assist users in "
                + "exploring an information collection, there is yet little support "
                + "for users in choosing a relevant item from the set of items returned"
                + " from a filtering process. In this paper, we propose using "
                + "a multi-dimensional visualization as an alternative to the "
                + "linear listing of focus items. We describe how visual abstraction "
                + "based on a combination of structural equivalence and conceptual"
                + " structure can be used to deal with a large number of items, "
                + "as well as visual ordering based on the importance of facet "
                + "values to support cross-facets comparison of focus items. "
                + "This visual support for faceted browsing has been developed"
                + " for visual exploration of text collections.";

        Client client  = new Client("127.0.0.1", 19999);
          
        List<SentenceTags> sentTagList = client.requestTTIProcess(abst1);
        System.out.println("tt size (number of sentence):" + sentTagList.size());
        
        List<TTIResult> ttiResutlOveral = new ArrayList<TTIResult>(); 
        
        for(SentenceTags sts: sentTagList){
            System.out.println("output for sentence: " + sts.getSentencePosition());
      //  Collections.sort(ttrList);
        for (TTIResultSet ttiSet : sts.getTagSet()) {
            System.out.println(" *** Output for " + ttiSet.getTaggerID());
            ttiResutlOveral.addAll(ttiSet.getTtiResultList());
            Collections.sort(ttiSet.getTtiResultList());
            
            for(TTIResult ttr: ttiSet.getTtiResultList()){
                System.out.println(ttr.getTermString()); //+ " / " + ttr.getConfidenceValue());
            }
        }
        System.out.print("---");
        }
        
        System.out.println("***********");
          
        Collections.sort(ttiResutlOveral);
                    for(TTIResult ttr: ttiResutlOveral){
                System.out.println(ttr.getTermString() );//+ " / " + ttr.getConfidenceValue());
            }

        
    }




}
