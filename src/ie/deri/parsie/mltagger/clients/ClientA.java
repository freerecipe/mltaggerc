/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.clients;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import ie.deri.parsie.mltagger.object.Request;
import ie.deri.parsie.mltagger.object.SentenceTags;
import ie.deri.parsie.mltagger.object.TTIResult;
import ie.deri.parsie.mltagger.object.TTIResultSet;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author behqas
 */
public class ClientA {

    public static List<SentenceTags> requestTTIProcess(String sentence) {
        UUID id = UUID.randomUUID();
        Request rq = new Request();
        rq.setRequestID(id.toString());
        rq.setSentenceString(sentence);
        rq.setOos(null);
        rq.setClient(null);
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        Socket socket = null;
        try {
            // open a socket connection
            socket = new Socket("1127.0.0.1", 10000);
            // open I/O streams for objects
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());
            oos.writeObject(rq);

            List<SentenceTags> ttiLi = new ArrayList<SentenceTags>();
            SentenceTags tt;


            while ((tt = (SentenceTags) ois.readObject()) != null) {
                ttiLi.add(tt);
            }
            oos.close();
            ois.close();
//            System.err.println("List has " + ttiLi.size());
//            for (TTIResult tr : ttiLi) {
//                System.err.println(tr.getTermString() + tr.getConfidenceValue());
//            }
            return ttiLi;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static void main(String[] art) {
        String s2 = "How to use the profitcode in deliverynotes?";
        List<SentenceTags> sentTagList = requestTTIProcess(s2);
        System.err.println("tt size (number of sentence):" + sentTagList.size());
        for (SentenceTags sts : sentTagList) {
            System.out.println("output for sentence: " + sts.getSentencePosition());
            for (TTIResultSet ttiSet : sts.getTagSet()) {
                System.out.println(" *** Output for " + ttiSet.getTaggerID());
                Collections.sort(ttiSet.getTtiResultList());
                for (TTIResult ttr : ttiSet.getTtiResultList()) {
                    System.out.println(ttr.getTermString() + " / " + ttr.getConfidenceValue());
                }
            }
            System.err.println("---");
        }
    }
}
