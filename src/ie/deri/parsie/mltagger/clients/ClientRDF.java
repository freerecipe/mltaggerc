/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.clients;

import ie.deri.parsie.mltagger.object.TTIResult;
import ie.deri.parsie.mltagger.object.SentenceTags;
import ie.deri.parsie.mltagger.object.Request;
import ie.deri.parsie.mltagger.object.TTIResultSet;

import ie.deri.nlp.misc.GetFiles;
import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URLEncoder;

import java.util.ArrayList;

import java.util.List;
import java.util.Scanner;
import java.util.UUID;

import java.util.logging.Logger;
import org.openrdf.model.BNode;
import org.openrdf.model.Literal;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFHandler;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.ntriples.NTriplesWriter;
import org.openrdf.sail.memory.MemoryStore;

/**
 *
 * @author behqas
 */
public class ClientRDF {

    static Repository tagRepository;
    static RepositoryConnection con;
    static final String prefix = "http://example.org/";
    static ValueFactory f;
    static Logger logger;

    public static List<SentenceTags> requestTTIProcess(String sentence) {
        UUID id = UUID.randomUUID();
        Request rq = new Request();
        rq.setRequestID(id.toString());
        rq.setSentenceString(sentence);
        rq.setOos(null);
        rq.setClient(null);
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        Socket socket = null;
        try {
            // open a socket connection
            socket = new Socket("127.0.0.1", 18888);
            // open I/O streams for objects
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());
            oos.writeObject(rq);

            List<SentenceTags> ttiLi = new ArrayList<SentenceTags>();
            SentenceTags tt;

            while ((tt = (SentenceTags) ois.readObject()) != null) {
                ttiLi.add(tt);
            }
            oos.close();
            ois.close();
            return ttiLi;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }


    }

    public static void main(String[] art) {
        try {
            initMemRDFRep();
            // GetFiles gf = new GetFiles();
//            gf.getCorpusFiles("C:\\RandomWikipediaAbstract\\wikisample");
//            for (String path : gf.getPDFFiles()) {
                Scanner sc = new Scanner(new File("c:\\something"));
                String url = sc.nextLine();
                System.out.println("Processing " + url);
                String content = sc.nextLine();
                sc.close();
                addTag(url, requestTTIProcess(content));
//            }
            shutDownRDFRep("C:\\RandomWikipediaAbstract\\tag.nt");
            //    




        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private static void initMemRDFRep() throws RepositoryException {
        MemoryStore memStore = new MemoryStore();
        tagRepository = new SailRepository(memStore);
        tagRepository.initialize();
        con = tagRepository.getConnection();
        // make sure that the value factory is init once to make sure of consistencies for the blank nods
        f = tagRepository.getValueFactory();
    }

    private static void addTag(String articleURI,
            List<SentenceTags> sentenceTagList) throws RepositoryException, UnsupportedEncodingException {

        org.openrdf.model.URI article = f.createURI(articleURI);
        org.openrdf.model.URI articleClass = f.createURI(prefix + "article");
        con.add(article, RDF.TYPE, articleClass);
        con.add(articleClass, RDF.TYPE, RDFS.CLASS);
        //    URI fromArticle = f.createURI(prefix + "fromArticle");
        URI hasA = f.createURI(prefix + "has_a");
        URI hasStartPositio = f.createURI(prefix + "has_start_position");
        URI hasEndPositio = f.createURI(prefix + "has_end_position");
        URI hasConfidence = f.createURI(prefix + "has_confidence");
        URI hasTerm = f.createURI(prefix + "has_term");
        URI fromSentence = f.createURI(prefix + "from_sentence");
        for (SentenceTags st : sentenceTagList) {
            URI sentence = f.createURI(articleURI + "/" + "sentence_" + st.getSentencePosition());
            URI sentenceClass = f.createURI(prefix + "sentence");
            con.add(sentence, RDF.TYPE, sentenceClass);
            con.add(sentenceClass, RDF.TYPE, RDFS.CLASS);
            con.add(article, hasA, sentence);


            for (TTIResultSet ttis : st.getTagSet()) {
                URI classifierClass = f.createURI(prefix + "classifier");
                URI classifier = f.createURI(prefix + "classifier:" + ttis.getTaggerID());
                con.add(classifier, RDF.TYPE, classifierClass);
                con.add(classifierClass, RDF.TYPE, RDFS.CLASS);

                for (TTIResult ttir : ttis.getTtiResultList()) {
                    URI term = f.createURI(prefix + "term/" + URLEncoder.encode(ttir.getTermString(), "UTF-8"));
                    URI termClass = f.createURI(prefix + "identified_term");
                    con.add(term, RDF.TYPE, termClass);
                    con.add(termClass, RDF.TYPE, RDFS.CLASS);
                    Literal termString = f.createLiteral(ttir.getTermString());
                    con.add(term, RDFS.LABEL, termString);

                    Literal termConfidence = f.createLiteral(ttir.getConfidenceValue());

                    BNode bn = f.createBNode();


                    con.add(bn, hasConfidence, termConfidence);
                    con.add(bn, hasStartPositio, f.createLiteral(ttir.getStartPosition()));
                    con.add(bn, hasEndPositio, f.createLiteral(ttir.getEndPosition()));
                    con.add(bn, hasTerm, term);
                    con.add(bn, fromSentence, sentence);


                }
            }

        }

    }

    private static void shutDownRDFRep(String path) throws RepositoryException, RDFHandlerException, FileNotFoundException, IOException {
        FileOutputStream fos = new FileOutputStream(new File(path));
        RDFHandler rdfxmlWriter = new NTriplesWriter(fos);
        con.export(rdfxmlWriter);
        fos.close();
        con.close();

    }
}
