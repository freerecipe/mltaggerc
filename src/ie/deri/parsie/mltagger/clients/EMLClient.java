/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.clients;


import ie.deri.parsie.mltagger.object.TTIResult;
import ie.deri.parsie.mltagger.object.SentenceTags;
import ie.deri.parsie.mltagger.object.Request;
import ie.deri.parsie.mltagger.object.TTIResultSet;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 *
 * @author behqas
 */
public class EMLClient {
    static final Logger logger = Logger.getLogger("BatchLog");
	Set<String> processedFile;

    public static List<SentenceTags> requestTTIProcess(String sentence) {
        UUID id = UUID.randomUUID();
        Request rq = new Request();
        rq.setRequestID(id.toString());
        rq.setSentenceString(sentence);
        rq.setOos(null);
        rq.setClient(null);
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        Socket socket = null;
        try {
            // open a socket connection
            socket = new Socket("127.0.0.1", 17179);
            // open I/O streams for objects
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());
            oos.writeObject(rq);

            List<SentenceTags> ttiLi = new ArrayList<SentenceTags>();
            SentenceTags tt;

            while ((tt = (SentenceTags) ois.readObject()) != null) {
                ttiLi.add(tt);
            }
            oos.close();
            ois.close();
//            System.err.println("List has " + ttiLi.size());
//            for (TTIResult tr : ttiLi) {
//                System.err.println(tr.getTermString() + tr.getConfidenceValue());
//            }
            return ttiLi;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }


    }

    public static void main(String[] art) throws FileNotFoundException, MessagingException, IOException, javax.mail.MessagingException {
        String s32=""
                + "EEYORETTT is a Technical Term Tagger.";
        String s444 = "The tool is a Support Vector Machine based system and exploits linguistic features from Stanford PoS Tagger  as well as dependency parses from Malt Parser.";
          String s2= "Protégé with the SKOSEd plugin but it's a bit overkill and Protégé is an ontology editor and not great for just browsing the hierarchy. Thanks";
                  
          String s9="Essentially, everything functions in CUPC except for status changes.&nbsp; Phone control, search for contacts, everything fine, but not presence status.&nbsp; Presence status shows when they are doing a search for a user, but as soon as they add the user to the contacts, presence status is lost.";
        List<SentenceTags> sentTagList = requestTTIProcess( parsEML("c:\\4.eml") );
        System.err.println("tt size (number of sentence):" + sentTagList.size());
        for(SentenceTags sts: sentTagList){
            System.out.println("output for sentence: " + sts.getSentencePosition());
      //  Collections.sort(ttrList);
        for (TTIResultSet ttiSet : sts.getTagSet()) {
            System.out.println(" *** Output for " + ttiSet.getTaggerID());
            Collections.sort(ttiSet.getTtiResultList());
            for(TTIResult ttr: ttiSet.getTtiResultList()){
                System.out.println(ttr.getTermString() + " / " + ttr.getConfidenceValue());
            }
        }
        System.out.print("---");
        }
    }

    public static String parsEML(String fileName) throws FileNotFoundException, MessagingException, IOException, javax.mail.MessagingException {
        Whitelist whitelist = Whitelist.none();
        StringBuilder sb = new StringBuilder();
        InputStream source = new FileInputStream(fileName);
        MimeMessage mimeMessage = new MimeMessage(null, source);
        if (mimeMessage.getContent() instanceof Multipart) {
            Multipart mp = (Multipart) mimeMessage.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                if (mp.getBodyPart(i).getContentType().startsWith("text")) {
                    String clean = Jsoup.clean((String) mp.getBodyPart(i).getContent(), whitelist);
                    sb.append(clean);
                }
            }
            return sb.toString();
        } else if (mimeMessage.getContent() instanceof String) {
            String toProcess = (String) mimeMessage.getContent();
            String clean = Jsoup.clean(toProcess, whitelist);
            return clean;

        } else {
            return "ERROR 000";
        }
    }


 private void setAndAnalyzeLog(String loggerPath) throws IOException, ParserConfigurationException, SAXException {
        processedFile = new HashSet<String>();

         try{
            System.err.println("Load logs from " + loggerPath);
            File file = new File(loggerPath);
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            builder.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                    if (systemId.contains("logger.dtd")) {
                        return new InputSource(new StringReader(""));
                    } else {
                        return null;
                    }
                }
            });
            Document doc;
            doc = builder.parse(file);
            // StyleTable Node
            NodeList recordList = doc.getElementsByTagName("record");
            for (int i = 0; i < recordList.getLength(); i++) {
                NodeList recordNodes = recordList.item(i).getChildNodes();
                for (int j = 0; j < recordNodes.getLength(); j++) {
                    if(recordNodes.item(j).getNodeName().equals("level") ){
                        if(recordNodes.item(j).getTextContent().trim().equals("FINEST")){
                            for (int k = 0; k < recordNodes.getLength(); k++) {
                                if(recordNodes.item(k).getNodeName().trim().equals("message")){
                                    String fileName= recordNodes.item(k).getTextContent();
                                    processedFile.add(fileName);
                                }

                            }
                        }
                    }

                }

            }
        } catch(FileNotFoundException ef){
            System.err.println("No Previous log file" + ef);
        }

        FileHandler fh;
        fh = new FileHandler(loggerPath, true);
        logger.addHandler(fh);
        logger.setLevel(Level.ALL);
//        SimpleFormatter formatter = new SimpleFormatter();
//
//        fh.setFormatter(formatter);

    }
}
