/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.db;

import ie.deri.nlp.db.h2.DBPaperAssertion;

import ie.deri.parsie.mltagger.object.BootstrapInstance;
import ie.deri.parsie.mltagger.object.BootstrapTerm;
import ie.deri.parsie.mltagger.object.Feature;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
public class MLTaggerIndex extends DBPaperAssertion {

    public MLTaggerIndex(Connection con) {
        this.con =con;
    }
    
    
     public void indexBootstrapTerm(BootstrapTerm bst) throws SQLException {
        PreparedStatement pstmtBoot = con.prepareStatement(
                "SELECT bootstrap_id, bootstrap_frequency FROM dbo_bootstrap WHERE "
                + "(( bootstrap_string = ? ) AND ( bootstrap_source= ?)) ");
        pstmtBoot.setString(1, bst.getBootstrapString());
        pstmtBoot.setString(2, bst.getSource());
        ResultSet rs = pstmtBoot.executeQuery();
        if (rs.next()) {
            double bootID = rs.getDouble("bootstrap_id");
            PreparedStatement jstmtUpdtTable =
                    con.prepareStatement(
                    "UPDATE dbo_bootstrap SET bootstrap_frequency= ? "
                    + "WHERE bootstrap_id = ?");
            int NewFrequency = rs.getInt("bootstrap_frequency");
            NewFrequency++;
            jstmtUpdtTable.setInt(1, NewFrequency);
            jstmtUpdtTable.setDouble(2, bootID);
            jstmtUpdtTable.executeUpdate();
            con.commit();
            jstmtUpdtTable.close();
            bst.setId(bootID);
            rs.close();
            pstmtBoot.close();
            return;
        } else {
            rs.close();
            pstmtBoot.close();
            PreparedStatement instBootstrap = con.prepareStatement(
                    "INSERT INTO dbo_bootstrap ("
                    + "bootstrap_id, "
                    + "bootstrap_string, "
                    + "bootstrap_source, "
                    + "bootstrap_length,"
                    + "is_positive_example,"
                    + "is_reviewed,"
                    + "bootstrap_frequency,"
                    + "jackard_value,"
                    + "bootstrap_yahoo_hits,"
                    + " bootstrap_callocation_yahoo_hits)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            instBootstrap.setDouble(1, bst.getId());
            instBootstrap.setString(2, bst.getBootstrapString());
            instBootstrap.setString(3, bst.getSource());
            instBootstrap.setInt(4, bst.getLength());
            instBootstrap.setBoolean(5, bst.isPositiveExample());
            instBootstrap.setBoolean(6, false);
            instBootstrap.setInt(7, 1);
            instBootstrap.setDouble(8, bst.getJackarValue());
            instBootstrap.setDouble(9, bst.getYahooHits());
            instBootstrap.setDouble(10, bst.getCollocationYahooHits());
            instBootstrap.executeUpdate();
            con.commit();
            instBootstrap.close();
            con.commit();
        }
    }

    
    /*
     * retrieve bootstrap id or assert the new one // sync with db
     */
    
    
    
    public void indexBootstrapInstance(BootstrapInstance bstInst) throws SQLException {
        PreparedStatement pstmtBoot = con.prepareStatement(
                "SELECT bootstrap_instance_id, bootstrap_id "
                + " FROM dbo_bootstrap_instance WHERE "
                + "(( bootstrap_id = ? )  AND ( sentence_id = ? ) AND"
                + " (start_position = ? ) AND (end_position = ? ))");
        pstmtBoot.setDouble(1, bstInst.getBootstrapID());
        pstmtBoot.setDouble(2, bstInst.getSentenceID());
        pstmtBoot.setInt(3, bstInst.getStartPosition());
        pstmtBoot.setInt(4, bstInst.getEndPosition());
        ResultSet rs = pstmtBoot.executeQuery();
        if (rs.next()) {
            bstInst.setInstanceID(rs.getDouble("bootstrap_instance_id"));
            rs.close();
            pstmtBoot.close();
            return;
        } else {
            rs.close();
            pstmtBoot.close();
            PreparedStatement instBootstrap = con.prepareStatement(
                    "INSERT INTO dbo_bootstrap_instance "
                    + "VALUES (?, ?, ?, ? , ?)");

            instBootstrap.setDouble(1, bstInst.getInstanceID());
            instBootstrap.setDouble(2, bstInst.getBootstrapID());
            instBootstrap.setDouble(3, bstInst.getSentenceID());
            instBootstrap.setInt(4, bstInst.getStartPosition());
            instBootstrap.setInt(5, bstInst.getEndPosition());
            instBootstrap.executeUpdate();
            con.commit();
            instBootstrap.close();
        }
    }
    /*
     * Sync feature with db
     */

    public void indexFeatureTTI(Feature f) throws SQLException {
        PreparedStatement pstmtBoot = con.prepareStatement(
                "SELECT feature_db_id, feature_type, feature_id, feature_source, description, frequency"
                + " FROM dbo_feature_tti WHERE "
                + "( ( feature_type = ? )  AND ( feature_id = ? ) )");
        // I have excluded the source for the safty reason
        pstmtBoot.setString(1, f.getFeatureType());
        pstmtBoot.setString(2, f.getFeatureID());
        ResultSet rs = pstmtBoot.executeQuery();
        if (rs.next()) {
            double featDBID = rs.getDouble("feature_db_id");
            int newFrequency = rs.getInt("frequency");
            PreparedStatement freqFTI = con.prepareStatement(
                    "UPDATE dbo_feature_tti SET frequency= ? "
                    + "WHERE feature_db_id = ?");
            newFrequency++;
            freqFTI.setInt(1, newFrequency);
            freqFTI.setDouble(2, featDBID);
            freqFTI.executeUpdate();
            con.commit();
            freqFTI.close();
            f.setFeatureDBID(featDBID);
            rs.close();
            pstmtBoot.close();
            return;
        } else {
            rs.close();
            pstmtBoot.close();
            PreparedStatement instBootstrap = con.prepareStatement(
                    "INSERT INTO  dbo_feature_tti "
                    + "VALUES (?, ?, ?, ? , ?, ?)");

            instBootstrap.setDouble(1, f.getFeatureDBID());
            instBootstrap.setString(2, f.getFeatureType());
            instBootstrap.setString(3, f.getFeatureID());
            instBootstrap.setString(4, f.getSource());
            instBootstrap.setString(5, f.getDesc());
            instBootstrap.setInt(6, 1);
            instBootstrap.executeUpdate();
            con.commit();
            instBootstrap.close();
        }
    }

    /**
     * WARNING >>>>>>>>>>> this may  need to be changed, if you are considering
     * to set the values of features into something rather than
     * average then this need to be changed
     * @param bootstrapInstanceID
     * @param featureDBID
     * @param value
     * @throws SQLException
     */
    public void indexFeatureTTIInstance(double bootstrapInstanceID,
            double featureDBID, double value) throws SQLException {
        PreparedStatement pstmtBoot = con.prepareStatement(
                "SELECT feature_db_id, bootstrap_instance_id, value, frequency "
                + " FROM dbo_feature_tti_instance WHERE "
                + "((bootstrap_instance_id = ? )  AND ( feature_db_id = ? ))");
        pstmtBoot.setDouble(1, bootstrapInstanceID);
        pstmtBoot.setDouble(2, featureDBID);
        ResultSet rs = pstmtBoot.executeQuery();
        if (rs.next()) {
            // this may cause confusion in the future
            Double valueInDB = rs.getDouble("value");
            Double newValue = valueInDB + value;
            int newFreq = rs.getInt("frequency");
            newFreq++;
            rs.close();
            pstmtBoot.close();
            PreparedStatement jstmtUpdtTable =
                    con.prepareStatement(
                    "UPDATE dbo_feature_tti_instance SET "
                    + "value= ?, frequency = ? "
                    + "WHERE (( bootstrap_instance_id = ?)"
                    + " AND (feature_db_id=?))");
            jstmtUpdtTable.setDouble(1, newValue);
            jstmtUpdtTable.setInt(2, newFreq);
            jstmtUpdtTable.setDouble(3, bootstrapInstanceID);
            jstmtUpdtTable.setDouble(4, featureDBID);
            jstmtUpdtTable.executeUpdate();
            con.commit();
            jstmtUpdtTable.close();
            return;
        } else {
            rs.close();
            pstmtBoot.close();
            PreparedStatement instBootstrap = con.prepareStatement(
                    "INSERT INTO  dbo_feature_tti_instance "
                    + "VALUES (?, ?, ?, ? )");
            instBootstrap.setDouble(1, bootstrapInstanceID);
            instBootstrap.setDouble(2, featureDBID);
            instBootstrap.setDouble(3, value);
            instBootstrap.setInt(4, 1);
            instBootstrap.executeUpdate();
            con.commit();
            instBootstrap.close();
        }
    }

    
}
