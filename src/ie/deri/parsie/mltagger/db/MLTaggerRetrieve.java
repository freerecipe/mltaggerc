/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.db;

import ie.deri.nlp.db.h2.DBPaperRetrieve;
import ie.deri.parsie.mltagger.object.BootstrapTerm;
import ie.deri.parsie.mltagger.object.DBFeatureValue;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Behrang BB QasemiZadeh < behrang.qasemizadeh@deri.org>
 */
public class MLTaggerRetrieve extends DBPaperRetrieve {
    
       
     public List<BootstrapTerm> retBootstrapTerm() throws java.sql.SQLException {
        List<BootstrapTerm> btList = new ArrayList<BootstrapTerm>();
        PreparedStatement pstmtBoot = con.prepareStatement(
                "SELECT * FROM dbo_bootstrap");
        ResultSet rs = pstmtBoot.executeQuery();
        while (rs.next()) {
            BootstrapTerm bst = new BootstrapTerm();
            bst.setId(rs.getDouble("bootstrap_id"));
            bst.setBootstrapString(rs.getString("bootstrap_string"));
            bst.setSource(rs.getString("bootstrap_source"));
            bst.setLength(rs.getInt("bootstrap_length"));
            bst.setPositiveExample(rs.getBoolean("is_positive_example"));
            bst.setIsReviewed(rs.getBoolean("is_reviewed"));
            bst.setFrequency(rs.getInt("bootstrap_frequency"));
            bst.setJackarValue(rs.getDouble("jackard_value"));
            bst.setJackarValue(rs.getDouble("bootstrap_yahoo_hits"));
            bst.setJackarValue(rs.getDouble("bootstrap_callocation_yahoo_hits"));
            btList.add(bst);
        }
        rs.close();
        pstmtBoot.close();
        return btList;
    }

    public void updateBootstrapTerm(double bootstrapID, Boolean state) throws java.sql.SQLException {
        PreparedStatement pstmtBoot = con.prepareStatement(
                "UPDATE dbo_bootstrap SET is_positive_example= ?, is_reviewed = ? "
                + " WHERE bootstrap_id = ?");
        pstmtBoot.setBoolean(1, state);
        pstmtBoot.setBoolean(2, true);
        pstmtBoot.setDouble(3, bootstrapID);
        pstmtBoot.executeUpdate();
        con.commit();
        pstmtBoot.close();
    }

    public ResultSet retDistinctSentenceBootstrapInstance() throws SQLException {
        PreparedStatement pstmtBoot = con.prepareStatement(
                "SELECT distinct(sentence_id) FROM dbo_bootstrap_instance");
        ResultSet rs = pstmtBoot.executeQuery();
        return rs;
    }

    public ResultSet retBootstrapsInstancesForSentence(double sentenceID) throws SQLException {
        PreparedStatement pstmtBoot = con.prepareStatement(
                "SELECT "
                + "bootstrap_instance_id, bootstrap_id, start_position, end_position "
                + "FROM dbo_bootstrap_instance WHERE sentence_id = ?");
        pstmtBoot.setDouble(1, sentenceID);
        ResultSet rs = pstmtBoot.executeQuery();
        return rs;
    }

    public ResultSet retBootstrapInstance() throws SQLException {
        PreparedStatement pstmtBoot = con.prepareStatement(
                "SELECT bootstrap_instance_id,"
                + " bootstrap_id, "
                + "sentence_id,"
                + "start_position,"
                + "end_position FROM dbo_bootstrap_instance");
        ResultSet rs = pstmtBoot.executeQuery();
        return rs;
    }

    public ResultSet retFeatureTTI() throws SQLException {
        PreparedStatement pstmtBoot = con.prepareStatement(
                "SELECT"
                + " feature_db_id,"
                + " feature_type, "
                + " feature_id,"
                + " feature_source,"
                + " description,"
                + " frequency"
                + " FROM dbo_feature_tti");
        ResultSet rs = pstmtBoot.executeQuery();
        return rs;

    }

    public List<DBFeatureValue> retFeatureTTIForBootstrapInstance(double bootstrapInstanceID) throws SQLException {
        List<DBFeatureValue> returnList = new ArrayList<DBFeatureValue>();
        PreparedStatement pstmtBoot = con.prepareStatement(
                "SELECT"
                + " feature_db_id,"
                + " value, "
                + " frequency "
                + " FROM dbo_feature_tti_instance WHERE bootstrap_instance_id =?");
        pstmtBoot.setDouble(1, bootstrapInstanceID);
        ResultSet rs = pstmtBoot.executeQuery();
        while (rs.next()) {
            DBFeatureValue dbv = new DBFeatureValue();
            dbv.setFeatureDBID(rs.getDouble("feature_db_id"));
            dbv.setValue(rs.getDouble("value"));
            dbv.setFreq(rs.getInt("frequency"));
            returnList.add(dbv);
        }
        return returnList;

    }

    // this mainly developed for test and train purposes
    public ResultSet retBootstrapInstanceByBootstrapID(double bootstrapID) throws SQLException {
        PreparedStatement pstmtBoot = con.prepareStatement(
                "SELECT bootstrap_instance_id,"
                + " bootstrap_id, "
                + "sentence_id,"
                + "start_position,"
                + "end_position FROM dbo_bootstrap_instance"
                + " WHERE bootstrap_id = ?");
        pstmtBoot.setDouble(1, bootstrapID);
        ResultSet rs = pstmtBoot.executeQuery();
        return rs;
    }
    

   
}
