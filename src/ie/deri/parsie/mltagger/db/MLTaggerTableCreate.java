/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.db;


import ie.deri.nlp.db.h2.DBLengthSettings;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class MLTaggerTableCreate extends DBLengthSettings {

    private Connection con;
    private Statement st;

    public void cerateTTRecognitionTable(Connection con) {
        // bootstraping and mining table
        this.con = con;
        
        
        createDBOBootstrap();
        createDBOBootstrapInstance();
        createDBOFeatureTTI();
        createDBOFeatureTTIInstance();

    }

    private boolean createDBOBootstrap() {
        try {
            st = con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS `dbo_bootstrap` ("
                    + " `bootstrap_id` " + ID_TYPE + " NOT NULL,"
                    + " `bootstrap_string` varchar(255) default NULL,"
                    + " `bootstrap_source` varchar(255) default NULL,"
                    + " `bootstrap_length` SMALLINT default NULL,"
                    + " `is_positive_example` BOOLEAN NOT NULL,"
                    + " `is_reviewed` BOOLEAN NOT NULL,"
                    + " `bootstrap_frequency` int(10) default NULL,"
                    + " `jackard_value` DOUBLE default NULL," // frq(phrase) / sigma freq(lexemes)
                    + " `bootstrap_yahoo_hits` DOUBLE default NULL," // frq(phrase) / sigma freq(lexemes)
                    + " `bootstrap_callocation_yahoo_hits` DOUBLE default NULL," // frq(phrase) / sigma freq(lexemes)
                    + " PRIMARY KEY  (`bootstrap_id`))");
            st.executeUpdate("CREATE INDEX IF NOT EXISTS Index_is_positive_example ON "
                    + "dbo_bootstrap (is_positive_example)");
            st.executeUpdate("CREATE INDEX IF NOT EXISTS Index_is_reviewed ON "
                    + "dbo_bootstrap (is_reviewed)");
            st.executeUpdate("CREATE INDEX IF NOT EXISTS Index_bootstrap_string ON "
                    + "dbo_bootstrap (bootstrap_string)");

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MLTaggerTableCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean createDBOBootstrapInstance() {
        try {
            st = con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS `dbo_bootstrap_instance` ("
                    + "`bootstrap_instance_id` "+ ID_TYPE + " NOT NULL,"
                    + "`bootstrap_id` " + ID_TYPE + " default NULL,"
                    + "`sentence_id` " + ID_TYPE + " default NULL, "
                    + "`start_position` SMALLINT default NULL, "
                    + "`end_position` SMALLINT default NULL, "
                    + "PRIMARY KEY  (`bootstrap_instance_id`)) ");
            st.executeUpdate("CREATE INDEX IF NOT EXISTS Index_bootstrap_id_instanace ON "
                    + "dbo_bootstrap_instance (bootstrap_id)");
            st.executeUpdate("CREATE INDEX IF NOT EXISTS Index_bootstrap_sentence_id ON "
                    + "dbo_bootstrap_instance (sentence_id)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MLTaggerTableCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private boolean createDBOFeatureTTI() {
        try {
            st = con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS `dbo_feature_tti` ("
                    + "`feature_db_id` " + ID_TYPE + " NOT NULL,"
                    + "`feature_type` varchar(255) default NULL,"
                    + "`feature_id` varchar(255) default NULL,"
                    + "`feature_source` varchar(255) default NULL,"
                    + "`description` varchar(500) default NULL,"
                    + "`frequency` int(10) default NULL, "
                    + "PRIMARY KEY  (`feature_db_id`))");

            st.executeUpdate("CREATE INDEX IF NOT EXISTS Index_feature_type ON "
                    + "dbo_feature_tti (feature_type)");
            st.executeUpdate("CREATE INDEX IF NOT EXISTS Index_feature_id ON "
                    + "dbo_feature_tti (feature_id)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MLTaggerTableCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }


    }

    private boolean createDBOFeatureTTIInstance() {
        try {
            st = con.createStatement();
            st.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS `dbo_feature_tti_instance` ("
                    + "`bootstrap_instance_id` " + ID_TYPE + " NOT NULL,"
                    + "`feature_db_id` "+ID_TYPE+" NOT NULL,"
                    + "`value` DOUBLE default NULL,"
                    + "`frequency` int(10) default NULL,"
                    + "PRIMARY KEY  (`feature_db_id`,`bootstrap_instance_id`))");
            st.executeUpdate("CREATE INDEX IF NOT EXISTS Index_feature_feature_db_id ON "
                    + "dbo_feature_tti_instance (feature_db_id)");
            st.executeUpdate("CREATE INDEX IF NOT EXISTS Index_bootstrap_instance_id ON "
                    + "dbo_feature_tti_instance (bootstrap_instance_id)");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MLTaggerTableCreate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }


}
