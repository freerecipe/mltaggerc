/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.fe;

import ie.deri.nlp.analyzer.objects.Chunk;
import ie.deri.parsie.mltagger.object.Feature;
import ie.deri.parsie.mltagger.object.DBFeatureValue;
import ie.deri.nlp.db.objects.DBDependency;
import ie.deri.nlp.db.objects.DBLexeme;
import ie.deri.nlp.db.objects.DBSentence;
import ie.deri.nlp.db.objects.DBSentenceDependency;
import ie.deri.parsie.mltagger.object.FeatureRecordPredict;
import ie.deri.parsie.mltagger.object.TTRefineStopWord;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.SetUtils;
//import rita.wordnet.RiWordnet;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class FeatureExtraction {

    private DBSentence dbSentence;
    private List<Chunk> chunkList;
    private List<DBSentenceDependency> sentDep;
    private FeatureRecordPredict currentFeatureRecord;
    private List<FeatureRecordPredict> sentenceFeatRecordList;
//    private final rita.wordnet.RiWordnet wordNet = new RiWordnet();;

    public void generateFeatures(DBSentence dbSentence, Map<String, Integer> featureMap) {

        sentenceFeatRecordList = new ArrayList<FeatureRecordPredict>();
        this.dbSentence = dbSentence;
        sentDep = new ArrayList<DBSentenceDependency>();
        List<int[]> candidPhraseBoundaries = extractCandidTerms(dbSentence);
        for (int[] boundary : candidPhraseBoundaries) {
            currentFeatureRecord = new FeatureRecordPredict();
            
            currentFeatureRecord.setStartPosition(boundary[0]);
            currentFeatureRecord.setEndPosition(boundary[1]);
            
            genUniGram();
            genBiGram();
            genTriGram();
            genFourGram();
            genTermPoSChain();
            genDepBasedFeature();

            // index features then
//            Collections.sort(currentFeatureRecord);

            Set<DBFeatureValue> featValueSet = new HashSet<DBFeatureValue>();
            for (Feature f : currentFeatureRecord.getFeatureList()) {
                if (featureMap.containsKey(f.getFeatureID())) {
                    DBFeatureValue dbfv = new DBFeatureValue();
                    dbfv.setFeatureDBID(f.getFeatureDBID()); // not necessay
                    dbfv.setNumericID(featureMap.get(f.getFeatureID()));
                    dbfv.setValue(f.getValue());
                    dbfv.setFreq(1);
                    featValueSet.add(dbfv);
                }
            }
            if (featValueSet.size() > 0) {
                currentFeatureRecord.setDbFeatureValueList(SetUtils.orderedSet(featValueSet));
                sentenceFeatRecordList.add(currentFeatureRecord);
            }
        }

    }

    
    
        public void generateFeaturesForCandidPhrase(List<int[]> candidPhraseBoundaries, DBSentence dbSentence, Map<String, Integer> featureMap) {

        sentenceFeatRecordList = new ArrayList<FeatureRecordPredict>();
        this.dbSentence = dbSentence;
        sentDep = new ArrayList<DBSentenceDependency>();
        for (int[] boundary : candidPhraseBoundaries) {
            currentFeatureRecord = new FeatureRecordPredict();
            
            currentFeatureRecord.setStartPosition(boundary[0]);
            currentFeatureRecord.setEndPosition(boundary[1]);
            
            genUniGram();
            genBiGram();
            genTriGram();
            genFourGram();
            genTermPoSChain();
            genDepBasedFeature();

            // index features then
//            Collections.sort(currentFeatureRecord);

            Set<DBFeatureValue> featValueSet = new HashSet<DBFeatureValue>();
            for (Feature f : currentFeatureRecord.getFeatureList()) {
                if (featureMap.containsKey(f.getFeatureID())) {
                    DBFeatureValue dbfv = new DBFeatureValue();
                    dbfv.setFeatureDBID(f.getFeatureDBID()); // not necessay
                    dbfv.setNumericID(featureMap.get(f.getFeatureID()));
                    dbfv.setValue(f.getValue());
                    dbfv.setFreq(1);
                    featValueSet.add(dbfv);
                }
            }
            if (featValueSet.size() > 0) {
                currentFeatureRecord.setDbFeatureValueList(org.apache.commons.collections.SetUtils.orderedSet(featValueSet));
                sentenceFeatRecordList.add(currentFeatureRecord);
            }
        }

    }

    
    
// lexeme and pos before 1gram
    private void genUniGram() {
        if (currentFeatureRecord.getStartPosition() > 0) {
            Feature f = new Feature();
            DBLexeme dbl = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 1);
            f.setFeatureType("lex_before");
            f.setFeatureID("lb_" + dbl.getId());
            f.setValue(1);
            f.setDesc("keyword lb_ + LexemeID from Repository");

            currentFeatureRecord.addFeature(f);
            Feature f2;
            f2 = new Feature();
            f2.setFeatureType("pos_bef");
            f2.setFeatureID("pb_" + dbl.getPos());
            f2.setValue(1);
            f2.setSource("Penn Pos taggs");
            f2.setDesc("`Keyword pb_ + Penn PoS tag before tech term");
            currentFeatureRecord.addFeature(f2);

        }
        // lexeme and pos after 1gram
        if (currentFeatureRecord.getEndPosition()
                < dbSentence.getDbLexemeList().size() - 1) {
            Feature f = new Feature();
            f.setFeatureType("lex_after");
            DBLexeme dbl = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 1);
            f.setFeatureID("la_" + dbl.getId());
            f.setValue(1);
            f.setDesc("Keyword la_ + LexemeID from Repository ");

            currentFeatureRecord.addFeature(f);
            Feature f2;
            f2 = new Feature();
            f2.setFeatureType("pos_after");
            f2.setFeatureID("pa_" + dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 1).getPos());
            f2.setValue(1);
            f2.setDesc("keyword pa_ + Penn PoS tag");
            currentFeatureRecord.addFeature(f2);
        }

    }

    private void genBiGram() {
        // lexeme and pos bigram before
        if (currentFeatureRecord.getStartPosition() > 1) {
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 2);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 1);
            Feature f = new Feature();
            f.setFeatureType("2gram_lexeme_before");
            f.setFeatureID("2lb_" + dblF.getId() + "_" + dblA.getId());
            f.setDesc("keyword 2lb + lexeme id from repo");
            f.setValue(1);
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("2gram_pos_before");
            f2.setFeatureID("2pb_" + dblF.getPos() + "_" + dblA.getPos());
            f2.setDesc("keyword 2pb_ + lexeme id");
            f2.setValue(1);
            currentFeatureRecord.addFeature(f2);
        }

        // lexeme and pos bigram after
        if (currentFeatureRecord.getEndPosition() < dbSentence.getDbLexemeList().size() - 2) {
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 1);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 2);
            Feature f = new Feature();
            f.setFeatureType("2gram_lexeme_after");
            f.setFeatureID("2la_" + dblF.getId() + "_" + dblA.getId());
            f.setValue(1);
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("2gram_pos_after");
            f2.setFeatureID("2pa_" + dblF.getPos() + "_" + dblA.getPos());
            f2.setValue(1);
            currentFeatureRecord.addFeature(f2);
        }

    }

    private void genTriGram() {
        // lexeme and pos Trigram before
        if (currentFeatureRecord.getStartPosition() > 2) {
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 3);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 2);
            DBLexeme dblM = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 1);

            Feature f = new Feature();
            f.setFeatureType("3gram_lexeme_before");
            f.setFeatureID("3lb_" + dblF.getId() + "_"
                    + dblA.getId() + "_" + dblM.getId());
            f.setValue(1);
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("3gram_pos_before");
            f2.setFeatureID("3bp_" + dblF.getPos()
                    + "_" + dblA.getPos() + "_" + dblM.getPos());
            f2.setValue(1);
            currentFeatureRecord.addFeature(f2);
        }

        // lexeme and pos Trigram after
        if (currentFeatureRecord.getEndPosition() < dbSentence.getDbLexemeList().size() - 3) {
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 1);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 2);
            DBLexeme dblM = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 3);
            Feature f = new Feature();
            f.setFeatureType("3gram_lexeme_after");
            f.setFeatureID("3la_" + dblF.getId()
                    + "_" + dblA.getId() + "_" + dblM.getId());
            f.setValue(1);
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("3gram_pos_after");
            f2.setFeatureID("3pa_" + dblF.getPos()
                    + "_" + dblA.getPos() + "_" + dblM.getPos());
            f2.setValue(1);
            currentFeatureRecord.addFeature(f2);
        }

    }

    private void genFourGram() {
        // lexeme and pos Trigram before
        if (currentFeatureRecord.getStartPosition() > 3) {
            DBLexeme dblE = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 4);
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 3);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 2);
            DBLexeme dblM = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 1);

            Feature f = new Feature();
            f.setFeatureType("4gram_lexeme_before");
            f.setFeatureID("4lb_" + dblE.getId() + "_" + dblF.getId() + "_"
                    + dblA.getId() + "_" + dblM.getId());
            f.setValue(1);
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("4gram_pos_before");
            f2.setFeatureID("4bp_" + dblE.getPos() + "_" + dblF.getPos()
                    + "_" + dblA.getPos() + "_" + dblM.getPos());
            f2.setValue(1);
            currentFeatureRecord.addFeature(f2);
        }

        // lexeme and pos Trigram after
        if (currentFeatureRecord.getEndPosition() < dbSentence.getDbLexemeList().size() - 4) {
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 1);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 2);
            DBLexeme dblM = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 3);
            DBLexeme dblN = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 4);

            Feature f = new Feature();
            f.setFeatureType("4gram_lexeme_after");
            f.setFeatureID("4la_" + dblF.getId()
                    + "_" + dblA.getId() + "_" + dblM.getId() + "_" + dblN.getId());
            f.setValue(1);
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("4gram_pos_after");
            f2.setFeatureID("4pa_" + dblF.getPos()
                    + "_" + dblA.getPos() + "_" + dblM.getPos() + "_" + dblN.getPos());
            f2.setValue(1);
            currentFeatureRecord.addFeature(f2);
        }
    }

    private void genTermPoSChain() {
        Feature phPoSGram = new Feature();
        phPoSGram.setFeatureType("phrase-pos-list");
        String posOfPhase = "";
        for (int i = currentFeatureRecord.getStartPosition();
                i < currentFeatureRecord.getEndPosition() + 1; i++) {
            posOfPhase += dbSentence.getDbLexemeList().get(i).getPos() + "_";
        }
        phPoSGram.setFeatureID("php_" + posOfPhase);
        phPoSGram.setValue(1);
        phPoSGram.setDesc("simply the pos tag list for candid phrase");
        currentFeatureRecord.addFeature(phPoSGram);
    }

    private void genDepBasedFeature() {
        sentDep =
                dbSentence.getSentenceDependencyList();
        for (int i = currentFeatureRecord.getStartPosition();
                i < currentFeatureRecord.getEndPosition() + 1; i++) {
            for (int j = 0; j < sentDep.size(); j++) {
                if (sentDep.get(j).getGovernorPosition() == i) {
                    DBDependency dbdep = new DBDependency();
                    dbdep = sentDep.get(j).getDbDependency();
                    Feature f = new Feature();
                    f.setFeatureType("dependency_lexeme_id_govener");
                    f.setFeatureID("dlig_" + dbdep.getDependencyID());
                    f.setValue(1);
                    f.setDesc("dependency id from db");
                    currentFeatureRecord.addFeature(f);
                    Feature f2 = new Feature();
                    f2.setFeatureType("dependency_type");
                    f2.setFeatureID("dligt_" + dbdep.getDependencyType());
                    f2.setValue(1);
                    currentFeatureRecord.addFeature(f2);

                    Feature f3 = new Feature();
                    f3.setFeatureType("dependency_gov_lexeme");
                    f3.setFeatureID("dligl_" + dbdep.getGovernorLexemeID());
                    f3.setValue(1);
                    f3.setDesc("lexeme id for the word that is in gramatical relation to phrase");
                    currentFeatureRecord.addFeature(f3);

                    Feature f4 = new Feature();
                    f4.setFeatureType("dependency_gov_lexeme");
                    f4.setFeatureID("dliglt_" + sentDep.get(j).getDbDependency().getDependencyType() + "_"
                            + dbdep.getGovernorLexemeID());
                    f4.setValue(1);
                    f4.setDesc("lexeme id together with the dep type for the word that is in gramatical relation to phrase");
                    currentFeatureRecord.addFeature(f4);
                }
            }

            for (int j = 0; j < sentDep.size(); j++) {
                if (sentDep.get(j).getRegentPosition() == i) {
                    DBDependency dbdep = new DBDependency();
                    dbdep = sentDep.get(j).getDbDependency();

                    Feature f = new Feature();
                    f.setFeatureType("dependency_lexeme_id_regent");
                    f.setFeatureID("dlir_" + dbdep.getDependencyID());
                    f.setValue(1);
                    f.setDesc("dependency id from db");
                    currentFeatureRecord.addFeature(f);
                    Feature f2 = new Feature();
                    f2.setFeatureType("dependency_type_regent");
                    f2.setFeatureID("dlirt_" + dbdep.getDependencyType());
                    f2.setValue(1);
                    currentFeatureRecord.addFeature(f2);

                    Feature f3 = new Feature();
                    f3.setFeatureType("dependency_gov_lexeme");
                    f3.setFeatureID("dlirl_" + dbdep.getGovernorLexemeID());
                    f3.setValue(1);
                    f3.setDesc("lexeme id for the word that is in gramatical relation to phrase");
                    currentFeatureRecord.addFeature(f3);

                    Feature f4 = new Feature();
                    f4.setFeatureType("dependency_gov_lexeme");
                    f4.setFeatureID("dlirlt_" + dbdep.getDependencyType() + "_"
                            + dbdep.getGovernorLexemeID());
                    f4.setValue(1);
                    f4.setDesc("lexeme id together with the dep type for the word that is in gramatical relation to phrase");
                    currentFeatureRecord.addFeature(f4);

                }
            }

        }
    }
    
public List<int[]> extractCandidTermsOpenNLP(DBSentence dbs) {
     List<int[]> boundaryLists = new ArrayList<int[]>();
     
     return boundaryLists;
}
    private List<int[]> extractCandidTerms(DBSentence dbs) {
        List<int[]> boundaryLists = new ArrayList<int[]>();
        for (int i = 0; i < dbs.getDbLexemeList().size(); i++) {
            String pos = dbs.getDbLexemeList().get(i).getPos();
            String word = dbs.getDbLexemeList().get(i).getWord();
            // this if means a candid term should necessary start with
            // one of the following PoSs
            if ((word.equalsIgnoreCase("the")
                    || pos.equals("FW")
                    || pos.equals("NN")
                    || pos.equals("NP")
                    || pos.equals("NPS")
                    || pos.equals("NNS")
                    || pos.equals("NNP")
                    || pos.equals("NNPS")
                    || pos.equals("JJ"))
                    && (!TTRefineStopWord.isStopWord(word))) {
                int startPosition = i;
                int endPosition = i;
                boolean innerLoop = false;
                int j = i;
                if (j + 1 < dbs.getDbLexemeList().size()) {
                    for (j = i + 1; j < dbs.getDbLexemeList().size(); j++) {

                        String posx = dbs.getDbLexemeList().get(j).getPos();
                        if ((posx.equals("NN")
                                || posx.equals("NP")
                                || posx.equals("NPS")
                                || posx.equals("NNS")
                                || posx.equals("NNP")
                                || posx.equals("NNPS")
                                || posx.equals("VBG")
                                || posx.equals("VBN")
                                || posx.equals("JJ")
                                || posx.equals("POS")
                                || posx.equals("CC")
                                || posx.equals("IN")
                                || posx.equals("SYM")
                                || posx.equals("TO")) && (!TTRefineStopWord.isStopWord(word))) {
                            endPosition = j;
                            i = j;
                            innerLoop = true;
                        } else {
                            i = j;
                            List<int[]> l = new ArrayList<int[]>();
                            if (!(dbs.getDbLexemeList().get(endPosition).getPos().equals("VBN")
                                    || dbs.getDbLexemeList().get(endPosition).getPos().equals("VBG")
                                    || dbs.getDbLexemeList().get(endPosition).getPos().equals("POS")
                                    || dbs.getDbLexemeList().get(endPosition).getPos().equals("CC")
                                    || dbs.getDbLexemeList().get(endPosition).getPos().equals("IN")
                                    || dbs.getDbLexemeList().get(endPosition).getPos().equals("TO"))) {
                                l = getCandidPhraseBoundaries(startPosition, endPosition, dbs.getDbLexemeList());
                                boundaryLists.addAll(l);
                            }
                            innerLoop = false;
                            break;
                        }
                    }
                }
                if (innerLoop) {
                    boundaryLists.addAll(getCandidPhraseBoundaries(startPosition, endPosition, dbs.getDbLexemeList()));
                }
            }
        }
        postProcessBoundaryLists(boundaryLists, dbs);
        return boundaryLists;

    }

    private List<int[]> getCandidPhraseBoundaries(int lowerBound, int upperBound, List<DBLexeme> dbLexemeList) {
        List<int[]> phraseBoundary = new ArrayList<int[]>();

        // ezafe kardan in shart ke bishtar az do ta harf e ezafe nemishe dasht
        int countStopWords = 0;
        for (int k = lowerBound; k <= upperBound; k++) {
            if (!(dbLexemeList.get(k).getPos().equals("IN")
                    || dbLexemeList.get(k).getPos().equals("CC")
                    || dbLexemeList.get(k).getPos().equals("VBG")
                    || dbLexemeList.get(k).getPos().equals("VBN")
                    || dbLexemeList.get(k).getPos().equals("POS")
                    || dbLexemeList.get(k).getPos().equals("TO"))) {
                countStopWords = 0;
                for (int i = k; i <= upperBound; i++) {
                    if ((dbLexemeList.get(i).getPos().equals("IN")
                            || dbLexemeList.get(i).getPos().equals("CC")
                            || dbLexemeList.get(i).getPos().equals("VBG")
                            || dbLexemeList.get(i).getPos().equals("VBN")
                            || dbLexemeList.get(i).getPos().equals("POS")
                            || dbLexemeList.get(i).getPos().equals("TO"))) {
                        countStopWords++;
                    }
                    int[] phraseBound = new int[2];
                    phraseBound[0] = k;
                    phraseBound[1] = i;
                    if (!(dbLexemeList.get(i).getPos().equals("IN")
                            || dbLexemeList.get(i).getPos().equals("CC")
                            || dbLexemeList.get(i).getPos().equals("VBG")
                            || dbLexemeList.get(i).getPos().equals("VBN")
                            || dbLexemeList.get(i).getPos().equals("POS")
                            || dbLexemeList.get(i).getPos().equals("TO"))) {
                        if (countStopWords < 3) {
                            phraseBoundary.add(phraseBound);
                        }
                    }
                }
            }
        }
        // search me here
        // postProcessBoundaryLists(phraseBoundary, dbSentence);

        return phraseBoundary;

    }

    public List<FeatureRecordPredict> getSentenceFeatRecordList() {
        return sentenceFeatRecordList;
    }

    private void postProcessBoundaryLists(List<int[]> boundaryLists, DBSentence dbs) {
        for (int i = 0; i < boundaryLists.size(); i++) {
            int[] boundary = boundaryLists.get(i);

            if (boundary[0] == boundary[1]) {
              
                if (dbs.getDbLexemeList().get(boundary[0]).getPos().equalsIgnoreCase("dt")) {
                    boundaryLists.remove(i);
                }
//                 else if (wordNet.isAdjective(dbs.getDbLexemeList().get(boundary[0]).getWord())) {
//                    // we remove all the words that are listed as adjective in wordnet
//                    boundaryLists.remove(i);
//                    // we check if it is name 
//                } else if (wordNet.isNoun(dbs.getDbLexemeList().get(boundary[0]).getWord())) {
//                    // if yes then it should not have more than 3 hypernym then
//                    String[] s = wordNet.getAllHypernyms(
//                            dbs.getDbLexemeList().get(boundary[0]).getWord(),
//                            RiWordnet.NOUN);
//                    if (s != null) {
//                        if (s.length > 3) {
//                            // too generic
//                            boundaryLists.remove(i);
//                        }
//                    }
//
//                }
                // all other rules for single word terms can be listed here
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().length() == 1) {
                boundaryLists.remove(i);
            } else if ("IN".equals(dbs.getDbLexemeList().get(boundary[0]).getPos())) {
                boundaryLists.remove(i);
            } else if ("TO".equals(dbs.getDbLexemeList().get(boundary[0]).getPos())) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().startsWith("<")) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().startsWith("%")) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().startsWith("+")) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().startsWith("#")) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().startsWith("\\/")) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().startsWith("^")) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().startsWith("_")) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().startsWith("~")) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().startsWith("+")) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().startsWith("based")) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[0]).getWord().startsWith("able")) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[1]).getWord().length() == 1) {
                boundaryLists.remove(i);
            } else if ("IN".equals(dbs.getDbLexemeList().get(boundary[1]).getPos())) {
                boundaryLists.remove(i);
            } else if ("TO".equals(dbs.getDbLexemeList().get(boundary[1]).getPos())) {
                boundaryLists.remove(i);
            } else if (dbs.getDbLexemeList().get(boundary[1]).getWord().startsWith(">")) {
                boundaryLists.remove(i);
            }

        }
    }

    public void setDbSentence(DBSentence dbSentence) {
        this.dbSentence = dbSentence;
    }
}
