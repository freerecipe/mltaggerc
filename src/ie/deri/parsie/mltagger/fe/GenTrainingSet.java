/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.fe;


import ie.deri.parsie.mltagger.object.FeatMap;
import ie.deri.parsie.mltagger.object.FeatureVector;
import ie.deri.parsie.mltagger.object.DBFeatureValue;
import ie.deri.parsie.mltagger.object.DBFeature;
import ie.deri.parsie.mltagger.object.BootstrapTerm;
import ie.deri.parsie.mltagger.db.MLTaggerRetrieve;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class GenTrainingSet {

    MLTaggerRetrieve dbr;
    List<FeatureVector> fvList;
    Map<Double, Integer> sdhFeatureTTIMap;
    List<DBFeature> dbFeatureList;

    public void init(MLTaggerRetrieve dbr) {
        this.dbr = dbr;
        sdhFeatureTTIMap = new HashMap<Double, Integer>();
        dbFeatureList = new ArrayList<DBFeature>();
    }

    public void setSdhFeatureTTIMap(Map<Double, Integer> sdhFeatureTTIMap) {
        this.sdhFeatureTTIMap = sdhFeatureTTIMap;
    }


    public void genTrainingSetForSentence() throws SQLException, IOException {
        Map<Double, BootstrapTerm> bootMap =
                new HashMap<Double, BootstrapTerm>();
        List<BootstrapTerm> bootList = dbr.retBootstrapTerm();
        for (BootstrapTerm boot : bootList) {
            if (!bootMap.containsKey(boot.getId())) {
                bootMap.put(boot.getId(), boot);
            }
        }
        //loadDBOFeatureTTIMapFromDB();
        System.err.println("Feature Table Generated...");

        ResultSet bootstrapInstRs = dbr.retBootstrapInstance();
        fvList = new ArrayList<FeatureVector>();
        System.err.println("Generating Training set");
        while (bootstrapInstRs.next()) {
            FeatureVector fv = new FeatureVector();
            double sourceSentenceID = bootstrapInstRs.getDouble("sentence_id");
            double bootInstID = bootstrapInstRs.getDouble("bootstrap_instance_id");
            double bootID = bootstrapInstRs.getDouble("bootstrap_id");
            fv.setSourceSentenceID(sourceSentenceID);
//            fv.setBootstrapID(bootInstID);
            fv.setBootstrapID(bootID);
            fv.setAnnotation(bootMap.get(bootID).isPositiveExample());
            List<DBFeatureValue> dbfvList = dbr.retFeatureTTIForBootstrapInstance(
                    bootInstID);
            for (DBFeatureValue dbfv : dbfvList) {
                int numericID = (Integer) sdhFeatureTTIMap.get(dbfv.getFeatureDBID());
                dbfv.setNumericID(numericID);
                double normalVal = dbfv.getValue() / dbfv.getFreq();
                dbfv.setValue(normalVal);
            }
            Collections.sort(dbfvList);
            fv.setDbfeatureValueList(dbfvList);
            fvList.add(fv);
        }
        System.err.println("done !" + fvList.size());

    }

    /*
     * for all bootstap_instance_id
     *  get bootstrap annotation
     * for all bootstrap feature id get feat numeric key and value
     *
     *
     */


    public boolean loadDBOFeatureTTIMapFromDB() throws SQLException, IOException {
        // black list can go here
        System.err.println("Loading TTI feature table from DB");
        ResultSet rs = this.dbr.retFeatureTTI();
        int featureNumericID = 1;
        while (rs.next()) {
            double featDBID = rs.getDouble("feature_db_id");
            String featType = rs.getString("feature_type");
            String featID = rs.getString("feature_id");
            DBFeature dBFeature = new DBFeature();

            dBFeature.setFeatureDBID(featDBID);
            dBFeature.setFeatureID(featID);
            dBFeature.setFeatureType(featType);
            dbFeatureList.add(dBFeature);

            if (!this.sdhFeatureTTIMap.containsKey(featDBID)) {
                this.sdhFeatureTTIMap.put(featDBID, featureNumericID);
                featureNumericID++;
            }
        }
        rs.close();
        System.err.println("Done loading feature map of size " + featureNumericID );
        return true;
    }



    public boolean storeDBFeatureMap(String fileName) throws IOException {
        System.err.println("Storing DBO feature TTI final map to " + fileName);
        File outputFile = new File(fileName);
        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream(outputFile));
        FeatMap fm;
        for (DBFeature dbftti : dbFeatureList) {
            fm = new FeatMap();
            fm.setFeatureDBID(dbftti.getFeatureDBID());
            fm.setFeatureNumericID(sdhFeatureTTIMap.get(dbftti.getFeatureDBID()));
            fm.setFeatureType(dbftti.getFeatureType());
            fm.setFeatureRealID(dbftti.getFeatureID());
            out.writeObject(fm);
        }
        out.writeObject(null);
        out.flush();
        out.close();
        System.err.println("Done storing dbo feature tti");
        return true;
    }

//    // not this one and the next one really usefull
//    public boolean storeDBOFeatureTTIMAP(String fileName) throws IOException {
//        System.err.println("Storing TTI feature map to " + fileName);
//        FeatMap fm = null;
//        File outputFile = new File(fileName);
//        ObjectOutputStream out = new ObjectOutputStream(
//                new FileOutputStream(outputFile));
//
//        for (String featIndex : sdhFeatureTTIMap.keySet()) {
//            fm = new FeatMap();
//            fm.setFeatureDBID(featIndex);
//            fm.setFeatureNumericID(sdhFeatureTTIMap.get(featIndex));
//
//            out.writeObject(fm);
//        }
//        out.writeObject(null);
//        out.flush();
//        out.close();
//        System.err.println("Done storing feature map");
//        return true;
//    }
//
//    public boolean storeDBFeatureTTIList(String fileName) throws IOException {
//        System.err.println("Storing DBO feature TTI to " + fileName);
//        File outputFile = new File(fileName);
//        ObjectOutputStream out = new ObjectOutputStream(
//                new FileOutputStream(outputFile));
//        for (DBFeature dbftti : dbFeatureList) {
//            out.writeObject(dbftti);
//        }
//        out.writeObject(null);
//        out.flush();
//        out.close();
//        System.err.println("Done storing dbo feature tti");
//        return true;
//    }
//




    public void dumpTrainingSet(String filePath) throws IOException {
        File f = new File(filePath);
        FileWriter writer = new FileWriter(f);
        for (FeatureVector fv : fvList) {

            int label = fv.isAnnotation() ? +1 : -1;
            writer.write(label + " ");
            for (DBFeatureValue dbfv : fv.getDbfeatureValueList()) {
                writer.write(dbfv.getNumericID() + ":" + dbfv.getValue() + " ");
            }
            writer.write("\n");
        }
        writer.close();
    }

    public void dumpFoldedTrainingSet(String filePath) throws IOException, SQLException {
        Map<Double, BootstrapTerm> bootMap =
                new HashMap<Double, BootstrapTerm>();
        List<BootstrapTerm> bootList = dbr.retBootstrapTerm();
        for (BootstrapTerm boot : bootList) {
            if (!bootMap.containsKey(boot.getId())) {
                bootMap.put(boot.getId(), boot);
            }
        }
        List<FeatureVector> fvTrainList = new ArrayList<FeatureVector>();
        List<FeatureVector> fvTestList = new ArrayList<FeatureVector>();
        int j = 0;
        int limit = bootList.size() * 1 /2;
        System.err.println("There will be "  +limit + " train records");
        for (int i = 0; i <  limit; i++) {
            ResultSet bootstrapInstRs = dbr.retBootstrapInstanceByBootstrapID(bootList.get(i).getId());
            fvTrainList.addAll(getFeatVector( bootMap, bootstrapInstRs));
            j++;
        }

        for (int i = j+1; i < bootList.size(); i++) {
            ResultSet bootstrapInstRs = dbr.retBootstrapInstanceByBootstrapID(bootList.get(i).getId());
            fvTestList.addAll(getFeatVector(bootMap, bootstrapInstRs));
        }

        File train = new File(filePath + "_trainFolded");
        FileWriter writerTrain = new FileWriter(train);
        for (FeatureVector fv : fvTrainList) {
            int label = fv.isAnnotation() ? +1 : -1;
            writerTrain.write(label + " ");
            for (DBFeatureValue dbfv : fv.getDbfeatureValueList()) {
                writerTrain.write(dbfv.getNumericID() + ":" + dbfv.getValue() + " ");
            }
            writerTrain.write("\n");
        }
        writerTrain.close();

        File test = new File(filePath + "_testFolded");
        FileWriter writerTest = new FileWriter(test);
        for (FeatureVector fv : fvTestList) {
            int label = fv.isAnnotation() ? +1 : -1;
            writerTest.write(label + " ");
            for (DBFeatureValue dbfv : fv.getDbfeatureValueList()) {
                writerTest.write(dbfv.getNumericID() + ":" + dbfv.getValue() + " ");
            }
            writerTest.write("\n");
        }
        writerTest.close();

    }

    private List<FeatureVector> getFeatVector(Map<Double, BootstrapTerm> bootMap, ResultSet bootstrapInstRs) throws SQLException {
        List<FeatureVector> fvectList = new ArrayList<FeatureVector>();
        while (bootstrapInstRs.next()) {
            FeatureVector fv = new FeatureVector();
            double bootInstID = bootstrapInstRs.getDouble("bootstrap_instance_id");
            double bootID = bootstrapInstRs.getDouble("bootstrap_id");
//            fv.setBootstrapID(bootInstID);
            fv.setBootstrapID(bootID);
            fv.setAnnotation(bootMap.get(bootID).isPositiveExample());
            List<DBFeatureValue> dbfvList = dbr.retFeatureTTIForBootstrapInstance(
                    bootInstID);
            for (DBFeatureValue dbfv : dbfvList) {
                int numericID = (Integer) sdhFeatureTTIMap.get(dbfv.getFeatureDBID());
                dbfv.setNumericID(numericID);
                double normalVal = dbfv.getValue() / dbfv.getFreq();
                dbfv.setValue(normalVal);
            }
            Collections.sort(dbfvList);
            fv.setDbfeatureValueList(dbfvList);
            fvectList.add(fv);
        }
        return fvectList;
    }



    public void dumpTrainingSetForExperiment1(String filePath) throws IOException {
        File f = new File(filePath);
        FileWriter writer = new FileWriter(f);
        for (FeatureVector fv : fvList) {
            writer.write(fv.getSourceSentenceID() + "\t");
            writer.write(fv.getBootstrapID() + "\t");
            int label = fv.isAnnotation() ? +1 : -1;
            writer.write(label + " ");
            for (DBFeatureValue dbfv : fv.getDbfeatureValueList()) {
                writer.write(dbfv.getNumericID() + ":" + dbfv.getValue() + " ");
            }
            writer.write("\n");
        }
        writer.close();
    }


      public void storeDBFeatureMapForExperiment1(String fileName) throws IOException {
        File outputFile = new File(fileName);
        FileWriter writer = new FileWriter(outputFile);
          for (DBFeature dbftti : dbFeatureList) {
//              writer.write(dbftti.getFeatureDBID() + "\t");
              writer.write(sdhFeatureTTIMap.get(dbftti.getFeatureDBID()) + "\t");
              writer.write(dbftti.getFeatureType()+"\t");
              writer.write(dbftti.getFeatureID()+"\t");
              writer.write("\n");
        }
        writer.flush();
        writer.close();

    }
}
