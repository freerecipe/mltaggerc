/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.fe;

import ie.deri.parsie.mltagger.object.FeatureRecord;
import ie.deri.parsie.mltagger.object.Feature;
import ie.deri.nlp.db.objects.DBDependency;
import ie.deri.nlp.db.objects.DBLexeme;
import ie.deri.nlp.db.objects.DBSentence;
import ie.deri.nlp.db.objects.DBSentenceDependency;
import ie.deri.parsie.mltagger.db.MLTaggerIndex;
import ie.deri.parsie.mltagger.db.MLTaggerRetrieve;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class TTIFeatureExtraction {

    private MLTaggerIndex dba;
    private MLTaggerRetrieve dbr;
    private DBSentence dbSentence;
    List<DBSentenceDependency> sentDep;
    FeatureRecord currentFeatureRecord;
    Set<DBLexeme> lexemesAsFeatureSet;
    Set<DBDependency> dependenciesAsFeatureSet;

    public TTIFeatureExtraction(MLTaggerIndex dba, MLTaggerRetrieve dbr) {
        this.dba = dba;
        this.dbr = dbr;
        lexemesAsFeatureSet = new HashSet<DBLexeme>();
        dependenciesAsFeatureSet = new HashSet<DBDependency>();
    }

    public void generateFeatures() throws SQLException {
        ResultSet allSentenceInstances = dbr.retDistinctSentenceBootstrapInstance();
        ResultSet allBootstrapInstances = null;
        while (allSentenceInstances.next()) {
            sentDep = new ArrayList<DBSentenceDependency>();
            dbSentence = new DBSentence();
            double currentSentenceID = allSentenceInstances.getDouble("sentence_id");
            dbSentence = dbr.retSentenceByID(currentSentenceID);
            allBootstrapInstances =
                    dbr.retBootstrapsInstancesForSentence(currentSentenceID);
            while (allBootstrapInstances.next()) {
                currentFeatureRecord = new FeatureRecord();
                double bootstrapInstanceID = allBootstrapInstances.getDouble("bootstrap_instance_id");
                int startPositio = allBootstrapInstances.getInt("start_position");
                int endPositio = allBootstrapInstances.getInt("end_position");
                currentFeatureRecord.setBootstrapInstanceID(bootstrapInstanceID);
                currentFeatureRecord.setStartPosition(startPositio);
                currentFeatureRecord.setEndPosition(endPositio);
                genUniGram();
                genBiGram();
                genTriGram();
                genFourGram();
                genTermPoSChain();
                // a bit of work with dependencies
                genDepBasedFeature();
                // index features then

                indexCurrentFeatures();

            }
            allBootstrapInstances.close();
            lexemesAsFeatureSet.addAll(dbSentence.getDbLexemeList());
            for (DBSentenceDependency dbd : sentDep) {
                dependenciesAsFeatureSet.add(dbd.getDbDependency());
                lexemesAsFeatureSet.add(dbr.retLexemeByID(dbd.getDbDependency().getGovernorLexemeID()));
                lexemesAsFeatureSet.add(dbr.retLexemeByID(dbd.getDbDependency().getRegentLexemeID()));
            }
        }
        allSentenceInstances.close();
    }

// lexeme and pos before 1gram
    
    private void genUniGram() {
        if (currentFeatureRecord.getStartPosition() > 0) {
            Feature f = new Feature();
            DBLexeme dbl = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 1);
            f.setFeatureType("lex_before");
            f.setFeatureID("lb_" + dbl.getId());
            f.setValue(1);
            f.setDesc("keyword lb_ + LexemeID");
            f.setSource("pdfprocess");

            currentFeatureRecord.addFeature(f);
            Feature f2;
            f2 = new Feature();
            f2.setFeatureType("pos_bef");
            f2.setFeatureID("pb_" + dbl.getPos());
            f2.setValue(1);
            f2.setSource("Penn Pos taggs");
            f2.setDesc("`Keyword pb_ + Penn_PoS  before");
            currentFeatureRecord.addFeature(f2);

        }
        // lexeme and pos after 1gram
        if (currentFeatureRecord.getEndPosition() <
                dbSentence.getDbLexemeList().size() - 1) {
            Feature f = new Feature();
            f.setFeatureType("lex_after");
            DBLexeme dbl = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 1);
            f.setFeatureID("la_" + dbl.getId());
            f.setValue(1);
            f.setDesc("Keyword la_ + LexemeID");

            currentFeatureRecord.addFeature(f);
            Feature f2;
            f2 = new Feature();
            f2.setFeatureType("pos_after");
            f2.setFeatureID("pa_" + dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 1).getPos());
            f2.setValue(1);
            f2.setDesc("keyword pa_ + Penn PoS tag");
            currentFeatureRecord.addFeature(f2);
        }

    }

    private void genBiGram() {
        // lexeme and pos bigram before
        if (currentFeatureRecord.getStartPosition() > 1) {
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 2);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 1);
            Feature f = new Feature();
            f.setFeatureType("2gram_lexeme_before");
            f.setFeatureID("2lb_" + dblF.getId() + "_"+ dblA.getId());
            f.setDesc("keyword 2lb + lexeme id");
            f.setValue(1);
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("2gram_pos_before");
            f2.setFeatureID("2pb_" + dblF.getPos()+ "_" + dblA.getPos());
            f2.setDesc("keyword 2pb_ + lexeme id");
            f2.setValue(1);
            currentFeatureRecord.addFeature(f2);
        }

        // lexeme and pos bigram after
        if (currentFeatureRecord.getEndPosition() < dbSentence.getDbLexemeList().size() - 2) {
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 1);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 2);
            Feature f = new Feature();
            f.setFeatureType("2gram_lexeme_after");
            f.setFeatureID("2la_" + dblF.getId() +"_"+ dblA.getId());
             f.setDesc("keyword 2la_ + 2 lexeme id");
            f.setValue(1);
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("2gram_pos_after");
            f2.setFeatureID("2pa_" + dblF.getPos() +"_"+ dblA.getPos());
            f.setDesc("keyword 2pa_ + 2 lexeme id");
            f2.setValue(1);
            currentFeatureRecord.addFeature(f2);
        }

    }

    private void genTriGram() {
        // lexeme and pos Trigram before
        if (currentFeatureRecord.getStartPosition() > 2) {
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 3);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 2);
            DBLexeme dblM = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 1);

            Feature f = new Feature();
            f.setFeatureType("3gram_lexeme_before");
            f.setFeatureID("3lb_" + dblF.getId() +"_"+
                    dblA.getId() +"_"+ dblM.getId());
            f.setDesc("keyword 3lb_ + 3 lexeme id");
            f.setValue(1);
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("3gram_pos_before");
            f2.setFeatureID("3bp_" + dblF.getPos()
                    +"_"+ dblA.getPos() +"_"+ dblM.getPos());
            f2.setValue(1);
            f2.setDesc("keyword 3bp_ + 3 pen id");
            currentFeatureRecord.addFeature(f2);
        }

        // lexeme and pos Trigram after
        if (currentFeatureRecord.getEndPosition() < dbSentence.getDbLexemeList().size() - 3) {
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 1);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 2);
            DBLexeme dblM = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 3);
            Feature f = new Feature();
            f.setFeatureType("3gram_lexeme_after");
            f.setFeatureID("3la_" + dblF.getId()
                    +"_"+ dblA.getId() + "_" + dblM.getId());
            f.setValue(1);
              f.setDesc("keyword 3la_ + 3 lexeme_id");
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("3gram_pos_after");
            f2.setFeatureID("3pa_" + dblF.getPos()
                   + "_" +  dblA.getPos() + "_" +  dblM.getPos());
            f2.setValue(1);
            f2.setDesc("keyword 3pa_ + 3 pen id");
            currentFeatureRecord.addFeature(f2);
        }

    }

     private void genFourGram() {
        // lexeme and pos Trigram before
        if (currentFeatureRecord.getStartPosition() > 3) {
            DBLexeme dblE = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 4);
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 3);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 2);
            DBLexeme dblM = dbSentence.getDbLexemeList().get(currentFeatureRecord.getStartPosition() - 1);

            Feature f = new Feature();
            f.setFeatureType("4gram_lexeme_before");
            f.setFeatureID("4lb_" +dblE.getId()+"_"+ dblF.getId() + "_"
                    + dblA.getId() + "_" + dblM.getId());
            f.setValue(1);
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("4gram_pos_before");
            f2.setFeatureID("4bp_" + dblE.getPos()+"_"+dblF.getPos()
                    + "_" + dblA.getPos() + "_" + dblM.getPos());
            f2.setValue(1);
            currentFeatureRecord.addFeature(f2);
        }

        // lexeme and pos Trigram after
        if (currentFeatureRecord.getEndPosition() < dbSentence.getDbLexemeList().size() - 4) {
            DBLexeme dblF = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 1);
            DBLexeme dblA = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 2);
            DBLexeme dblM = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 3);
            DBLexeme dblN = dbSentence.getDbLexemeList().get(currentFeatureRecord.getEndPosition() + 4);

            Feature f = new Feature();
            f.setFeatureType("4gram_lexeme_after");
            f.setFeatureID("4la_" +  dblF.getId()
                    + "_" + dblA.getId() + "_" + dblM.getId()+"_"+ dblN.getId());
            f.setValue(1);
            currentFeatureRecord.addFeature(f);
            Feature f2 = new Feature();
            f2.setFeatureType("4gram_pos_after");
            f2.setFeatureID("4pa_" + dblF.getPos()
                    + "_" + dblA.getPos() + "_" + dblM.getPos()+"_"+dblN.getPos());
            f2.setValue(1);
            currentFeatureRecord.addFeature(f2);
        }
    }
  
    private void genTermPoSChain() {
        Feature phPoSGram = new Feature();
        phPoSGram.setFeatureType("phrase-pos-list");
        String posOfPhase = "";
        for (int i = currentFeatureRecord.getStartPosition();
                i < currentFeatureRecord.getEndPosition() + 1; i++) {
            posOfPhase += dbSentence.getDbLexemeList().get(i).getPos()+"_";
        }
        phPoSGram.setFeatureID("php_" + posOfPhase);
        phPoSGram.setValue(1);
        phPoSGram.setDesc("simply the pos tag list for candid phrase");
        currentFeatureRecord.addFeature(phPoSGram);
    }

    private void genDepBasedFeature() {
        sentDep =
                dbSentence.getSentenceDependencyList();
        for (int i = currentFeatureRecord.getStartPosition();
                i < currentFeatureRecord.getEndPosition() + 1; i++) {
            for (int j = 0; j < sentDep.size(); j++) {
                if (sentDep.get(j).getGovernorPosition() == i) {
                    DBDependency dbdep = new DBDependency();
                    dbdep = sentDep.get(j).getDbDependency();
                    Feature f = new Feature();
                    f.setFeatureType("dependency_lexeme_id_govener");
                    f.setFeatureID("dlig_" + dbdep.getDependencyID());
                    f.setValue(1);
                    f.setDesc("dependency id from db");
                    currentFeatureRecord.addFeature(f);
                    Feature f2 = new Feature();
                    f2.setFeatureType("dependency_type");
                    f2.setFeatureID("dligt_" + dbdep.getDependencyType());
                    f2.setValue(1);
                    f2.setDesc("dependency id from db");
                    currentFeatureRecord.addFeature(f2);

                    Feature f3 = new Feature();
                    f3.setFeatureType("dependency_gov_lexeme");
                    f3.setFeatureID("dligl_" + dbdep.getGovernorLexemeID());
                    f3.setValue(1);
                    f3.setDesc("lexeme id for the word that is in gramatical relation to phrase");
                    currentFeatureRecord.addFeature(f3);

                    Feature f4 = new Feature();
                    f4.setFeatureType("dependency_gov_lexeme");
                    f4.setFeatureID("dliglt_" + sentDep.get(j).getDbDependency().getDependencyType()+"_" +
                            dbdep.getGovernorLexemeID());
                    f4.setValue(1);
                    f4.setDesc("lexeme id together with the dep type for the word that is in gramatical relation to phrase");
                    currentFeatureRecord.addFeature(f4);
                }
            }

             for (int j = 0; j < sentDep.size(); j++) {
                if (sentDep.get(j).getRegentPosition() == i) {
                    DBDependency dbdep = new DBDependency();
                    dbdep = sentDep.get(j).getDbDependency();

                    Feature f = new Feature();
                    f.setFeatureType("dependency_lexeme_id_regent");
                    f.setFeatureID("dlir_" + dbdep.getDependencyID());
                    f.setValue(1);
                    f.setDesc("dependency id from db");
                    currentFeatureRecord.addFeature(f);
                    Feature f2 = new Feature();
                    f2.setFeatureType("dependency_type_regent");
                    f2.setFeatureID("dlirt_" + dbdep.getDependencyType());
                    f2.setValue(1);
                    currentFeatureRecord.addFeature(f2);

                    Feature f3 = new Feature();
                    f3.setFeatureType("dependency_gov_lexeme");
                    f3.setFeatureID("dlirl_" + dbdep.getGovernorLexemeID());
                    f3.setValue(1);
                    f3.setDesc("lexeme id for the word that is in gramatical relation to phrase");
                    currentFeatureRecord.addFeature(f3);

                     Feature f4 = new Feature();
                    f4.setFeatureType("dependency_gov_lexeme");
                    f4.setFeatureID("dlirlt_" + dbdep.getDependencyType()+"_" +
                            dbdep.getGovernorLexemeID());
                    f4.setValue(1);
                    f4.setDesc("lexeme id together with the dep type for the word that is in gramatical relation to phrase");
                    currentFeatureRecord.addFeature(f4);

                }
            }

        }
    }


    private void indexCurrentFeatures() throws SQLException {
        for (Feature f : currentFeatureRecord.getFeatureList()) {
            dba.indexFeatureTTI(f);
//            System.err.println("Indexing : " +  currentFeatureRecord.getBootstrapInstanceID()+" >> "+  f.getFeatureDBID());
            dba.indexFeatureTTIInstance(
                    currentFeatureRecord.getBootstrapInstanceID(), f.getFeatureDBID(), f.getValue());

        }

    }

   public boolean storeDBLExemeAsFeat(String fileName) throws IOException {
        System.err.println("Storing DBLexeme as feature to " + fileName);

        File outputFile = new File(fileName);
        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream(outputFile));
        for (DBLexeme dbl : lexemesAsFeatureSet) {
            out.writeObject(dbl);
        }
        out.writeObject(null);
        out.flush();
        out.close();
        System.err.println("Done ");
        return true;
    }

    public boolean storeDBDependencyAsFeat(String fileName) throws IOException {
        System.err.println("Storing DBDependency as feature to " + fileName);

        File outputFile = new File(fileName);
        if(outputFile.isFile()){
            System.err.println("File");
        } else{
            outputFile.createNewFile();
}
        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream(outputFile));
        for (DBDependency dbd : dependenciesAsFeatureSet) {
            out.writeObject(dbd);
        }
        out.writeObject(null);
        out.flush();
        out.close();
        System.err.println("Done ");
        return true;
    }


}
