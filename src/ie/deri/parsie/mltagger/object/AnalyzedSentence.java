/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

import ie.deri.nlp.analyzer.objects.PLexeme;
import ie.deri.nlp.analyzer.objects.SentenceDependency;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author behqas
 */
public class AnalyzedSentence implements Serializable{

    private List<PLexeme> plexemeList;
    private List<SentenceDependency> sentenceDependency;


    public AnalyzedSentence() {
        plexemeList = new ArrayList<PLexeme>();
        sentenceDependency = new ArrayList<SentenceDependency>();

    }

    public List<PLexeme> getPlexemeList() {
        return plexemeList;
    }

    public List<SentenceDependency> getSentenceDependency() {
        return sentenceDependency;
    }

    public void setPlexemeList(List<PLexeme> plexemeList) {
        this.plexemeList = plexemeList;
    }

    public void setSentenceDependency(List<SentenceDependency> sentenceDependency) {
        this.sentenceDependency = sentenceDependency;
    }







}
