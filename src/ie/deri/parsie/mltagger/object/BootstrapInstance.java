/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

import ie.deri.nlp.db.retrieve.util.PhraseSearchResult;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class BootstrapInstance {

    private double instanceID;
    private double bootstrapID;
    private double sentenceID;
    private int startPosition;
    private int endPosition;

    public void setInstanceID(double instanceID) {
        this.instanceID = instanceID;
    }

    public double getInstanceID() {
        return instanceID;
    }

  
    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }

    public void setSentenceID(double sentenceID) {
        this.sentenceID = sentenceID;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

  
    public int getEndPosition() {
        return endPosition;
    }

    public double getSentenceID() {
        return sentenceID;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setBootstrapInstance(double bootStrapTermID, PhraseSearchResult psr) {
        this.bootstrapID = bootStrapTermID;
        this.endPosition = psr.getEndPosition();
        this.startPosition = psr.getStartPostion();
        this.sentenceID = psr.getSentenceID();
    }

    public BootstrapInstance(double bootStrapTermID, PhraseSearchResult psr) {
        this.bootstrapID = bootStrapTermID;
        this.endPosition = psr.getEndPosition();
        this.startPosition = psr.getStartPostion();
        this.sentenceID = psr.getSentenceID();

    }

    public double getBootstrapID() {
        return bootstrapID;
    }

    public void setBootstrapID(double bootstrapID) {
        this.bootstrapID = bootstrapID;
    }


    public BootstrapInstance() {
    }
}
