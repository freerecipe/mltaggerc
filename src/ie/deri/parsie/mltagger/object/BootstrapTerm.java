/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class BootstrapTerm {

    private double id;
    private String bootstrapString;
    private boolean positiveExample;
    private String source;
    private double jackarValue;
    private double yahooHits;
    private double collocationYahooHits;
    private int length;
    private boolean isReviewed;
    private int frequency;



    public void setIsReviewed(boolean isReviewed) {
        this.isReviewed = isReviewed;
    }

    public boolean isIsReviewed() {
        return isReviewed;
    }



    public void setId(double id) {
        this.id = id;
    }


    public void setPositiveExample(boolean positiveExample) {
        this.positiveExample = positiveExample;
    }

    public void setBootstrapString(String termString) {
        this.bootstrapString = termString;
    }

    public double getId() {
        return id;
    }

    public String getBootstrapString() {
        return bootstrapString;
    }

    public boolean isPositiveExample() {
        return positiveExample;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }






    @Override
    public boolean equals(Object obj) {
        if(obj instanceof BootstrapTerm ){
            BootstrapTerm bt = (BootstrapTerm) obj;
            if(this.bootstrapString.equals(bt.getBootstrapString())){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (this.bootstrapString != null ? this.bootstrapString.hashCode() : 0);
        return hash;
    }

    public void setJackarValue(double jackarValue) {
        this.jackarValue = jackarValue;
    }

    public double getJackarValue() {
        return jackarValue;
    }

    public void setCollocationYahooHits(double collocationYahooHits) {
        this.collocationYahooHits = collocationYahooHits;
    }

    public void setSearchHits(double yahooHits) {
        this.yahooHits = yahooHits;
    }

    public double getCollocationYahooHits() {
        return collocationYahooHits;
    }

    public double getYahooHits() {
        return yahooHits;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getFrequency() {
        return frequency;
    }




    
}
