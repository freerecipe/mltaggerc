/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;



/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class CandidTechTerm {

    private int startPosition;
    private int endPosition;
    private FeatureVector fv;
    private double label;
    private double weight;

    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }

    public int getEndPosition() {
        return endPosition;
    }

    public void setFv(FeatureVector fv) {
        this.fv = fv;
    }

    public void setLabel(double label) {
        this.label = label;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public FeatureVector getFv() {
        return fv;
    }

    public double getLabel() {
        return label;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public double getWeight() {
        return weight;
    }
}
