/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

import java.io.Serializable;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class DBFeature implements Serializable{
    private double featureDBID;
    private String featureType;
    private String featureID;

    public void setFeatureDBID(double featureDBID) {
        this.featureDBID = featureDBID;
    }

    public void setFeatureID(String featureID) {
        this.featureID = featureID;
    }

    public void setFeatureType(String featureType) {
        this.featureType = featureType;
    }

    public double getFeatureDBID() {
        return featureDBID;
    }

    public String getFeatureID() {
        return featureID;
    }

    public String getFeatureType() {
        return featureType;
    }


}
