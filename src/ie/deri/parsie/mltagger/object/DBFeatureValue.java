/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class DBFeatureValue implements Comparable {

    int numericID;
    double featureDBID;
    double value;
    int freq;

    public void setNumericID(int numericID) {
        this.numericID = numericID;
    }

    public int getNumericID() {
        return numericID;
    }

    public void setFeatureDBID(double featureDBID) {
        this.featureDBID = featureDBID;
    }

    public void setFreq(int freq) {
        this.freq = freq;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getFeatureDBID() {
        return featureDBID;
    }

    public int getFreq() {
        return freq;
    }

    public double getValue() {
        return value;
    }

    public int compareTo(Object o) {
        if (!(o instanceof DBFeatureValue)) {
            throw new ClassCastException("A TechnologyTerm object expected.");
        }
        int position = ((DBFeatureValue) o).getNumericID();
        return this.numericID - position;
    }
}
