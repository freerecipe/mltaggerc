/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

import ie.deri.nlp.misc.UID;
import java.io.Serializable;


/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class Feature implements Serializable {
    private double featureDBID;
    private String featureType;
    private String featureID;
    private String source;
    private double value;
    private String desc;
    private int numericID;

    public Feature() {
        featureDBID = UID.getUID();
    }



    public void setFeatureDBID(double featureDBID) {
        this.featureDBID = featureDBID;
    }

    public double getFeatureDBID() {
        return featureDBID;
    }


    public void setFeatureID(String featureID) {
        this.featureID = featureID;
    }

    public void setFeatureType(String featureType) {
        this.featureType = featureType;
    }


    public String getFeatureID() {
        return featureID;
    }

    public String getFeatureType() {
        return featureType;
    }

    public double getValue() {
        return value;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setNumericID(int numericID) {
        this.numericID = numericID;
    }

    public int getNumericID() {
        return numericID;
    }

    public void setValue(double value) {
        this.value = value;
    }







}
