/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class FeatureDependency {
    private double regentLexemeID;
    private double governorLexemeID;
    private String depType;

    public void setDepType(String depType) {
        this.depType = depType;
    }

    public void setGovernorLexemeID(double governorLexemeID) {
        this.governorLexemeID = governorLexemeID;
    }

    public void setRegentLexemeID(double regentLexemeID) {
        this.regentLexemeID = regentLexemeID;
    }

    public String getDepType() {
        return depType;
    }

    public double getGovernorLexemeID() {
        return governorLexemeID;
    }

    public double getRegentLexemeID() {
        return regentLexemeID;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FeatureDependency) {
            FeatureDependency fd = (FeatureDependency) obj;
            if (this.depType.equals(fd.getDepType())
                    && this.regentLexemeID==fd.getRegentLexemeID()
                    && this.governorLexemeID==fd.getGovernorLexemeID() ) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.regentLexemeID) ^ (Double.doubleToLongBits(this.regentLexemeID) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.governorLexemeID) ^ (Double.doubleToLongBits(this.governorLexemeID) >>> 32));
        hash = 97 * hash + (this.depType != null ? this.depType.hashCode() : 0);
        return hash;
    }

  


}
