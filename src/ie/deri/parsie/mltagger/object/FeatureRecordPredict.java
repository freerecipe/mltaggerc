/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;



import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class FeatureRecordPredict {

    private double bootstrapInstanceID;
    private double sentenceID;
    private int startPosition;
    private int endPosition;
    private List<Feature> featureList;
    private Set<DBFeatureValue> dbFeatureValueList;

    public FeatureRecordPredict() {
        featureList = new ArrayList<Feature>();
        dbFeatureValueList = new HashSet<DBFeatureValue>();
    }

    public void setFeatureList(List<Feature> featureList) {
        this.featureList = featureList;
    }

    public List<Feature> getFeatureList() {
        return featureList;
    }


    public void addFeature(Feature feature) {
        this.featureList.add(feature);
    }


    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }


    public void setSentenceID(double sentenceID) {
        this.sentenceID = sentenceID;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }


    public int getEndPosition() {
        return endPosition;
    }


    public double getSentenceID() {
        return sentenceID;
    }

    public int getStartPosition() {
        return startPosition;
    }


    public void setBootstrapInstanceID(double bootstrapInstanceID) {
        this.bootstrapInstanceID = bootstrapInstanceID;
    }

    public double getBootstrapInstanceID() {
        return bootstrapInstanceID;
    }

    public void setDbFeatureValueList(Set<DBFeatureValue> dbFeatureValueList) {
        this.dbFeatureValueList = dbFeatureValueList;
    }

    public Set<DBFeatureValue> getDbFeatureValueList() {
        return dbFeatureValueList;
    }







}
