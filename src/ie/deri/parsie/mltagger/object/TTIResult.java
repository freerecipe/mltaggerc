/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

import java.io.Serializable;

/**
 *
 * @author behqas
 */
public class TTIResult implements Serializable, Comparable {
    private String taggerID;
    private String termString;
    private double confidenceValue;
    private int startPosition;
    private int endPosition;

    public void setConfidenceValue(double confidenceValue) {
        this.confidenceValue = confidenceValue;
    }

    public void setTermString(String termString) {
        this.termString = termString;
    }

    public double getConfidenceValue() {
        return confidenceValue;
    }

    public String getTermString() {
        return termString;
    }

    public void setTaggerID(String taggerID) {
        this.taggerID = taggerID;
    }

    public String getTaggerID() {
        return taggerID;
    }

    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }

    public int getEndPosition() {
        return endPosition;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }






    @Override
    public boolean equals(Object obj) {
        if(obj instanceof TTIResult){
            TTIResult tti = (TTIResult)obj;
            if(this.termString.equalsIgnoreCase(tti.getTermString())){
                return true;
            } else {
                return false;
            }
        }else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + (this.termString != null ? this.termString.hashCode() : 0);
        return hash;
    }

    public int compareTo(Object o) {
        if (!(o instanceof TTIResult)) {
            throw new ClassCastException("Only works for TTI RESULT!");
        }
        TTIResult tti = (TTIResult) o;
        if (this.confidenceValue - tti.getConfidenceValue() > 0) {
            return -1;
         } else if (this.confidenceValue - tti.getConfidenceValue() == 0){
            return 0;
        } else {
            return 1;
        }


    }


}
