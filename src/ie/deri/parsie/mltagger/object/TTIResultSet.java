/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

import ie.deri.nlp.db.objects.DBSentence;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author behqas
 */
public class TTIResultSet implements Serializable{

    private List<TTIResult> ttiResultList;
    private String taggerID;

    public TTIResultSet() {
        ttiResultList = new ArrayList<TTIResult>();
    }

    public String getTaggerID() {
        return taggerID;
    }

    public void setTaggerID(String taggerID) {
        this.taggerID = taggerID;
    }

    public void setTtiResultList(List<TTIResult> ttiResultList) {
        this.ttiResultList = ttiResultList;
    }

    public List<TTIResult> getTtiResultList() {
        return ttiResultList;
    }








}
