/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class TTRefineStopWord {

    static Set<String> stopWord = new HashSet<String>();
    static String stopWordList = "state-of-the-art" +
            "and or key current specific future " +
            "alternative concrete other different appropriate recent " +
            "used critical prevalent generation underlying core" +
            " identical reliable new cutting-edge competing such " +
            "effective developed robust sister several magnificent " +
            "impressive following ordinary chosen excepted leveraging " +
            "given umerous proprietary relevant proper existing effectiv " +
            "numerous first suitable many necessary resulting \\* " +
            "required fundamental various improvement represented " +
            "similar standard tools upcoming related improved " +
            "available certain optional useful common existing " +
            "chosen contributed preferred presented dominant "  +
            "next generation including art back-end implemented additional " +
            "introducing active State-ofthe-art integrating own combined base " +
            "individual participating realistic primary whose known convinient well" +
            " traditional enabling same novel candidate practical" +
            " mature innovative same contemporary related supporting fashionable" +
            " described several major present-day using current" +
            " attractive generated new new-generation generation  wit wagen wird ihn"
            + " state-of-art WHOM  Whole whose what when  \\/ ~  _ "
            + "University usual a b c d e f g h i j k l m n o p q r s t u v w x y z ii iii iv iiv"
            + " York City successful popular particular conventional such corresponding corresponds "
            + "times original good mentioned acceptable above a.k.a European due because becoming current Current Currently default straightforward including such";
    static {
        String[] list = stopWordList.split(" ");
        for (int i = 0; i < list.length; i++) {
           stopWord.add(list[i].toLowerCase());
        }
    }

    public static boolean isStopWord(String stopWord) {
        return TTRefineStopWord.stopWord.contains(stopWord.toLowerCase());
    }



}
