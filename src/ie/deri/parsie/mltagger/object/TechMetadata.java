/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

/**
 *
 * @author behqas
 */
public class TechMetadata {

    private int frequency = 0;
    private double totalDesicionValue = 0;
    private double maxDesicionValue = 0;

    public void incFrequency() {
        this.frequency++;
    }

    public void incTotalDesicionValue(double newDesicionValue) {
        this.totalDesicionValue += newDesicionValue;
    }

    public void decideMaxDesicionValue(double maxDesicionValue) {
        if (this.maxDesicionValue < maxDesicionValue) {
            this.maxDesicionValue = maxDesicionValue;
        }
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public void setMaxDesicionValue(double maxDesicionValue) {
        this.maxDesicionValue = maxDesicionValue;
    }

    public void setTotalDesicionValue(double totalDesicionValue) {
        this.totalDesicionValue = totalDesicionValue;
    }

    public int getFrequency() {
        return frequency;
    }

    public double getMaxDesicionValue() {
        return maxDesicionValue;
    }

    public double getTotalDesicionValue() {
        return totalDesicionValue;
    }
}
