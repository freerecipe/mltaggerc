/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.object;

import ie.deri.nlp.db.objects.DBLexeme;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class TechnologyTerm {
    private double technologyTermID;
    private double originSentenceID;
    private List<DBLexeme> dbLexemeList;
    private DBLexeme lexemeBefore;
    private int freq;
  


    public TechnologyTerm() {
        dbLexemeList = new LinkedList<DBLexeme>();
        lexemeBefore = new DBLexeme();
    }

    public void setDbLexemeList(List<DBLexeme> dbLexemeList) {
        this.dbLexemeList = dbLexemeList;
    }

    public void pushDbLexeme(DBLexeme dbLexeme) {
        this.dbLexemeList.add(0, dbLexeme);
    }

    public void setLexemeBefore(DBLexeme lexemeBefore) {
        this.lexemeBefore = lexemeBefore;
    }


    public void setTechnologyTermID(double technologyTermID) {
        this.technologyTermID = technologyTermID;
    }

    public List<DBLexeme> getDbLexemeList() {
        return dbLexemeList;
    }

    public DBLexeme getLexemeBefore() {
        return lexemeBefore;
    }


    public double getTechnologyTermID() {
        return technologyTermID;
    }

    public double getOriginSentenceID() {
        return originSentenceID;
    }

    public void setOriginSentenceID(double originSentenceID) {
        this.originSentenceID = originSentenceID;
    }

    public void setFreq(int freq) {
        this.freq = freq;
    }

    public int getFreq() {
        return freq;
    }

    public void incFreq() {
        this.freq++;
    }

    public String toPlainText() {
        String str="";
        for(DBLexeme l: dbLexemeList){
            str+=  l.getWord()+ " ";
        }
        return str.trim();
    }



    @Override
    public String toString() {
        String str="";
        for(DBLexeme l: dbLexemeList){
            str+=  l.getWord()+"/"+l.getPos() + " ";
        }
        return str.trim();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TechnologyTerm) {
            TechnologyTerm tt = (TechnologyTerm) obj;

            if(tt.getLexemeBefore() == null && this.lexemeBefore == null){
                if (tt.getDbLexemeList().equals(this.dbLexemeList)){
                    return true;
                }else {
                    return false;
                }
            } else
            if (tt.getDbLexemeList().equals(this.dbLexemeList)
                    && tt.getLexemeBefore().equals(this.lexemeBefore)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + (this.dbLexemeList != null ? this.dbLexemeList.hashCode() : 0);
        hash = 11 * hash + (this.lexemeBefore != null ? this.lexemeBefore.hashCode() : 0);
        return hash;
    }
public int getLength(){
    return dbLexemeList.size();
}




}
