/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.server;


import ie.deri.nlp.liblinear.ModelFilesContainer;
import ie.deri.parsie.mltagger.object.FeatureDependency;
import edu.stanford.nlp.ling.WordLemmaTag;
import ie.deri.nlp.analyzer.objects.BPSentence;
import ie.deri.nlp.analyzer.objects.Chunk;
import ie.deri.nlp.analyzer.objects.PLexeme;
import ie.deri.nlp.analyzer.objects.SentenceDependency;
import ie.deri.nlp.db.objects.DBChunk;
import ie.deri.nlp.db.objects.DBDependency;
import ie.deri.nlp.db.objects.DBLexeme;
import ie.deri.nlp.db.objects.DBSentence;
import ie.deri.nlp.db.objects.DBSentenceDependency;
import ie.deri.nlp.linguistic.analyzer.Pipeline6;
import ie.deri.nlp.sc.svm.SCModelFilesContainer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Your Name <behrang.qasemizadeh at deri.org>
 */
public class LingAnalysis {

    private Pipeline6 pl6;

    public void init(Pipeline6 pl6) {
        this.pl6 = pl6;
    }

    public List<DBSentence> analyzedParagraphToFeatDBSentence(
            List<BPSentence> analSentList, ModelFilesContainer mfc) {

        List<DBSentence> dbSentenceList = new ArrayList<DBSentence>();
        for (BPSentence as : analSentList) {
            List<DBLexeme> dblList = new ArrayList<DBLexeme>();
            DBSentence dbsent = new DBSentence();

            for (PLexeme pl : as.getPLexemeList()) {
                DBLexeme dbl = new DBLexeme();
                double dbid = 0;
                if (mfc.getpLexemeMap().containsKey(pl)) {
                    dbid = mfc.getpLexemeMap().get(pl);
                }

                dbl.setLemma(pl.getLemma());
                dbl.setPos(pl.getPos());
                dbl.setWord(pl.getWord());
                dbl.setId(dbid);
                dblList.add(dbl);
            }
            dbsent.setDbLexemeList(dblList);


            List<DBSentenceDependency> finalSentenceDepList =
                    new ArrayList<DBSentenceDependency>();

            for (SentenceDependency sd : as.getSentenceDependencies()) {
                DBSentenceDependency dbs = new DBSentenceDependency();
                DBDependency dbp = new DBDependency();
                FeatureDependency fd = new FeatureDependency();
                fd.setDepType(sd.getDependency().getDependencyType());
                fd.setGovernorLexemeID(
                        dblList.get(sd.getGovernorPosition()).getId());
                fd.setRegentLexemeID(
                        dblList.get(sd.getRegentPosition()).getId());

                if (mfc.getpFeatureDepMap().containsKey(fd)) {
                    dbp.setDependencyID(mfc.getpFeatureDepMap().get(fd));
                } else {
                    dbp.setDependencyID(0);
                }
                dbp.setGovernorLexemeID(fd.getGovernorLexemeID());
                dbp.setRegentLexemeID(fd.getRegentLexemeID());
                dbp.setFrequency(1);
                dbp.setDependencyType(fd.getDepType());
                dbs.setDbDependency(dbp);
                dbs.setGovernorPosition(sd.getGovernorPosition());
                dbs.setRegentPosition(sd.getRegentPosition());
                finalSentenceDepList.add(dbs);
            }
            dbsent.setSentenceDependencyList(finalSentenceDepList);

            dbSentenceList.add(dbsent);
        }


        return dbSentenceList;
    }

    public List<BPSentence> analyzeParagraph(String paragraph) {
        List<List<WordLemmaTag>> sentenceList = pl6.process(paragraph);
        List<BPSentence> analSentenceList = new ArrayList<BPSentence>();
        for (List<WordLemmaTag> sentence : sentenceList) {
            BPSentence analSentence = new BPSentence();
            List<PLexeme> plexemeList = new ArrayList<PLexeme>();
            for (WordLemmaTag wlt : sentence) {
                PLexeme pl = new PLexeme();
                pl.setLemma(wlt.lemma());
                pl.setPos(wlt.tag());
                pl.setWord(wlt.word());
                plexemeList.add(pl);
            }
            analSentence.setLexemeList(plexemeList);
            List<SentenceDependency> dpList = pl6.maltParse(plexemeList);
            analSentence.setSentenceDependencies(dpList);
            
            List<Chunk> chunkList = pl6.doChunking(sentence);
            analSentence.setSentenceChunks(chunkList);
            analSentenceList.add(analSentence);
        }
        return analSentenceList;

    }

    public DBSentence analyzedSentToFeatDBSentence(
            BPSentence as, ModelFilesContainer mfc) {


        
        List<DBLexeme> dblList = new ArrayList<DBLexeme>();
        DBSentence dbsent = new DBSentence();

        
        
        
        
        
        for (PLexeme pl : as.getPLexemeList()) {
            DBLexeme dbl = new DBLexeme();
            double dbid = 0;
            if (mfc.getpLexemeMap().containsKey(pl)) {
                dbid = mfc.getpLexemeMap().get(pl);
            }

            dbl.setLemma(pl.getLemma());
            dbl.setPos(pl.getPos());
            dbl.setWord(pl.getWord());
            dbl.setId(dbid);
            dblList.add(dbl);
        }
        dbsent.setDbLexemeList(dblList);


        List<DBSentenceDependency> finalSentenceDepList =
                new ArrayList<DBSentenceDependency>();

        for (SentenceDependency sd : as.getSentenceDependencies()) {
            DBSentenceDependency dbs = new DBSentenceDependency();
            DBDependency dbp = new DBDependency();
            FeatureDependency fd = new FeatureDependency();
            fd.setDepType(sd.getDependency().getDependencyType());
            fd.setGovernorLexemeID(
                    dblList.get(sd.getGovernorPosition()).getId());
            fd.setRegentLexemeID(
                    dblList.get(sd.getRegentPosition()).getId());

            if (mfc.getpFeatureDepMap().containsKey(fd)) {
                dbp.setDependencyID(mfc.getpFeatureDepMap().get(fd));
            } else {
                dbp.setDependencyID(0);
            }
            dbp.setGovernorLexemeID(fd.getGovernorLexemeID());
            dbp.setRegentLexemeID(fd.getRegentLexemeID());
            dbp.setFrequency(1);
            dbp.setDependencyType(fd.getDepType());
            dbs.setDbDependency(dbp);
            dbs.setGovernorPosition(sd.getGovernorPosition());
            dbs.setRegentPosition(sd.getRegentPosition());
            finalSentenceDepList.add(dbs);
        }
        dbsent.setSentenceDependencyList(finalSentenceDepList);

        
        
        
        return dbsent;
    }

    public DBSentence analyzedSentToFeatDBSentence(
            BPSentence as, SCModelFilesContainer mfc) {



        List<DBLexeme> dblList = new ArrayList<DBLexeme>();
        DBSentence dbsent = new DBSentence();

        for (PLexeme pl : as.getPLexemeList()) {
            DBLexeme dbl = new DBLexeme();
            double dbid = 0;
            if (mfc.getpLexemeMap().containsKey(pl)) {
                dbid = mfc.getpLexemeMap().get(pl);
            }

            dbl.setLemma(pl.getLemma());
            dbl.setPos(pl.getPos());
            dbl.setWord(pl.getWord());
            dbl.setId(dbid);
            dblList.add(dbl);
        }
        dbsent.setDbLexemeList(dblList);


        List<DBSentenceDependency> finalSentenceDepList =
                new ArrayList<DBSentenceDependency>();

        for (SentenceDependency sd : as.getSentenceDependencies()) {
            DBSentenceDependency dbs = new DBSentenceDependency();
            DBDependency dbp = new DBDependency();
            FeatureDependency fd = new FeatureDependency();
            fd.setDepType(sd.getDependency().getDependencyType());
            fd.setGovernorLexemeID(
                    dblList.get(sd.getGovernorPosition()).getId());
            fd.setRegentLexemeID(
                    dblList.get(sd.getRegentPosition()).getId());

            if (mfc.getpFeatureDepMap().containsKey(fd)) {
                dbp.setDependencyID(mfc.getpFeatureDepMap().get(fd));
            } else {
                dbp.setDependencyID(0);
            }
            dbp.setGovernorLexemeID(fd.getGovernorLexemeID());
            dbp.setRegentLexemeID(fd.getRegentLexemeID());
            dbp.setFrequency(1);
            dbp.setDependencyType(fd.getDepType());
            dbs.setDbDependency(dbp);
            dbs.setGovernorPosition(sd.getGovernorPosition());
            dbs.setRegentPosition(sd.getRegentPosition());
            finalSentenceDepList.add(dbs);
        }
        dbsent.setSentenceDependencyList(finalSentenceDepList);

        return dbsent;
    }


}
