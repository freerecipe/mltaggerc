/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.server;

import ie.deri.parsie.mltagger.object.Request;


import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author behqas
 */
public class TTIServer extends Thread {

    private ServerSocket ttiServer;
//    UserQueue userQueue;
    List<Request> userRequsetList = new ArrayList<Request>();

    public TTIServer(int portNumber) throws Exception {
//        this.userQueue = userQueue;
//        settings = pss;
//        int portNumber = settings.getTaggerListSetting().getPortNumber();
        ttiServer = new ServerSocket(portNumber);
        System.out.println("TTI Server listening on port >>> "
                + portNumber);
        this.start();
    }


    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("Waiting for connections.");
                Socket client = ttiServer.accept();
                System.out.println("Accepted a connection from: "
                        + client.getInetAddress());
                Connect c = new Connect(client, this);
            } catch (Exception e) {
                System.err.println("?? >> Exception " + e);
            }
        }
    }

    public synchronized void put(Request request) throws InterruptedException {
        userRequsetList.add(request);
        notifyAll();

    }

    public synchronized Request getUser()
            throws InterruptedException {
        notify();
        while (userRequsetList.isEmpty()) {
            wait();
        }
        Request request = userRequsetList.get(0);
        userRequsetList.remove(0);
        return request;
    }
}


