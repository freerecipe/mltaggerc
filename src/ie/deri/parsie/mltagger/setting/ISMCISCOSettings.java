/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.setting;

import ie.deri.nlp.settings.LingSettings;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ISMCISCOSettings {

    private LingSettings lingSettings;
    private TaggerSetting ttiSettings;

    public ISMCISCOSettings() {
        lingSettings = new LingSettings();



        ttiSettings = new TaggerSetting();
    }

    public void fromXmlFile(String filePath) throws ParserConfigurationException, SAXException, IOException {

        System.err.println("Load setting from " + filePath);
        File file = new File(filePath);
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc;
        doc = builder.parse(file);
        // StyleTable Node

        NodeList lingSettingsNode = doc.getElementsByTagName("LingSettings");


        NodeList ttiSettingNodeList = doc.getElementsByTagName("TTISettings");

        lingSettings.fromXMLNode(lingSettingsNode.item(0));


        ttiSettings.initFromNode(ttiSettingNodeList.item(0));
    }

    public LingSettings getLingSettings() {
        return lingSettings;
    }



  


    public TaggerSetting getTtiSettings() {
        return ttiSettings;
    }


}
