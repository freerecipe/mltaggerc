/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.setting;


import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Settings extends ie.deri.nlp.settings.Settings {

    private TaggerListSetting taggerListSetting;

    

    @Override
   public void fromXmlFile(String filePath) throws ParserConfigurationException, SAXException, IOException {
        super.fromXmlFile(filePath);
        taggerListSetting = new TaggerListSetting();
        NodeList taggerListNodeList = doc.getElementsByTagName("MLSettings");
        taggerListSetting.initFromNode(taggerListNodeList.item(0));
    }

    public TaggerListSetting getTaggerListSetting() {
        return taggerListSetting;
    }
}
