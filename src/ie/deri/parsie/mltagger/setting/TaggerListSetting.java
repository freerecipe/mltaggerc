/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.setting;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author behqas
 */
public class TaggerListSetting {
    private List<TaggerSetting> taggerSettingList;
    private int portNumber;


    public void initFromNode(Node taggerListNode){
        taggerSettingList = new ArrayList<TaggerSetting>();
        portNumber = 0;
           NodeList taggerListNodeList = taggerListNode.getChildNodes();
        for (int i = 0; i < taggerListNodeList.getLength(); i++) {
            if ("Port".equals(taggerListNodeList.item(i).getNodeName())) {
                portNumber = Integer.parseInt(taggerListNodeList.item(i).getTextContent().trim());
            } else if ("MLTagger".equals(taggerListNodeList.item(i).getNodeName())) {
                TaggerSetting ts = new TaggerSetting();
                ts.initFromNode(taggerListNodeList.item(i));
                taggerSettingList.add(ts);
            }
        }


    }

    public int getPortNumber() {
        return portNumber;
    }

    public List<TaggerSetting> getTaggerSettingList() {
        return taggerSettingList;
    }




}
