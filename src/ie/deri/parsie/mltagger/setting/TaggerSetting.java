/* 
 * Copyright (C) 2015 Behrang QasemiZadeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ie.deri.parsie.mltagger.setting;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author behqas
 */
public class TaggerSetting {

    private String taggerID;
    private String dbLexemePath;
    private String dbDependencyPath;
    private String ttiMap;
    private String ttiModel;
    

    public void setDbDependencyPath(String dbDependencyPath) {
        this.dbDependencyPath = dbDependencyPath;
    }

    public void setDbLexemePath(String dbLexemePath) {
        this.dbLexemePath = dbLexemePath;
    }

    public void setTtiMap(String ttiMap) {
        this.ttiMap = ttiMap;
    }

    public void setTtiModel(String ttiModel) {
        this.ttiModel = ttiModel;
    }

    public String getDbDependencyPath() {
        return dbDependencyPath;
    }

    public String getDbLexemePath() {
        return dbLexemePath;
    }

    public String getTtiMap() {
        return ttiMap;
    }

    public String getTtiModel() {
        return ttiModel;
    }

    public String getTaggerID() {
        return taggerID;
    }




    public void initFromNode(Node ttiNode) {
        NodeList ttiNodeList = ttiNode.getChildNodes();
        for (int i = 0; i < ttiNodeList.getLength(); i++) {

            if ("ID".equals(ttiNodeList.item(i).getNodeName())) {
                taggerID = ttiNodeList.item(i).getTextContent().trim();
            } else if ("DBLexeme".equals(ttiNodeList.item(i).getNodeName())) {
                dbLexemePath = ttiNodeList.item(i).getTextContent().trim();
            } else if ("DBDependency".equals(ttiNodeList.item(i).getNodeName())) {
                dbDependencyPath = ttiNodeList.item(i).getTextContent().trim();
            } else if ("FeatMap".equals(ttiNodeList.item(i).getNodeName())) {
                ttiMap = ttiNodeList.item(i).getTextContent().trim();
            } else if ("MLModel".equals(ttiNodeList.item(i).getNodeName())) {
                ttiModel = ttiNodeList.item(i).getTextContent().trim();
            }
        }
    }
}
